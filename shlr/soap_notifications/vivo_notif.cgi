#!/usr/bin/env python
#******************************************************************************
# file: $Id: vivo_notif.cgi,v 1.14 2011-06-07 12:47:19 ndt Exp $
# author:  Benjamin Portier
#*****************************************************************************/
import os, re
from shlr_notif_mo_parsing import *
from shlr_notif_definitions import AsyncACK, sendNotif

class Response:
	def __init__(self):
		pass

	def sendResponse(self, s , noExit=0):
		if (s=="OK"):
			LOG.info("-- HTTP request succeeded: %s --"%s)
		else:
			LOG.warning("-- HTTP request aborted: %s --"%s)
		text = "<msg>%s</msg>"%s
		print 'Status: 200 OK'
		print 'Content-Type: text/xml;'
		print 'Content-Length: %d'%len(text)
		print ''
		print text

		if noExit == 0:
			LOG.end()
			sys.exit(0)
		else:
			sys.stdout.flush()

RESP=Response()


## REQUEST PARSING
try:
	qs = os.environ['QUERY_STRING']
except:
	RESP.sendResponse("BAD_REQUEST")
url_string= qs

"""

initialSMS="C00110C8080102030405060708C90711121314151617CA09212223242526272829"
# 010301 <type>
selectItemResponse="C00110C10400000001CB0F010301240002028281030100100103"
getInputResponse="C00110C10400000002CB210103012300020282810301000D1304313233343536373839303132333435363738"
getInputResponse="C00110C10400000002CB210103012300020282810301000D1304414233343536373839303132333435363738"
displayTextResponse="C00110C10400000003CB0B010301210002028281030111"
confirmationSMS="C00110C10400000004CC102143658709214365870921436587FF"

url_string="http://demo1.com/client/sms?type=MO&id=11/01/12:09:15:47.975_+33608223728:0&msisdn=+33608223728&msisdn_port=0&sme=11520&sme_port=0&pid=00&dcs=08&encoding=binary&udhi=true&scts=2011/01/25 14:45:36&smsc=popup&sms=%s"%getInputResponse

#http://172.25.31.45:9908/cgi-bin/shlr_applet_mo_server.cgi?type=MO&id=11/01/12:09:15:47.975_+33608223728:0&msisdn=+33608223728&msisdn_port=0&sme=11520&sme_port=0&pid=00&dcs=08&encoding=binary&udhi=true&scts=2011/01/25 14:45:36&smsc=popup&sms=C00110C10400000004CC102143658709214365870921436587FF
"""

fields = re.split("[&=]", url_string)
LOG.debug(str(fields))

fieldList = []
i = 0
while i < len(fields)-1:
	fieldList.append([fields[i], fields[i+1]])
	i = i + 2

# Check HTTP request is linked with valid MO reception, destinated to applet
# Extract SMS data from this request
moFound = 0
porFound = 0
msisdnFound = 0
smeOK=0
data=None
rpackFound=0
status=""
messageError=""
tid=""

LOG.debug(fieldList.__str__())

for elem in fieldList:
	if string.lower(elem[0])=='type' and string.upper(elem[1])=='MO':
		moFound = 1
	if string.lower(elem[0])=='type' and string.upper(elem[1])=='RPACK':
		rpackFound = 1
	elif string.lower(elem[0])=='msisdn':
		msisdnFound = 1
		msisdn = elem[1].split(":")[0]
	elif string.lower(elem[0])=='sme' and elem[1].split(":")[0]==CFG.getvalue('HRS_SME'):
		smeOK=1
	elif string.lower(elem[0])=='sms':
		data = elem[1]
	if string.lower(elem[0])=='client_reference_id':
		tid = elem[1].split("_")[0]
	if string.lower(elem[0])=='status':
		status = elem[1]
		if status!="OK":
			status="TO_RETRY"
	elif string.lower(elem[0])=='message':
		messageError = elem[1]

if msisdnFound==0:
	LOG.warning("IncomingRequest not including source MSISDN")
	RESP.sendResponse("UNSUPPORTED_GET_REQUEST : no MSISDN")
elif ((moFound==0) and (rpackFound==0)):
	LOG.warning("IncomingRequest not linked with a MO")
	RESP.sendResponse("UNSUPPORTED_GET_REQUEST : only RPACK and MO supported")
elif (moFound==1) and (smeOK==0):
	LOG.warning("MO not destinated to expected SME")
	RESP.sendResponse("UNSUPPORTED_GET_REQUEST : bad SME")
elif (moFound==1) and data is None:
	LOG.warning("MO data empty")
	RESP.sendResponse("UNSUPPORTED_GET_REQUEST : no data found")
elif ((rpackFound==1) and (status=="") and (tid=="")):
	LOG.warning("RPACK without status and/or transaction ID")
	RESP.sendResponse("UNSUPPORTED_GET_REQUEST : bad RPACK")

if moFound and data[:4]=="0271":
	moFound=0
	porFound=1

LOG.setMsisdn(msisdn)
if moFound:
	notif = parseMO(msisdn, data)
	LOG.info("About treat SMS data: %s"%data)
	if notif is None:
		LOG.warning("MO not matching any predefined cmd type")
		RESP.sendResponse("UNSUPPORTED_GET_REQUEST")
	LOG.info(notif.__str__())
	LOG.info("\n%s"%(notif.soapNotif()))
else:
	notif = AsyncACK(msisdn, tid)
	notif.setPorData("")
	if porFound:
		status, message, data = parsePOR(data)
		notif.setDeliveryStatus(status)
		notif.setMessage(message)
		notif.setPorData(data)
	else:
		notif.setDeliveryStatus(status)
		notif.setMessage(messageError)

# Send HTTP response asap to MTMO, not to block SH and AC 
RESP.sendResponse("OK", noExit=1)

# Send SOAP notif to 1st URL OK among list of 4 ordered candidates
st = sendNotif(notif, CFG)
#st = sendNotif_NoBlocking(notif, CFG)
LOG.end()
