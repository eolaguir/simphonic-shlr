#!/bin/sh

if [ ! $# -eq 0 ]; then
	shl_log=$1
else
	shl_log="/export/home/sfovivo/exe/sys/shlr2ats/log/`date +%m%d`/vivo.log"
fi

nawk '
BEGIN{
	for (i=0; i < 24; i++){
		async[i]=0;
		async_OK[i]=0;
		async_TO_RETRY[i]=0;
		async_FAILED[i]=0;
		unk_subs[i]=0;
		subs_abs[i]=0;
		sm_undel[i]=0;
		mt_busyf[i]=0;
		sys_fail[i]=0;
		pend_ses[i]=0;
		ope_fail[i]=0;
		int_erro[i]=0;
		no_sessi[i]=0;
		g348_low[i]=0;
		md_timeo[i]=0;
		sm_expir[i]=0;
		unk_erro[i]=0;
		smsc_err[i]=0;
		send_url1[i]=0;
		send_url2[i]=0;
	}
}

{
	time=substr($1,1,2);
	if (time < 10)
		hh=substr($1,2,1)
	else
		hh=time;
}

$2 ~ /AsyncACK/ {
        async[hh]+=1;
	if ($5 ~ /OK/)
		async_OK[hh]+=1;
	if ($5 ~ /TO_RETRY/)
		async_TO_RETRY[hh]+=1;
	if ($5 ~ /FAILED/)
		async_FAILED[hh]+=1;
	if ((($7 ~ /Unknown/) && ($8 ~ /subscriber/)) || ($6 ~ /NetworkDiagErr_UNKNOWN_SUBSCRIBER/))
		unk_subs[hh]+=1;
	if ((($7 ~ /Subscriber/) && ($8 ~ /absent/)) || ($6 ~ /NetworkDiagErr_ABSENT_SUBSCRIBER/))
		subs_abs[hh]+=1;	
        if ((($7 ~ /SM/) && ($8 ~ /undeliverable/)) || ($6 ~ /NetworkDiagErr_SM_UNDELIVERABLE/))
                sm_undel[hh]+=1;
        if ((($7 ~ /MT/) && ($8 ~ /Busy/)) || ($6 ~ /NetworkDiagErr_MT_BUSY_FOR_MT_SMS/))
                mt_busyf[hh]+=1;
        if ((($7 ~ /System/) && ($8 ~ /failure/)) || ($6 ~ /NetworkDiagErr_SYSTEM_FAILURE/))
                sys_fail[hh]+=1;
        if (($7 ~ /Pending/) && ($8 ~ /session/))
                pend_ses[hh]+=1;
        if (($7 ~ /Operation/) && ($8 ~ /failed/))
                ope_fail[hh]+=1;
        if (($6 ~ /Internal/) && ($7 ~ /error/))
                int_erro[hh]+=1;
        if (($6 ~ /No/) && ($7 ~ /available/))
                no_sessi[hh]+=1;
	if ($6 ~ /G348_COUNTER_LOW/)
		g348_low[hh]+=1;
        if (($6 ~ /MD/) && ($7 ~ /timed/))
                md_timeo[hh]+=1;
        if ($6 ~ /NetworkDiagErr_SM_EXPIRED/)
                sm_expir[hh]+=1;
        if (($7 ~ /Unknown/) && ($8 ~ /error/))
                unk_erro[hh]+=1;
        if (($7 ~ /SMSC/) && ($8 ~ /Error/))
                smsc_err[hh]+=1;
}

(($2 ~ /Tries/) && ($6 ~ /1/)){
                send_url1[hh]+=1;
}
(($2 ~ /Tries/) && ($6 ~ /2/)){
                send_url2[hh]+=1;
}

END{ 
	printf("hour\ttotal\t\tasy:OK \tasy:TORETRY \t\tasy:FAIL \tunk_subs \tsubs_abs \tsm_undel \tmt_busyf \tsys_fail \tpend_ses \tope_fail \tint_erro \tno_sessi \tg348_low \tmd_timeo \tsm_expir \tunk_erro \tsmsc_err \turl[1]\turl[2]\n");
	for (i=0; i < 24 ; i++){ 
                if (async[i]!=0){
                        async_OK_pc[i]=async_OK[i]/async[i]*100;
			async_TO_RETRY_pc[i]=async_TO_RETRY[i]/async[i]*100;
			async_FAILED_pc[i]=async_FAILED[i]/async[i]*100;
                }
		if (async_TO_RETRY[i]!=0){
			unk_subs_pc[i]=unk_subs[i]/async_TO_RETRY[i]*100;
			subs_abs_pc[i]=subs_abs[i]/async_TO_RETRY[i]*100;
			sm_undel_pc[i]=sm_undel[i]/async_TO_RETRY[i]*100;
			mt_busyf_pc[i]=mt_busyf[i]/async_TO_RETRY[i]*100;
			sys_fail_pc[i]=sys_fail[i]/async_TO_RETRY[i]*100;
			pend_ses_pc[i]=pend_ses[i]/async_TO_RETRY[i]*100;
			ope_fail_pc[i]=ope_fail[i]/async_TO_RETRY[i]*100;
			int_erro_pc[i]=int_erro[i]/async_TO_RETRY[i]*100;
			no_sessi_pc[i]=no_sessi[i]/async_TO_RETRY[i]*100;
			g348_low_pc[i]=g348_low[i]/async_TO_RETRY[i]*100;
			md_timeo_pc[i]=md_timeo[i]/async_TO_RETRY[i]*100;
			sm_expir_pc[i]=sm_expir[i]/async_TO_RETRY[i]*100;
			unk_erro_pc[i]=unk_erro[i]/async_TO_RETRY[i]*100;
			smsc_err_pc[i]=smsc_err[i]/async_TO_RETRY[i]*100;
		}
		printf("%02d:00\t%6d\t%6d (%.2f)\t%6d (%.2f)\t%6d (%.2f)\t%6d (%.2f)\t%6d (%.2f)\t%6d (%.2f)\t%6d (%.2f)\t%6d (%.2f)\t%6d (%.2f)\t%6d (%.2f)\t%6d (%.2f)\t%6d (%.2f)\t%6d (%.2f)\t%6d (%.2f)\t%6d (%.2f)\t%6d (%.2f)\t%6d (%.2f)\t%6d\t%6d\n",i,async[i],async_OK[i],async_OK_pc[i],async_TO_RETRY[i],async_TO_RETRY_pc[i],async_FAILED[i],async_FAILED_pc[i],unk_subs[i],unk_subs_pc[i],subs_abs[i],subs_abs_pc[i],sm_undel[i],sm_undel_pc[i],mt_busyf[i],mt_busyf_pc[i],sys_fail[i],sys_fail_pc[i],pend_ses[i],pend_ses_pc[i],ope_fail[i],ope_fail_pc[i],int_erro[i],int_erro_pc[i],no_sessi[i],no_sessi_pc[i],g348_low[i],g348_low_pc[i],md_timeo[i],md_timeo_pc[i],sm_expir[i],sm_expir_pc[i],unk_erro[i],unk_erro_pc[i],smsc_err[i],smsc_err_pc[i],send_url1[i],send_url2[i]);
	}
}
' $shl_log
