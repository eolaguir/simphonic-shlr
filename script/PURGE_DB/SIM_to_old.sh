# Test of parameters number
if [ "$#" -lt 1 ]; then
echo -e "\nNumber of days parameter missing !!!"
echo -e "USAGE for last command older than 180 days :"
echo -e "      SIM_to_old.sh 180\n"
exit 3
fi
date
sqlplus SIMOTA_01ALL0101_SCH/oberthur@ASA01.vivo.com.br @SIM_to_old.sql &1 SIM_to_old.ASA01.`date +%y%m%d`
sqlplus SIMOTA_01ALL0101_SCH/oberthur@ASA02.vivo.com.br @SIM_to_old.sql &1 SIM_to_old.ASA02.`date +%y%m%d`
sqlplus SIMOTA_01ALL0101_SCH/oberthur@ASA03.vivo.com.br @SIM_to_old.sql &1 SIM_to_old.ASA03.`date +%y%m%d`
sqlplus SIMOTA_01ALL0101_SCH/oberthur@ASA04.vivo.com.br @SIM_to_old.sql &1 SIM_to_old.ASA04.`date +%y%m%d`
awk '{print $1}' SIM_to_old.*.`date +%y%m%d` |sort -u >ICCID_to_old.`date +%y%m%d`
date
