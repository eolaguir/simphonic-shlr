set serveroutput on
set echo on
spool grow_Number_Of_Sim
alter table SIM_CARD modify (ID NUMBER(12));
alter table CMM_SIM_CARD modify (ID NUMBER(12));
alter table COUNTER modify (ID NUMBER(12));
alter table CMM_SIM_KEYSET_COUNTER modify (ID NUMBER(12), SIM_CARD_ID NUMBER(12));
alter table SIM_CARD_COUNTER modify (SIM_CARD_ID NUMBER(12), COUNTER_ID NUMBER(12));
alter table SUBSCRIBER modify (SIM_CARD_ID NUMBER(12));
alter table SUBSCRIBER_HISTORY modify (OLD_SIM_ID NUMBER(12));
alter table CAMPAIGN_PROCESSING_SIM_CARD modify (SIM_CARD_ID NUMBER(12));
alter table CAMPAIGN_SIM_CARD modify (SIM_CARD_ID NUMBER(12));
alter table CMM_SIM_CARD_GP_KEYS modify (SIM_CARD_ID NUMBER(12));
alter table CMM_SIM_TAR_COUNTER modify (SIM_CARD_ID NUMBER(12));
alter table GEMPLUS_CARD modify(SIM_CARD_ID NUMBER(12));
alter table GROUP_SIM_CARD modify (SIM_CARD_ID NUMBER(12));
alter table M_SIM_CARD modify (SIM_CARD_ID NUMBER(12));
alter table OBTV1_CARD modify (SIM_CARD_ID NUMBER(12));
alter table OCP_COUNTER modify (COUNTER_ID NUMBER(12));
alter table ORGA_CARD modify (SIM_CARD_ID NUMBER(12));
alter table PRIVTRG_SIM_CARD modify (SIM_CARD_ID NUMBER(12));
alter table RULE_COUNTER_LINK modify (COUNTER_ID NUMBER(12));
alter table SCHLUMBERGER_CARD modify (SIM_CARD_ID NUMBER(12));
alter table SIM_CARD_APPLICATION modify (SIM_CARD_ID NUMBER(12));
alter table SIM_CARD_COUNTER modify (SIM_CARD_ID NUMBER(12), COUNTER_ID NUMBER(12));
alter table SIM_CARD_GSMFILES modify (SIM_CARD_ID NUMBER(12));
alter table SIM_CARD_PACKAGE modify (SIM_CARD_ID NUMBER(12));
alter table STAT_COUNTER_INSTANCE modify (ID NUMBER(12), COUNTER_ID NUMBER(12));
alter table USERS modify (SIM_CARD_ID NUMBER(12));
@grow_sequences.sql 140000000 1 2140000000
set echo off
@CreateTempProc.sql
set echo on
execute sql_tools_pack.compile_objects();
spool off
exit
