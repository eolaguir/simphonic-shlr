
#include <event.h>
#include <sys_code.h>

#ifndef __EVENT_ALARM__
#define __EVENT_ALARM__


#ifdef __cplusplus
extern "C" {
#endif
	


evStatus sendAlarm(char* modName, evClass  alarm_class,  evCode code, evComment comment);


#ifdef __cplusplus
}
#endif


#endif	
