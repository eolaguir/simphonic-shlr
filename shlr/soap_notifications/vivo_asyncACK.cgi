#! /usr/bin/env python
#******************************************************************************
# file: $Id: vivo_asyncACK.cgi,v 1.3 2011-04-04 10:39:24 ndt Exp $
# author:  Benjamin Portier
#*****************************************************************************/
# This script is charge of implementing the alm api in python
# it is based on ZSI library, and is used as a soap server

import os
import sys
import time
import string

# ZSI imports
#from ZSI import dispatch
from ZSI import TC, ParsedSoap, SoapWriter, ParseException, Fault, FaultFromException, FaultFromZSIException

from shlr_asyncACK import *
from shlr_notif_definitions import sendNotif_NoBlocking


gettypecode = lambda mod,e: getattr(mod, str(e.localName)).typecode

def _CGISendXML(text, code=200, **kw):
	print 'Status: %d' % code
	print 'Content-Type: text/xml; charset="utf-8"'
	print 'Content-Length: %d' % len(text)
	print ''
	print text
	# Allows notif_connector to switch to finish this treatment asap
	sys.stdout.flush()

def _CGISendFault(f, **kw):
	_CGISendXML(f.AsSOAP(), 500, **kw)
	# Allows notif_connector to switch to finish this treatment asap
	sys.stdout.flush()


LOG.debug('notif_handler.cgi called')


try:
	#
	# Get the SOAP message
	#
	if os.environ.get('REQUEST_METHOD') != 'POST':
		_CGISendFault(FaultFromZSIException ('Must use POST'))
		sys.exit(1)

	ct = os.environ['CONTENT_TYPE']
	try:
		if ct.startswith('multipart/'):
			cid = resolvers.MIMEResolver (ct, sys.stdin)
			xml = cid.GetSOAPPart()
			LOG.debug ('')
			LOG.debug ('--------------------------------------------')
			LOG.debug ("Soap message : " + xml)
			LOG.debug ('--------------------------------------------')
			LOG.debug ('')
			ps = ParsedSoap (xml, resolver=cid.Resolve)
		else:
			length = int(os.environ['CONTENT_LENGTH'])
			soapRequest = sys.stdin.read (length)
			LOG.debug ('')
			LOG.debug ('--------------------------------------------')
			LOG.debug ("Soap message : " + soapRequest)
			LOG.debug ('--------------------------------------------')
			LOG.debug ('')
			ps = ParsedSoap (soapRequest)
	except ParseException, e:
		LOG.error("Exception raised for SOAP notif: <%s>"% e.__str__())
		_CGISendFault (FaultFromZSIException (e))
		sys.exit (1)

	# Get the name of the callback function
	#
	what = str (ps.body_root.localName)


	#
	# Get the handler function to call
	#
	modules = ( sys.modules['__main__'], )
	handlers = [ getattr(m, what) for m in modules if hasattr(m, what) ]
	if len(handlers) == 0:
		raise TypeError ("Unknown method " + what)

	# Of those modules, see who's callable.
	handlers = [ h for h in handlers if callable(h) ]
	if len(handlers) == 0:
		raise TypeError ("Unimplemented method " + what)
	if len(handlers) > 1:
		raise TypeError ("Multiple implementations found: " + `handlers`)
	handler = handlers[0]

	LOG.debug ("Handler function to call is " + str(handler))

	#
	# Call the handler function
	#
	if 1:
		notif, result = handler(ps)
		tc = TC.Any(pname=what+'Response')	#tc = TC.XML(aslist=1, pname=what+'Response')
		sw = SoapWriter(nsdict={'ALMNotif':'ALMNotif'})
		sw.serialize(result, tc)
		response = str(sw)

	LOG.debug ('')
	LOG.debug ('--------------------------------------------')
	LOG.debug ("Soap response : " + response)
	LOG.debug ('--------------------------------------------')
	LOG.debug ('')

	# Reply HTTP response asap, not to block notif_connector
	_CGISendXML(response)

	# Send notif to ATS
	st = sendNotif_NoBlocking(notif, CFG)

except Fault, e:
	_CGISendFault(e)

except Exception, e:
	# Something went wrong, send a fault.
	_CGISendFault(FaultFromException (e, 0, sys.exc_info()[2]))
