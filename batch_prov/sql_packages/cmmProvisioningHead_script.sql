-------------------------------------------------------------------------------
--File          :     $Id: cmmProvisioningHead_script.sql,v 1.1 2011-11-16 02:29:13 saadamra Exp $
--Project       :     CMM
--Author        :     Eric Baquiran
-----------------------------------------------------------------------------
--Version       :     $Revision: 1.1 $
-----------------------------------------------------------------------------
--Description   :     PL/SQL package for provisioning interface functions
-------------------------------------------------------------------------------
CREATE OR REPLACE PACKAGE cmmProvisioning_script AS
  TYPE CharArray30_index IS TABLE OF VARCHAR2(30) INDEX BY BINARY_INTEGER ;
  TYPE CharArray256_index IS TABLE OF VARCHAR2(256) INDEX BY BINARY_INTEGER ;
  TYPE CharArray64_index IS TABLE OF VARCHAR2(64) INDEX BY BINARY_INTEGER ;
  
  field_separator VARCHAR2(1) := '|';

  PROCEDURE createSimCard_script (
    p_sim_data            IN   CharArray64_index,
    p_app_counter         IN   CharArray30_index, 
    p_app_keyset_counter  IN   CharArray256_index
  );

END cmmProvisioning_script;
/

show errors;
