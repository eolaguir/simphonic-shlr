#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

from mmi_tools import *
from SimphonicMMI import *

class NavigationFrame:
	"""
	The purpose of this class is to generate the Simphonic MMI navigation
	bar.


	Mandatory attributes:

	allowedServices -- list of service ids.

	serv_tree -- sorted list of services. List of tuples.
                Each tuple is composed of:
                        - service id
                        - service desc (string or image)
                        - service url (cgi)
			- service titlebar (optional)

	Attempt is made to match service ids in 'allowedServices' with those in
	'serv_tree'. When matching, service desription is displayed.

	Optionnal attribute:

	automatic_title -- flag, set to 1 to handle titles in the title bar
		automatically when clicking on the menu link.
		Uses service titlebar if provided, service desc else.
		Uses title.cgi and title_template.html by default.
		Default = 1.


	WARNING: DO NOT CHANGE THE FOLLOWING !!
	If you do, make sure you know what you are doing...

	form_name -- default: "navig"
	msisdnField_name -- default: "stored_msisdn"
	hiddenAction_name -- default: "hiddenAction"
	titleframe_name -- default: "titleframec"
	titleframe_src -- default:"/cgi-bin/main/title.cgi?title="
	bodyframe_name -- default:"bodyframe"
	"""

	def __init__(self,allowedServices,serv_tree,**kw):

		self.allowedServices = allowedServices
		self.serv_tree = serv_tree
		# default values
		self.automatic_title = 1
		self.titleframe_name = "titleframec"
		self.titleframe_src = "/cgi-bin/main/title.cgi?title="
		self.bodyframe_name = "bodyframe"
		self.form_name = "navig"
		self.msisdnField_name = "stored_msisdn"
		self.hiddenAction_name = "hiddenAction"
		self.menuframe=self.bodyframe_name
		self.debug=0
		# overlay default values
		overlay_values(self,kw)

	def write(self,file=None):
		"Creates and emits the HTML code"

		#-- set language -----------------------------------------------

		lm=LanguageManager()

		#-- HTML container ---------------------------------------------

		doc = Document(   title='Oberthur Technologies',
                		        cgi=1,
		                        http='cache-control: private',
					charset=lm.charset,
					leftmargin=0,
					topmargin=0,
					marginwidth=0,
					marginheight=0,
					stylesheet='/css/main.css',
				  cssclass='nav_frame',
				  #onLoad="parent.bodyframe.location='/cgi-bin/service/subscriber/subscriberDetails.cgi';"
				  onLoad="parent.bodyframe.location='/cgi-bin/simphonic/monitoring/session.cgi';"
					)
# 		doc = Document(   title='Oberthur Card Systems',
#                 		        cgi=1,
# 		                        http='cache-control: private',
# 					charset=lm.charset,
# 					leftmargin=0,
# 					topmargin=0,
# 					marginwidth=0,
# 					marginheight=0
# 					)
		
		#-- header image -----------------------------------------------

		#image_h=Image(src="/images/OCSRapsodia_WEB.gif")

		#-- Generate menu ----------------------------------------------

		menu=_Menu(self.allowedServices,self.serv_tree,debug=self.debug)
		menu.menuframe=self.menuframe
		menu.urlframe=self.bodyframe_name

		#-- Form -------------------------------------------------------

		form = Form(method='POST',
		            name=self.form_name,
		            target=self.bodyframe_name)

		action_serv=Input(type='hidden',
				  name=self.hiddenAction_name,
				  value='none'	)
		stored_msisdn=Hidden(name=self.msisdnField_name,value="")
		form.append(action_serv,stored_msisdn)

		#-- Javascript -------------------------------------------------

		#-- optionnal js code
		setTitle_code = """
		function setTitle(title){
		"""
		if self.automatic_title:
			setTitle_code += """
			//set title
			parent.%(titleframe_name)s.location.href="%(titleframe_src)s" """ % {
				'titleframe_name' : self.titleframe_name,
				'titleframe_src' : self.titleframe_src}
			if lm.charset=='UTF-8':
				setTitle_code += "+ encodeURIComponent(title) ;"
			else:
				setTitle_code += "+ escape(title) ;"
		setTitle_code += """
		}
		"""

		#-- generic js code
		javascript_code = Script (code = setTitle_code + """

		function actionSetValue(Value) {
		        document.navig.hiddenAction.value=Value;
		}

		function gotoURL(url,title){
			setTitle(title);
			document.%(form_name)s.action=url;
			document.%(form_name)s.submit() ;			
		}

		// targetdocument is a document attribute of a frame/window
		// is used by writeMenus to render menu HTML
		var targetdocument = parent.navFrame.document;
		var targetwindow = parent.navFrame;

		function addMenus() {
		     targetdocument = parent.%(bodyframe_name)s.document;
		     targetwindow = parent.%(bodyframe_name)s;
		     writeMenus();		   
		}
		""" % self.__dict__ )

		doc.append(javascript_code)
		javascript_code2 = Script(language='JavaScript1.2" type="text/javascript', src="/html/mm_menu.js")
		#doc.append(Script(language="JavaScript1.2",code="mmLoadMenus();"))

		img_navbg = Image(name="header_nav_bg", src="/nav_images/header_nav_bg.jpg", width="274", height="32", border="0", alt="")
		img_subscriber_bt = Image(name="subscriber_bt", src="/nav_images/subscriber_bt.jpg", width="125", height="32", border="0", alt="")
		img_campaign_bt = Image(name="campaign_bt", src="/nav_images/campaign_bt.jpg", width="125", heigth="32", border="0", alt="")
		img_provisioning_bt = Image(name="provisioning_bt", src="/nav_images/provisioning_bt.jpg", width="125", heigth="32", border="0", alt="")
		img_accessright_bt = Image(name="accessright_bt", src="/nav_images/accessright_bt.jpg", width="125", heigth="32", border="0", alt="")
		img_admin_bt = Image(name="admin_bt", src="/nav_images/admin_bt.jpg", width="125", heigth="32", border="0", alt="")
		img_spacer = Image(name="spacer", src="/nav_images/subscriber_page_slice.jpg", width="1", heigth="32", border="0", alt="")
		
				
		#href_sub = Href("javascript:;", onMouseOut="MM_startTimeout();",
		#	    onMouseOver="MM_showMenu(menu_SUBS,0,0,null,'subscriber_bt');"
		#	    )

		#href_camp = Href("javascript:;", onMouseOut="MM_startTimeout();",
		#	    onMouseOver="MM_showMenu(menu_CMP,0,0,null,'campaign_bt');"
		#	    )
			    
		#href_prov = Href("javascript:;", onMouseOut="MM_startTimeout();",
		#	    onMouseOver="MM_showMenu(menu_PROV,0,0,null,'provisioning_bt');"
		#	    )
			    
		#href_access = Href("javascript:;", onMouseOut="MM_startTimeout();",
		#	    onMouseOver="MM_showMenu(menu_ACC,0,0,null,'accessright_bt');"
		#	    )

		href_admin = Href("javascript:;", onMouseOut="MM_startTimeout();",
			    onMouseOver="MM_showMenu(menu_ADM,0,0,null,'admin_bt');"
			    )


			

		tr = TR()
		
		tdmargin = TD()
		tdmargin.append(img_navbg)
		
		#tdsub = TD()
		#href_sub.append(img_subscriber_bt)
		#tdsub.append(href_sub)
		
		#tdspacer = TD()
		#tdspacer.append(img_spacer)
		
		#tdcampaign = TD()
		#href_camp.append(img_campaign_bt)
		#tdcampaign.append(href_camp)
		
		#tdprovisioningbt = TD()
		#href_prov.append(img_provisioning_bt)
		#tdprovisioningbt.append(href_prov)
		
		#tdaccessright = TD()
		#href_access.append(img_accessright_bt)
		#tdaccessright.append(href_access)
		
		tdadminbt = TD()
		href_admin.append(img_admin_bt)
		tdadminbt.append(href_admin)


		tr.append(tdmargin)
		#tr.append(tdsub)
		#tr.append(tdspacer)
		#tr.append(tdcampaign)
		#tr.append(tdspacer)
		#tr.append(tdprovisioningbt)
		#tr.append(tdspacer)
		#tr.append(tdaccessright)
		#tr.append(tdspacer)
		tr.append(tdadminbt)
		

						
		menuTable = TableLite(border="0", cellpadding="0", cellspacing="0")		
		menuTable.attr_dict['class'] = ' class="nav_frame"'
		menuTable.append(tr)
		doc.script = javascript_code2
		
		#-- wrap and emit ----------------------------------------------

		#doc.append(Center(image_h))
		doc.append(menuTable)
		doc.append(menu)
		doc.append(form)

		doc.write(file)


#-- Classes internally used to construct the JS menu --------------------------


class _Service:

	debug=0

	def __init__(self,menuname,name,desc="",url="",titlebar="",**kw):
		# init
		self.menuname=menuname
		self.name=name				# service name
		self.desc=desc				# service description
		self.url=url
		self.titlebar=titlebar

		# deduced from name
		self.level=self.getLevel()
		self.father=self.getAscendant()		# service path

		# defaults
		self.has_child=0
		self.isFirst=0
		self.isLast=0
		self.target="_self"
		self.action=self.url
		self.leftmargin=3

		# constants	
		self.width=150
		self.height=15
		self.image=""
		self.image2=""
		self.bgcolor="#C0C0C0"
		self.bgcolor2="#C0C0C0"
		self.fgcolor="#000000"
		self.fgcolor2="#FFFFFF"
		self.statusbar=""

		for name, value in kw.items():
			setattr(self, name, value)


	def setChild(self):
		#if self.level==1 and not self.has_child: self.desc=self.desc+'...'
		self.has_child=1

	def getAscendant(self):
		splitted=map(string.strip,string.split(self.name,'/'))
		return string.join(splitted[:-1],'/')

	def getLevel(self):
		return len(string.split(self.name,'/'))-1

	def logDebug(self,txt=None):
		if not self.debug: return
		if txt: print txt
		else: print str(self)

	def __repr__(self):
		return "<%(name)s level=%(level)s father=%(father)s first=%(isFirst)s last=%(isLast)s>" % self.__dict__

	def __str__(self):
		#if self.desc: self.desc="&nbsp;"*self.leftmargin+self.desc
		#return """%(menuname)s.awBmnbspM("%(name)s","%(father)s","%(desc)s","%(action)s","%(target)s",%(width)s,%(height)s,"%(image)s","%(image2)s","%(bgcolor)s","%(bgcolor2)s","%(fgcolor)s","%(fgcolor2)s","%(statusbar)s");\n""" % self.__dict__
		return self.menuname+""".addMenuItem("%(desc)s","%(action)s");\n""" % self.__dict__
	
	def writeSelf(self, menuname=None):
		if menuname == None: menuname = self.menuname
		return menuname+""".addMenuItem("%(desc)s","%(action)s");\n""" % self.__dict__



class _CategoryTitle(_Service):

	def __init__(self,menuname,name,image):
		_Service.__init__(self,menuname,name)
		self.image=image
		self.height=24
		self.bgcolor="#444444"
		self.bgcolor2="#444444"
		self.fgcolor="#ffffff"
		self.fgcolor2="#ffffff"


class _Separator(_Service):
	
	def __init__(self,menuname,index):
		_Service.__init__(self,menuname,"SEP%s" % index)
		self.height=0
		self.bgcolor="#ffffff"
		self.bgcolor2="#ffffff"
		self.fgcolor="#ffffff"
		self.fgcolor2="#ffffff"


class _HL(_Service):
	
	def __init__(self,menuname,father,index):
		_Service.__init__(self,menuname,"HL%s" % index)
		self.father=father
		self.image="/images/CategoryTop_WEB.gif"
		self.height=5		
		self.bgcolor="#ffffff"
		self.bgcolor2="#ffffff"
		self.fgcolor="#ffffff"
		self.fgcolor2="#ffffff"


class _BL(_Service):
	
	def __init__(self,menuname,father,index):
		_Service.__init__(self,menuname,"BL%s" % index)
		self.father=father
		self.image="/images/CategoryBottom_WEB.gif"
		self.height=4		
		self.bgcolor="#ffffff"
		self.bgcolor2="#ffffff"
		self.fgcolor="#ffffff"
		self.fgcolor2="#ffffff"



class _Menu:
		
	def __init__(self,allowedServices=[],tree=None,debug=0):

		# constants
		self.scriptSrc="/html/mm_menu.js"
		self.menuname="navmenu"
		self.scrolling=0
		self.horizontal=0
		self.Xtopleft=0
		self.Ytopleft=65
		self.gap=0
		self.click_needed=0
		self.timeout=1000
		self.textoffest=0
		self.fontface="Verdana, Arial, Helvetica, sans-serif"
		self.fontsize=10
		self.bold=0
		self.italic=0
		self.bordersize=0
		self.bordercolor="#444444"
		self.Xsubmenuoffset=40
		self.Ysubmenuoffset=-90
		self.glide=0
		self.glidestep=5
		self.glidespeed=1
		self.filter=""
		self.menuWidth = 125
		self.menuItemHeight = 19
		self.fontColor = "#cccccc"
		self.fontColorHilite = "#ffffff"
		self.menuItemBgColor = "#444444"
		self.menuHiliteBgColor = "#0066ff"

		# init
		self.debug=debug
		self.menuframe=""
		self.urlframe=""

		# process
		#list of service ids
		self.allowedServices=self.interpol(allowedServices)
		#tree is a comprehensive list of all MMI services
		self.tree=self.makeTree(tree)

		self.sortedServices,\
		self.serv_level0,\
		self.serv_level1,\
		self.serv_level2=self.sortAllowedServices()

		self.setPositions()


	def makeTree(self,tree):
		tmp=[]
		if tree:
			for serv in tree:
				try:
					name,desc,url,titlebar=serv
				except:
					name,desc,url=serv
					titlebar=desc
				titlebar=string.replace(titlebar,"'","\\\\'")
				tmp.append(_Service(self.menuname,name,desc,url,titlebar))
		return tmp

	def interpol(self,leaves):
		"constructs the whole tree from the leaves"
		tmp=[]
		levelCurrent=leaves[:]
		"""	
		import sys
		sys.stderr.write("leaves*****************\n")
		for leaf in leaves: sys.stderr.write(leaf+"\n")
		sys.stderr.write("***********************\n")
		"""
		while 1:
			levelUp=self.getFathers(levelCurrent)
			"""	
			sys.stderr.write("levelUp*****************\n")
			for i in levelUp: sys.stderr.write(i+"\n")	
			sys.stderr.write("************************\n")
			"""
			if not levelUp: break
			tmp=tmp+levelUp
			levelCurrent=levelUp
		return leaves+tmp

	def getFathers(self,leaves):
		tmp=[]
		for leaf in leaves:
                        service=_Service(self.menuname,leaf)
                        father=service.getAscendant()
                        if father and father not in leaves+tmp:     
				tmp.append(father)
		return tmp
	
	def sortAllowedServices(self):
		"Returns services,level0,level1,level2"
		tmp=[];tmp0=[];tmp1=[];tmp2=[]
		tmp_allowedServices=self.allowedServices[:]
		for s in self.tree:
		  for v_as in tmp_allowedServices:
		    if v_as==s.name or v_as=='*':
		    	s.debug=self.debug
		    	try: self.getService(s.father).setChild()
		    	except AttributeError: pass
		    	tmp.append(s)
		    	if s.level==0: tmp0.append(s)
		    	elif s.level==1: tmp1.append(s)
		    	elif s.level==2: tmp2.append(s)
			#tmp_allowedServices.remove(v_as)
# 		self.logDebug(tmp0)
# 		self.logDebug(tmp1)
# 		self.logDebug(tmp2)
# 		self.logDebug(tmp)
		return tmp,tmp0,tmp1,tmp2

	def setPosition(self,services):
		prev_services=[None]+services[:-1]
		next_services=services[1:]+[None]
		for prev,s,next in zip(prev_services,services,next_services):
			if prev:
				self.logDebug('prev:');prev.logDebug()
				if prev.father!=s.father: s.isFirst=1
			else:
				self.logDebug('prev is None')
				s.isFirst=1
			if next:
				self.logDebug('next:');next.logDebug()
				if next.father!=s.father: s.isLast=1
			else:
				self.logDebug('next is None')
				s.isLast=1
			self.logDebug('current:');s.logDebug()
			self.logDebug('---')

	def setPositions(self):
		for serv_list in [self.serv_level0,self.serv_level1,self.serv_level2]:
			self.setPosition(serv_list)

	def getService(self,name):
		for s in self.tree:
			if s.name==name: return s

	def __str__(self):
		self.logDebug(self.serv_level0)
		JScode=[];self.nb_sep=0;self.nb_hl=0;self.nb_bl=0
		#JScode.append(self.writeNewMenuObject())
		self.logDebug(self.sortedServices)
# 		for s1 in self.serv_level1:
# 			if s1.has_child:
# 				JScode.append(self.writeNewMenuObject(s1))
# 				for s2 in self.serv_level2:
# 					if s2.father == s1.name:
# 						JScode.append(self.writeService(s2))
# 					
# 				
# 			
		l1 = ""
		for s in self.sortedServices:
			#s.logDebug(s.menuname)
			if s.level==0:
				#set menuname
				s.menuname = "menu_%s"%s.name.replace("/","_")
				if len(l1) > 0:
					JScode.append(self.writeNewMenuObject(self.serv_level0[self.serv_level0.index(s)-1]))
					JScode.append(l1)
				#reset l1 item list
				l1 = ""
				#JScode.append(self.writeNewMenuObject(s))
				#JScode.append(self.writeSeparator())
				continue
			# level is >0 and has child
			if s.level == 1:
				if s.has_child:
					#create new menu
					JScode.append(self.writeNewMenuObject(s))
				else:
					# add the item to father menu
					l1 = l1+(self.writeService(s))
				continue
			# level is 2
			#if s.isFirst and s.level==2: JScode.append(self.writeHL(s.father))
			JScode.append(self.writeService(s))
			#if s.isLast: JScode.append(self.writeBL(s.father))
			if s.isLast and s.level==2: l1 = l1 + (self.writeNewChildMenuObject(s))
		#append the last level 0 item and any child items it contains
		JScode.append(self.writeNewMenuObject(self.serv_level0[len(self.serv_level0)-1]))
		JScode.append(l1)
		#JScode.append(self.writeMenuExec())
		#script1=Script(src=self.scriptSrc)
		script2=Script(code=string.join(JScode,''))
		return str(script2)

	def writeService(self,service):
		if service.has_child: service.target="_self"
		elif service.name=='MMI/QUIT': service.target='top'
		else: service.target=self.urlframe
		#if service.level==1: service.father=""
		if service.url and service.name!='MMI/QUIT': 
			service.action="javascript: parent.navFrame.gotoURL('%(url)s','%(titlebar)s')" % service.__dict__
		else: service.action=service.url
		return service.writeSelf(self.getService(service.father).menuname)

	def writeSeparator(self):
		self.nb_sep+=1
		return str(_Separator(self.menuname,self.nb_sep))

	def writeCategoryTitle(self,service):
		return str(_CategoryTitle(self.menuname,service.name,service.desc))

	def writeHL(self,father):
		self.nb_hl+=1
		return str(_HL(self.menuname,father,self.nb_hl))

	def writeBL(self,father):
		self.nb_bl+=1
		return str(_BL(self.menuname,father,self.nb_bl))

	def writeNewMenuObject(self, service):
		#return """var awbMNBSpm=new awbmnbspm();\n%(menuname)s=new TJSMenuType2("%(menuname)s","%(menuframe)s",%(scrolling)s,%(horizontal)s,%(Xtopleft)s,%(Ytopleft)s,%(gap)s,0,%(click_needed)s,%(timeout)s,%(textoffest)s,"%(fontface)s",%(fontsize)s,%(bold)s,%(italic)s,%(bordersize)s,"%(bordercolor)s",%(Xsubmenuoffset)s,%(Ysubmenuoffset)s,%(glide)s,%(glidestep)s,%(glidespeed)s,"%(filter)s");\n""" % self.__dict__ 
		service.menuname = "menu_%s"%service.name.replace("/","_")
		return "window.%s"%service.menuname +" = new Menu('"+service.desc+"""',%(menuWidth)s,%(menuItemHeight)s,"%(fontface)s",%(fontsize)s,"%(fontColor)s","%(fontColorHilite)s","%(menuItemBgColor)s","%(menuHiliteBgColor)s","center","middle",2,0,1000,0,0,true,true,true,0,false,false);\n"""%(self.__dict__)

	def writeNewChildMenuObject(self, service):
		fatherservice = self.getService(service.father)
		#childmenu = self.writeNewMenuObject(service)
		return self.getService(fatherservice.father).menuname+".addMenuItem(%s);\n"%fatherservice.menuname
		
	def writeMenuExec(self):
		return self.menuname+".awBmNBspm();"

	def logDebug(self,txt=None):
		if not self.debug: return
		if txt: print txt


#-- Test ---------------------------------------------------------------------

def _test():
	import os

	file="./test.cfg"
	os.environ['CFGFILEPATH']=file

	# test cfg file
	f=open(file,'w')
	f.write('LANGUAGE french\nCHARSET iso-8859-1')
	f.close()

	#language
	lm=LanguageManager()

	# menu tree
	menu_tree=[
	(       'SUB',
        	lm.getImgPath("CategorySubscriber_WEB.gif"),
		""
	),
        (       'SUB/USERINFO',
                "LABEL_USERINFO",
                "/cgi-bin/service/subscriber/subscr_info.cgi",
		"titleBAR ' s title"
        ),
	(	'SUB/RIGHT',
		"LABEL ' SUB_RIGHT",
		"/cgi-bin/service/inforights/inforights.cgi"
	)]

	# class instance
	nav=NavigationFrame(['*'],menu_tree, automatic_title=1,debug=1)
	nav.write()

if __name__=='__main__':
	_test()

