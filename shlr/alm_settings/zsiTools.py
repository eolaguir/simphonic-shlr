#!/bin/env python

import soap
import alm
import string
import re
import random
import alm_tools
import thread
import os
import ZSI
from alm import almProvCPSecurityTable
from alm import almProvFileMapping
from alm import almProvisionGetCardProfile
from alm import ALM_PROVISION_MAX_NB_ELEMENT

from pyClient import *
from dao_idl import _0_ci
from ci import *
from cmm_idl import _0_ci
from os import environ
from datetime import datetime
from datetime import date

CARD_PROFILE_DEFAULT_SECURITY_DOMAIN='CARD_DOMAIN'

#class almAddReqOp:
#	def __init__(self, lPart):
#		self.lPart=lPart

#used for regEx validation

OK = "OK"
NOT_OK = "NOT_OK"

ESAPDU = "ENHANCED_SEND_APDU"
TSMSTM = "TSM_SEND_TEXT_MESSAGE"

class Error(Exception):
	def __init__(self, code, message):
		self.code = code
		self.message = message
	def __str__(self):
		errorMessage = 'code: %d | message: %s' % (self.code, self.message)
		return repr(errorMessage)

class ResponseCode:
	def __init__(self, code):
		self._code = code
		
	def __int__(self):
		return self._code

ResponseCode.typecode = ZSI.TCnumbers.Iint(pname="code", aname="_code", typed=False, encoded=None, minOccurs=1, maxOccurs=1, nillable=True)

class ResponseTSMReferenceId:
	def __init__(self, tsmReferenceId):
		self._tsmReferenceId = tsmReferenceId
		
	def __int__(self):
		return self._tsmReferenceId

ResponseTSMReferenceId.typecode = ZSI.TCnumbers.Iint(pname="tsmReferenceId", aname="_tsmReferenceId", typed=False, encoded=None, minOccurs=1, maxOccurs=1, nillable=True)


def checkDir():
	dirname = environ['SIMphonIC']+'/asp/'+ environ['ASP_NAME']+'/mmi/alm/soap/log/timelog'
	logInfo("dirName <%s>"%(dirname))
	try:
		os.makedirs(dirname)
		return 0
 	except:
		if os.path.exists(dirname):
			return 0
		else:
			return 1
            
def logInfo(text):
	f=open(environ['SIMphonIC']+'/asp/'+ environ['ASP_NAME']+'/mmi/alm/soap/log/almzsi.log', 'a+b')
	f.write(datetime.today().isoformat(' ')+' '+text +'\n')
	f.close()
	
def logNotifInfo(text):
	f=open(environ['SIMphonIC']+'/asp/'+ environ['ASP_NAME']+'/mmi/alm/soap/log/almnotif.log', 'a+b')
	f.write(datetime.today().isoformat(' ')+' '+text +'\n')
	f.close()

def logSoapExecutionTime(text, msisdn, tsmRef):
	st = checkDir()
	logInfo("cd <%d>"%(st))
	if st == 0:
      		l = thread.allocate_lock()
      		l.acquire()
      		logInfo("SIMphonIC <%s> ASP_NAME <%s>"%(environ['SIMphonIC'], environ['ASP_NAME']))
      		timeInfo = datetime.today()
      		strTime = timeInfo.strftime("%m%d")
      		strDate = 'soap_timelog_' + strTime
      		logInfo("strDate <%s>"%(strDate))
      		fileName = environ['SIMphonIC']+'/asp/'+ environ['ASP_NAME']+'/mmi/alm/soap/log/timelog/'+strDate
      		logInfo("fileName <%s>"%(fileName))
      		f=open(fileName, 'a+b')
      		f.write('['+msisdn +' '+str(tsmRef) +']'+' '+'<START> '+datetime.today().isoformat(' ')+'\n')
      		f.close()
      		l.release()
	else:
  		logInfo("error in opening/creating directory")

def getAlmReInitAPI (sessionID):
	if not(re.match("^[-\d]+$", sessionID)):
		return alm.almErr_NO_SESSION
	else:
		cfg = soap.soapGetConfig()
		# retrieving userid/session key from the ALM sessionID to be able to init standard alm API
		st, sess_key, userid = soap._getSessionParams(sessionID);
		# init of alm API for direct calls to the ALM api (without the ALM layer)
		st = alm.almReInitAPI(cfg.almAddress, cfg.almPort, userid, sess_key)
		st = alm.almCheckSession()
		logInfo('almCheckSession status: %s' % (st))
		return st

#get list of parameters
#def getListParam(attribute):
#	return_dict = {}
#	print "attribute:%s" %(type(attribute))
#	#for member in dir(attribute.__class__):
	
#	#print "member : %s" %member
#	#if type(attribute) == type(0):
#	#	print 'code:%d '%(attribute)
#	#	return_dict['code']=int(attribute)
#	#tuple
#	if type(attribute) == type((1,2,3)):
#	#elif type(attribute) == type((1,2,3)):
#		print 'tuple:%s '%str(attribute)
#		for item in attribute:
#			for member in [ "%s"%mb for mb in dir(item.__class__) if '__' not in "%s"%mb ]:
#				if type(item.__getattr__(member)) == type(0):
#					return_dict[member]= int(item.__getattr__(member))
#				elif type(item.__getattr__(member)) == type('0'):
#					return_dict[member]= str(item.__getattr__(member))
#				elif ((-1 != (type(member)).find('class')) or (type (member) == type((1,2,3)))):
#					print 'int:%s %d'%(member, int(parameter))
#					return_dict[member] = getListParam(member)
#	# class attribute
#	elif (-1 != (str(type(attribute)).find('class'))):
#	#elif 1:
#		print 'class:%s '%(attribute)
#		#classname = str(attribute).split('<C')[1].split('instance')[0].strip()
#		#return_dict[classname]={}
#		class_dict = {}
#		for member in [ "%s"%mb for mb in dir(attribute.__class__) if '__' not in "%s"%mb ]:
#			att_member = attribute.__getattr__(member)
#			# printing members names, types and values 
#			if type(att_member) == type(0):
#				print 'class:%s '%(attribute)
#				class_dict[member]=int(att_member)
#			elif type(att_member) == type('0'):
#				class_dict[member]=str(att_member)
#			elif ((-1 != str(type(att_member)).find('class')) or (str(type(att_member)) == type((1,2,3)))):
#				class_dict[member]=getListParam(att_member)
#		#return_dict[classname] = class_dict
#		return_dict = class_dict
#	else:
#		print 'invalid type for %s'%str(attribute)
#		raise 'invalid type'
#	return return_dict

#print "	%s	%s"%(dir(getAppliInfo_result[2].__class__), type(getAppliInfo_result[2]))

def status(op, code, msisdn='', tsmReferenceId=0, requestID=0):
	message = alm.almStatusToString(code)
	if op == ESAPDU or op == TSMSTM:
		return {'code': code, 'message': message, 'msisdn': msisdn, 'tsmReferenceId': tsmReferenceId, 'requestID': requestID }
	
def statusMessage(op, code, message, msisdn='', tsmReferenceId=0, requestID=0):
	message = '%s (%s)' % (alm.almStatusToString(code), message)
	if op == ESAPDU or op == TSMSTM:
		return {'code': code, 'message': message, 'msisdn': msisdn, 'tsmReferenceId': tsmReferenceId, 'requestID': requestID }

def getFileUpdateMode(mode):
        if (mode == "updateModeDirect"):
                return 0
        elif (mode == "updateModeDelta"):
                return 1
        else:
                return -1

def get0348Params(gsm0348Params):
        tar = gsm0348Params._tar
        ciphering = gsm0348Params._ciphering
        porType = getPorType (gsm0348Params._porType)
        porCiphering = gsm0348Params._porCiphering
        porMedia = getPorMedia (gsm0348Params._porMedia)

        p = soap.ALM__gsm0348Params()
        p.tar = tar
        p.ciphering = ciphering
        p.porType = porType
        p.porCiphering = porCiphering
        p.porMedia = porMedia
        params = soap.ALM__gsm0348ParamsPtr (p)	
	return params

def getAuth (type):
        if (type.upper() == "authNone".upper()):
                return 0
        elif (type.upper() == "authRC".upper()):
                return 1
        elif (type.upper() == "authCC".upper()):
                return 2
        else:
                return alm.almErr_INVALID_ENUM_VALUE

def getPorType (type):
        if (type.upper() == "porTypeNone".upper()):
                return 0
        elif (type.upper() == "porTypeAlways".upper()):
                return 1
        elif (type.upper() == "porTypeOnError".upper()):
                return 2
        else:
                return alm.almErr_INVALID_ENUM_VALUE

def getPorMedia (media):
        if (media.upper() == "porMediaSmsDeliverReport".upper()):
                return 0
        elif (media.upper() == "porMediaSmsSubmit".upper()):
                return 1
        else:
                return alm.almErr_INVALID_ENUM_VALUE

def getTargetType(type):
	if (type == "campaignTargetGroup"):
		return 0
	elif (stype == "campaignTargetFile"):
		return 1
	elif (stype == "campaignTargetList"):
		return 2
	else:
		return -1

def getTargetState(state):
	if (state == "appletstateLocked"):
		return 0
	elif (state == "appletstateUnlocked"):
		return 1
	else:
		return -1

def getCampaignState(state):
	if (state == "campaignStateDisabled"):
		return 0
	elif (state == "campaignStateWaiting"):
		return 1
	elif (state == "campaignStateRunning"):
		return 2
	elif (state == "campaignStateFinished"):
		return 3
	elif (state == "campaignStateCreating"):
		return 4
	elif (state == "campaignStateTerminating"):
		return 5
	else:
		return -1

def getTimeslots (tab):
	ret = []
	for i in range(0, len(tab)):
		slot = soap.ALM__campaignTimeSlot()
		slot.begin = tab[i]._begin
		slot.end = tab[i]._end
		slotPtr = soap.ALM__campaignTimeSlotPtr (slot)
		ret.append (slotPtr)
        return ret

def getRecordData(tab):
	ret = []
	for i in range(0, len(tab)):
		record = soap.ALM__recordData()
		record.position = tab[i]._position
		record.data = tab[i]._data
		recordPtr = soap.ALM__recordDataPtr (record)
		ret.append (recordPtr)
	return ret

def getRecordFiles(tab):
	ret = []
	for i in range(0, len(tab)):
		record = soap.ALM__recordFileData()
		record.position = tab[i]._position
		record.data = tab[i]._data
		recordPtr = soap.ALM__recordFileDataPtr (record)
		ret.append (recordPtr)
	return ret


def getRecordFiles1(tab):
        ret = []
        for i in range(0, len(tab)):
                record = soap.ALM__recordData()
                record.position = tab[i]._position
                record.data = tab[i]._data
                recordPtr = soap.ALM__recordDataPtr (record)
                ret.append (recordPtr)
        return ret

def getOperationTypeString (val):
	if (val == 0):
		return "campaignOperationSetAppletState"
	elif (val == 1):
		return "campaignOperationUpdateFile"
	elif (val == 2):
		return "campaignOperationSendAPDU"
	else:
		return "null"

def getGSMTypeString (val):
	if (val == alm.almGSMType_BINARY):
		return "BINARY" 
	elif (val == alm.almGSMType_RECORD):
		return "RECORD"
	else:
		return "null"

def getTokenFlag (tokenFlag):
	if (tokenFlag == "none"):
			return 0
	elif (tokenFlag == "auto-generate"):
			return 1
	elif (tokenFlag == "user-supplied"):
			return 2
	else:
			return -1

def getAdnMode (val):
	if (val.upper() == "ADN"):
		return 1 
	elif (val.upper() == "FDN"):
		return 0
	else:
		return alm.almErr_INVALID_ENUM_VALUE
		
def getAdnModeName (val):
	if (val.upper() == 't'.upper()):
		return "ADN" 
	elif (val.upper() == 'f'.upper()):
		return "FDN"
	else:
		return ""

def getUseLoginCATTP (val):
	if (val.upper() == "loginCATTPEnabled".upper()):
		return 1
	elif (val.upper() == "loginCATTPDisabled".upper()):
		return 0
	else:
		return alm.almErr_INVALID_ENUM_VALUE
		
def getLockStatus (val):
	if (val.upper() == "locked".upper()):
		return 1 
	elif (val.upper() == "unlocked".upper()):
		return 0
	else:
		return alm.almErr_INVALID_ENUM_VALUE

def getLockStatusName (val):
	if (val.upper() == 't'.upper()):
		return "locked" 
	elif (val.upper() == 'f'.upper()):
		return "unlocked"
	else:
		return ''

def getTarCounterList (cardProfName, counterValue):
	appTypes = []
	st, CPDataMMI, CPSecuMMI, CPFileMapped, CPOpeTypeMMI, is_card_cattp, use_push_secu_cattp =\
	alm.almProvisionGetCardProfile(
	cardProfName, 2 * alm_tools.NB_MAX_TAR, alm_tools.NB_MAX_MAPPINGS, alm_tools.NB_MAX_OPE_TYPES)
	for elem in CPSecuMMI:
		tar_found = 0
		for f in appTypes:
			if elem.app_type_name == f.app_type_name and elem.app_type_tar == f.app_type_tar and elem.app_type_cmd_set == f.app_type_cmd_set:
				tar_found = 1
		if tar_found == 0:
			tmp = alm.almTarCounter()
			tmp.counter_index = 1
			tmp.counter_value = counterValue
			tmp.app_type_name = elem.app_type_name
			tmp.app_type_tar = elem.app_type_tar
			tmp.app_type_cmd_set = elem.app_type_cmd_set
			appTypes.append (tmp)
	return appTypes
	
def getGsmServiceList (tab):
	ret = []
	for i in range(0, len(tab)):
		tmp = alm.almOcpGsmFiles()
		tmp.name = str(tab[i]._name).upper()
		tmp.ref = str(tab[i]._ref).upper()
		tmp.tagLen = int(tab[i]._tagLen)
		tmp.size = int(tab[i]._size)
		tmp.recLen = int(tab[i]._recLen)
		
		ret.append (tmp)
	return ret

def getAppliList (tab):
				
	ret = []
	#checks for duplicate aid
	if len(tab) > 1:
		for i in range(0, len(tab)):
			if str(tab[i]._aid) != '':
				for j in range(i + 1, len(tab)):
					if str(tab[i]._aid).upper() == str(tab[j]._aid).upper():
						return alm.almErr_DUPLICATE_APPLICATION_AID

	#checks application aid existence
	if len(tab) > 0:
		for i in range(0, len(tab)):
			if str(tab[i]._aid) != '':
				st = validateAppliAid(str(tab[i]._aid))
				if st != alm.almProvision_REFERENCED_APPLICATION_AID_ALREADY_EXIST:
					return alm.almProvision_REFERENCED_APPLICATION_AID_NOT_EXIST

	#checks for duplicate TAR
	appliTarList = []
	for i in range(0, len(tab)):
		if str(tab[i]._aid) != '':
			applicationRec = applicationFactory.getApplication(str(tab[i]._aid).upper()).getRecord()
			appliTarList.append(applicationRec.TAR.replace(' ', '').strip())

	for i in range(0, len(appliTarList)):
		if appliTarList[i] != '':
			for j in range(i + 1, len(appliTarList)):
				if str(appliTarList[i]).upper() == str(appliTarList[j]).upper():
					return alm.almErr_DUPLICATE_TAR

	for i in range(0, len(tab)):
		tmp = alm.almOcpAppliTable()
		tmp.aid = str(tab[i]._aid).upper()
		tmp.name = str(tab[i]._name)
		lockStatus = int(getLockStatus(tab[i]._lock))
		if lockStatus == alm.almErr_INVALID_ENUM_VALUE:
			return alm.almErr_INVALID_ENUM_VALUE

		if lockStatus == 1:
			tmp.lock = 't'
		else:
			tmp.lock = 'f'


		gpState = str(tab[i]._state).upper()
		if gpState != '':
			if (gpState == "installed".upper()):
				tmp.status = 'i'
			elif (gpState == "selectable".upper()):
                                tmp.status = 's'
			elif (gpState == 'NONE'):
                                tmp.status = 's'
                        else:
                                return alm.almErr_GSM_PROFILE_STATE
		else:
			tmp.status = 's'
		
		ret.append (tmp)
	return ret

def getPackList (aidList):

	#checks for duplicate aid
	for i in range(0, len(aidList)):
		if str(aidList[i]) == '':
			return alm.almErr_INVALID_FIELD
		if str(aidList[i]) != '':
			for j in range(i + 1, len(aidList)):
				if str(aidList[i]).upper() == str(aidList[j]).upper():
					return alm.almErr_DUPLICATE_PACKAGE_AID
			status = validatePackageAid(str(aidList[i]).upper())
			if status == 0: #package aid not exists
				return alm.almProvision_REFERENCED_PACKAGE_AID_NOT_EXIST

	ret = []
	for i in range(0, len(aidList)):
		if str(aidList[i]) != '':
			ret.append (str(aidList[i]).upper())
	return ret
	
def validateAppliAid (aid):

	#existence check for application aid
	st, AppliListDB = alm.almProvisionGetAppliList(alm_tools.NB_MAX_APPLI)
	
	if st == alm.almOK:
		for appli in AppliListDB:
			if str(aid).upper() == str(appli.aid).upper():
				return alm.almProvision_REFERENCED_APPLICATION_AID_ALREADY_EXIST
	
	return st
		
def validatePackageAid (aid):

	#existence check for package aid
	try:
		packageRec = packageFactory.getPackageInterface(str(aid).upper()).getRecord()
		return 1 #package already exists
		
	except Exception, e:
		return 0

def validateAppliName (appliName):
	
	#existence check for appliName
	st, AppliListDB = alm.almProvisionGetAppliList(alm_tools.NB_MAX_APPLI)
	
	if st == alm.almOK:
		for appli in AppliListDB:
			if appliName == appli.name:
				return alm.almProvision_REFERENCED_APPLICATION_NAME_ALREADY_EXIST
	
	return st

def validateCardProfile(cardProfName):
	#existence check for card profile
	st, CPDataMMI, CPSecuMMI, CPFileMapped, CPOpeTypeMMI, is_card_cattp, use_push_secu_cattp = almProvisionGetCardProfile(cardProfName, 2 * alm_tools.NB_MAX_TAR, alm_tools.NB_MAX_MAPPINGS, alm_tools.NB_MAX_OPE_TYPES)
	return st, int(is_card_cattp)

def getInstalledAidList (aidList):
	ret = []
	for i in range(0, len(aidList)):
		ret.append (str(aidList[i]))
	return ret
	
def getSecurityDomainList (sdList):
	ret = []
	for i in range(0, len(sdList)):
		ret.append (str(sdList[i]))
	return ret

def getALMStatusString ():
	return almStatusToString(st)

def getAppliType(type):
	if (type.upper() == "Proprietary".upper()):
		return 'p'
	elif (type.upper() == "Standard".upper()):
		return 's'
	else:
		return alm.almErr_INVALID_ENUM_VALUE
		
def getAppliTypeName(type):
	if (type.upper() == 'P'):
		return "Proprietary"
	elif (type.upper() == 'S'):
		return "Standard"
	else:
		return "Unknown"
		
def getDLProtocol(type):
	if (type.upper() == "USIM_R6".upper()):
		return 'USIM'
	elif (type.upper() == "SIMphonic_USIM_V2".upper()):
		return 'USIM2'
	elif (type.upper() == "Customer1_1".upper()):
		return 'CUST1_1'
	elif (type.upper() == "SIMphonic_V1".upper()):
		return 'v1.1'
	elif (type.upper() == "SIMphonic_V2_1C_OC_056262_V2_1D".upper()):
		return 'v2.1.c_2'
	elif (type.upper() == "SIMphonic_V2_1G_V3_1B".upper()):
		return 'v3.1.b'
	elif (type.upper() == "SIMphonic_V2_1C".upper()):
		return 'v2.1.c'
	elif (type.upper() == "SIMphonic_V2_1C_OC_056241".upper()):
		return 'v2.1.c_1'
	elif (type.upper() == "SIMERA_V2".upper()):
		return 'SIMERA2'
	elif (type.upper() == "SIMERA_V3".upper()):
		return 'SCHLUM_v3'
	elif (type.upper() == "SIMtelligence_32_16".upper()):
		return 'v1'
	elif (type.upper() == "Standard_03_48_V8_4_0_V8_6_0".upper()):
		return 'Default'
	elif (type.upper() == "Excel_data_V3".upper()):
		return 'EXCEL_v3'
	elif (type.upper() == "GemXplore_98_V1".upper()):
		return 'ESMS_v1'
	elif (type.upper() == "GemXplore_98_V2".upper()):
		return 'ESMS_v2'
	elif (type.upper() == "GemXplore_Expresso".upper()):
		return 'GEM_v3'	
	else:
		return alm.almErr_INVALID_ENUM_VALUE
		
def getKeyType(type):
	if (type.upper() == "INDEX"):
		return 'INDEX'
	elif (type.upper() == "KEY_SET"):
		return 'KEY_SET'
	else:
		return alm.almErr_INVALID_ENUM_VALUE
		
def getSecurity(type):
	if (type.upper() == "Auth_DES_CBC".upper()):
		return 'Auth DES CBC'
	elif (type.upper() == "Auth_PORAuth_DES_CBC".upper()):
		return 'Auth+PORAuth DES CBC'
	elif (type.upper() == "CT_Auth_3DES_Cipher_3DES2".upper()):
		return 'CT Auth 3DES Cipher 3DES2'
	elif (type.upper() == "CT_Auth_3DES2".upper()):
		return 'CT Auth 3DES2'
	elif (type.upper() == "CT_Auth_3DES2_No_POR".upper()):
		return 'CT Auth 3DES2 No POR'
	elif (type.upper() == "CT_Auth_3DES3".upper()):
		return 'CT Auth 3DES3'
	elif (type.upper() == "CT_Auth_DES_CBC".upper()):
		return 'CT Auth DES CBC'
	elif (type.upper() == "CT_Auth_DES_CBC_Cipher_3DES2".upper()):
		return 'CT Auth DES CBC Cipher 3DES2'
	elif (type.upper() == "CT_Auth_PORAuth_DES_CBC".upper()):
		return 'CT Auth+PORAuth DES CBC'
	elif (type.upper() == "CT_Cipher_Auth_DES_CBC".upper()):
		return 'CT Cipher+Auth DES CBC'
	elif (type.upper() == "CT_Cipher_Auth_PORAuth_DES_CBC".upper()):
		return 'CT Cipher+Auth+PORAuth DES CBC'
	elif (type.upper() == "CT_No_security".upper()):
		return 'CT No security'
	elif (type.upper() == "Cipher_Auth_DES_CBC".upper()):
		return 'Cipher+Auth DES CBC'
	elif (type.upper() == "Cipher_Auth_PORAuth_DES_CBC".upper()):
		return 'Cipher+Auth+PORAuth DES CBC'
	elif (type.upper() == "No_security".upper()):
		return 'No security'
	elif (type.upper() == "No_security_No_POR".upper()):
		return 'No security No POR'
	elif (type.upper() == "Auth_3DES2".upper()):
		return 'Auth 3DES2'
	elif (type.upper() == "Auth_3DES3".upper()):
		return 'Auth 3DES3'
	elif (type.upper() == "Cipher_Auth_3DES2".upper()):
		return 'Cipher+Auth 3DES2'
	elif (type.upper() == "Cipher_Auth_3DES3".upper()):
		return 'Cipher+Auth 3DES3'
	elif (type=='100'):
		return 'VIVO RAM'
	elif (type=='101'):
		return 'VIVO SIMchronize'
	elif (type=='102'):
		return 'VIVO HRS interact no POR)'
	else:
		return alm.almErr_INVALID_ENUM_VALUE

def getOperationType(type):
	if (type.upper() == "download_application".upper()):
		return 'Download application'
	elif (type.upper() == "remove_application".upper()):
		return 'Remove application'
	elif (type.upper() == "replace_application".upper()):
		return 'Replace application'
	elif (type.upper() == "lock_application".upper()):
		return 'Lock application'
	elif (type.upper() == "unlock_application".upper()):
		return 'Unlock application'
	elif (type.upper() == "card_audit".upper()):
		return 'Card Audit'
	elif (type.upper() == "switch_ADN_FDN".upper()):
		return 'Switch ADN-FDN'
	elif (type.upper() == "switch_FDN_ADN".upper()):
		return 'Switch FDN-ADN'
	elif (type.upper() == "update_GSM_file".upper()):
		return 'Update GSM file'
	elif (type.upper() == "resize_GSM_file".upper()):
		return 'Resize GSM file'
	elif (type.upper() == "send_command".upper()):
		return 'Send command'
	elif (type.upper() == "partial_card_audit".upper()):
		return 'Partial card audit'
	elif (type.upper() == "create_GSM_file".upper()):
		return 'Create GSM file'
	elif (type.upper() == "delete_GSM_file".upper()):
		return 'Delete GSM file'
	elif (type.upper() == "activate_GSM_file".upper()):
		return 'Activate GSM file'
	elif (type.upper() == "deactivate_GSM_file".upper()):
		return 'Deactivate GSM file'	
	else:
		return alm.almErr_INVALID_ENUM_VALUE

def getSupplierType(type):
	if (type.upper() == "G_and_D".upper()):
		return 'G &  D'
	elif (type.upper() == "Gemalto".upper()):
		return 'Gemalto'
	elif (type.upper() == "MicroElectronica".upper()):
		return 'MicroElectronica'
	elif (type.upper() == "Oberthur_Technologies".upper()):
		return 'Oberthur Technologies'
	elif (type.upper() == "Orga".upper()):
		return 'Orga'
	elif (type.upper() == "Schlumberger".upper()):
		return 'Schlumberger'
	elif (type.upper() == "Austria".upper()):
		return 'Austria'
	else:
		return alm.almErr_INVALID_ENUM_VALUE
	
def getSupplierTypeName(type):
	if (type.upper() == "G &  D".upper()):
		return "G_and_D"
	elif (type.upper() == "Gemalto".upper()):
		return "Gemalto"
	elif (type.upper() == "MicroElectronica".upper()):
		return "MicroElectronica"
	elif (type.upper() == "Oberthur Technologies".upper()):
		return "Oberthur_Technologies"
	elif (type.upper() == "Orga".upper()):
		return "Orga"
	elif (type.upper() == "Schlumberger".upper()):
		return "Schlumberger"
	elif (type.upper() == "Austria".upper()):
		return "Austria"
	else:
		return "Unknown"
	
def getAppletStatus(status):
	if (status.upper() == "Reference".upper()):
		return 'R'
	elif (status.upper() == "Obsolete".upper()):
		return 'O'
	elif (status.upper() == "Downloadable".upper()):
		return 'D'
	else:
		return alm.almErr_INVALID_ENUM_VALUE
		
def getAppletStatusName(status):
	if (status.upper() == 'R'):
		return "Reference"
	elif (status.upper() == 'O'):
		return "Obsolete"
	elif (status.upper() == 'D'):
		return "Downloadable"
	else:
		return "Unknown"

def getPropertyType(pType):
	if (pType.upper() == "Applet".upper()):
		return 'A'
	elif (pType.upper() == "Utility".upper()):
		return 'U'
	else:
		return alm.almErr_INVALID_ENUM_VALUE

def getPropertyTypeName(pType):
	if (pType.upper() == 'A'):
		return "Applet"
	elif (pType.upper() == 'U'):
		return "Utility"
	else:
		return "Unknown"

def getCardProfileList (cardProfName):
	#checks for duplicate card profile
	for i in range(0, len(cardProfName)):
		if str(cardProfName[i]) == '':
			return alm.almErr_INVALID_FIELD
		if str(cardProfName[i]) != '':
			for j in range(i + 1, len(cardProfName)):
				if str(cardProfName[i]) == str(cardProfName[j]):
					return alm.almErr_DUPLICATE_CARD_PROFILE_NAME

	ret = []
	for i in range(0, len(cardProfName)):
		if str(cardProfName[i]) != '':
			ret.append (str(cardProfName[i]))
	return ret

def validateCardProfileList (cardProfName):
	st = alm.almOK
	for i in range(0, len(cardProfName)):
		if str(cardProfName[i]) != '':
			st, is_card_cattp = validateCardProfile(str(cardProfName[i]))
			if st == alm.almOK:
				st = alm.almOK
			else:
				return st
	return st
def getTokenFlag2(tokenFlag):
	if (tokenFlag.upper() == "REQUIRED_TOKEN"):
		return _0_ci.REQUIRED_TOKEN	
	elif (tokenFlag.upper() == "NO_TOKEN"):
		return _0_ci.NO_TOKEN
	else:
		return alm.almErr_INVALID_ENUM_VALUE

def getG0348Counter(val):
	if (val.upper() == "AVAILABLE"):
		return _0_ci.COUNTER_AVAILABLE
	elif (val.upper() == "GREATER_THAN"):
		return _0_ci.GREATER_THAN
	elif (val.upper() == "ONE_GREATER_THAN"):
		return _0_ci.ONE_GREATER_THAN
	elif (val.upper() == "NONE"):
		return _0_ci.COUNTER_NONE
	else:
		return alm.almErr_INVALID_ENUM_VALUE

def getG0348Ciphering(val):
	if (val.upper() == "CIPHERING"):
		return _0_ci.CIPHERING
	elif (val.upper() == "NONE"):
		return _0_ci.CIPHERING_NONE
	else:
		return alm.almErr_INVALID_ENUM_VALUE

def getG0348Checksum(val):
	if (val.upper() == "RC"):
		return _0_ci.RC
	elif (val.upper() == "CC"):
		return _0_ci.CC
	elif (val.upper() == "DS"):
		return _0_ci.DS
	elif (val.upper() == "NONE"):
		return _0_ci.CHECKSUM_NONE
	else:
		return alm.almErr_INVALID_ENUM_VALUE

def getG0348PoR(val):
	if (val.upper() == "REQUIRED"):
		return _0_ci.REQUIRED
	elif (val.upper() == "ON_ERROR"):
		return _0_ci.ON_ERROR
	elif (val.upper() == "NONE"):
		return _0_ci.POR_NONE
	else:
		return alm.almErr_INVALID_ENUM_VALUE

def getG0348PoRMedia(val):
	if (val.upper() == "SMS_SUBMIT"):
		return _0_ci.SMS_SUBMIT
	elif (val.upper() == "SMS_DELIVER_REPORT"):
		return _0_ci.SMS_DELIVER_REPORT
	else:
		return alm.almErr_INVALID_ENUM_VALUE

def getG0348Algo(val):
	if (val.upper() == "DES"):
		return _0_ci.DES
	elif (val.upper() == "PROPRIETARY"):
		return _0_ci.PROPRIETARY
	elif (val.upper() == "IMPLICIT"):
		return _0_ci.IMPLICIT
	else:
		return alm.almErr_INVALID_ENUM_VALUE

def getG0348DESMode(val):
	if (val.upper() == "TRIPLE_DES2"):
		return _0_ci.TRIPLE_DES2
	elif (val.upper() == "TRIPLE_DES3"):
		return _0_ci.TRIPLE_DES3
	elif (val.upper() == "DES_ECB"):
		return _0_ci.DES_ECB
	elif (val.upper() == "DES_CBC"):
		return _0_ci.DES_CBC
	else:
		return alm.almErr_INVALID_ENUM_VALUE
		
def getG0348CounterValue(val):
	if (val == _0_ci.COUNTER_AVAILABLE):
		return "AVAILABLE"
	elif (val == _0_ci.GREATER_THAN):
		return "GREATER_THAN"
	elif (val == _0_ci.ONE_GREATER_THAN):
		return "ONE_GREATER_THAN"
	elif (val == _0_ci.COUNTER_NONE):
		return "NONE"
	else:
		return "Unknown"

def getG0348CipheringValue(val):
	if (val == _0_ci.CIPHERING):
		return "CIPHERING"
	elif (val == _0_ci.CIPHERING_NONE):
		return "NONE"
	else:
		return "Unknown"

def getG0348ChecksumValue(val):
	if (val == _0_ci.RC):
		return "RC"
	elif (val == _0_ci.CC):
		return "CC"
	elif (val ==  _0_ci.DS):
		return "DS"
	elif (val == _0_ci.CHECKSUM_NONE):
		return "NONE"
	else:
		return "Unknown"

def getG0348PoRValue(val):
	if (val == _0_ci.REQUIRED):
		return "REQUIRED"
	elif (val == _0_ci.ON_ERROR):
		return "ON_ERROR"
	elif (val == _0_ci.POR_NONE):
		return "NONE"
	else:
		return "Unknown"

def getG0348PoRMediaValue(val):
	if (val == _0_ci.SMS_SUBMIT):
		return "SMS_SUBMIT"
	elif (val == _0_ci.SMS_DELIVER_REPORT):
		return "SMS_DELIVER_REPORT"
	else:
		return "Unknown"

def getG0348AlgoValue(val):
	if (val == _0_ci.DES):
		return "DES"
	elif (val == _0_ci.PROPRIETARY):
		return "PROPRIETARY"
	elif (val == _0_ci.IMPLICIT):
		return "IMPLICIT"
	else:
		return "Unknown"

def getG0348DESModeValue(val):
	if (val == _0_ci.TRIPLE_DES2):
		return "TRIPLE_DES2"
	elif (val == _0_ci.TRIPLE_DES3):
		return "TRIPLE_DES3"
	elif (val == _0_ci.DES_ECB):
		return "DES_ECB"
	elif (val == _0_ci.DES_CBC):
		return "DES_CBC"
	else:
		return "Unknown"

def getBearer (bearer):
	if (bearer.upper() == "sms".upper()):
			return _0_ci.Media_SMS
	elif (bearer.upper() == "cattp".upper()):
			return _0_ci.Media_CATTP
	elif (bearer.upper() == "cattp_fallback_to_sms".upper()):
			return _0_ci.Media_SMSCATTP
	else:
			return alm.almErr_INVALID_ENUM_VALUE
			
def getBearer2 (bearer):
	if (bearer.upper() == "sms".upper()):
			return alm.almMedia_OTA;
	elif (bearer.upper() == "cattp".upper()):
			return alm.almMedia_CATTP
	else:
			return alm.almErr_INVALID_ENUM_VALUE
			
def getInstallOption (optInstall):
	logInfo('getInstallOption::optInstall ' + optInstall)
	if (optInstall.upper() == "install_for_make_selectable".upper()):
			return 0
	elif (optInstall.upper() == "install_for_install".upper()):
			logInfo('optInstall.upper ' + optInstall.upper())
			return 1
	elif (optInstall.upper() == "install_for_install_and_make_selectable".upper()):
			return 2
	else:
			return alm.almErr_INVALID_ENUM_VALUE

def getCPSecurityList (tab,flCattp):
	ret = []
				
	for i in range(0, len(tab)):
		tmp = almProvCPSecurityTable()
		
		secuDesc = getSecurity(tab[i]._secuDesc)
		if secuDesc == alm.almErr_INVALID_ENUM_VALUE:
			logInfo ('\n\tsecuDesc : %s'%(secuDesc))
			return alm.almErr_INVALID_ENUM_VALUE
		tmp.secuDesc = secuDesc
		
		keyType = getKeyType(tab[i]._keyType)
		if keyType == alm.almErr_INVALID_ENUM_VALUE:
			logInfo ('\n\tkeyType : %s'%(keyType))
			return alm.almErr_INVALID_ENUM_VALUE
		tmp.keyType = keyType
		
		if str(tab[i]._kicIndex) == '':
			return alm.almErr_INVALID_FIELD
		if validateIndex(str(tab[i]._kicIndex)) == NOT_OK:
			return alm.almErr_INVALID_FIELD
		tmp.kicIndex = int(tab[i]._kicIndex)
		
		if str(tab[i]._kidIndex) == '':
			return alm.almErr_INVALID_FIELD
		if validateIndex(str(tab[i]._kidIndex)) == NOT_OK:
			return alm.almErr_INVALID_FIELD
		tmp.kidIndex = int(tab[i]._kidIndex)
		
		tmp.app_type_name = str(tab[i]._appTypeName).upper()
		
		tmp.app_type_tar = str(tab[i]._appTypeTar).upper()
		
		tmp.app_type_cmd_set = str(tab[i]._appTypeCmdSet).upper()
				
		logInfo ('\n\tbearer : %s'%(tab[i]._bearer))
		bearer = getBearer2(tab[i]._bearer)
		if bearer == alm.almErr_INVALID_ENUM_VALUE:
			logInfo ('\n\tbearer : %s'%(bearer))
			return alm.almErr_INVALID_ENUM_VALUE
		if (flCattp == "0") and (bearer == alm.almMedia_CATTP):
			return alm.almErr_INVALID_ENUM_VALUE
			
		tmp.bearer = bearer
		
		ret.append (tmp)
	return ret

def getFileMappingList (tab,apptypeTab):
	mapListR = []
	ret = []
	for i in range(0, len(tab)):
		temp1 = str(tab[i]._origFile).strip().upper()
		temp2 = str(tab[i]._destFile).strip().upper()
		if (temp1 != '') and (temp2 != ''):
			tmp = almProvFileMapping()
			tmp.orig_file = temp1
			tmp.dest_file = temp2
			mapListR.append (tmp)
		else:
			return alm.almErr_INVALID_FIELD
	
	logInfo ('\n\tdone with almProvFileMapping')
	if len(mapListR) > 1:
		for i in range(0, len(mapListR)):
			for j in range(i + 1, len(mapListR)):
				if mapListR[i].orig_file == mapListR[j].orig_file:
					return alm.almProvision_DUPLICATE_ORIGINAL_GSM_FILE			
	
	CPFileMappeable = []
	CPFileMapped = []
	CPFileMappeable_Bin = []
	CPFileMappeable_Rec = []
	logInfo ('\tlen(mapListR) : %s'%(len(mapListR)))
	if len(apptypeTab) > 0:
		st, session = alm.almGetDefaultUserSession()
		st, files = alm.almGetGSMFileType(session, apptypeTab, alm_tools.NB_MAX_GSM_NAME)
		logInfo ('\n\tALL THE GSM FILES:')
		for ef in files:			
			CPFileMappeable.append(ef)
		for ef in CPFileMappeable:
			logInfo ('\tgsm : %s'%(ef.gsm_name))
		for ef in CPFileMappeable:
			if ef.type == alm.almGSMType_BINARY:
				CPFileMappeable_Bin.append(ef)
			else:
				CPFileMappeable_Rec.append(ef)

		
	if len(mapListR) > 0:
		for i in range(0, len(mapListR)):
			logInfo ('\n\tgetGSMFile')
			temp1 = str(mapListR[i].orig_file).upper()
			logInfo ('\ttemp1 : %s'%(temp1))
			temp2 = str(mapListR[i].dest_file).upper()
			found = 0
			for ef in CPFileMappeable:
				if ef.gsm_name == temp1:
					found = 1
					CPFileMapped.append(ef)
					break
						
			if found == 0:
				return alm.admErr_FILE_NOT_FOUND
				
		logInfo ('\n\tCPFileMapped:')
		cnt = 0
		for mapped in CPFileMapped:
			logInfo ('\tmapped : %s'%(mapped.gsm_name))
			logInfo ('\ttype : %s'%(mapped.type))
			temp2 = str(mapListR[cnt].dest_file).upper()
			if mapped.type == alm.almGSMType_BINARY:
				subEFList = CPFileMappeable_Bin
			else:
				subEFList = CPFileMappeable_Rec
				
			dest_found = 0
			if temp2 == mapped.gsm_name:
				return alm.admErr_INVALID_GSM_NAME
			else:
				for elem in subEFList:
					if elem.gsm_name == temp2:
						dest_found = 1
						break
						
			if dest_found == 0:
				return alm.admErr_INVALID_GSM_NAME

			cnt = cnt + 1
			
	#ret = []
	#for i in range(0, len(tab)):
	#	tmp = almProvFileMapping()
	#	tmp.orig_file = str(tab[i]._origFile)
	#	tmp.dest_file = str(tab[i]._destFile)              	
	#	ret.append (tmp)
	return mapListR


def getOperationTypeStringList (opeList):
	opeListR = []
	for i in range(0, len(opeList)):
		temp = str(opeList[i]).strip()
		if temp != '':
			opeListR.append (temp)
	
	opeListArr = []
	for i in range(0, len(opeListR)):
		temp = getOperationType(opeListR[i])
		if str(temp) == str(alm.almErr_INVALID_ENUM_VALUE):
			return alm.almErr_INVALID_ENUM_VALUE
		else:
			opeListArr.append (str(temp))
	
	
	for i in range(0, len(opeListArr)):
		for j in range(i + 1, len(opeListArr)):
			if str(opeListArr[i]).upper() == str(opeListArr[j]).upper():
				return alm.almProvision_DUPLICATE_OPERATION_TYPE
				
		#status = validateOpeType(str(opeListR[i]))
		#if status == 0: 
		#	return alm.almProvision_REFERENCED_OPERATION_TYPE_NOT_EXIST

	#ret = []
	#for i in range(0, len(opeListR)):
	#	ret.append (str(opeListR[i]))
	return opeListArr

def getIntValueList (intValueList):
	ret = []
	for i in range(0, len(intValueList)):
		ret.append (int(intValueList[i]))
	return ret

def convertStrToHexa(strInput):
	try:
		strInput = strInput.replace(' ','').strip()
		return strInput.decode('hex')
	except:
		return ''
		
def validateGSMProfile(ocpName):
	st, gsm_profiles = alm.almProvisionGetOcpList(ALM_PROVISION_MAX_NB_ELEMENT)
	
	for gsmProf in gsm_profiles:
		if ocpName == gsmProf:
			return st, OK
	return st, NOT_OK
	
def getCardProfileViaGSMProfile(ocpName):
	#completeOCP=alm.almProvisionGetCompleteOcp(
	#			ocpName,
	#			ALM_PROVISION_MAX_NB_ELEMENT, ALM_PROVISION_MAX_NB_ELEMENT, ALM_PROVISION_MAX_NB_ELEMENT, ALM_PROVISION_MAX_NB_ELEMENT)
	#st=completeOCP[0]
	#card_profile = completeOCP[3]
	#logInfo("moncp = %s"%str(completeOCP))
	#return card_profile
	return ocpName

def getKeyTypeAndKeyIndexes(cardProfileName):
	st,cardProfile,cpSecurityList,cpMappingList,cpOpeTypeMMI, is_card_cattp, use_push_secu_cattp=alm.almProvisionGetCardProfile(
		cardProfileName,
		2 * alm_tools.NB_MAX_TAR,
		alm_tools.NB_MAX_OPE_TYPES,
		alm_tools.NB_MAX_MAPPINGS
	)
	
	keyType = ""
	key_indexes = ""
	
	if st==0:
		# if no cpSecurityList, we are dealing with a V1 profile. Nothing to be done here
		if(len(cpSecurityList)>0):
			# for a V3 card.
			keyType = cpSecurityList[0].keyType

			tmp_indexes_dict = {}
			# tmp_kic_index_dict = {}
			# tmp_kid_index_dict = {}
			for secu_idx in range(0, len(cpSecurityList)):
				tmp_indexes_dict[cpSecurityList[secu_idx].kicIndex] = 1
				tmp_indexes_dict[cpSecurityList[secu_idx].kidIndex] = 1
				# tmp_kic_index_dict[cpSecurityList[secu_idx].kicIndex] = 1
				# tmp_kid_index_dict[cpSecurityList[secu_idx].kidIndex] = 1
				
			# building the list of kics, kids, and indexes defined for this profile
			key_indexes = tmp_indexes_dict.keys()
			# kic_indexes = tmp_kic_index_dict.keys()
			# kid_indexes = tmp_kid_index_dict.keys()
	
	return keyType, key_indexes

#def getKeyList (ocpName, keyList):
def getKeyList (ocpName):
	cardProfileName = getCardProfileViaGSMProfile(ocpName)
	key_type, key_indexes = getKeyTypeAndKeyIndexes(cardProfileName)
	ret = []
	
	if key_type == "" and key_indexes == "": # no security
		key = alm.almKey()
		key.index = 3
		key.value = ' '
		ret.append(key)
	else:
		# for i in range(0, len(keyList)):
		for i in range(0, len(key_indexes)):
			if key_type == 'KEY_SET':
				key = alm.almKeySet()
				# key.index = int(keyList[i]._index)
				# key.kic = str(keyList[i]._kic)
				# key.kid = str(keyList[i]._kid)
				# key.kik = str(keyList[i]._kik)
				key.index = key_indexes[i]
				key.kic = ' '
				key.kid = ' '
				key.kik = ' '
				ret.append(key)
			if key_type == 'INDEX':
				key = alm.almKey()
				key.index = key_indexes[i]
				key.value = ' '
				ret.append(key)

	return key_type, ret

def makeCardProfileCMMData (cardProfile, cpSecurityList):
	ret = []
	cardProfileCMMData = _0_ci.ProfileData
	cardProfileCMMData.profileName = cardProfile.desc
	cardProfileCMMData.dlProtocol = cardProfile.dlProtocol
	cardProfileCMMData.profileSecurityData = []
	for tmp in cpSecurityList:
		cardProfileCMMSecurityData = _0_ci.ProfileSecurityData(tmp.app_type_name, CARD_PROFILE_DEFAULT_SECURITY_DOMAIN,[])
		cardProfileCMMSecurityData.keysetIndexes.append(tmp.kicIndex)
		cardProfileCMMData.profileSecurityData.append(cardProfileCMMSecurityData)
	ret.append(cardProfileCMMData)
	return ret

def getCardProfileSecurity(cardProfName):
	st, CPDataMMI, CPSecuMMI, CPFileMapped, CPOpeTypeMMI, is_card_cattp, use_push_secu_cattp =\
	alm.almProvisionGetCardProfile(
	cardProfName, 2 * alm_tools.NB_MAX_TAR, alm_tools.NB_MAX_MAPPINGS, alm_tools.NB_MAX_OPE_TYPES)
	return CPSecuMMI
	
def getSimTarCounterList (simTarCounterList):
	ret = []
	for i in range(0, len(simTarCounterList)):
		simTarData = _0_ci.SimTarCounterData('','')
		simTarData.appTypeName = str(simTarCounterList[i]._tarAppTypeName)
		simTarData.counterValue = convertStrToHexa(str(simTarCounterList[i]._tarCounterValue))
		ret.append (simTarData)
	return ret

def getSimKeySetCounterList (appTypeName, kicImplicitAlgo, kidImplicitAlgo, \
	kikImplicitAlgo, keySetCounter, simKeySetCounterList):
	
	ret = []
	for i in range(0, len(simKeySetCounterList)):
		simKeysetData = _0_ci.SimKeysetCounterData('','','','','','','','','',)
		simKeysetData.appTypeName = appTypeName
		simKeysetData.keysetIndex = int(simKeySetCounterList[i]._keySetIndex)
		simKeysetData.kicImplicitAlgo = kicImplicitAlgo
		simKeysetData.kidImplicitAlgo = kidImplicitAlgo
		simKeysetData.kikImplicitAlgo = kikImplicitAlgo
		simKeysetData.kicValue = convertStrToHexa(str(simKeySetCounterList[i]._kicValue))
		simKeysetData.kidValue = convertStrToHexa(str(simKeySetCounterList[i]._kidValue))
		
		#note that KIK value is optional unlike KIC and KID value so validation of KIK field is only done if there is an input data
		simKeysetData.kikValue = ''
		if str(simKeySetCounterList[i]._kikValue) != '' and simKeySetCounterList[i]._kikValue != None:
			if validateKIK(str(simKeySetCounterList[i]._kikValue)) == OK:
				simKeysetData.kikValue = convertStrToHexa(str(simKeySetCounterList[i]._kikValue))
			else:
				simKeysetData.kikValue = NOT_OK
				
		simKeysetData.counterValue = convertStrToHexa(keySetCounter)
		ret.append (simKeysetData)
	return ret
	
def getSimCardKeyList (iccid, ocpName, cmmSimCardKeyData):
	cpName = getCardProfileViaGSMProfile(ocpName)
	cpSecurity = getCardProfileSecurity(cpName)
	appTypeName = 'APP_MG'
	kicImplicitAlgo = 'NO'
	kidImplicitAlgo = 'NO'
	kikImplicitAlgo = 'NO'
	keySetCounter = '0000000000'
	
	for secu in cpSecurity: # based on card profile security
		appTypeName = secu.app_type_name
		# kicImplicitAlgo = secu.kicAlgo
		# kidImplicitAlgo = secu.kidAlgo
		break
		
	ret = []
	simData = _0_ci.SimCardData('','','','',[],[])
	simData.iccid = str(iccid)
	simData.profileName = cpName
	simData.counterType = int(cmmSimCardKeyData._counterType)
	simData.counterValue = convertStrToHexa(str(cmmSimCardKeyData._counterValue))
	#simData.counterValue = str(simCardKeyList[i]._counterValue)
	simData.simTarCounterData = getSimTarCounterList(cmmSimCardKeyData._simTarCounterList._item)
	simData.simKeysetCounterData = getSimKeySetCounterList( \
		appTypeName, kicImplicitAlgo, kidImplicitAlgo, kikImplicitAlgo, \
		keySetCounter, cmmSimCardKeyData._simKeySetCounterList._item)
	ret.append (simData)
	
	return ret

def getUpdatedSimKeySetCounterList (appTypeName, kicImplicitAlgo, \
	kidImplicitAlgo, kikImplicitAlgo, simKeySetCounterList):
	
	ret = []
	for i in range(0, len(simKeySetCounterList)):
		simKeysetData = _0_ci.SimKeysetCounterData('','','','','','','','','',)
		simKeysetData.appTypeName = appTypeName
		simKeysetData.keysetIndex = int(simKeySetCounterList[i]._keySetIndex)
		simKeysetData.kicImplicitAlgo = kicImplicitAlgo
		simKeysetData.kidImplicitAlgo = kidImplicitAlgo
		simKeysetData.kikImplicitAlgo = kikImplicitAlgo
		simKeysetData.kicValue = convertStrToHexa(str(simKeySetCounterList[i]._kicValue))
		simKeysetData.kidValue = convertStrToHexa(str(simKeySetCounterList[i]._kidValue))
		
		#note that KIK value is optional unlike KIC and KID value so validation of KIK field is only done if there is an input data
		simKeysetData.kikValue = ''
		if str(simKeySetCounterList[i]._kikValue) != '' and simKeySetCounterList[i]._kikValue != None:
			if validateKIK(str(simKeySetCounterList[i]._kikValue)) == OK:
				simKeysetData.kikValue = convertStrToHexa(str(simKeySetCounterList[i]._kikValue))
			else:
				simKeysetData.kikValue = NOT_OK
				
		simKeysetData.counterValue = convertStrToHexa(str(simKeySetCounterList[i]._keySetCounterValue))
		ret.append (simKeysetData)
	return ret
	
def getUpdatedSimCardKeyList (iccid, cpName, counterType, \
			counterValue, simTarCounterList, simKeySetCounterList):
	appTypeName = 'APP_MG'
	kicImplicitAlgo = 'NO'
	kidImplicitAlgo = 'NO'
	kikImplicitAlgo = 'NO'

	ret = []
	simData = _0_ci.SimCardData('','','','',[],[])
	simData.iccid = iccid
	simData.profileName = cpName
	
	cpSecurity = getCardProfileSecurity(simData.profileName)
	for secu in cpSecurity: # based on card profile security
		appTypeName = secu.app_type_name
		# kicImplicitAlgo = secu.kicAlgo
		# kidImplicitAlgo = secu.kidAlgo
		break
	
	simData.counterType = counterType
	simData.counterValue = convertStrToHexa(counterValue)
	simData.simTarCounterData = getSimTarCounterList(simTarCounterList)
	simData.simKeysetCounterData = getUpdatedSimKeySetCounterList( \
		appTypeName, kicImplicitAlgo, kidImplicitAlgo, kikImplicitAlgo, simKeySetCounterList)
	ret.append (simData)
	
	return ret
	
def getInstalledAidList (aidList):
	ret = []
	for i in range(0, len(aidList)):
		ret.append (str(aidList[i]))
	return ret
	
def getSecurityDomainList (sdList):
	ret = []
	for i in range(0, len(sdList)):
		ret.append (str(sdList[i]))
	return ret

def getFileDetails (tab):	
	retPackageTag = []	
	retExtension = []	
	retBuffer = []	
	for i in range(0, len(tab)):
		retPackageTag.append (str(tab[i]._packageTag))
		retExtension.append (str(tab[i]._extension))
		retBuffer.append (str(tab[i]._buffer))	
	return retPackageTag, retExtension, retBuffer

def validateInstallParameters(installParams):
	#install parameters : blocks of 2n hexa digits.
	if not(re.match("^[A-Fa-f0-9]{2,256}$", installParams) and len(installParams) % 2 == 0):
		return NOT_OK
	else:
		return OK
		
def validatePrivilege(priv):
	#privilege :2 hexa digits long.
	if not(re.match("^[A-Fa-f0-9]{2,6}$", priv) and len(priv) % 2 == 0):
		return NOT_OK
	else:
		return OK
		
def getAppletPackages(appletAid, elfAID):

	appletPackAids = []
	elfAidInd = NOT_OK
	
	appletPacks = applicationFactory.getApplication(appletAid).getAppletPackages()
	logInfo ('\n\t***** Applet Packages *****')
	
	if len(appletPacks) == 0:
		return elfAidInd, NOT_OK #applet aid does not exists in db
		
	for pack in appletPacks:
		logInfo("\t%s" % (pack))
		if str(elfAID) == str(pack):
			elfAidInd = OK #indicates that elfAID is one of the applet packages
		else:
			appletPackAids.append(pack) #append package AIDs where elfAID is excluded
	
	return elfAidInd, appletPackAids

def validateAID(aid):
	#aid : should be in hexa format
	if not(re.match("^[A-Fa-f0-9]{2,32}$", aid) and len(aid) % 2 == 0):
		return NOT_OK
	else:
		return OK

def validateString(var):
	#validates string input if it contains valid characters
	if not(re.match("^[-_./A-Za-z\s\d]+$", var)) or re.match("^[-./_\s]+$", var):
		return NOT_OK
	else:
		return OK

def validateMSISDN(msisdn): 
	#msisdn : numeric with/without (+) sign 
	#if not(re.match("^[0-9\+]{2,20}$", msisdn)) or re.match("^[\+]+$", msisdn): 
	#if not(re.match("^\+[0-9][0-9*#Pp]{0,19}$|^[0-9][0-9*#Pp]{0,19}$", msisdn)):
	if not(re.match("^\+[A-Za-z0-9][A-Za-z0-9*#]{0,19}$|^[A-Za-z0-9][A-Za-z0-9*#]{0,19}$", msisdn)):
		return NOT_OK 
	else: 
		return OK 

def validateOpeType (opeType):
	#existence check for operation type
	CPOpeTypeListArr=[]
	st, CPOpeTypeListDB = alm.almProvisionGetOperationTypeList(alm_tools.NB_MAX_OPE_TYPES)
	
	if st == alm.almOK:
		for opt in CPOpeTypeListDB:
			CPOpeTypeListArr.append(opt)
	
		for ope in CPOpeTypeListArr:
			if str(opeType).upper() == str(ope).upper():
				return alm.almProvision_REFERENCED_OPERATION_TYPE_EXISTS
					
	return st
	
def validateCounter(counter):
	#counter : should be in hexa format
	if not(re.match("^[A-Fa-f0-9]{2,10}$", counter) and len(counter) % 2 == 0):
		return NOT_OK
	else:
		return OK
		
def validateVolumeName(volumeName):
	st, VolParamInfo = alm.almProvisionGetVolumeParam(volumeName);
	return st

def validateTAR(tar):
	#TAR : should be in hexa format - 6 characters long
	#if not(re.match("^[A-Fa-f0-9]{6}$", tar)):
	#	return NOT_OK
	#else:
	return OK

def validateIMSI(imsi):
	#IMSI should be numeric 1-16 characters long
	if not(re.match("^[0-9]{1,16}$", imsi)):
		return NOT_OK
	else:
		return OK

def validateICCID(iccid):
	#ICCID should be numeric 1-20 characters long
	if not(re.match("^[0-9]{1,20}$", iccid)):
		return NOT_OK
	else:
		return OK
		
def validateIndex(index):
	#Index accepted values 1-15
	if not(re.match("^[0-9]{1,2}$", index)):
		return NOT_OK
	else:
		if int(index) >= 1 and int(index) <= 15:
			return OK
		else:
			return NOT_OK

def validateKIC_KID(key):
	#kic, kid value must be blocks of 16-48 hexa digits long
	if not(re.match("^[A-Fa-f0-9]{16,48}$", key) and len(key) % 16 == 0):
		return NOT_OK
	else:
		return OK
		
def validateKIK(key):
	#kik value must be blocks of 16-64 hexa digits long
	if not(re.match("^[A-Fa-f0-9]{16,64}$", key) and len(key) % 16 == 0):
		return NOT_OK
	else:
		return OK
		
def validateCounterType(counterType):
	#counterType accepted values: 1-3
	if not(re.match("^[0-9]{1}$", counterType)):
		return NOT_OK
	else:
		if int(counterType) >= 1 and int(counterType) <= 3:
			return OK
		else:
			return NOT_OK
			
def validateImplicitAlgo(algo):
	#validates string input if it contains valid characters
	if not(re.match("^[-_A-Za-z\d]{1,4}$", algo)) or re.match("^[-_]+$", algo):
		return NOT_OK
	else:
		return OK

def validateISC1(isc1):
	#validates isc1 input if it contains valid characters
	if not(re.match("^[-_A-Za-z\d]{1,16}$", isc1)) or re.match("^[-_]+$", isc1):
		return NOT_OK
	else:
		return OK
		
def validateDate(mDate):
	if not(re.match("^\d\d\-\d\d\-20\d\d$", mDate)):
		return NOT_OK
	else:
		return OK
		
def validateAppTypeName(cardProfName, appTypeName):
	st, CPDataMMI, CPSecuMMI, CPFileMapped, CPOpeTypeMMI, is_card_cattp, use_push_secu_cattp =\
	alm.almProvisionGetCardProfile(
	cardProfName, 2 * alm_tools.NB_MAX_TAR, alm_tools.NB_MAX_MAPPINGS, alm_tools.NB_MAX_OPE_TYPES)
	# logInfo ('\n\t***** Card Profile Application Types *****')
	# for elem in CPSecuMMI:
		# logInfo('\t%s' % (elem.app_type_name))
		# logInfo('\t%s' % (elem.app_type_tar))
		# logInfo('\t%s' % (elem.app_type_cmd_set))
		# logInfo('\t----------------------------')
	for elem in CPSecuMMI:
		if appTypeName == elem.app_type_name:
			return OK
	return NOT_OK

def validateCardProfileViaGSMProfile(ocpName, cpName):
	completeOCP=alm.almProvisionGetCompleteOcp(
				ocpName,
				ALM_PROVISION_MAX_NB_ELEMENT, ALM_PROVISION_MAX_NB_ELEMENT, ALM_PROVISION_MAX_NB_ELEMENT, ALM_PROVISION_MAX_NB_ELEMENT)
	st=completeOCP[0]
	card_profile = completeOCP[3]
	logInfo('\n\t***** Associated Card Profile to %s *****' % (ocpName))
	logInfo('\t%s' % (card_profile))
	if cpName == card_profile:
		return OK
	else:
		return NOT_OK
		
def validateBuffer(buffer):
	#A-Za-z0-9+/= and \n
	if not(re.match("[A-Za-z0-9+/=\n]", buffer)):
		return NOT_OK
	else:
		return OK
		
def getPackagePropertyType(pType):
	if (pType.upper() == "Applet".upper()):
		return 'a'
	elif (pType.upper() == "Utility".upper()):
		return 'u'
	else:
		return alm.almErr_INVALID_ENUM_VALUE
		
def getPackageStatus(status):
	if (status.upper() == "Reference".upper()):
		return 'R'
	elif (status.upper() == "Download".upper()):
		return 'D'
	else:
		return alm.almErr_INVALID_ENUM_VALUE

def getPackageStatusName(status):
	if (status.upper() == 'R'):
		return "Reference"
	elif (status.upper() == 'D'):
		return "Download"
	else:
		return "Unknown"

def validateRam(val):
	logInfo ('\tval : %s'%(val))
	if not(re.match("^[0-9]{1,6}$", val)):
		return NOT_OK
	else:
		if int(val) >= 0 and int(val) <= 999999:
			return OK
		else:
			return NOT_OK
			
def validateSmsConcat(val):
	if not(re.match("^[0-9]{1,3}$", val)):
		return NOT_OK
	else:
		if int(val) >= 0 and int(val) <= 999:
			return OK
		else:
			return NOT_OK
			
def validateDesc(var):
	if not(re.match("^[-_./A-Za-z\s\d]{1,30}$", var)):
		return NOT_OK
	else:
		return OK
		
def validateTag(var):
	if not(re.match("^[A-Za-z\d]{1,10}$", var)):
		return NOT_OK
	else:
		return OK
		
def validateVersion(var):
	if not(re.match("^[-_./A-Za-z\s\d]{1,10}$", var)):
		return NOT_OK
	else:
		return OK
		
def validateCattp(val):
	if not(re.match("^[0-1]{1,1}$", val)):
		return NOT_OK
	else:
		return OK

def generateRandomIDs():
	return random.randint(1, 2147483647)

def validateEeprom(eeprom):
	#numeral 
	if not(re.match("^[0-9]{1,6}$", str(eeprom))):
		logInfo('NOT_OK')
		return NOT_OK
	else:
		logInfo('OK')
		return OK

def getCardProfileApplications(cp):
	st, all_applications = alm.almProvisionGetAppliList(ALM_PROVISION_MAX_NB_ELEMENT)
	if cp is None: return []
	filteredApplis=[]
	for appli in all_applications:
		if appli.cp_desc==cp:
			filteredApplis.append(appli)
	return filteredApplis

def getPackagesFromApplis(applis):
	packages=[]
	packages_aid=[]
	for ctr in range(0, len(applis)):
		st,appli_info,appli_packages=alm.almGetAppliInfo(applis[ctr]['aid'],100)
		for pack in appli_packages:
			if pack.aid not in packages_aid: 
				#packages.append(pack)
				packages.append(pack.aid)
				packages_aid.append(pack.aid)
	
	logInfo('%s' % (packages))
	return packages
	
def getAppliStatusName(status):
	if status.upper() == 'I':
		return "Installed"
	elif status.upper() == 'P':
		return "Personalized"
	elif status.upper() == 'S':
		return "Selectable"
	elif status.upper() == 'L':
		return "Locked"
	else:
		return "Unknown"

def getBinG0348Counter(val):
	if (val.upper() == "AVAILABLE"):
		return '01' #8
	elif (val.upper() == "GREATER_THAN"):
		return '10' #16
	elif (val.upper() == "ONE_GREATER_THAN"):
		return '11' #24
	elif (val.upper() == "NONE"):
		return '00' #0
	else:
		return alm.almErr_INVALID_ENUM_VALUE

def getIDLCounter(val):
	if val == _0_ci.COUNTER_AVAILABLE:
		val = '01'
	elif val == _0_ci.GREATER_THAN:
		val = '10'
	elif val == _0_ci.ONE_GREATER_THAN:
		val = '11'
	else: #val == _0_ci.COUNTER_NONE:
		val = '00'
	return val

def getBinG0348Ciphering(val):
	if (val.upper() == "CIPHERING"):
		return '1' #4
	elif (val.upper() == "NONE"):
		return '0'
	else:
		return alm.almErr_INVALID_ENUM_VALUE

def getIDLCiphering(val):
	if val == _0_ci.CIPHERING:
		val = '1'
	else:
		val = '0'
	return val

def getBinG0348Checksum(val):
	if (val.upper() == "RC"):
		return '01' #1
	elif (val.upper() == "CC"):
		return '10' #2
	elif (val.upper() == "DS"):
		return '11' #3
	elif (val.upper() == "NONE"):
		return '00' #0
	else:
		return alm.almErr_INVALID_ENUM_VALUE

def getIDLChecksum(val):
	if val == _0_ci.RC:
		val = '01'
	elif val == _0_ci.CC:
		val = '10'
	elif val == _0_ci.DS:
		val = '11'
	else: #val == _0_ci.CHECKSUM_NONE:
		val = '00'
	return val

def getBinG0348PoR(val):
	if (val.upper() == "REQUIRED"):
		return '01' #1
	elif (val.upper() == "ON_ERROR"):
		return '10' #2
	#elif (val.upper() == "INVALID"):
	#	return '11' #3
	elif (val.upper() == "NONE"):
		return '00' #0
	else:
		return alm.almErr_INVALID_ENUM_VALUE
		
def getIDLPor(val):
	if val == _0_ci.REQUIRED:
		val = '01'
	elif val == _0_ci.ON_ERROR:
		val = '10'
	else: #val == _0_ci.POR_NONE:
		val = '00'
	return val
	
def getBinG0348PoRMedia(val):
	if (val.upper() == "SMS_SUBMIT"):
		return '1' #32
	elif (val.upper() == "SMS_DELIVER_REPORT"):
		return '0' #0
	else:
		return alm.almErr_INVALID_ENUM_VALUE
		
def getIDLPorMedia(val):
	if val == _0_ci.SMS_SUBMIT:
		val = '1'
	else: #val == SMS_DELIVER_REPORT
		val = '0'
	return val

def getBinG0348Algo(val):
	if (val.upper() == "DES"):
		return '01' #1
	#elif (val.upper() == "RESERVED"):
	#	return '10' #2
	elif (val.upper() == "PROPRIETARY"):
		return '11' #3
	elif (val.upper() == "IMPLICIT"):
		return '00' #0
	else:
		return alm.almErr_INVALID_ENUM_VALUE

def getIDLAlgo(val):
	if val == _0_ci.DES:
		val = '01'
	elif val == _0_ci.PROPRIETARY:
		val = '11'
	else: # val = _0_ci.IMPLICIT
		val = '00'
	return val
		
def getBinG0348DESMode(val):
	if (val.upper() == "TRIPLE_DES2"):
		return '01' #4
	elif (val.upper() == "TRIPLE_DES3"):
		return '10' #8
	elif (val.upper() == "DES_ECB"):
		return '11' #12
	elif (val.upper() == "DES_CBC"):
		return '00' #0
	else:
		return alm.almErr_INVALID_ENUM_VALUE

def getIDLDesMode(val):
	if val == _0_ci.TRIPLE_DES2:
		val = '01'
	elif val == _0_ci.TRIPLE_DES3:
		val = '10'
	elif val == _0_ci.DES_ECB:
		val = '11'
	else: # val == _0_ci.DES_CBC:
		val = '00'
	return val
		
def getTargetType(type):
	if type.upper() == "targetTypeMsisdn".upper():
		return 0
	elif type.upper() == "targetTypeIccid".upper():
		return 1
	else:
		return alm.almErr_INVALID_ENUM_VALUE

def getMedia(media):
	if media.upper() == "mediaSmsOta".upper():
		return 0
	elif media.upper() == "mediaSoapResponse".upper():
		return 1
	else:
		return alm.almErr_INVALID_ENUM_VALUE
		
def validateKeysetIndex(index):
	if index >= 1 and index <= 15:
		return OK
	else:
		return NOT_OK

def validateAPDU(apdu):
	#APDU must be blocks of 2-8400 hexa digits long
	apdu = string.replace(apdu, '\n', '')
	if not(re.match("^[A-Fa-f0-9]{2,8400}$", apdu) and len(apdu) % 2 == 0):
		return NOT_OK
	else:
		return OK
		
def convertIntToHex(intValue):
	if intValue == 10:
		return 'A'
	elif intValue == 11:
		return 'B'
	elif intValue == 12:
		return 'C'
	elif intValue == 13:
		return 'D'
	elif intValue == 14:
		return 'E'
	elif intValue == 15:
		return 'F'
	else: # 0 - 9
		return str(intValue)

def validateDCS(dcs):
	if dcs != 'F2' and dcs != 'F0':
		return NOT_OK
	else:
		return OK

def isMSISDNProvisioned(msisdn):
	code = 0
	message = 'OK'
	
	try:
		isMsisdnProv = bool(subsFactory.getSubscriber(msisdn).isMsisdnExisting())
	except SubscriberException, subsExcept:
		message = "Subscriber Exception: %s"% (subsExcept.errorMessage)
	except Exception, e:
		message = "CORBA Exception: %s"%(e)
	
	if message != 'OK':
		code = alm.almErr_CORBA_DAO_RELATED
	elif isMsisdnProv == False:
		code = alm.almProvision_REFERENCED_MSISDN_NOT_EXIST
		message = alm.almStatusToString(code)
	elif isMsisdnProv == True:
		code = alm.almProvision_REFERENCED_MSISDN_ALREADY_EXIST
		message = alm.almStatusToString(code)
	
	return code, message



