#!/usr/bin/env python
import copy
import re
import time
import os
import os.path
import string
import sys
import prov_sql
import threading
import shutil
from prov_config import NB_THREADS
import datetime 
from batch_tools import Fifo, GetConfigFakeParameter, GetInHMS
global TH_ID
TH_ID=0
lockT = threading.Lock()
lock_total = threading.Lock()
global tmp_output_file 
global mk_translation_dict
mk_translation_dict={} 
#total, total_ok = 0, 0
y, _m, _d, _h, _min, _sec, p,q,r = time.localtime()
#name_file = "sim_%d%d%d_%d%d%d_LLLLLLL.dat" % (y, _m, _d, _h, _min, _sec)
today =  '%d%d%d_%02d%02d%02d' % (y, _m, _d, _h, _min, _sec)

provisioning_list  = Fifo()

class ProvisionAction(threading.Thread):
        def __init__(self):
                threading.Thread.__init__(self)
                self._isStopped = threading.Event()
		self.finished = 0
		self.local_ok = 0
		self.local_ko = 0 	
	def getOKs(self):
		return self.local_ok
	def getKOs(self):
		return self.local_ko
	def addOK(self):
		self.local_ok = self.local_ok + 1
	def addKO(self):
		self.local_ko = self.local_ko + 1

	def EndThread(self):
                self._isStopped.set()

        def isStopped(self):
                return self._isStopped.isSet()

	def run(self):
		global total_ko, total_ok
		i = 0
		k = 0
		l_con = prov_sql.getDBConnection()
		cursor = l_con.cursor()
		while 1:
			try :
				iccid, imsi, msisdn, id_profile, MK = provisioning_list.dequeue()
				tranlated_key = mk_translation_dict[MK].strip('\n')
				st , message = prov_sql.provision_card(cursor, iccid, imsi, msisdn, id_profile, tranlated_key)
				tmp_output_file.write("%s|%s|%d|%s\n" % (iccid, imsi, st, message))	
				if st == 1000:	
					self.addOK()	
				else:
					self.addKO()	
			except IndexError:
				print "waiting for data to be read"
				time.sleep(0.1)
				i = i+1
				print "state " + str(self.isStopped()) 	
				if self.isStopped() == True:
					print "stopping thread"
					cursor.close()
					prov_sql.closeCon(l_con)
					self.finished = 1
					return 
			except KeyError, e:
				print ("provision_card  exception : <%s>"%(e.__str__())) 
				self.addKO()
				tmp_output_file.write("%s|%s|%d|%s\n" % (iccid, imsi,1007 , "PROV: TRANSLATION OF MASTER KEY NOT FOUND"))
			except	Exception, e:
				print ("provision_card exception : <%s>"%(e.__str__())) 
				tmp_output_file.write("%s|%s|%d|%s\n" % (iccid, imsi, 1, e.__str__()))
				self.addKO()	
		cursor.close()
		prov_sql.closeCon(l_con)

if __name__=='__main__':
	if len(sys.argv) != 5:
		print """incorrect syntax 
	./batch_prov.py path_input_directory path_out_directory  path_MK_TranslationsFile path_fake_config_file
		"""
		sys.exit(0)
	path  			= sys.argv[1]
	path_out 		= sys.argv[2]
	mk_transfileName 	= sys.argv[3]
	path_fake_config_file	= sys.argv[4]

	dico_fake_parameter = GetConfigFakeParameter(path_fake_config_file)

	mk_transfile =  open(mk_transfileName, 'r')
	line_mkfile = mk_transfile.readline()

	while line_mkfile != '':
		mk_data = line_mkfile.split('|')
		if len(mk_data) == 2 :
			mk_translation_dict[mk_data[0]]	=  mk_data[1]
	        line_mkfile = mk_transfile.readline()

	mk_transfile.close()	

	dirList=os.listdir(path)
	
	for file_name in dirList:
		if ((file_name[-3:]== 'dat') or (file_name[-3:]== 'DAT')): 
			global total_ko, total_ok
			y, _m, _d, _h, _min, _sec, p,q,r = time.localtime()
			name_out_file = "sim_%d%d%d_%d%d%d_%s.out" % (y, _m, _d, _h, _min, int(_sec), file_name[-11:-4])
			tmp_output_file = open(path_out + '/' + file_name[:-3] + "tmp" , "w")
			output_file = open(path_out + '/' + file_name[:-3] + 'header', "w")
			infile = open(path+'/'+file_name,"r")
			provision_threads = []
			for i in range(0, NB_THREADS):
				provision_threads.append(ProvisionAction()) 
				provision_threads[i].start()	
			line = infile.readline()
			start_time =  time.time()	
			#output_file.write("                                               ")	
			tmp_output_file.write("BEGIN\n")	
			while  line != '':
				if line[0]!='#':
					l_data = line.split('|')
					if (len(l_data) > 12):
						iccid, imsi, id_profile,MK   = l_data[0],  l_data[1], l_data[4], l_data[12][16:] 
						try:
							msisdn = dico_fake_parameter[imsi[:7]] + imsi[7:]
						except:
							print "Default value used for MSISDN_Fake: %s"%(dico_fake_parameter['default'])
							msisdn = dico_fake_parameter['default'] + imsi[7:]


						provisioning_list.enqueue((iccid, imsi, msisdn, id_profile, MK))
				line = infile.readline()
				
				
			for i in range(0, NB_THREADS):	
				provision_threads[i].EndThread()
	
			while provisioning_list.getSize() != 0:
				time.sleep(1)
			
			for i in range(0, NB_THREADS):
				while provision_threads[i].finished == 0:
					time.sleep(0.1)
			total_ok ,  total_ko = 0,0	
			for i in range(0, NB_THREADS):	
				total_ok = total_ok + provision_threads[i].getOKs()
				total_ko = total_ko +  provision_threads[i].getKOs()
			stop_time =  time.time()
			time.sleep(0.2)
			tmp_output_file.write("END\n")
		        tmp_output_file.close()
			tmp_output_file = open(path_out + '/' + file_name[:-3] + "tmp" , "r")
			duration = int(round(stop_time - start_time)) 
			#output_file.seek(0)
		 	duration = 	GetInHMS(duration)
			#total_ok, total_ko = 0, 0
			output_file.write("%s|%s|%d|%d|%d\n" % (today , duration, total_ok + total_ko , total_ok, total_ko))
			shutil.copyfileobj(tmp_output_file, output_file)
			tmp_output_file.close()
			output_file.close()
			os.remove(path_out + '/' + file_name[:-3] + "tmp" )
			shutil.move(path_out + '/' + file_name[:-3]+ "header", path_out + '/' + file_name[:-3]+ "out");
			
		
		
	#prov_sql.closeCon(con)


	
