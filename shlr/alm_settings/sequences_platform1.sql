DROP sequence seq_campaign;
DROP sequence seq_sim_card_id;
DROP sequence seq_volume_id;
DROP sequence seq_cattp_parameter_set;
DROP sequence seq_admServer_id;
DROP sequence seq_ocp_counter_id;
DROP sequence seq_sim_counter_id;
DROP sequence seq_operation;
DROP sequence TSM_NOTIF_LOG_ID_SEQ;
DROP sequence seq_user_id;
DROP SEQUENCE CMM_APP_PROF_SECU_KEYSET_SEQ;
DROP SEQUENCE CMM_APP_PROFILE_SECURITY_SEQ;
DROP SEQUENCE CMM_COUNTER_SEQ;
DROP SEQUENCE CMM_DOMAIN_KEYSET_SEQ;
DROP SEQUENCE CMM_SECURITY_DOMAIN_SEQ;
DROP SEQUENCE CMM_SECURITY_SEQ;
DROP SEQUENCE CMM_SIM_CARD_GP_KEYS_SEQ;
DROP SEQUENCE CMM_SIM_CARD_SEQ;
DROP SEQUENCE CMM_SIM_KEYSET_COUNTER_SEQ;
DROP SEQUENCE CMM_SIM_KEYSET_COUNTER_SEQ;
DROP SEQUENCE CMM_SIM_TAR_COUNTER_SEQ;
DROP SEQUENCE stat_counter_instance_seq;
DROP SEQUENCE stat_counter_cfg_seq;
DROP SEQUENCE translation_seq;
DROP SEQUENCE monitoring_seq;
DROP SEQUENCE notif_monitoring_seq;

CREATE SEQUENCE seq_campaign START WITH 1 INCREMENT BY 10 MAXVALUE 999999999 ORDER;
CREATE SEQUENCE seq_sim_card_id START WITH 1 INCREMENT BY 10 cache 20;
CREATE SEQUENCE seq_volume_id START WITH 1 INCREMENT BY 10 cache 20;
CREATE SEQUENCE seq_cattp_parameter_set START WITH 1 INCREMENT BY 10 cache 20;
CREATE SEQUENCE seq_admServer_id START WITH 1 INCREMENT BY 10 cache 20;
CREATE SEQUENCE seq_ocp_counter_id START WITH 1 INCREMENT BY 10 cache 20;
CREATE SEQUENCE seq_sim_counter_id START WITH 1 INCREMENT BY 10 cache 20;
CREATE SEQUENCE seq_operation START WITH 1 INCREMENT BY 10 MAXVALUE 999999999 ORDER;
CREATE SEQUENCE TSM_NOTIF_LOG_ID_SEQ START WITH 1 INCREMENT BY 10 MAXVALUE 999999999 ORDER;
CREATE SEQUENCE seq_user_id START WITH 1 INCREMENT BY 10 cache 20;


-- CMM_APP_PROFILE_SECU_KEYSET
CREATE SEQUENCE CMM_APP_PROF_SECU_KEYSET_SEQ
    MINVALUE 1
    MAXVALUE 99999999
    START WITH 1 INCREMENT BY 10
    NOCACHE;
CREATE OR REPLACE TRIGGER CMM_PROFILE_SECU_KEYSET_TRG_1
BEFORE INSERT
ON CMM_APP_PROFILE_SECU_KEYSET
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
BEGIN
  tmpVar := 0;

  SELECT CMM_APP_PROF_SECU_KEYSET_SEQ.NEXTVAL INTO tmpVar FROM dual;
  :NEW.ID := tmpVar;

  EXCEPTION
    WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
      RAISE;
END ;
/

-- CMM_APP_PROFILE_SECURITY
CREATE SEQUENCE CMM_APP_PROFILE_SECURITY_SEQ
    MINVALUE 1
    MAXVALUE 99999999
    START WITH 1 INCREMENT BY 10
    NOCACHE;

CREATE OR REPLACE TRIGGER CMM_APP_PROFILE_SECURITY_TRG_1
BEFORE INSERT
ON CMM_APP_PROFILE_SECURITY
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
BEGIN
  tmpVar := 0;

  SELECT CMM_APP_PROFILE_SECURITY_SEQ.NEXTVAL INTO tmpVar FROM dual;
  :NEW.ID := tmpVar;

  EXCEPTION
    WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
      RAISE;
END ;
/

-- CMM_COUNTER
CREATE SEQUENCE CMM_COUNTER_SEQ
    MINVALUE 1
    MAXVALUE 999999999999
    START WITH 1 INCREMENT BY 10
    NOCACHE;
CREATE OR REPLACE TRIGGER CMM_COUNTER_TRG_1
BEFORE INSERT
ON CMM_COUNTER
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
BEGIN
  tmpVar := 0;

  SELECT CMM_COUNTER_SEQ.NEXTVAL INTO tmpVar FROM dual;
  :NEW.ID := tmpVar;

  EXCEPTION
    WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
      RAISE;
END ;
/


-- CMM_DOMAIN_KEYSET
CREATE SEQUENCE CMM_DOMAIN_KEYSET_SEQ
    MINVALUE 1
    MAXVALUE 9999999999
    START WITH 1 INCREMENT BY 10
    NOCACHE;
CREATE OR REPLACE TRIGGER CMM_DOMAIN_KEYSET_TRG_1
BEFORE INSERT
ON CMM_DOMAIN_KEYSET
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
BEGIN
  tmpVar := 0;

  SELECT CMM_DOMAIN_KEYSET_SEQ.NEXTVAL INTO tmpVar FROM dual;
  :NEW.ID := tmpVar;

  EXCEPTION
    WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
      RAISE;
END ;
/

-- CMM_SECURITY_DOMAIN
CREATE SEQUENCE CMM_SECURITY_DOMAIN_SEQ
    MINVALUE 1
    MAXVALUE 99999999
    START WITH 1 INCREMENT BY 10
    NOCACHE;
CREATE OR REPLACE TRIGGER CMM_SECURITY_DOMAIN_TRG_1
BEFORE INSERT
ON CMM_SECURITY_DOMAIN
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
BEGIN
  tmpVar := 0;

  SELECT CMM_SECURITY_DOMAIN_SEQ.NEXTVAL INTO tmpVar FROM dual;
  :NEW.ID := tmpVar;

  EXCEPTION
    WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
      RAISE;
END ;
/

-- CMM_SECURITY
CREATE SEQUENCE CMM_SECURITY_SEQ
    MINVALUE 1
    MAXVALUE 99999999
    START WITH 1 INCREMENT BY 10
    NOCACHE;

-- CMM_SIM_CARD_GP_KEYS
CREATE SEQUENCE CMM_SIM_CARD_GP_KEYS_SEQ
    MINVALUE 1
    MAXVALUE 9999999999
    START WITH 1 INCREMENT BY 10
    NOCACHE;
CREATE OR REPLACE TRIGGER CMM_SIM_CARD_GP_KEYS_TRG_1
BEFORE INSERT
ON CMM_SIM_CARD_GP_KEYS
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
BEGIN
  tmpVar := 0;

  SELECT CMM_SIM_CARD_GP_KEYS_SEQ.NEXTVAL INTO tmpVar FROM dual;
  :NEW.ID := tmpVar;

  EXCEPTION
    WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
      RAISE;
END ;
/

-- CMM_SIM_CARD
CREATE SEQUENCE CMM_SIM_CARD_SEQ
    MINVALUE 1
    MAXVALUE 99999999
    START WITH 1 INCREMENT BY 10
    NOCACHE;
CREATE OR REPLACE TRIGGER CMM_SIM_CARD_TRG_1
BEFORE INSERT
ON CMM_SIM_CARD
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
BEGIN
  tmpVar := 0;

  SELECT CMM_SIM_CARD_SEQ.NEXTVAL INTO tmpVar FROM dual;
  :NEW.ID := tmpVar;

  EXCEPTION
    WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
      RAISE;
END ;
/


-- CMM_SIM_KEYSET_COUNTER
CREATE SEQUENCE CMM_SIM_KEYSET_COUNTER_SEQ
    MINVALUE 1
    MAXVALUE 99999999
    START WITH 1 INCREMENT BY 10
    NOCACHE;
CREATE OR REPLACE TRIGGER CMM_SIM_KEYSET_COUNTER_TRG_1
BEFORE INSERT
ON CMM_SIM_KEYSET_COUNTER
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
BEGIN
  tmpVar := 0;

  SELECT CMM_SIM_KEYSET_COUNTER_SEQ.NEXTVAL INTO tmpVar FROM dual;
  :NEW.ID := tmpVar;

  EXCEPTION
    WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
      RAISE;
END ;

-- CMM_SIM_TAR_COUNTER
CREATE SEQUENCE CMM_SIM_TAR_COUNTER_SEQ
    MINVALUE 1
    MAXVALUE 99999999
    START WITH 1 INCREMENT BY 10
    NOCACHE;
CREATE OR REPLACE TRIGGER CMM_SIM_TAR_COUNTER_TRG_1
BEFORE INSERT
ON CMM_SIM_TAR_COUNTER
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
BEGIN
  tmpVar := 0;

  SELECT CMM_SIM_TAR_COUNTER_SEQ.NEXTVAL INTO tmpVar FROM dual;
  :NEW.ID := tmpVar;

  EXCEPTION
    WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
      RAISE;
END ;
/

-- Billing
CREATE SEQUENCE stat_counter_instance_seq
        START WITH 1 INCREMENT BY 10
        NOCYCLE
        NOCACHE;
CREATE SEQUENCE stat_counter_cfg_seq
        START WITH 1 INCREMENT BY 10
        NOCYCLE
        NOCACHE;
CREATE SEQUENCE translation_seq
        START WITH 1 INCREMENT BY 10
        NOCYCLE
        NOCACHE;
CREATE SEQUENCE monitoring_seq
        START WITH 1 INCREMENT BY 10
        NOCYCLE
        NOCACHE;
CREATE SEQUENCE notif_monitoring_seq
        START WITH 1 INCREMENT BY 10
        MINVALUE 0
        MAXVALUE 9999999
        CYCLE
        CACHE 500;

