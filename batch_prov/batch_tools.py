#!/usr/bin/env python

def GetInHMS(seconds):
    hours = seconds / 3600
    seconds -= 3600*hours
    minutes = seconds / 60
    seconds -= 60*minutes
    return "%02d%02d%02d" % (hours, minutes, seconds)

def GetConfigFakeParameter(pathToFile):

    fileHandle = open(pathToFile)
    line = fileHandle.readline()
    dico_fake_parameter = {}

    try:
    	while  line != '':
    		if line[0]!='#':
               		splitLine = line.split('|')
               		key = str(splitLine[0])
			if key in dico_fake_parameter:
				print ("Error in dictionary: double entry in dictionary, check %s"%(pathToFile))
				raise KeyError('Error in dictionary: double entry in dictionary')

			#Remove \n from the last split parameter
               		dico_fake_parameter[key] = splitLine[1].rstrip()
       		line = fileHandle.readline()

    	keyD=str('default')
    	if keyD not in dico_fake_parameter:
    		print 'Missing default parameter in %s'%(pathToFile)
		raise KeyError('Missing default parameter')

    except KeyError, keyerr:
    	raise KeyError(keyerr.__str__())


    return dico_fake_parameter

class Fifo(object):
        __slots__ = ('front', 'back')

        def __init__(self):
                self.front = []
                self.back = []

        def enqueue(self, value):
                self.back.append(value)

        def dequeue(self):
                front = self.front
                if not front:
                        self.front, self.back = self.back, front
                        front = self.front
                        front.reverse()
                return front.pop()

        def getSize(self):
                return (len(self.front)+len(self.back))


