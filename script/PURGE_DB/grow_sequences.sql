execute DBMS_REPUTIL.REPLICATION_OFF;
DROP sequence seq_sim_card_id;
DROP sequence seq_sim_counter_id;
DROP SEQUENCE CMM_COUNTER_SEQ;
DROP SEQUENCE CMM_SIM_CARD_SEQ;
DROP SEQUENCE CMM_SIM_KEYSET_COUNTER_SEQ;
DROP SEQUENCE CMM_SIM_TAR_COUNTER_SEQ;

CREATE SEQUENCE seq_sim_card_id MINVALUE 1 START WITH &1 INCREMENT BY &2  cache 20 MAXVALUE &3;
CREATE SEQUENCE seq_sim_counter_id MINVALUE 1 START WITH &1 INCREMENT BY &2 cache 20 MAXVALUE &3;
-- CMM_COUNTER
CREATE SEQUENCE CMM_COUNTER_SEQ
    MINVALUE 1
    MAXVALUE &3
    START WITH &1 INCREMENT BY &2
    NOCACHE;

-- CMM_SIM_CARD
CREATE SEQUENCE CMM_SIM_CARD_SEQ
    MINVALUE 1
    MAXVALUE &3
    START WITH &1 INCREMENT BY &2 
    NOCACHE;

-- CMM_SIM_KEYSET_COUNTER
CREATE SEQUENCE CMM_SIM_KEYSET_COUNTER_SEQ
    MINVALUE 1
    MAXVALUE &3
    START WITH &1 INCREMENT BY &2 
    NOCACHE;

-- CMM_SIM_TAR_COUNTER
CREATE SEQUENCE CMM_SIM_TAR_COUNTER_SEQ
    MINVALUE 1
    MAXVALUE &3 
    START WITH &1 INCREMENT BY &2
    NOCACHE;
