#!/usr/bin/env python
#******************************************************************************
# file: $Id: shlr_tools.py,v 1.12 2011-07-13 09:17:38 portier Exp $
# author:  Saad Amrani
#
# Changes:
#       13 Aug 2013, Oleg Usoltsev
#           1. Added "\[\]" to the regular expression in Cfg.
#*****************************************************************************/
import re
import time
import os
import os.path
import string
import sys
import math
from datetime import datetime
import threading

# get the PATH from the environment variable
try:
    LOGFILEPATH = os.environ.get("LOGFILEPATH")
except:
    LOGFILEPATH = "/home/sfosaad/exe/asp/almsaad/mmi/alm/soap/"

# ALM-3895
global lockLogFDescriptor
lockLogFDescriptor = threading.Lock()

def FormatInputMsisdn(msisdn):
    if msisdn[0]!="+":
        out="+"+msisdn
    else:
        out=msisdn
    return out

def FormatOutputMsisdn(msisdn):
    if msisdn[0]=="+":
        out=msisdn[1:]
    else:
        out=msisdn
    return out

# CONFIG FILE READING #########################################################
class Cfg:
    """
    Use this class to read config files. Its syntax is pretty much the same
    as FieldStorage one's (in std cgi module).
    Default cfg file path is read from CFGFILEPATH environment variable.

    example:
    cfg = Cfg()
    admin_addr = cfg.getvalue('ADMIN_ADDR')
    admin_port = cfg.getvalue('ADMIN_PORT',5600)
    """

    def __init__(self,cfgpath,raiseIOError=0):
        self.raiseIOError=raiseIOError
        self.cfgpath = cfgpath
        self.parameters = {}
        self.fileExists = 0

        # file format
        #regular expressions
        self._blk_line = re.compile(r'^\s*$') # blank line
        self._cmt_line = re.compile(r'^#.*$') # detects '#'
        #detects 'LABEL value', where value is text
        #with . / - space , # :  allowed
        self._p_line = re.compile(r'^(\w+)[\s]+([\/\.\ \?\,\-\#\:\w\|\[\]]+)[\s]*$')
        self._read()

    def _read(self):
        """Read the config file
        """
        try:
            ifile = open(self.cfgpath,'r')
        except IOError:
            error="config file %s could not be opened"%self.cfgpath
            if self.raiseIOError: raise IOError, error
            ifile = None
        if ifile:
            for l in ifile:
                #ignore blank and comment lines
                if self._cmt_line.match(l): continue
                if self._blk_line.match(l): continue
                m = self._p_line.match(l)
                if m:
                    new_p = m.group(1)
                    new_value = m.group(2)
                    self.parameters[new_p]=new_value
            ifile.close()

    def getvalue(self,label,default=None):
        """Returns a string or a list of strings

        The character | (pipe) is used as a separator.

        For example:
            in the cfg file:
                LABEL1  value1|value2
                LABEL2  value3
            using gevalue:
                getvalue('LABEL1') -> ['value1','value2']
                getvalue('LABEL2') -> 'value3'
                getvalue('LABEL0') -> None
        """
        value=self.parameters.get(label,default)
        if value == default: return default
        value=string.split(str(value),'|')
        if len(value)==1: return value[0]
        return value

    def getvalues(self,*labels):
        """Retrieve several values at a time

        If only one value found: return it
        If several values found: return a list
        """
        values = []
        for label in labels:
            values.append(self.getvalue(label))
        if len(values)==1: return values[0]
        return values

    def __repr__(self):
        ifile = open(self.cfgpath,'r')
        return string.join(ifile)

try:
    CFGFILE = os.environ.get("SHLR_CFGFILE")
except:
    CFGFILE = "/home/sfosaad/exe/asp/almsaad/mmi/alm/soap/"
CFG=Cfg("%s"%(CFGFILE))

class Log:
    def __init__(self, _cfg=CFG, _logfp=LOGFILEPATH, _pid=str(os.getpid())):
        self.cfg = _cfg
        y, _m, _d, _h, _min, _sec, p,q,r = time.localtime()
        self.m = _m
        self.d = _d
        self.h = _h
        self.min = _min
        self.sec = _sec
        self.imsi=""
        self.msisdn=""
        self.ef=""
        self.lpid = _pid
        self.level=int(_cfg.getvalue("logLevel", 4))
        self.folder_of_the_day = "%s/%02d%02d"%(_logfp, self.m, self.d)
        self.logfolder=_logfp
        self.f = self._init()

    def _init(self):
        not_exists = os.path.isdir(self.folder_of_the_day)
        if (not_exists == 0):
            os.mkdir(self.folder_of_the_day)
        try:
            return open("%s/vivo.log"%self.folder_of_the_day, "a")
        except IOError, ioe:
            print "Failed to open vivo.log IOError: %s"%ioe.__str__()
            return None
        except OSError, ose:
            print "Failed to open vivo.log OSError: %s"%ose.__str__()
            return None

    def setImsi(self, _imsi):
        self.imsi=_imsi

    def getImsi(self):
        return self.imsi

    def setMsisdn(self, _msisdn):
        self.msisdn=_msisdn

    def getMsisdn(self):
        return self.msisdn

    def setTargetedFile(self, _ef):
        self.ef=_ef

    def setMinute(self, _min):
        self.min = _min

    def getMinute(self):
        return self.min

    def setLogLevel(self, _lcfg):
        self.level=int(_lcfg.getvalue("logLevel", 4))

    def setFolderOftheDay(self, _fd):
        self.folder_of_the_day=_fd

    def _log(self, s, _flag, min_level, display=0, noMsisdn=0):
        try:
            if ( min_level <= self.level or display ==1):
                #y, m, d, h, min, _sec, p,q,r = time.localtime()
                class_time = datetime.now()
                m, d, h, min, _sec, _microsec = class_time.month, class_time.day, class_time.hour, class_time.minute, class_time.second,  class_time.microsecond
                update_folder_of_the_day = "%s/%02d%02d"%(self.logfolder, m, d)
                # reupadte the name of the log file
                if (update_folder_of_the_day != self.folder_of_the_day or self.f is None):
                    # ALM-3895
                    lockLogFDescriptor.acquire()
                    self.end()
                    self.setFolderOftheDay(update_folder_of_the_day)
                    self.f = self._init()
                    lockLogFDescriptor.release()
                todisp="%02d:%02d:%02d:%d|%s|"%(h,min, _sec,_microsec,   _flag)
                # Add thread name
                todisp = todisp + threading.currentThread().getName() +"|"
                if (self.imsi!=''):
                    todisp = todisp + str(self.imsi) + "|"
                if (self.msisdn!='' and noMsisdn==0):
                    todisp = todisp + str(self.msisdn) + "|"
                if (self.ef!=''):
                    todisp = todisp + str(self.ef)+ "|"
                if (self.lpid!=''):
                    todisp = todisp +"PID:" + str(self.lpid) + "|"

                todisp = todisp+"\t"+s+"\n"

                # ALM-3895
                lockLogFDescriptor.acquire()
                try:
                    try:
                        if self.f is None:
                            return
                        self.f.write(todisp)
                        self.flush()
                        # Keeps file opened for performances
                    except IOError, ioe:
                        print "%s failed to write/flush: IOError: %s"\
                        %(threading.currentThread().getName(), ioe.__str__())
                        self.end()
                        return
                    except OSError, ose:
                        print "%s failed to write/flush: OSError: %s"\
                        %(threading.currentThread().getName(), ose.__str__())
                        self.end()
                        return
                finally:
                    lockLogFDescriptor.release()
        except Exception, e:
            # don't release log, since applicative exception not supposed
            # to occur during log file manipulation
            print "%s cannot log: Exception: %s"\
            %(threading.currentThread().getName(), e.__str__())
            return

    def end(self):
        if self.f is None:
            return
        self.f.close()
        self.f = None

    def debug(self, s, display=0):
        self._log(s, "D", 6, display)

    def info(self, s, display=0):
        self._log(s, "I", 5, display)

    def warning(self, s, display=0):
        self._log(s, "W", 4, display)

    def error(self, s, display=0):
        self._log(s, "E", 3, display)

    # For second thread, don't display MSISDN (put it directly in the notif)
    def debug2(self, s, display=0):
        self._log(s, "D", 6, display, noMsisdn=1)

    def info2(self, s, display=0):
        self._log(s, "I", 5, display, noMsisdn=1)

    def warning2(self, s, display=0):
        self._log(s, "W", 4, display, noMsisdn=1)

    def error2(self, s, display=0):
        self._log(s, "E", 3, display, noMsisdn=1)

    def flush(self):
        self.f.flush()


class Fifo(object):
    __slots__ = ('front', 'back')

    def __init__(self):
        self.front = []
        self.back = []

    def enqueue(self, value):
        self.back.append(value)

    def dequeue(self):
        front = self.front
        if not front:
            self.front, self.back = self.back, front
            front = self.front
            front.reverse()
        return front.pop()

    def getSize(self):
        return (len(self.front)+len(self.back))
