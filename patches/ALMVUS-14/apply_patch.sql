CLEAR SCREEN
SET FEEDBACK OFF
SET VERIFY OFF
SET SERVEROUTPUT ON
SPOOL apply_patch

BEGIN
  dbms_output.put_line('Patch started @ ' || TO_CHAR(SYSDATE, 'HH24:MI:SS DD-MON-YYYY'));
END;
/


@vivo_bp_patch_new_ocp.sql


BEGIN
  dbms_output.put_line('Patch completed @ ' || TO_CHAR(SYSDATE, 'HH24:MI:SS DD-MON-YYYY'));
END;
/


SPOOL OFF
SET FEEDBACK ON
SET VERIFY ON
SET SERVEROUTPUT OFF