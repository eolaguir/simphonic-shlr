## File:        test_ActivateHRS_LTE.py
## Description: The test of ACTIVATE_HRS_LTE request processing chain.
## Author:      Oleg Usoltsev <o.usoltsev@external.oberthur.com>
## Created:     08 August 2013

import sys
import unittest

sys.path.append("../../soap_common")
from mock_shlr_req_process import processActivateHRS_LTE

class ActivateHRS_LTE_TestCase(unittest.TestCase):

    # Expected result for the input parameters
    APDU_EXPECTED = str.upper("c10400000016c4050400000015c582c60c0791551010010201ffffffffc7077f106f42001c01c7077f108f42001c01c60101c7063F005F490000c609087942019180845651c7067f206f070000c60bffffffff27f4010000ff01c7067f206f7e0000c60effffffffffffff27f4010000ff01c7067f206f530000c60c0791557169523002ffffffffc7077f106f40000f01")

    def test_requestProcessing(self):
        """
        Scenario: Call processActivateHRS_LTE with valid input parameters.
        Expected result: APDU_EXPECTED is generated.
        """

        # Test variables.
        orchId          = 1
        msisdn          = "+557055018910"
        labelCommand    = "Activate_HRS_LTE"
        tid             = 16
        config          = "0400000015"
        imsiReal        = "724101908486515"
        smspReal        = "+550101102010"
        msisdnReal      = "+551796250320"

        req = processActivateHRS_LTE(
            orchId,
            msisdn,
            labelCommand,
            tid,
            config,
            imsiReal,
            smspReal,
            msisdnReal)

        apdu = req.getApdu()
        self.assertEqual(apdu, str.upper(ActivateHRS_LTE_TestCase.APDU_EXPECTED))

if __name__ == "__main__":
    unittest.main()
