CREATE OR REPLACE PACKAGE BODY provsimcard AS 
 
-------------------------------------------------------------------------- 
-- file		:	$RCSfile: ProcProvSimCardBody.sql,v $ 
-- project	:	ALM 
-- author	:	Dalod Gaetan - 2001/10/22 
-- last modified:	$Author: portier $ 
--------------------------------------------------------------------------- 
-- version	:	$Revision: 1.1 $ ($Date: 2011-07-07 13:00:15 $) 
--------------------------------------------------------------------------- 
-- description	:	Stored procedure used to work with the provisioning server 
--			For Sim card  
--------------------------------------------------------------------------- 
 
/*---------------------------------------------------------*\ 
 | REMARQUE : 						| 
 | 		 					| 
 |		 					| 
\*---------------------------------------------------------*/ 
 
------------------------------------------------------------------------------- 
--									     -- 
--									     -- 
--									     -- 
--									     -- 
--									     -- 
--									     -- 
--									     -- 
--									     -- 
--									     -- 
--   Package containing all procedures used for processing SIM Card Prov commands  -- 
--									     -- 
--									     -- 
--				     Body				     -- 
--				 					     -- 
--									     -- 
--									     -- 
--									     -- 
--									     -- 
--									     -- 
--									     -- 
--									     -- 
--									     -- 
------------------------------------------------------------------------------- 
 
PROCEDURE delete_SIM_no_commit (p_imsi IN sim_card.imsi%TYPE); 
PROCEDURE get_profile_IDs( 
	the_ocp_name IN origin_card_profile.name%TYPE, 
	the_ocp_id OUT origin_card_profile.id%TYPE, 
	the_cp_id OUT card_profile.id%TYPE); 
 
------------------------------------------------------------------------------- 
--									     -- 
----									   ---- 
------									 ------ 
--------                 Create all the global cursors                 -------- 
------									 ------ 
----									   ---- 
--									     -- 
------------------------------------------------------------------------------- 
 
-- 
 
 
  -- Retreive the EEPROM, the DL_PROTOCOL 
  -- and the SUPPLIER_ID from the card_profile table 
 
  CURSOR Cursorcardprofilevalues (the_cp_id IN card_profile.id%TYPE) IS 
  SELECT dl_protocol, supplier_id FROM card_profile 
    WHERE id = the_cp_id; 
 
	-- Retreive the CNTR, the ADN_MODE  
	-- from the origin_card_profile 
 
	CURSOR Cursorgetocpvalues (the_cp_id IN card_profile.id%TYPE) IS 
	SELECT ro_available_eeprom, adn_mode, cntr FROM origin_card_profile 
		WHERE id = the_cp_id;  
		 
	-- Select APPLICATION_ID, LOCKED and STATUS  from ocp_application 
	CURSOR Cursorocpappli (the_ocp_id IN origin_card_profile.id%TYPE) IS 
	SELECT  application_id, locked,status FROM ocp_application 
		WHERE ocp_id = the_ocp_id; 
 
	-- Select PACAKGE_ID from ocp_package 
	CURSOR Cursorocppackage (the_ocp_id IN origin_card_profile.id%TYPE) IS 
	SELECT package_id FROM ocp_package 
		WHERE ocp_id = the_ocp_id; 
		 
	-- Get key types from card_profile_security for cp_id 
--	CURSOR CursorgetKeyType (the_cp_id IN card_profile.id%TYPE) IS 
--	SELECT DISTINCT key_type FROM card_profile_security  
--		WHERE  card_profile_id = the_cp_id; 
 
	-- Get key indexes needed by a specific card_profile 
	CURSOR CursorgetKeyIndexes (the_cp_id IN card_profile.id%TYPE) IS 
	SELECT kic_index,kid_index FROM card_profile_security 
		WHERE card_profile_id = the_cp_id; 
 
------------------------------------------------------------------------------- 
--									     -- 
----									   ---- 
------									 ------ 
--------                    Create Procedures Body                     -------- 
------									 ------ 
----									   ---- 
--									     -- 
------------------------------------------------------------------------------- 
 
------------------------------------------------------------------------------- 
-- 
--	SIM CARD FUNCTIONS 
-- 
------------------------------------------------------------------------------- 
PROCEDURE get_config( 
	p_ref IN config.parameter_name%TYPE,  
	p_val OUT config.value%TYPE 
	) IS 
BEGIN 
	SELECT NVL(value, 'no_val')	 
		INTO p_val 
		FROM config 
		WHERE parameter_name = p_ref; 
EXCEPTION 
	WHEN NO_DATA_FOUND THEN p_val := 'no_val'; 
END; 
 
------------------------------------------------------------------------------- 
PROCEDURE get_new_SIM_Id( 
	p_sim_card_id OUT sim_card.id%TYPE 
	) IS 
 
BEGIN		 
	SELECT seq_sim_card_id.nextval 
		INTO p_sim_card_id 
		FROM dual; 
 
EXCEPTION 
 
	WHEN OTHERS THEN 
	ROLLBACK; 
	RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
	'New MAX SIM CARD ID ABORTED : ORACLE DEBUG'); 
 
END;			 
 
 
------------------------------------------------------------------------------- 
 
PROCEDURE check_IMSI( 
  p_imsi IN sim_card.imsi%TYPE, 
  p_iccid IN sim_card.iccid%TYPE, 
  p_result OUT NUMBER,  -- 0: not found, 1: found with same iccid, 2: found with other ICCID 
  p_sim_card_id OUT sim_card.id%TYPE, 
  p_ocp_id OUT sim_card.ocp_id%TYPE 
  ) IS 
 
  loc_iccid sim_card.iccid%TYPE; 
 
BEGIN 
 
  p_result := -1; 
  BEGIN 
    SELECT NVL(id,0), NVL(iccid, 'no_iccid'), NVL(ocp_id, '0') 
      INTO p_sim_card_id, loc_iccid, p_ocp_id 
      FROM sim_card 
      WHERE imsi = RTRIM (p_imsi, ' '); 
 
  EXCEPTION 
    WHEN NO_DATA_FOUND THEN p_result := 0; 
    WHEN OTHERS THEN RAISE; 
  END; 
 
  IF p_result <> 0 THEN 
    IF loc_iccid = p_iccid THEN 
      p_result := 1; 
    ELSE 
      p_result := 2; 
    END IF; 
  END IF; 
END; 
 
PROCEDURE get_IMSI_state ( 
	p_imsi IN sim_card.imsi%TYPE, 
	p_iccid IN sim_card.iccid%TYPE, 
	p_ocp_name IN origin_card_profile.name%TYPE, 
	p_card_exists OUT NUMBER,  -- 0 : card exists. 1 : card should be created 
	p_sim_card_id OUT sim_card.id%TYPE, 
	p_cp_id OUT sim_card.card_profile_id%TYPE, 
	p_ocp_id OUT sim_card.ocp_id%TYPE 
	) IS 
	loc_auto_replace config.value%TYPE; 
	loc_overwrite_keys config.value%TYPE; 
	the_current_ocp_id origin_card_profile.id%TYPE; 
BEGIN 
	BEGIN 
		SELECT	id, DECODE(iccid, p_iccid, 1, 0), ocp_id 
		INTO	p_sim_card_id, p_card_exists, the_current_ocp_id 
		FROM	sim_card  
		WHERE	imsi = RTRIM (p_imsi, ' '); 
 
		IF p_card_exists = 1 THEN 
			-- card already exists 
			-- Check that profile is still the same 
			get_profile_IDs(p_ocp_name, p_ocp_id, p_cp_id); 
			IF p_ocp_id != the_current_ocp_id THEN 
				RAISE DUP_VAL_ON_INDEX; 
			END IF; 
 
			-- Are keys updatable 
			get_config('OVERWRITE_KEYS', loc_overwrite_keys); 
			IF loc_overwrite_keys <> 'YES' THEN 
				RAISE DUP_VAL_ON_INDEX; 
			END IF; 
		 
		ELSE 
			-- found with other ICCID : Delete and then re-create 
			get_config('SIM_AUTO_REPLACE', loc_auto_replace); 
			IF loc_auto_replace = 'YES' THEN 
				delete_SIM_no_commit(p_imsi); 
			ELSE 
				RAISE DUP_VAL_ON_INDEX; 
			END IF; 
		END IF; 
	 
	EXCEPTION 
		WHEN NO_DATA_FOUND THEN 
			-- imsi is not known yet 
			p_card_exists := 0; 
			NULL; 
		WHEN OTHERS THEN 
			RAISE; 
	END; 
END; 
		 
 
------------------------------------------------------------------------------- 
PROCEDURE get_SIM_Parameters( 
				p_ocp_name IN origin_card_profile.name%TYPE, 
				p_ocp_id OUT origin_card_profile.id%TYPE, 
				p_cp_id OUT card_profile.id%TYPE, 
				p_eeprom OUT card_profile.eeprom%TYPE, 
				p_dl_protocol OUT card_profile.dl_protocol%TYPE, 
				p_adn_mode OUT origin_card_profile.adn_mode%TYPE, 
				p_cntr_index OUT NumberArray, 
				p_cntr_value OUT CharArray10, 
				p_app_type_id OUT NumberArray 
				) IS 
-- Cursor 
	CURSOR getOcpCounter(ocpName IN origin_card_profile.name%TYPE) IS 
		SELECT	c.id, c.value, b.application_type_id  
		FROM	origin_card_profile a, ocp_counter b, ocp_counter_value c 
		WHERE	a.name = p_ocp_name 
		AND	a.id = b.ocp_id 
		AND	b.counter_id = c.id; 
 
-- Local Variable 
	imsi_exist sim_card.id%TYPE; 
	imsi_count number; 
	loc_cntr origin_card_profile.cntr%TYPE; 
	loc_index NUMBER; 
	 
-- Exception  
	err_cursor_not_found EXCEPTION; 
	err_unknown_dl_protocol EXCEPTION; 
	 
BEGIN 
 
	--------------------------------------------- 
	--- F E C T H    O C P I D and C P I D    --- 	 
	--------------------------------------------- 
 
	get_profile_IDs(p_ocp_name, p_ocp_id, p_cp_id); 
	 
	------------------------------------------------------- 
	--- F E T C H   D D L, S U P P L I E R  ---  
	------------------------------------------------------- 
 
	SELECT	dl_protocol 
	INTO	p_dl_protocol 
	FROM	card_profile 
	WHERE	id = p_cp_id; 
 
	IF (p_dl_protocol IS NULL) THEN 
		RAISE err_unknown_dl_protocol; 
	END IF; 
 
	------------------------------------------------------- 
	--- F E T C H  E E P R O M,  A D N M O D E, C N T R ---  
	------------------------------------------------------- 
			 
	-- Open cursor to get the orign_card_profile values 
	IF Cursorgetocpvalues%ISOPEN THEN 
		CLOSE Cursorgetocpvalues; 
	END IF; 
	OPEN Cursorgetocpvalues (p_ocp_id); 
 
	-- Fetched values 
	FETCH Cursorgetocpvalues INTO p_eeprom, p_adn_mode, loc_cntr; 
	IF Cursorgetocpvalues%NOTFOUND THEN 
		CLOSE Cursorgetocpvalues; 
		RAISE err_cursor_not_found; 
	END IF; 
	CLOSE Cursorgetocpvalues; 
 
	IF (loc_cntr IS NULL OR loc_cntr = '') THEN 
		loc_index := 0; 
		FOR initial_counter IN getOcpCounter(p_ocp_name) LOOP 
			loc_index := loc_index + 1; 
			p_cntr_index(loc_index) := initial_counter.id; 
			p_cntr_value(loc_index) := initial_counter.value; 
			p_app_type_id(loc_index) := initial_counter.application_type_id; 
		END LOOP; 
	ELSE 
		p_cntr_index(1) := -1; 
		p_cntr_value(1) := loc_cntr; 
		p_app_type_id(1) := ''; 
	END IF; 
 
	 
 
EXCEPTION 
		 
	WHEN err_cursor_not_found THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_INTERNAL_DATABASE_ERROR_CURSOR_NOT_FOUND'); 
		 
	WHEN err_unknown_dl_protocol THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_SUPPLIER_IS_UNKNOW'); 
 
	WHEN OTHERS THEN 
		ROLLBACK; 
		RAISE; 
 
 
END; 
 
------------------------------------------------------------------------------- 
-- Retrieve origin_card_profile_id and card profile_id for an OCP name 
------------------------------------------------------------------------------- 
PROCEDURE get_profile_IDs( 
	the_ocp_name IN origin_card_profile.name%TYPE, 
	the_ocp_id OUT origin_card_profile.id%TYPE, 
	the_cp_id OUT card_profile.id%TYPE) IS 
 
BEGIN 
 
	-- Check that the OCP NAME exist inthe database and retreive the ID 
 
	BEGIN 
		SELECT	id  
		INTO	the_ocp_id 
		FROM	origin_card_profile  
		WHERE	name = RTRIM(the_ocp_name, ' '); 
	 
	EXCEPTION 
		WHEN NO_DATA_FOUND THEN 
			ROLLBACK; 
			RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
			'almProvision_REFERENCED_OCP_NAME_NOT_EXIST'); 
 
	END; 
	 
	-- Get card profile ID 
 
	SELECT	cp_id  
	INTO	the_cp_id 
	FROM	ocp_card_profile  
	WHERE	ocp_id = the_ocp_id; 
 
	EXCEPTION 
		WHEN NO_DATA_FOUND THEN 
			ROLLBACK; 
			RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
			'almProvision_REFERENCED_OCP_NAME_IS_NOT_LINKED_WITH_A_CARD_PROFILE'); 
		 
END; 
 
 
 
------------------------------------------------------------------------------- 
-- 
-- CreateSIMgeneric : static generic procedure that create a new sim  
--                    card entry in the table 
-- 
PROCEDURE create_SIM_generic ( 
				p_ocp_name IN origin_card_profile.name%TYPE, 
				p_imsi IN sim_card.imsi%TYPE, 
				p_iccid IN sim_card.iccid%TYPE, 
				p_isc1 IN sim_card.ics1%TYPE, 
				p_sim_card_id OUT sim_card.id%TYPE, 
				p_cp_id OUT card_profile.id%TYPE, 
				p_ocp_id OUT origin_card_profile.id%TYPE 
) IS 
 
	------------------------------------------------------- 
	--- L O C A L  V A R I A B L E S                    ---  
	------------------------------------------------------- 
	current_ocp_id origin_card_profile.id%TYPE; 
	the_eeprom card_profile.eeprom%TYPE; 
	the_dl_protocol card_profile.dl_protocol%TYPE; 
	the_adn_mode origin_card_profile.adn_mode%TYPE; 
	the_cntr_index NumberArray;	-- read from ocp. 
	the_cntr_value CharArray10;	-- values read in ocp, to be set in counter 
	the_app_type_id NumberArray;	-- values read in ocp, to be set in sim_card_counter 
	the_import_date sim_card.import_date%TYPE; 
	the_cntr sim_card.cntr%TYPE;	-- value to be set in sim_card 
	seen_cntr_mmi_id NumberArray;	-- the_cntr_index values already seen. 
	seen_cntr_db_id NumberArray;	-- index to be set in sim_card_counter and counter 
	the_application_id ocp_application.application_id%TYPE; 
	the_application_locked ocp_application.locked%TYPE; 
	the_application_status ocp_application.status%TYPE; 
	the_package_id ocp_package.package_id%TYPE; 
	loc_cntr_db_id NUMBER;		-- local id to be used 
	rc NUMBER; 
	found NUMBER; 
	
	v_aid application.AID%TYPE;
	v_install_params application.INSTALL_PARAMETERS%TYPE;
	v_tar application.TAR%TYPE;
 
	------------------------------------------------------- 
	---  E X C E P T I O N 			    		        --- 
	------------------------------------------------------- 
BEGIN 
	---------------------------------------------------------- 
	-- Checks the values in entry, and retreives the data  
	-- to create the sim card new record 
	---------------------------------------------------------- 
 
	get_SIM_Parameters( 
			p_ocp_name , 
			p_ocp_id , 
			p_cp_id, 
			the_eeprom , 
			the_dl_protocol , 
			the_adn_mode , 
			the_cntr_index, 
			the_cntr_value, 
			the_app_type_id 
			); 
		 
	-- Create the import date 
	the_import_date:= SYSDATE; 
	the_cntr := ''; 
 
	-- Sets the cntr value in sim_card if the  
	-- single initial value found in DB is not 
	-- attached to any tar 
	IF the_cntr_index.COUNT = 1 THEN 
		IF the_cntr_index(1) = -1 THEN 
			the_cntr := the_cntr_value(1); 
		END IF; 
	END IF; 
 
	------------------------------------------------------- 
	--- S I M  C A R D 				    ---	  
	------------------------------------------------------- 
	get_new_SIM_Id(p_sim_card_id); 
 
	BEGIN
		INSERT INTO sim_card(	id, 
					card_profile_id, 
					ocp_id, 
					imsi, 
					iccid, 
					eeprom, 
					cntr, 
					adn_mode, 
					ics1, 
					synchro_date, 
					import_date, 
					activity, 
					state, 
					change_date 
					) VALUES ( 
						p_sim_card_id, 
						p_cp_id, 
						p_ocp_id,	 
						p_imsi, 
						p_iccid, 
						the_eeprom, 
						the_cntr, 
						the_adn_mode, 
						p_isc1, 
						NULL, 
						the_import_date, 
						'A', 
						'U', 
						the_import_date 
						); 

	EXCEPTION
                WHEN dup_val_on_index THEN
			ROLLBACK;
			RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR,
	                'almProvision_REFERENCED_ICCID_ALREADY_EXIST');
			
        END;
	 
	-- Creates ocp_counter entries if necessary. 
	IF the_cntr_index.COUNT > 0 THEN 
		-- at least 1 counter attached to a tar  
		IF the_cntr_index(1) <> -1 THEN 
			FOR i IN 1..the_cntr_index.COUNT LOOP 
				found := 0; 
				-- checks if the mmi index is diff from every one already seen. 
				FOR j IN 1..seen_cntr_mmi_id.COUNT LOOP 
					-- yes -> must add a new entry in ocp_counter, and counter 
					IF (the_cntr_index(i) = seen_cntr_mmi_id(j)) THEN 
						found := 1; 
						-- Just makes the links between the card and the counter, for this tar. 
						INSERT INTO sim_card_counter 
						VALUES(p_sim_card_id, the_app_type_id(i), seen_cntr_db_id(j)); 
 
						EXIT; 
					END IF; 
				END LOOP; 
				IF found = 0 THEN 
					-- Adds a new entry for it in counter and sim_card_counter 
					SELECT seq_sim_counter_id.NEXTVAL 
					INTO loc_cntr_db_id 
					FROM dual; 
 
					INSERT INTO counter  
					VALUES(loc_cntr_db_id, the_cntr_value(i)); 
 
					INSERT INTO sim_card_counter 
					VALUES(p_sim_card_id,the_app_type_id(i),loc_cntr_db_id); 
 
					seen_cntr_mmi_id(seen_cntr_mmi_id.COUNT + 1) := the_cntr_index(i); 
					seen_cntr_db_id(seen_cntr_db_id.COUNT + 1) := loc_cntr_db_id; 
				END IF; 
			END LOOP; 
		END IF; 
	END IF; 
	 
 
	-------------------------------------- 
    -- Create sim_card_gsmfiles records -- 
    -------------------------------------- 
   -- INSERT INTO sim_card_gsmfiles (SIM_CARD_ID,  
      -- GSMFILES_EF_NAME, FILE_REFERENCE,  
      -- TAG_LENGTH, FILE_SIZE, REC_LENGTH)  
      -- (SELECT p_sim_card_id, 
           -- GSMFILES_EF_NAME, FILE_REFERENCE,  
           -- TAG_LENGTH, FILE_SIZE, REC_LENGTH  
           -- FROM ocp_gsmfiles WHERE ORIGIN_CARD_PROFILE_ID = p_ocp_id); 
     
	-- Query Remove --
     
    -------------------------------------- 
	-- Create sim_card_application link -- 
	-------------------------------------- 
         
	-- Open cursor to select data from ocp_application 
        IF Cursorocpappli%ISOPEN THEN 
                CLOSE Cursorocpappli; 
        END IF; 
        OPEN Cursorocpappli (p_ocp_id); 
 
		-- Fetched values 
        LOOP 
                FETCH Cursorocpappli INTO 
                                the_application_id, 
                                the_application_locked, 
                                the_application_status; 
                EXIT WHEN Cursorocpappli%NOTFOUND; 
	 
		------------------------------------------------------- 
		--- S I M  C A R D  A P P L I C A T I O N           ---  
		------------------------------------------------------- 
 
				BEGIN
					SELECT aid, install_parameters, tar into v_aid, v_install_params, v_tar
						FROM application
						WHERE id = the_application_id;
				EXCEPTION
					WHEN OTHERS THEN
						RAISE;
				END;
 
 
 
                INSERT INTO sim_card_application VALUES ( 
                                p_sim_card_id, 
                                the_application_id, 
                                the_import_date, 
                                the_application_locked, 
                                the_application_status,
                                NULL,
                                1,
								v_aid,
								v_tar,
								v_install_params
                                ); 
        END LOOP; 
        CLOSE Cursorocpappli; 
 
	---------------------------------- 
	-- Create sim_card_package link -- 
	---------------------------------- 
 
        -- Open cursor to retreive all the data of ocp_package 
        IF Cursorocppackage%ISOPEN THEN 
                CLOSE Cursorocppackage; 
        END IF; 
        OPEN Cursorocppackage (p_ocp_id); 
 
        -- Fetched values 
        LOOP 
                FETCH Cursorocppackage INTO the_package_id; 
 
                EXIT WHEN Cursorocppackage%NOTFOUND; 
 
		------------------------------------------------------- 
		--- S I M  C A R D  P A C K A G E 		    ---	 
		------------------------------------------------------- 
 
                INSERT INTO sim_card_package (sim_card_id, package_id)
                VALUES (p_sim_card_id, the_package_id);
        END LOOP; 
        CLOSE Cursorocppackage; 
 
	-- eventually creates a multiple simcard 
	Dual_pack.OnCreateSIM(	p_ocp_id, 
				p_sim_card_id, 
				p_iccid); 
						 
EXCEPTION 
	WHEN OTHERS THEN 
		RAISE; 
END; 
 
 
------------------------------------------------------------------------------- 
-- check card_profile_security config consitency 
------------------------------------------------------------------------------- 
 
FUNCTION has_secu (p_cp_id IN card_profile.id%TYPE, p_use_key_set IN NUMBER) 
RETURN NUMBER IS 
	err_wrong_key_type EXCEPTION; 
	use_key_set NUMBER; 
BEGIN 
		SELECT	DISTINCT DECODE(key_type, 'KEY_SET', 1, 0) 
		INTO	use_key_set 
		FROM	card_profile_security 
		WHERE	card_profile_id = p_cp_id 
		GROUP	BY DECODE(key_type, 'KEY_SET', 1, 0); 
		-- Exactly one result for the request 
		-- V1 card cannot use KEY_SET securities 
		IF use_key_set != p_use_key_set THEN 
			RAISE err_wrong_key_type; 
		END IF; 
 
		-- Only one type (and the good one) of secu found 
		RETURN 1; 
EXCEPTION 
	WHEN NO_DATA_FOUND THEN 
		-- no secu found for this card profile 
		RETURN 0; 
	WHEN OTHERS THEN 
		RETURN NULL; 
END; 
 
 
------------------------------------------------------------------------------- 
-- 
-- CreateSIM  commande : create a new sim card entry in the table 
-- In this particular case the sim card could be a V1 or a V3.  
-- The way to know wich one it is necessary to create is the presence or not  
-- of the card_profile_id in the card_profile table. 
-- 
 
PROCEDURE create_SIM ( 
				p_ocp_name IN origin_card_profile.name%TYPE, 
				p_imsi IN sim_card.imsi%TYPE, 
				p_iccid IN sim_card.iccid%TYPE, 
				p_isc1 IN sim_card.ics1%TYPE, 
				p_key_nb IN NUMBER, 
				p_key_index IN NumberArray, 
				p_key_value IN CharArray48 
				) IS 
	------------------------------------------------------- 
	--- L O C A L  V A R I A B L E S                    ---  
	------------------------------------------------------- 
	counter NUMBER; 
	counter3 NUMBER; 
	found NUMBER; 
	card_exists NUMBER; 
	 
	cp_id_exists_in_secu NUMBER; 
	the_ocp_id origin_card_profile.id%TYPE; 
 
	fl_newkey NUMBER; 
	 
	cp_key_indexes	NumberArray;	-- key indexes needed by card profile 
	 
	the_cp_id card_profile.id%TYPE; 
	the_dl_protocol card_profile.dl_protocol%TYPE; 
	the_sim_card_id sim_card.id%TYPE; 
	the_key_type card_profile_security.key_type%TYPE; 
 
	-- Special case with V1 cards 
	the_supplier_name supplier.name%TYPE; 
 
	-- Gemplus V1 
	the_gemplus_encryption gemplus_card_profile.encryption%TYPE; 
	the_gemplus_index_position gemplus_card_profile.index_position%TYPE; 
 
	-- Orga V1 
	the_orga_encryption orga_card_profile.encryption%TYPE; 
	 
	-- Axalto 16K 
	the_secu_level schlumberger_card_profile.security_level%TYPE; 
 
	-- Obtv1 V1 
	the_obtv1_authentic_algo obtv1_card_profile.authentic_algo%TYPE; 
	the_obtv1_cipher_algo obtv1_card_profile.cipher_algo%TYPE; 
 
	v_dl_protocol card_profile.DL_PROTOCOL%TYPE;
	--------------------------------------
	--- C U R S O R S		   --- 
	--------------------------------------
	CURSOR cr_getDlProtocol(crv_ocp_name IN origin_card_profile.NAME%TYPE) IS
		SELECT cp.DL_PROTOCOL
			FROM origin_card_profile ocp, card_profile cp, ocp_card_profile ocp_cp
			WHERE ocp.name = crv_ocp_name
				AND ocp.id = ocp_cp.ocp_id
				AND ocp_cp.cp_id = cp.id;
				
	------------------------------------------------------- 
	--- C U R S O R  E X C E P T I O N 		    --- 
	------------------------------------------------------- 
	err_cursor_not_found EXCEPTION; 
	err_null_ddl_key  EXCEPTION; 
	err_orga_cp_inexist EXCEPTION; 
	err_key_index_not_null EXCEPTION; 
	err_gemplus_cp_inexist EXCEPTION; 
	err_obtv1_cp_inexist EXCEPTION; 
	err_schlumberger_cp_inexist EXCEPTION; 
	err_unknown_supplier EXCEPTION; 
	err_unknown_dl_protocol EXCEPTION; 
	err_key_value_null EXCEPTION; 
	err_cp_key_not_set EXCEPTION; 
	err_imsi_already_exist EXCEPTION; 
	err_wrong_key_type EXCEPTION; 
 
BEGIN 
	-- TODO : get_profile_IDs should be here + check on card_profile_security consistency 
	-- it should be an entry data when checking the imsi state, and an entry for sim card creation 
	BEGIN 
		get_IMSI_state(p_imsi, p_iccid, p_ocp_name, card_exists, the_sim_card_id, the_cp_id, the_ocp_id); 
	EXCEPTION 
		WHEN DUP_VAL_ON_INDEX THEN 
			RAISE err_imsi_already_exist; 
		WHEN OTHERS THEN 
			RAISE; 
	end; 
 
	IF card_exists = 0 THEN  
		-- Create a new sim card entry in db 
		create_SIM_generic(p_ocp_name, p_imsi, p_iccid, p_isc1, the_sim_card_id, the_cp_id, the_ocp_id); 
	END IF; 
	 
	-- check card profile security consistency 
	cp_id_exists_in_secu := has_secu(the_cp_id, 0); 
 
	if cp_id_exists_in_secu is NULL THEN 
		RAISE err_wrong_key_type; 
	end if; 
 
	------------------------------------------------------- 
	--- D L  K E Y  				    ---	 
	------------------------------------------------------- 
 
	IF cp_id_exists_in_secu = 0 THEN 
 
		-- Fetch the supplier id, supplier name and dl_protocole 
		SELECT	cp.dl_protocol, s.name 
		INTO	the_dl_protocol, the_supplier_name 
		FROM	card_profile cp, supplier s 
		WHERE	cp.id = the_cp_id 
		AND	s.id = cp.supplier_id; 
 
		-- Proprietary Card : ORGA and AUSTRIA card 
		IF ( 
		((UPPER(the_supplier_name) = 'ORGA') AND 
			(UPPER(the_dl_protocol) = 'V1')) 
		OR 
		((UPPER(the_supplier_name) = 'AUSTRIA') AND 
			(UPPER(the_dl_protocol) = 'V11')) 
		) 
		THEN 
			IF (p_key_value(p_key_nb) IS NULL) THEN 
				RAISE err_key_value_null; 
			END IF; 
	 
			BEGIN 
				fl_newkey := 1; 
				INSERT INTO dlkey VALUES ( 
				  the_supplier_name || the_dl_protocol || '_' || the_sim_card_id, p_key_value (p_key_nb)); 
			EXCEPTION 
				WHEN DUP_VAL_ON_INDEX THEN 
					UPDATE dlkey SET VALUE =  p_key_value (p_key_nb)  
						WHERE REFERENCE = the_supplier_name || the_dl_protocol || '_' || the_sim_card_id; 
					fl_newkey := 0; 
	 
				WHEN OTHERS THEN 
					RAISE; 
			END; 
	 
			IF (fl_newkey <> 0) THEN 
				-- ocp_id exist  and encryption is not null 
				-- No need of cursor 
				SELECT encryption 
					INTO the_orga_encryption 
					FROM orga_card_profile 
					WHERE ocp_id = the_ocp_id ; 
					 
				IF (the_orga_encryption IS NULL) THEN 
					RAISE err_orga_cp_inexist; 
				END IF; 
		 
				------------------------------------------------------- 
				--- O R G A  C A R D  				    ---	 
				------------------------------------------------------- 
				INSERT INTO orga_card VALUES ( 
					the_sim_card_id, 
					the_orga_encryption, 
					the_supplier_name || the_dl_protocol || '_' || the_sim_card_id); 
			END IF; 
			 
			 
			-- Proprietary Card : GEMPLUS card 
	 
		ELSIF  ((UPPER(the_supplier_name) = 'GEMPLUS') AND 
			  ((UPPER(the_dl_protocol) = 'V1.1') OR 
			  (UPPER(the_dl_protocol) = 'ESMS_V2') OR 
			  (UPPER(the_dl_protocol) = 'ESMS_V1')))  THEN 
		 
			IF (p_key_value (p_key_nb) IS NULL) THEN 
				RAISE err_key_value_null; 
			END IF; 
			------------------------------------------------------- 
			--- D L  K E Y  				    ---	 
			------------------------------------------------------- 
			BEGIN 
				fl_newkey := 1; 
				INSERT INTO dlkey  
					VALUES ('GEMPLUSV1.1_' || the_sim_card_id,  
			  		p_key_value (p_key_nb));		 
	 
			EXCEPTION 
				WHEN DUP_VAL_ON_INDEX THEN 
					UPDATE dlkey SET VALUE =  p_key_value (p_key_nb)  
						WHERE REFERENCE = 'GEMPLUSV1.1_' || the_sim_card_id; 
					fl_newkey := 0; 
	 
				WHEN OTHERS THEN 
					RAISE; 
			END; 
	 
			IF (fl_newkey <> 0) THEN 
	 
			-- link between the obtv1_card_profile and  
			-- the origin_card_profile :2 
	 
				SELECT NVL(COUNT (ocp_id),0) INTO counter3 
					FROM gemplus_card_profile  
					WHERE ocp_id = the_ocp_id ; 
				IF (counter != 1) THEN 
					RAISE err_gemplus_cp_inexist; 
				END IF; 
			 
				-- ocp_id exist (check before)  
				-- and encryption,index_position 
				-- are not null. No need of cursor 
				SELECT	encryption, index_position 
				INTO	the_gemplus_encryption, the_gemplus_index_position 
				FROM	gemplus_card_profile 
				WHERE	ocp_id = the_ocp_id ; 
	 
				------------------------------------------------------- 
				--- G E M P L U S   				    ---	 
				------------------------------------------------------- 
	 
				INSERT INTO gemplus_card VALUES ( 
				  the_sim_card_id, 
				  the_gemplus_encryption, 
				  the_gemplus_index_position, 
				  'GEMPLUSV1.1_' || the_sim_card_id 
				  ); 
			END IF; 
			 
			 
			-- Proprietary Card : Oberthur V1.1 card 
	 
		ELSIF  ((UPPER(the_supplier_name) = 'OBERTHUR TECHNOLOGIES') AND 
			  (UPPER(the_dl_protocol) = 'V1.1'))  THEN 
		 
			IF (p_key_value (p_key_nb) IS NULL) THEN 
				RAISE err_key_value_null; 
			END IF; 
	 
			------------------------------------------------------- 
			--- D L  K E Y  				    ---	 
			------------------------------------------------------- 
			BEGIN 
				fl_newkey := 1; 
				INSERT INTO dlkey VALUES ( 
				  'OBTV1_' || the_sim_card_id, p_key_value (p_key_nb));	 
		 
			EXCEPTION 
				WHEN DUP_VAL_ON_INDEX THEN 
					UPDATE dlkey SET VALUE =  p_key_value (p_key_nb)  
						WHERE REFERENCE = 'OBTV1_'  || the_sim_card_id; 
					fl_newkey := 0; 
	 
				WHEN OTHERS THEN 
					RAISE; 
			END; 
	 
			IF (fl_newkey <> 0) THEN 
				-- link between the obtv1_card_profile  
				-- and the origin_card_profile :4 
	 
				SELECT	NVL(COUNT (ocp_id),0) 
				INTO	counter 
				FROM	obtv1_card_profile  
				WHERE	ocp_id = the_ocp_id ; 
				IF (counter != 1) THEN 
					RAISE err_obtv1_cp_inexist; 
				END IF; 
	 
	        		-- ocp_id exist (check before)  
				-- and authentic_algo, cipher_algo 
	        		-- are not null. No need of cursor 
	 
				SELECT authentic_algo, cipher_algo 
					INTO the_obtv1_authentic_algo,  
					  the_obtv1_cipher_algo 
					FROM obtv1_card_profile 
					WHERE ocp_id = the_ocp_id ; 
		 
				------------------------------------------------------- 
				--- O B T V 1 _  C A R D  			    ---	 
				------------------------------------------------------- 
	 
				INSERT INTO obtv1_card VALUES ( 
				  the_sim_card_id, 
				  'OBTV1_' || the_sim_card_id, 
				  the_obtv1_authentic_algo, 
				  'OBTV1_' || the_sim_card_id, 
				  the_obtv1_cipher_algo 
				); 
		 
			END IF; 
			 
		ELSIF ((UPPER(the_supplier_name) = 'SCHLUMBERGER')  
			AND (UPPER(the_dl_protocol) = 'V1'))  THEN 
 	          
			IF (p_key_value (p_key_nb) IS NULL) THEN 
				RAISE err_key_value_null; 
			END IF; 
 	  
			------------------------------------------------------- 
			--- D L  K E Y                                      ---         
			------------------------------------------------------- 
			BEGIN 
				fl_newkey := 1; 
				INSERT INTO dlkey  
					VALUES ('AXALTOV1_' || the_sim_card_id, p_key_value (p_key_nb));         
		 
			EXCEPTION 
				WHEN DUP_VAL_ON_INDEX THEN 
					UPDATE dlkey SET VALUE =  p_key_value (p_key_nb)  
						WHERE REFERENCE = 'AXALTOV1_' || the_sim_card_id; 
					fl_newkey := 0; 
	 
				WHEN OTHERS THEN 
					RAISE; 
			END; 
 	  
			IF (fl_newkey <> 0) THEN 
				-- link between the schlumberger_card_profile 
				-- and the origin_card_profile :4 
			 
				BEGIN 
					SELECT	security_level 
					INTO	the_secu_level 
					FROM	schlumberger_card_profile 
					WHERE	ocp_id = the_ocp_id ; 
 	     
				EXCEPTION 
 			    		WHEN NO_DATA_FOUND THEN 
 	    					RAISE err_schlumberger_cp_inexist; 
	 	    	END; 
 	                                  
				------------------------------------------------------- 
				--- SCHLUMBERGER 16K  _  C A R D                    ---         
				------------------------------------------------------- 
 	  
				INSERT INTO schlumberger_card  
					VALUES (the_sim_card_id, the_secu_level); 
			END IF; 
 	                          
 	                          
		END IF;		 
	ELSE  -- cp_id_exists_in_secu <> 0 
		-- Gets all indexes needed by cp 
		FOR elem IN CursorgetKeyIndexes(the_cp_id) LOOP 
			found := 0; 
			FOR i IN 1..cp_key_indexes.COUNT LOOP 
				IF elem.kic_index = cp_key_indexes(i) THEN 
					found := 1; 
					EXIT; 
				END IF; 
			END LOOP; 
			IF found = 0 THEN 
				cp_key_indexes(cp_key_indexes.COUNT + 1) := elem.kic_index; 
			END IF; 
			 
			found := 0; 
			FOR i IN 1..cp_key_indexes.COUNT LOOP 
				IF elem.kid_index = cp_key_indexes(i) THEN 
					found := 1; 
					EXIT; 
				END IF; 
			END LOOP; 
			IF found = 0 THEN 
				cp_key_indexes(cp_key_indexes.COUNT + 1) := elem.kid_index; 
			END IF; 
		END LOOP; 
		-- Checks that all key indexes needed by cp are set 
		FOR i IN 1..cp_key_indexes.COUNT LOOP 
			found := 0; 
			FOR j IN 1..p_key_index.COUNT LOOP 
				IF cp_key_indexes(i) = p_key_index(j) THEN 
					found := 1; 
					EXIT; 
				END IF; 
			END LOOP; 
			IF found = 0 THEN 
				RAISE err_cp_key_not_set; 
			END IF; 
		END LOOP; 
		-- Insert or Update 
		IF cr_getDlProtocol%ISOPEN THEN
			CLOSE cr_getDlProtocol;
		END IF;
		
		OPEN cr_getDlProtocol(p_ocp_name);
		
		FETCH cr_getDlProtocol INTO v_dl_protocol;
		
		CLOSE cr_getDlProtocol;
	 
		IF v_dl_protocol <> 'USIM' AND v_dl_protocol <> 'USIM2' THEN


			FOR i IN 1..p_key_nb LOOP 
		 
				IF (p_key_value (i) IS NULL) THEN 
					RAISE err_key_value_null; 
				END IF; 
	 
				------------------------------------------------------- 
				--- D L  K E Y  				    ---	 
				------------------------------------------------------- 
	 
				BEGIN 
					INSERT INTO dlkey VALUES  
						('KEY_'||p_key_index(i)||'_' || the_sim_card_id,  
						p_key_value(i)); 
	 
				EXCEPTION 
					WHEN DUP_VAL_ON_INDEX THEN 
						UPDATE dlkey SET VALUE =  p_key_value(i) 
							WHERE REFERENCE = 'KEY_'||p_key_index(i)||'_' || the_sim_card_id; 
	 
					WHEN OTHERS THEN 
						RAISE; 
				END; 
			END LOOP; 
		END IF; 
	END IF; 
				 
-- Commit Work 
	COMMIT ; 
	 
	 
EXCEPTION 
	WHEN err_cursor_not_found THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_INTERNAL_DATABASE_ERROR_CURSOR_NOT_FOUND'); 
 
	WHEN err_null_ddl_key THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_REFERENCED_DDL_MUST_NOT_BE_NULL'); 
 
	WHEN err_key_index_not_null THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_SOME_OF_THE_REFERENCED_KEY_VALUES_ARE_NULL'); 
 
	WHEN err_orga_cp_inexist THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_ORGA_CARD_PROFILE_ID_NOT_EXIST'); 
 
	WHEN err_gemplus_cp_inexist THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_GEMPLUS_CARD_PROFILE_ID_NOT_EXIST'); 
	 
	WHEN err_obtv1_cp_inexist THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_OBT_CARD_PROFILE_ID_NOT_EXIST'); 
 
	WHEN err_schlumberger_cp_inexist THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_AXALTO_CARD_PROFILE_ID_NOT_EXIST'); 
		 
	WHEN err_unknown_supplier THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_SUPPLIER_IS_UNKNOW'); 
 
	WHEN err_unknown_dl_protocol THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_SUPPLIER_IS_UNKNOW'); 
 
	WHEN err_key_value_null THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_REFERENCED_KEY_VALUE_MUST_NOT_BE_NULL'); 
 
	WHEN err_wrong_key_type THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_INCORRECT_KEY_TYPE'); 
 
	WHEN err_cp_key_not_set THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_KEY_MISSING'); 
	WHEN err_imsi_already_exist THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR(INDEX_PROV_ERROR, 
		'almProvision_REFERENCED_IMSI_ALREADY_EXIST'); 
	WHEN OTHERS THEN 
		ROLLBACK; 
		RAISE; 
 
END; 
 
 
 
------------------------------------------------------------------------------- 
-- 
-- CreateSIM  commande : create a new sim card entry in the table 
-- In this particular case the sim card could only be a V3.  
-- 
 
PROCEDURE create_SIM_KeySet ( 
					p_ocp_name IN origin_card_profile.name%TYPE, 
					p_imsi IN sim_card.imsi%TYPE, 
					p_iccid IN sim_card.iccid%TYPE, 
					p_isc1 IN sim_card.ics1%TYPE, 
					p_key_set_nb IN NUMBER, 
					p_key_set_index IN NumberArray, 
					p_kic_values_tab IN CharArray48, 
					p_kid_values_tab IN CharArray48, 
					p_kik_values_tab IN CharArray48 
					) IS 
 
	------------------------------------------------------- 
	--- L O C A L  V A R I A B L E S                    ---  
	------------------------------------------------------- 
 
	card_exists NUMBER; 
	found NUMBER; 
	 
	cp_kic_indexes NumberArray;	-- distinct kic indexes in cp 
	cp_kid_indexes NumberArray;	-- distinct kid indexed in cp 
	 
	the_cp_id card_profile.id%TYPE; 
	the_ocp_id origin_card_profile.id%TYPE; 
	the_sim_card_id sim_card.id%TYPE; 
	the_key_type card_profile_security.key_type%TYPE; 
 
	v_dl_protocol card_profile.DL_PROTOCOL%TYPE;
	--------------------------------------
	--- C U R S O R S		   --- 
	--------------------------------------
	CURSOR cr_getDlProtocol(crv_ocp_name IN origin_card_profile.NAME%TYPE) IS
		SELECT cp.DL_PROTOCOL
			FROM origin_card_profile ocp, card_profile cp, ocp_card_profile ocp_cp
			WHERE ocp.name = crv_ocp_name
				AND ocp.id = ocp_cp.ocp_id
				AND ocp_cp.cp_id = cp.id;
 
 
 
	------------------------------------------------------- 
	--- C U R S O R  E X C E P T I O N 		    --- 
	------------------------------------------------------- 
			 
	err_cursor_not_found EXCEPTION; 
	err_ocp_id_not_exist EXCEPTION; 
	err_null_ddl_key  EXCEPTION; 
	err_orga_cp_inexist EXCEPTION; 
	err_key_index_not_null EXCEPTION; 
	err_key_value_null EXCEPTION; 
	err_key_set_index_null EXCEPTION; 
	err_wrong_key_type EXCEPTION; 
	err_cp_key_not_set EXCEPTION; 
	err_imsi_already_exist EXCEPTION; 
	cp_id_exists_in_secu NUMBER; 
	 
BEGIN 
 
	BEGIN 
		get_IMSI_state(p_imsi, p_iccid, p_ocp_name, card_exists, the_sim_card_id, the_cp_id, the_ocp_id); 
	EXCEPTION 
		WHEN DUP_VAL_ON_INDEX THEN 
			RAISE err_imsi_already_exist; 
		WHEN OTHERS THEN 
			RAISE; 
	end; 
 
	IF card_exists = 0 THEN  
		-- Create a new sim card entry in db 
		create_SIM_generic(p_ocp_name, p_imsi, p_iccid, p_isc1, the_sim_card_id, the_cp_id, the_ocp_id); 
	END IF; 
 
	-- for card with keyset, we can only have key_set types and at least one 
	cp_id_exists_in_secu :=  has_secu(the_cp_id, 1); 
	IF cp_id_exists_in_secu IS NULL OR cp_id_exists_in_secu != 1 THEN 
		RAISE err_wrong_key_type; 
	END IF; 
 
	------------------------------------------------------- 
	--- D L  K E Y S  				    ---	 
	------------------------------------------------------- 
	-- Gets all kic, and kid indexes needed by cp 
	FOR elem IN CursorgetKeyIndexes(the_cp_id) LOOP 
		found := 0; 
		FOR i IN 1..cp_kic_indexes.COUNT LOOP 
			IF elem.kic_index = cp_kic_indexes(i) THEN 
				found := 1; 
				EXIT; 
			END IF; 
		END LOOP; 
		IF found = 0 THEN 
			cp_kic_indexes(cp_kic_indexes.COUNT + 1) := elem.kic_index; 
		END IF; 
		 
		found := 0; 
		FOR i IN 1..cp_kid_indexes.COUNT LOOP 
			IF elem.kid_index = cp_kid_indexes(i) THEN 
				found := 1; 
				EXIT; 
			END IF; 
		END LOOP; 
		IF found = 0 THEN 
			cp_kid_indexes(cp_kid_indexes.COUNT + 1) := elem.kid_index; 
		END IF; 
	END LOOP; 
	-- Checks that all kic indexes needed by cp are set 
	FOR i IN 1..cp_kic_indexes.COUNT LOOP 
		found := 0; 
		FOR j IN 1..p_key_set_nb LOOP 
			IF (cp_kic_indexes(i) = p_key_set_index(j)) AND (p_kic_values_tab(j) IS NOT NULL) THEN 
				found := 1; 
				EXIT; 
			END IF; 
		END LOOP; 
		IF found = 0 THEN 
			RAISE err_cp_key_not_set; 
		END IF; 
	END LOOP; 
	-- Checks that all kid indexes needed by cp are set 
	FOR i IN 1..cp_kid_indexes.COUNT LOOP 
		found := 0; 
		FOR j IN 1..p_key_set_nb LOOP 
			IF (cp_kid_indexes(i) = p_key_set_index(j)) AND (p_kid_values_tab(j) IS NOT NULL) THEN 
				found := 1; 
				EXIT; 
			END IF; 
		END LOOP; 
		IF found = 0 THEN 
			RAISE err_cp_key_not_set; 
		END IF; 
	END LOOP; 
 
	IF cr_getDlProtocol%ISOPEN THEN
		CLOSE cr_getDlProtocol;
	END IF;
	
	OPEN cr_getDlProtocol(p_ocp_name);
	
	FETCH cr_getDlProtocol INTO v_dl_protocol;
	
	CLOSE cr_getDlProtocol;
 
	IF v_dl_protocol <> 'USIM' AND v_dl_protocol <> 'USIM2' THEN

		FOR i IN 1..p_key_set_nb LOOP 
		 
				IF (p_key_set_index (i) IS NULL) THEN 
					RAISE err_key_set_index_null; 
				END IF; 
	 
				-- KIC 
				IF (p_kic_values_tab (i) IS NOT NULL) THEN 
					BEGIN 
					INSERT INTO dlkey VALUES ( 
					  'KIC_'||p_key_set_index(i)||'_' || the_sim_card_id, 
					  p_kic_values_tab(i)); 
					EXCEPTION 
						WHEN DUP_VAL_ON_INDEX THEN 
							UPDATE dlkey SET VALUE = p_kic_values_tab(i) WHERE 
								REFERENCE = 'KIC_'||p_key_set_index(i)||'_' || the_sim_card_id ; 
	 
						WHEN OTHERS THEN 
							ROLLBACK; 
							RAISE; 
					END; 
				END IF; 
				 
				-- KID 
				IF (p_kid_values_tab (i) IS NOT NULL) THEN 
					BEGIN 
					INSERT INTO dlkey VALUES ( 
					  'KID_'||p_key_set_index(i)||'_' || the_sim_card_id,  
					  p_kid_values_tab(i)); 
					EXCEPTION 
						WHEN DUP_VAL_ON_INDEX THEN 
							UPDATE dlkey SET VALUE = p_kid_values_tab(i) WHERE 
								REFERENCE = 'KID_'||p_key_set_index(i)||'_' || the_sim_card_id ; 
	 
						WHEN OTHERS THEN 
							ROLLBACK; 
							RAISE; 
					END; 
				END IF; 
				 
				-- KIK 
				IF (p_kik_values_tab (i) IS NOT NULL) THEN 
					BEGIN 
					INSERT INTO dlkey VALUES ( 
					  'KIK_'||p_key_set_index(i)||'_' || the_sim_card_id,  
					  p_kik_values_tab(i)); 
					EXCEPTION 
						WHEN DUP_VAL_ON_INDEX THEN 
							UPDATE dlkey SET VALUE = p_kik_values_tab(i) WHERE 
								REFERENCE = 'KIK_'||p_key_set_index(i)||'_' || the_sim_card_id ; 
	 
						WHEN OTHERS THEN 
							ROLLBACK; 
							RAISE; 
					END; 
				END IF; 
		END LOOP; 
	END IF;
 
-- Commit Work 
	COMMIT ; 
	 
	 
EXCEPTION 
 
	WHEN err_cursor_not_found THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_INTERNAL_DATABASE_ERROR_CURSOR_NOT_FOUND'); 
 
	WHEN err_null_ddl_key THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_REFERENCED_DDL_MUST_NOT_BE_NULL'); 
 
	WHEN err_key_index_not_null THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_SOME_OF_THE_REFERENCED_KEY_VALUES_ARE_NULL'); 
 
	WHEN err_key_value_null THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_REFERENCED_KEY_VALUE_MUST_NOT_BE_NULL'); 
 
	WHEN err_key_set_index_null THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_REFERENCED_KEY_SET_VALUE_MUST_NOT_BE_NULL'); 
	 
	WHEN err_cp_key_not_set THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_KEY_MISSING'); 
 
	WHEN err_wrong_key_type THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_INCORRECT_KEY_TYPE'); 
 
	WHEN err_imsi_already_exist THEN 
		ROLLBACK; 
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
		'almProvision_REFERENCED_IMSI_ALREADY_EXIST'); 
		 
	WHEN OTHERS THEN 
		ROLLBACK; 
		RAISE; 
 
END; 
 
 
 
------------------------------------------------------------------------------- 
--  
-- Delete SIM command : delete an existing sim card reference 
-- 
 
-- Commit Work 
PROCEDURE delete_SIM (p_imsi IN sim_card.imsi%TYPE) IS 
BEGIN 
	delete_SIM_no_commit(p_imsi); 
	COMMIT ; 
END; 
 
PROCEDURE delete_SIM_no_commit (p_imsi IN sim_card.imsi%TYPE) IS 
 
 
-- Local variables 
	delete_ok NUMBER; 
	orga_card_key orga_card.dl_key%TYPE; 
	gemplus_card_key gemplus_card.dl_key%TYPE; 
	obtv1_card_auth_key obtv1_card.authentic_key%TYPE; 
	obtv1_card_cipher_key obtv1_card.cipher_key%TYPE; 
	the_sim_card_id sim_card.id%TYPE; 
	the_sim_card_id_text VARCHAR2(30); 
	the_m_sim_id NUMBER; 
	the_ocp_id NUMBER; 
	i NUMBER; 
 
-- Exception 
	err_no_imsi_found EXCEPTION; 
BEGIN 
	delete_ok := 0; 
 
---------------------------------------------- 
-- Gets infos on sim before deleting it (for Dual Mgt) 
---------------------------------------------- 
	-- gets its id and ocp_id 
	BEGIN 
		SELECT id, NVL(ocp_id , -1) INTO the_sim_card_id, the_ocp_id 
			FROM sim_card 
			WHERE imsi = RTRIM (p_imsi, ' '); 
 
		EXCEPTION 
	        WHEN NO_DATA_FOUND THEN RAISE err_no_imsi_found; 
	END; 
		 
-- Delete values in counter and sim_card_counter tables. 
-- sim_card_counter is deleted via on delete cascade constraint 
	DELETE counter c 
		WHERE EXISTS (SELECT 1 
			FROM sim_card_counter sc 
			WHERE sc.counter_id = c.id 
			AND	sc.sim_card_id = the_sim_card_id); 
 
---------------------------------------------- 
-- Case of V1 
---------------------------------------------- 
 
--Select orga key reference 
	BEGIN 
		SELECT dl_key INTO orga_card_key 
			FROM orga_card  
			WHERE sim_card_id = the_sim_card_id; 
		 
		EXCEPTION 
			WHEN NO_DATA_FOUND THEN orga_card_key := 'NO_KEY'; 
	END; 
	 
	IF orga_card_key <> 'NO_KEY' THEN 
		DELETE FROM dlkey 
			WHERE reference = orga_card_key; 
-- No need to delete from orga_card : There is an on delete cascade constraint 
--		DELETE FROM orga_card  
--			WHERE sim_card_id = the_sim_card_id; 
		delete_ok := 1; 
	END IF; 
	 
	IF delete_ok = 0 THEN 
--Select gemplus key reference 
		BEGIN 
			SELECT dl_key INTO gemplus_card_key 
				FROM gemplus_card 
				WHERE sim_card_id = the_sim_card_id; 
		 
			EXCEPTION 
				WHEN NO_DATA_FOUND THEN gemplus_card_key := 'NO_KEY'; 
		END; 
 
		IF gemplus_card_key <> 'NO_KEY' THEN 
			DELETE FROM dlkey  
				WHERE reference = gemplus_card_key; 
-- No need to delete from gemplus_card : There is an on delete cascade constraint 
--			DELETE FROM gemplus_card  
--				WHERE sim_card_id = the_sim_card_id; 
			delete_ok := 1; 
		END IF;		 
	END IF; 
 
-- axalto/schlumberger V1 
	IF delete_ok = 0 THEN 
-- No need to delete from schlumberger_card : There is an on delete cascade constraint 
--		DELETE FROM schlumberger_card 
--			WHERE sim_card_id = the_sim_card_id; 
		DELETE FROM dlkey 
			WHERE reference = 'AXALTOV1_' || the_sim_card_id; 
		IF SQL%ROWCOUNT > 0 THEN 
			delete_ok := 1; 
		END IF; 
	END IF; 
 
 
	IF delete_ok = 0 THEN 
        --Select obtv1 key references 
		BEGIN 
			SELECT NVL(authentic_key, 'NO_KEY'), NVL(cipher_key, 'NO_KEY') 
				INTO obtv1_card_auth_key, obtv1_card_cipher_key 
				FROM obtv1_card 
				WHERE sim_card_id = the_sim_card_id; 
			 
			EXCEPTION  
				WHEN NO_DATA_FOUND THEN  
					obtv1_card_auth_key := 'NO_KEY'; 
					obtv1_card_cipher_key := 'NO_KEY'; 
		END; 
		 
		IF obtv1_card_auth_key <> 'NO_KEY' THEN 
			DELETE FROM dlkey 
				WHERE reference = obtv1_card_auth_key; 
			delete_ok := 1; 
		END IF; 
		IF obtv1_card_cipher_key <> 'NO_KEY' THEN 
			DELETE FROM dlkey 
				WHERE reference = obtv1_card_cipher_key; 
			delete_ok := 1; 
		END IF; 
-- No need to delete from obtv1_card : There is an on delete cascade constraint 
--		DELETE FROM obtv1_card 
--			WHERE sim_card_id = the_sim_card_id; 
	END IF; 
 
 
---------------------------------------------- 
-- Case of Non proprietary cards 
---------------------------------------------- 
	IF (delete_ok = 0) THEN 
	-- Do not use DELETE FROM dlkey WHERE reference LIKE ... 
	-- It does not use index and is terrible for performance 
	-- the code below is probably not the best but still is better 
		FOR i IN 0..15 LOOP 
			the_sim_card_id_text := to_char(i) || '_' || to_char(the_sim_card_id); 
			DELETE FROM dlkey WHERE reference = 'KIC_' || the_sim_card_id_text; 
			DELETE FROM dlkey WHERE reference = 'KID_' || the_sim_card_id_text; 
			DELETE FROM dlkey WHERE reference = 'KIK_' || the_sim_card_id_text; 
			DELETE FROM dlkey WHERE reference = 'KEY_' || the_sim_card_id_text; 
		END LOOP; 
	END IF;  
 
------------------------------------------------- 
-- DELETE SIM_CARD -- 
------------------------------------------------- 
 
-- Just one value to delete : ok ! 
	DELETE FROM sim_card WHERE ID = the_sim_card_id; 
 
 
------------------------------------------------- 
-- DELETE M_SIM_CARD -- 
------------------------------------------------- 
			 
-- get its m_sim_id 
	BEGIN 
		SELECT NVL(id, -1) INTO the_m_sim_id 
			FROM m_sim_card WHERE sim_card_id = the_sim_card_id;	 
		 
		EXCEPTION 
			WHEN NO_DATA_FOUND THEN the_m_sim_id := -1; 
	END; 
 
-- Delete the single entry associated in m_sim_card 
	IF the_m_sim_id <> -1 THEN 
		DELETE FROM m_sim_card 
			WHERE id = the_m_sim_id 
			AND sim_card_id = the_sim_card_id; 
-- Launch an action on its virtual sisters sim_card(s) 
		Dual_pack.OnDeleteSIM(the_m_sim_id, the_ocp_id); 
	END IF; 
 
 
------------------------ 
-- Exception handling -- 
------------------------ 
 
EXCEPTION 
	WHEN err_no_imsi_found THEN 
	ROLLBACK; 
	RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 
	'almProvision_REFERENCED_IMSI_NOT_EXIST'); 
 
	WHEN OTHERS THEN 
	ROLLBACK; 
	RAISE; 
 
 
END; 
 
------------------------------------------------------------------------------- 
-- Cleanup (not called by ALM, for maintenance only) 
------------------------------------------------------------------------------- 
 
-- Delete keys not linked to any card 
 
PROCEDURE cleanup_Keys(maxcom IN NUMBER) IS 
 
CURSOR Cursor_dlkeys IS 
 	SELECT reference, value from dlkey; 
 
the_sim_id sim_card.id%TYPE; 
the_iccid sim_card.iccid%TYPE; 
the_ref dlkey.reference%TYPE; 
the_value dlkey.value%TYPE; 
 
i NUMBER; 
pos NUMBER; 
 
BEGIN 
	 
	i := 0; 
	pos := 0; 
	IF Cursor_dlkeys%ISOPEN THEN 
		CLOSE Cursor_dlkeys; 
	END IF; 
	OPEN Cursor_dlkeys; 
    LOOP 
    	FETCH Cursor_dlkeys INTO the_ref, the_value; 
    	EXIT WHEN Cursor_dlkeys%NOTFOUND; 
	    IF (SUBSTR(the_ref, 1, 4) = 'KIC_') 
	    	OR (SUBSTR(the_ref, 1, 4) = 'KID_')  
	    	OR (SUBSTR(the_ref, 1, 4) = 'KIK_') 
	    	OR (SUBSTR(the_ref, 1, 4) = 'KEY_') THEN	    		     
	    	pos := instr(the_ref, '_', 1, 2) + 1; 
	    ELSE 
	    	pos := instr(the_ref, '_', 1, 1) + 1; 
	    END IF; 
	     
	    IF (pos > 1) THEN 
	    	the_sim_id := to_number(substr(the_ref, pos)); 
	    	BEGIN 
	    		SELECT iccid INTO the_iccid FROM SIM_CARD WHERE id = the_sim_id; 
	    	EXCEPTION 
	    		WHEN NO_DATA_FOUND THEN 
	    			DELETE dlkey where reference = the_ref; 
	    			i := i + 1; 
	    			IF i >= maxcom THEN  
	    				COMMIT; 
	    				i := 0; 
	    			END IF; 
	    	END; 
	    END IF; 
	END LOOP; 
	CLOSE Cursor_dlkeys; 
	COMMIT; 
END; 
 
 
------------------------------------------------------------------------------- 
-- delete SIMs not linked to a subscriber 
 
PROCEDURE cleanup_SIM IS 
 
CURSOR Cursor_sim IS 
	select imsi from sim_card where id not in (select sim_card_id from subscriber); 
 	 
the_imsi sim_card.imsi%TYPE; 
 
BEGIN 
	 
	IF Cursor_sim%ISOPEN THEN 
		CLOSE Cursor_sim; 
	END IF; 
	 
	OPEN Cursor_sim; 
	LOOP 
    		FETCH Cursor_sim INTO the_imsi; 
    		EXIT WHEN Cursor_sim%NOTFOUND; 
    		delete_SIM(the_imsi); 
	END LOOP; 
	CLOSE Cursor_sim; 
END; 
 
 
------------------------------------------------------------------------------- 
-- update sim card 
 
PROCEDURE update_sim_card( 
			  p_msisdn IN SUBSCRIBER.MSISDN%TYPE, 
			  p_id IN SIM_CARD.ID%TYPE, 
			  p_card_profile_id IN SIM_CARD.CARD_PROFILE_ID%TYPE, 
			  p_ocp_id IN SIM_CARD.OCP_ID%TYPE, 
			  p_imsi IN SIM_CARD.IMSI%TYPE, 
			  p_iccid IN SIM_CARD.ICCID%TYPE, 
			  p_eeprom IN SIM_CARD.EEPROM%TYPE, 
			  p_cntr IN SIM_CARD.CNTR%TYPE, 
			  p_adn_mode IN SIM_CARD.ADN_MODE%TYPE, 
			  p_ics1 IN SIM_CARD.ICS1%TYPE, 
			  p_synchro_date IN VARCHAR2,  
			  p_import_date IN VARCHAR2, 
			  p_activity IN SIM_CARD.ACTIVITY%TYPE, 
			  p_state IN SIM_CARD.STATE%TYPE 
)IS		 
	v_sim_card_id SIM_CARD.ID%TYPE; 
	v_card_profile_id SIM_CARD.CARD_PROFILE_ID%TYPE; 
	v_ocp_id  SIM_CARD.OCP_ID%TYPE; 
  	v_synchro_date  SIM_CARD.SYNCHRO_DATE%TYPE;  
	v_import_date  SIM_CARD.IMPORT_DATE%TYPE; 
	 
	 
	CURSOR crGetMsisdn(p_msisdn IN SUBSCRIBER.msisdn%TYPE 
		)IS 
		SELECT 
			sub.SIM_CARD_ID 
		FROM 
			SUBSCRIBER sub 
		WHERE 
			sub.MSISDN = p_msisdn; 
			 
	CURSOR crGetSimId(p_id IN SIM_CARD.ID%TYPE 
		)IS 
		SELECT 
			scard.ID 
		FROM 
			SIM_CARD scard 
		WHERE 
			scard.ID = p_id; 
	 
	CURSOR crGetCardProfileId( p_card_profile_id IN SIM_CARD.card_profile_id%TYPE 
		)IS 
		SELECT 
			cprofile.ID 
		FROM 
			CARD_PROFILE cprofile 
		WHERE 
			cprofile.ID = p_card_profile_id; 
			 
	CURSOR crGetOcpId( p_ocp_id IN SIM_CARD.ocp_id%TYPE 
		)IS 
		SELECT 
			ocprofile.ID 
		FROM 
			ORIGIN_CARD_PROFILE ocprofile 
		WHERE 
			ocprofile.ID = p_ocp_id; 
	 
	err_msisdn EXCEPTION; 
	err_sim_card_id EXCEPTION; 
		   
BEGIN 
 	 DBMS_OUTPUT.PUT_LINE ('INSIDE UPDATE'); 
	  
	 IF crGetMsisdn%ISOPEN THEN 
	 	CLOSE crGetMsisdn; 
	 END IF; 
	  
	OPEN crGetMsisdn(p_msisdn); 
	FETCH crGetMsisdn 
	INTO v_sim_card_id; 
		 
	IF crGetMsisdn%NOTFOUND THEN --msisdn not in table subscriber 
		CLOSE crGetMsisdn; 
		RAISE err_msisdn; 
	 
	ELSE  --crGetMsisdn%FOUND  
			-- start updates 
			-- card_profile 
			 
			 DBMS_OUTPUT.PUT_LINE ('*****************'); 
			 IF crGetCardProfileId%ISOPEN THEN 
	 		 	CLOSE crGetCardProfileId; 
	 		 END IF; 
			  
			 OPEN crGetCardProfileId(p_card_profile_id); 
			 FETCH crGetCardProfileId 
			 INTO v_card_profile_id; 
			  
		 
			 IF crGetCardProfileId%FOUND THEN -- can update card_profile_id 
		 	 	DBMS_OUTPUT.PUT_LINE ('INSIDE cardProfile:'); 
			 	UPDATE SIM_CARD  
					   SET CARD_PROFILE_ID= v_card_profile_id 
					   WHERE SIM_CARD.ID = v_sim_card_id; 
			 END IF;	 
			  
			 -- ocp_id 
			  IF crGetOcpId%ISOPEN THEN 
	 		 	CLOSE crGetOcpId; 
	 		 END IF; 
			  
			 OPEN crGetOcpId(p_ocp_id); 
			 FETCH crGetOcpId 
			 INTO v_ocp_id; 
			  
			 IF crGetOcpId%FOUND THEN -- can update ocp_id 
			 	DBMS_OUTPUT.PUT_LINE ('INSIDE ocp_id'); 
			 	UPDATE SIM_CARD  
					   SET OCP_ID=  v_ocp_id 
					   WHERE SIM_CARD.ID = v_sim_card_id; 
			 END IF;	 
			  
			IF ((LENGTH(p_imsi)<=16) AND (p_imsi IS NOT NULL)) THEN 
			   UPDATE SIM_CARD  
					   SET IMSI= p_imsi 
					   WHERE SIM_CARD.ID = v_sim_card_id; 
			 END IF;	 
			  
			IF ((LENGTH(p_iccid)<=20) AND (p_iccid IS NOT NULL))THEN 
			   UPDATE SIM_CARD  
					   SET ICCID= p_iccid 
					   WHERE SIM_CARD.ID = v_sim_card_id; 
			 END IF;	 
			  
			 DBMS_OUTPUT.PUT_LINE ('11111111#############'); 
			 DBMS_OUTPUT.PUT_LINE (p_eeprom); 
			 DBMS_OUTPUT.PUT_LINE (v_sim_card_id); 
			  
			IF ((p_eeprom>=0) AND (p_eeprom IS NOT NULL)) THEN 
			 DBMS_OUTPUT.PUT_LINE ('#############'); 
			   UPDATE SIM_CARD  
					   SET EEPROM= p_eeprom 
					   WHERE SIM_CARD.ID = v_sim_card_id; 
			 END IF;	 
			  
			IF (LENGTH(p_cntr)<=10) THEN 
			   UPDATE SIM_CARD  
					   SET CNTR= p_cntr 
					   WHERE SIM_CARD.ID = v_sim_card_id; 
			 END IF; 
			  
			IF ((LENGTH(p_adn_mode)=1) AND (p_adn_mode IS NOT NULL)) THEN 
			   UPDATE SIM_CARD  
					   SET ADN_MODE= p_adn_mode 
					   WHERE SIM_CARD.ID = v_sim_card_id; 
			 END IF; 
			  
			IF (LENGTH(p_ics1)<=16) THEN 
			   UPDATE SIM_CARD  
					   SET ICS1= p_ics1 
					   WHERE SIM_CARD.ID = v_sim_card_id; 
			 END IF; 
		  
			IF ((LENGTH(p_activity)=1) AND (p_activity IS NOT NULL)) THEN 
			   UPDATE SIM_CARD  
					   SET ACTIVITY= p_activity 
					   WHERE SIM_CARD.ID = v_sim_card_id; 
			 END IF; 
			  
			IF ((LENGTH(p_state)=1) AND (p_state IS NOT NULL))THEN 
			   UPDATE SIM_CARD  
					   SET STATE= p_state 
					   WHERE SIM_CARD.ID = v_sim_card_id; 
			 END IF; 
			  
			 ---* test for value -1  
			IF ((LENGTH(p_synchro_date)!= 0) AND (p_synchro_date != NULL) AND (p_synchro_date != '-1')) THEN 
			   v_synchro_date := TO_DATE( p_synchro_date , 'MM/DD/YYYY HH:MI:SS') ; 
			   UPDATE SIM_CARD  
			   		  SET SYNCHRO_DATE=  v_synchro_date 
					  WHERE SIM_CARD.ID = v_sim_card_id; 
			 END IF;	 
			  
			 IF ((LENGTH(p_import_date)!=0) AND (p_import_date != NULL) AND (p_import_date != '-1')) THEN 
			 	v_import_date := TO_DATE( p_import_date , 'MM/DD/YYYY HH:MI:SS') ; 
			   UPDATE SIM_CARD  
			   		  SET IMPORT_DATE=  v_import_date 
					  WHERE SIM_CARD.ID = v_sim_card_id; 
			 END IF;	 
		 
	   COMMIT;	  
       CLOSE crGetOcpId; 
	   CLOSE crGetMsisdn; 
	   CLOSE crGetCardProfileId; 
	END IF; --msisdn 
 
 
EXCEPTION 
	WHEN err_msisdn THEN 
		RAISE_APPLICATION_ERROR(-20000, 
		'msisdn not found '||p_msisdn); 
	 
	WHEN err_sim_card_id THEN 
		 RAISE_APPLICATION_ERROR(-20000, 
		 'sim_card_id in subscriber is not equal to input id' ||p_id);	 
 
END; 
 
 
END provsimcard;
/

show errors;
