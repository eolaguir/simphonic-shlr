-------------------------------------------------------------------------------
-- File:	$RCSfile: CreateTempProc.sql,v $
-- Project:	ALM
-- Author:	Gabriel Tourrand
-- Last Modif:	$Author: ndt $
-------------------------------------------------------------------------------
-- Version:	$Revision: 1.1 $ ($Date: 2012-04-17 15:30:07 $)
-------------------------------------------------------------------------------
-- Description: Creates temporary procedures
-------------------------------------------------------------------------------

CREATE OR REPLACE PACKAGE sql_tools_pack AS
	TYPE CharArray32 IS TABLE OF VARCHAR2(32) INDEX BY BINARY_INTEGER ;
	PROCEDURE drop_object (p_object_name IN VARCHAR2);
	PROCEDURE drop_constraint(p_constraint_name IN VARCHAR2);
	PROCEDURE drop_column (p_table_name IN VARCHAR2, p_column_name IN VARCHAR2);
	PROCEDURE compile_objects;
	PROCEDURE add_column (p_tab_name IN VARCHAR2, p_col_name IN VARCHAR2, p_col_type IN VARCHAR2, p_col_opt_params IN VARCHAR2);
	PROCEDURE upd_table (p_tab_name IN VARCHAR2, p_col_name IN CharArray32, p_col_type IN CharArray32, p_col_opt_params IN CharArray32);
	PROCEDURE upd_col (p_tab_name IN VARCHAR2, p_col_name IN VARCHAR2, p_col_type IN VARCHAR2, p_col_opt_params IN VARCHAR2);
	PROCEDURE drop_schema;
END sql_tools_pack;
/


CREATE OR REPLACE PACKAGE BODY sql_tools_pack AS

PROCEDURE drop_schema IS
	CURSOR c_objs IS
		SELECT	object_name
		FROM	user_objects
		WHERE	object_name != upper('sql_tools_pack');
	CURSOR c_const IS
		SELECT	constraint_name
		FROM	user_constraints
		ORDER BY constraint_type DESC;
	p_object	VARCHAR2(500);
BEGIN
	OPEN c_const;
	LOOP
		FETCH	c_const
		INTO	p_object;
		EXIT WHEN c_const%NOTFOUND;
		dbms_output.put_line('dropping constraint ' || p_object);
		drop_constraint(p_object);
	END LOOP;
	CLOSE c_const;

	OPEN c_objs;
	LOOP
		FETCH	c_objs
		INTO	p_object;
		EXIT WHEN c_objs%NOTFOUND;
		dbms_output.put_line('dropping object ' || p_object);
		drop_object(p_object);
	END LOOP;
END;

-- this procedure drops an object by its name.
PROCEDURE drop_object (p_object_name IN VARCHAR2) IS
	p_object_type	VARCHAR2(18);
	sql_querry	VARCHAR2(500);
	end_exception	EXCEPTION;
BEGIN
	BEGIN
		-- getting the object type to build the appropriate drop syntax
		SELECT	DISTINCT (DECODE (object_type, 'PACKAGE BODY', 'PACKAGE', object_type))
		INTO	p_object_type
		FROM	user_objects
		WHERE	object_name = UPPER(p_object_name);
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
		-- the object does not exist. Generating an info log.
		dbms_output.put_line ('object ' || p_object_name || ' does not exist');
		RAISE end_exception;
	WHEN OTHERS THEN
		RAISE;
	END;
	-- preparing the drop statement and executing it
	sql_querry := 'drop ' ||  p_object_type || ' ' || p_object_name ;
	if p_object_type = 'TABLE' THEN
		sql_querry := sql_querry || ' cascade constraints';
	END IF;
	EXECUTE IMMEDIATE sql_querry;
	dbms_output.put_line (p_object_type || ' ' || p_object_name || ' dropped');
EXCEPTION
	WHEN end_exception THEN
		NULL;
	WHEN OTHERS THEN
		RAISE;
END;

PROCEDURE drop_constraint(p_constraint_name IN VARCHAR2) IS
	p_table_name	VARCHAR2(30);
	sql_querry	VARCHAR2(500);
	end_exception	EXCEPTION;
BEGIN
	BEGIN
		SELECT	table_name
		INTO	p_table_name
		FROM	user_constraints
		WHERE	constraint_name = UPPER(p_constraint_name);
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
		-- the object does not exist. Generating an info log.
		dbms_output.put_line ('constraint ' || p_constraint_name || ' does not exist');
		RAISE end_exception;
	WHEN OTHERS THEN
		RAISE;
	END;
	sql_querry := 'alter table ' || p_table_name || ' drop constraint ' || p_constraint_name;
	EXECUTE IMMEDIATE sql_querry;
	dbms_output.put_line ('constraint' || p_constraint_name || ' on table ' || p_table_name || ' dropped');
EXCEPTION
	WHEN end_exception THEN
		NULL;
	WHEN OTHERS THEN
		RAISE;
END;

-- this procedure drops a column from a table (integrity checks or cascade should be added in a future version)
PROCEDURE drop_column (p_table_name IN VARCHAR2, p_column_name IN VARCHAR2) IS
	dummy		INTEGER;
	sql_querry	VARCHAR2(500);
	end_exception	EXCEPTION;
BEGIN
	BEGIN
		-- checking if the column to remove exists
		SELECT	1
		INTO	dummy
		FROM	user_tab_columns
		WHERE	table_name = UPPER(p_table_name)
		AND	column_name = UPPER(p_column_name);
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			-- the column does not exists. Generating an info log
			dbms_output.put_line ('table ' || p_table_name || ' has no column ' || p_column_name);
			RAISE end_exception;
		WHEN OTHERS THEN
			RAISE;
	END;
	-- preparing the drop column statement and executing it
	sql_querry := 'ALTER TABLE ' || p_table_name || ' DROP COLUMN ' || p_column_name;
	EXECUTE IMMEDIATE sql_querry;
	dbms_output.put_line ('column ' || p_column_name || ' of table ' || p_table_name || ' dropped');
EXCEPTION
	WHEN end_exception THEN
		NULL;
	WHEN OTHERS THEN
		RAISE;
END;

-- this procedure is able to recompile invalid package.
-- it is probably worth adding calling at the end of any migration script
PROCEDURE compile_objects IS
	p_object_name	VARCHAR2(128);
	p_object_type	VARCHAR2(18);
	sql_querry	VARCHAR2(500);
	cursor c_invalid_packs IS
		SELECT	object_name, object_type
		FROM	user_objects
		WHERE	object_type IN ('PACKAGE', 'PACKAGE BODY', 'TRIGGER')
		AND	status = 'INVALID'
		ORDER BY object_type;
	nb_pack		INTEGER;
	last_nb_pack	INTEGER;
BEGIN
	dbms_output.enable(10000);
	nb_pack := 1;
	last_nb_pack := 0;

	LOOP
		OPEN c_invalid_packs;
		LOOP
			FETCH	c_invalid_packs
			INTO	p_object_name,  p_object_type;
			EXIT WHEN c_invalid_packs%notfound;


			sql_querry := 'ALTER ' || NVL(SUBSTR(p_object_type, 0, INSTR(p_object_type, ' ')), p_object_type) || ' ' || p_object_name || ' compile';

			if p_object_type = 'PACKAGE BODY' THEN
				sql_querry := sql_querry || ' body';
			END IF;

			dbms_output.put_line(sql_querry);
			EXECUTE IMMEDIATE sql_querry;
		END LOOP;
		nb_pack := c_invalid_packs%rowcount;
		CLOSE c_invalid_packs;
		EXIT WHEN nb_pack = 0;
		EXIT WHEN nb_pack = last_nb_pack;

		last_nb_pack := nb_pack;
	END LOOP;
END;

PROCEDURE add_column (p_tab_name IN VARCHAR2, p_col_name IN VARCHAR2, p_col_type IN VARCHAR2, p_col_opt_params IN VARCHAR2) IS
	p_col_exists	INTEGER;
	sql_querry	VARCHAR2(500);
	already_not_null	EXCEPTION;
	PRAGMA EXCEPTION_INIT(already_not_null, -1442);
BEGIN
	SELECT	COUNT(1)
	INTO	p_col_exists
	FROM	user_tab_columns
	WHERE	table_name = upper(p_tab_name)
	AND	column_name = upper(p_col_name);
	dbms_output.put_line('p_col_exists ' || p_col_exists);
	dbms_output.put_line('table is ' || p_tab_name);
	IF p_col_exists = 1 THEN
		dbms_output.put_line('modifying');
		sql_querry := 'alter table ' || p_tab_name || ' modify (' || p_col_name || ' ' || p_col_type || ' ' || p_col_opt_params || ')';
	ELSE
		dbms_output.put_line('adding ' || p_tab_name || ' ' || p_col_name || ' ' || p_col_type || ' ' || p_col_opt_params);
		sql_querry := 'alter table ' || p_tab_name || ' add (' || p_col_name || ' ' || p_col_type || ' ' || p_col_opt_params || ')';
		dbms_output.put_line(sql_querry);
	END IF;
	EXECUTE IMMEDIATE sql_querry;
EXCEPTION
	WHEN already_not_null THEN
		dbms_output.put_line('already not null');
		NULL;
	WHEN OTHERS THEN
		RAISE;
END;

PROCEDURE upd_table (p_tab_name IN VARCHAR2, p_col_name IN CharArray32, p_col_type IN CharArray32, p_col_opt_params IN CharArray32) IS
	p_tab_exists	INTEGER;
	sql_querry	VARCHAR2(500);
	i		INTEGER := 1;
	ok		INTEGER := 1;
BEGIN
	SELECT	COUNT(1)
	INTO	p_tab_exists
	FROM	user_tables
	WHERE	table_name = upper(p_tab_name);
	WHILE ok = 1 LOOP
		BEGIN
			dbms_output.put_line('adding col ' || p_col_name(i));
			add_column(p_tab_name, p_col_name(i), p_col_type(i), p_col_opt_params(i));
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				ok := 0;
			WHEN OTHERS THEN
				dbms_output.put_line(sqlcode);
				RAISE;
		END;
		i := i + 1;
	END LOOP;
EXCEPTION
	WHEN OTHERS THEN
		dbms_output.put_line(sqlcode);
		RAISE;
END;

PROCEDURE upd_col (p_tab_name IN VARCHAR2, p_col_name IN VARCHAR2, p_col_type IN VARCHAR2, p_col_opt_params IN VARCHAR2) IS
	p_col_exists	INTEGER;
	sql_querry	VARCHAR2(500);
BEGIN
	SELECT	COUNT(1)
	INTO	p_col_exists
	FROM	user_tab_columns
	WHERE	table_name = upper(p_tab_name)
	AND	column_name = upper(p_col_name);
	add_column(p_tab_name, p_col_name, p_col_type, p_col_opt_params);
EXCEPTION
	WHEN OTHERS THEN
		dbms_output.put_line(sqlcode);
		RAISE;
END;

END sql_tools_pack;
/


CREATE OR REPLACE  PROCEDURE drop_object(p_object_name IN VARCHAR2) IS
BEGIN
	sql_tools_pack.drop_object(p_object_name);
END;
/
