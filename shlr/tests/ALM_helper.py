#!/usr/bin/env
# *****************************************************************************
#  file: $Id: ALM_helper.py,v 1.1 2011-04-08 17:04:46 ndt Exp $
#  author:	Jean-Marc Desprez
# -----------------------------------------------------------------------------
#  v1.0, 2010/01/04:    creation
# -----------------------------------------------------------------------------
#
# ****************************************************************************/

import ALM_server
import socket
import select
import soap
from ZSI.ServiceContainer import ServiceSOAPBinding, AsServer

from zsiTools import logInfo
import sdApi


# helper
class ALMServer:
	def __init__(self, address=None, port=None):
		self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
		if address == None:
			soap.soapLoadConfig('./services/almcfg.cfg')
			cfg = soap.soapGetConfig()
			address = cfg.almAddress
			port = cfg.almPort
		self.s.connect((address, port))
		logInfo('connected on %s %s' % (address, port))

	def send(self, text, to=1000):
		self.s.send(text)
		msg = ''
		while True:
			i, o, r = select.select([self.s], [], [], to)
			if len(i) == 0: return msg
			lg = 1000
			m = self.s.recv(lg)
			if len(m) == 0: return msg
			msg += m
			if len(m) != lg: return msg
			to = .2
		
		
class ALMHelper(ALM_server.ALM):
	def __init__(self, server=None, **kw):
		if server == None: 
			self.server = ALMServer()
		else:
			self.server = server
		ALM_server.ALM.__init__(self, '/soap/almzsi.cgi', **kw)

	def soap_EnhancedSendAPDU(self, ps, **kw):
		request, response = ALM_server.ALM.soap_EnhancedSendAPDU(self, ps, **kw)

		to_server = 'sessionID %s\r\n' % request._sessionID
		to_server += 'userName %s\r\n' % request._userName
		to_server += 'tsmReferenceId %s\r\n' % request._tsmReferenceId
		to_server += 'bearer %s\r\n' % request._bearer
		to_server += 'targetType %s\r\n' % request._targetType
		to_server += 'target %s\r\n' % request._target
		to_server += 'media %s\r\n' % request._media
		to_server += 'pid %s\r\n' % request._pid
		to_server += 'validityPeriod %s\r\n' % request._validityPeriod
		to_server += 'tar %s\r\n' % request._tar
		to_server += 'apdu %s\r\n' % request._apdu
		to_server += 'secu %s\r\n' % request._secu
		to_server += 'keyset %s\r\n' % request._keyset
		to_server += '\r\n'
		from_server = ALMServer().send(to_server)
		fields = {
			'sessionID':request._sessionID,
			'userName':request._userName,
			'tsmReferenceId':request._tsmReferenceId,
			'bearer':request._bearer,
			'targetType':request._targetType,
			'target':request._target,
			'media':request._media,
			'pid':request._pid,
			'validityPeriod':request._validityPeriod,
			'tar':request._tar,
			'apdu':request._apdu,
			'secu':request._secu,
			'keyset':request._keyset
			}
		valResponse = sdApi.validateEnhancedSendAPDU(fields)
		logInfo('%s' % (valResponse))
		
		response._code = valResponse['code']
		response._message = valResponse['message']
		response._msisdn = valResponse['msisdn']
		response._tsmReferenceId = valResponse['tsmReferenceId']
		response._requestID = valResponse['requestID']

		return request, response
