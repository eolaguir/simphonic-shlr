#! /usr/bin/env python
#******************************************************************************
# file: $Id: shlr_req_interface.py,v 1.19 2011-12-13 18:46:07 gabritou Exp $
# author:  Saad Amrani
#*****************************************************************************/
import sys 
import warnings
warnings.filterwarnings("ignore")
from ZSI import * 
from SMM_server import  * 
from shlr_req_process import *
from shlr_tools import FormatInputMsisdn, FormatOutputMsisdn
from SMM_types import ns0

def InteractUser(ps, LOG):
    l_InteractUserResponse = ns0.InteractUserResponse_Def(ZSI.TCcompound.ComplexType , "InteractUserResponse")
    try:
        req = ps.Parse(InteractUserRequest)
        if req._cmdType == "SELECT_ITEM":
            smmId, transactionId, msisdn, code, message  = processSelectItem(req._orchId, FormatInputMsisdn(req._msisdnFake), req._transactionId, req._config, req._cmdParams._sItem._title, req._cmdParams._sItem._itemArray._value, LOG)
        elif req._cmdType == "GET_INPUT":
            smmId, transactionId, msisdn, code, message  = processGetInput(req._orchId, FormatInputMsisdn(req._msisdnFake), req._transactionId, req._config, req._cmdParams._gInput._title, req._cmdParams._gInput._type, req._cmdParams._gInput._minLength, req._cmdParams._gInput._maxLength, LOG)

        elif req._cmdType == "DISPLAY_TEXT":    
            smmId, transactionId, msisdn, code, message = processDisplayText(req._orchId, FormatInputMsisdn(req._msisdnFake), req._transactionId, req._config, req._cmdParams._dText._title, LOG)
        elif req._cmdType == "PROVIDE_LOCAL_INFO":
            smmId, transactionId, msisdn, code, message = processProvideLocalInfo(req._orchId, FormatInputMsisdn(req._msisdnFake), req._transactionId, req._config, LOG)
        l_InteractUserResponse._smmId = smmId
        l_InteractUserResponse._msisdn = FormatOutputMsisdn(msisdn)
        l_InteractUserResponse._transactionId = transactionId
        l_InteractUserResponse._code = code
        l_InteractUserResponse._message = message
    except Exception, e:
        LOG.error("InteractUser failed: [%s]"%str(e))
        l_InteractUserResponse._smmId = getSmmId()
        l_InteractUserResponse._transactionId = -1
        l_InteractUserResponse._msisdn = ""
        l_InteractUserResponse._code = shlr_ota_wrong_param
        l_InteractUserResponse._message = shlr_statusToString(shlr_ota_wrong_param)

    resp =  InteractUserResponse()
    resp._InteractUserResponse = l_InteractUserResponse
    return resp     
    
def ActivateSIMchronize(ps, LOG):
    l_ActivateSIMchronizeResponse = ns0.ActivateSIMchronizeResponse_Def(ZSI.TCcompound.ComplexType , "ActivateSIMchronizeResponse")
    try:
        #Parsing 
        req = ps.Parse(ActivateSIMchronizeRequest)
        smmId, transactionId, msisdn, code, message = processActivateSIMchronize(req._orchId, FormatInputMsisdn(req._msisdnFake), req._transactionId, FormatInputMsisdn(req._smspReal), LOG)

        l_ActivateSIMchronizeResponse._smmId = smmId
        l_ActivateSIMchronizeResponse._msisdn = FormatOutputMsisdn(msisdn)
        l_ActivateSIMchronizeResponse._transactionId = transactionId
        l_ActivateSIMchronizeResponse._code = code
        l_ActivateSIMchronizeResponse._message = message
    except Exception, e:
        LOG.info("ActivateSIMchronize failed: [%s]"%str(e))
        l_ActivateSIMchronizeResponse._smmId = getSmmId()
        l_ActivateSIMchronizeResponse._transactionId = -1
        l_ActivateSIMchronizeResponse._msisdn = ""
        l_ActivateSIMchronizeResponse._code = shlr_ota_wrong_param
        l_ActivateSIMchronizeResponse._message = shlr_statusToString(shlr_ota_wrong_param)
    resp =  ActivateSIMchronizeResponse()
    resp._ActivateSIMchronizeResponse = l_ActivateSIMchronizeResponse
    return resp     

def ActivateIMEITracking(ps, LOG):
    l_ActivateIMEITrackingResponse = ns0.ActivateIMEITrackingResponse_Def(ZSI.TCcompound.ComplexType , "ActivateIMEITrackingResponse")
    try:
        #Parsing 
        req = ps.Parse(ActivateIMEITrackingRequest)
        smmId, transactionId, msisdn, code, message = processActivateImeiTracking(req._orchId, FormatInputMsisdn(req._msisdnFake), req._transactionId, FormatInputMsisdn(req._smspReal), LOG)
        l_ActivateIMEITrackingResponse._smmId = smmId
        l_ActivateIMEITrackingResponse._msisdn = FormatOutputMsisdn(msisdn)
        l_ActivateIMEITrackingResponse._transactionId = transactionId
        l_ActivateIMEITrackingResponse._code = code
        l_ActivateIMEITrackingResponse._message = message
    except Exception, e:
        LOG.info("ActivateIMEITracking failed: [%s]"%str(e))
        l_ActivateIMEITrackingResponse._smmId = getSmmId()
        l_ActivateIMEITrackingResponse._transactionId = -1
        l_ActivateIMEITrackingResponse._msisdn = ""
        l_ActivateIMEITrackingResponse._code = shlr_ota_wrong_param
        l_ActivateIMEITrackingResponse._message = shlr_statusToString(shlr_ota_wrong_param)
    resp =  ActivateIMEITrackingResponse()
    resp._ActivateIMEITrackingResponse = l_ActivateIMEITrackingResponse
    return resp 

def ActivateHRS(ps, LOG):
    l_ActivateHRSResponse = ns0.ActivateHRSResponse_Def(ZSI.TCcompound.ComplexType , "ActivateHRSResponse")
    try:
        req = ps.Parse(ActivateHRSRequest)
        smmId, transactionId, msisdn, code, message = processActivateHRS(req._orchId, FormatInputMsisdn(req._msisdnFake), req._transactionId,  req._config , req._imsiReal, req._imsiRoaming, FormatInputMsisdn(req._smspReal),  FormatInputMsisdn(req._msisdnReal), LOG)
        l_ActivateHRSResponse._smmId = smmId
        l_ActivateHRSResponse._msisdn = FormatOutputMsisdn(msisdn) 
        l_ActivateHRSResponse._transactionId = transactionId
        l_ActivateHRSResponse._code = code
        l_ActivateHRSResponse._message = message
    except Exception, e:
        LOG.info("ActivateHRS failed: [%s]"%str(e))
        l_ActivateHRSResponse._smmId = getSmmId()
        l_ActivateHRSResponse._transactionId = -1
        l_ActivateHRSResponse._msisdn = ""
        l_ActivateHRSResponse._code = shlr_ota_wrong_param
        l_ActivateHRSResponse._message = shlr_statusToString(shlr_ota_wrong_param)
    resp =  ActivateHRSResponse()
    resp._ActivateHRSResponse = l_ActivateHRSResponse
    return resp 

def ActivateHRS_LTE(ps, LOG):
    l_ActivateHRS_LTEResponse = ns0.ActivateHRS_LTEResponse_Def(ZSI.TCcompound.ComplexType , "ActivateHRS_LTEResponse")

    try:
        req = ps.Parse(ActivateHRS_LTERequest)
        smmId, transactionId, msisdn, code, message = \
            processActivateHRS_LTE(
                req._orchId,
                FormatInputMsisdn(req._msisdnFake),
                req._labelCommand,
                req._transactionId,
                req._config,
                req._imsiReal,
                FormatInputMsisdn(req._smspReal),
                FormatInputMsisdn(req._msisdnReal),
                LOG)

        l_ActivateHRS_LTEResponse._smmId = smmId
        l_ActivateHRS_LTEResponse._msisdn = FormatOutputMsisdn(msisdn) 
        l_ActivateHRS_LTEResponse._transactionId = transactionId
        l_ActivateHRS_LTEResponse._code = code
        l_ActivateHRS_LTEResponse._message = message

    except Exception, e:
        LOG.info("ActivateHRSLTE failed: [%s]" % str(e))
        l_ActivateHRS_LTEResponse._smmId = getSmmId()
        l_ActivateHRS_LTEResponse._transactionId = -1
        l_ActivateHRS_LTEResponse._msisdn = ""
        l_ActivateHRS_LTEResponse._code = shlr_ota_wrong_param
        l_ActivateHRS_LTEResponse._message = shlr_statusToString(shlr_ota_wrong_param)

    resp = ActivateHRS_LTEResponse()
    resp._ActivateHRS_LTEResponse = l_ActivateHRS_LTEResponse

    return resp
