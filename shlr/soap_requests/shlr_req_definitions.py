#!/bin/env python
#******************************************************************************
# file: $Id: shlr_req_definitions.py,v 1.57 2011-12-19 15:36:06 gabritou Exp $
# author:  Nathalie Desbonnet
#*****************************************************************************/
import os
import alm
import os.path
import time
from httplib import HTTPConnection
from ascii_lib import convertSOAPStringToHexaCoded8BitData
from shlr_tools import  Cfg
from ApduGenerator import ApduGenerator
from pyClient import *
from dao_idl import _0_ci

import threading

#import secu
import datetime

import minimal_import

cfgPath=os.environ.get("SHLR_CFGFILE")
CFG_SHLR=Cfg(cfgPath,0)

# ota
shlr_ota_ok     =   2000
shlr_ota_wrong_param    =   2001
shlr_unknow_msisdn  =   2003
shlr_susb_engaged   =   2004    
shlr_ota_error      =   2020

shlr_dict_status    = {
    shlr_ota_ok     : "OTA: SENT",
    shlr_ota_wrong_param    : "OTA: WRONG FORMAT OF PARAMETER",
    shlr_unknow_msisdn  : "OTA: UNKNOWN MSISDN",
    shlr_susb_engaged   : "OTA: SUBSCRIBER ALREADY ENGAGED",
    shlr_ota_error      : "OTA: INTERNAL ERROR"
}


alm_otaShlr_dic = {
    alm.almOK           :   shlr_ota_ok,
    alm.almErr_CARD_ENGAGED     :   shlr_susb_engaged,
    alm.admErr_UNKNOWN_MSISDN   :   shlr_unknow_msisdn
}

def almToOTAShlrStatus(st):
    return alm_otaShlr_dic.get(st, shlr_ota_error)


def shlr_statusToString(st):
    return shlr_dict_status[st]

TYPE_OTA=1

class CommonRequest:
    def __init__(self, tid=-1, msisdn="", code=-1, message=""):
        self.type = -1
        self.almLogin = CFG_SHLR.getvalue("ALMSRV_LOGIN")
        self.mtmoIp = CFG_SHLR.getvalue("MTMOSRV_IP")
        self.mtmoPort = CFG_SHLR.getvalue("MTMOSRV_PORT")
        self.mtmoUrl = CFG_SHLR.getvalue("MTMOSRV_URL")
        self.mtInterface = CFG_SHLR.getvalue("MT_INTERFACE")
        self.smmId = int(CFG_SHLR.getvalue("SMM_ID"))
        #delete the TSM_ref  from the config file 
        self.tid = tid
        self.msisdn = msisdn
        self.code = code
        self.message = message
    def __str__(self):
        s = "smmId <%s> tid <%s> msisdn <%s> code <%s> message <%s>" % (str(self.smmId), str(self.tid), self.msisdn, str(self.code), self.message)
        return s

    def formatResponse(self):
        return (self.smmId, self.tid, self.msisdn, self.code, self.message)



class OtaRequest(CommonRequest):
    def __init__(self, request="", orchId=-1, msisdn="", tid=-1):
        CommonRequest.__init__(self, tid, msisdn, 2000, "OK")
        self.type = TYPE_OTA
        self.request = request
        self.sessionId = ""
        self.kicHex='25'
        self.kidHex='25'
        self.keyType='KEY_SET'
        self.keySetVersion=2
        self.pid='7F'
        self.counterId=2
        self.vp = 60 # in seconds 
        if request == "ACTIVATE_IMEI_TRACKING":
            self.tar = "000000"
            self.spi = "1221"
            self.rpack = 0
            self.waitmo = 1
        elif request == "ACTIVATE_SIMCHRONIZE":
            self.tar = "524144"
            self.spi = "1221"
            self.rpack = 0
            self.waitmo = 1
        elif request == "ACTIVATE_HRS":
            self.tar = "53484C"
            self.spi = "1222"
            self.rpack = 1
            self.waitmo = 1 
        elif request == "ACTIVATE_HRS_LTE":
            self.tar = "53484C"
            self.spi = "1222"
            self.rpack = 1
            self.waitmo = 1 
        else:
            self.tar = "53484C"
            self.spi = "1200"
            self.rpack = 1
            self.waitmo = 0
        self.orchId = orchId
    def __str__(self):
        s = "[%s_REQUEST] orchId <%d> msisdn <%s> tid <%d>" % (self.request, self.orchId, self.msisdn, int(self.tid))
        return s
    def displayResponse(self):
        s = "[%s_RESULT] orchId <%d> msisdn <%s> tid <%d> code <%d> message <%s>" % (self.request, self.orchId, self.msisdn, int(self.tid), int(self.code), self.message)
        return s
    def check(self):
        if (self.tid == -1):
            self.code=2002
            self.message = "Mandatory transaction id"
        if (self.msisdn == ""):
            self.code=2002
            self.message = "Mandatory MSISDN"
    def getApdu(self):
        s = "C104%08d" %(self.tid)
        return s
            
    def executeOtaCommand(self, LOG):
        self.check()
        if (self.code == 2000):
            apdu = self.getApdu();
            if (apdu==None):
                self.code = 2020
                self.message = "Can't format APDU"
            elif (self.mtInterface=="MTMOSrv"):
                kic="25"
                kid="25"
                kickey  = "F8195839102919DEAB908947198438FA"
                kidkey  = "F8195839102919DEAB908947198438FA"
                tnow = int((time.time()-1301577593)*10)
                counter = "%010X" %tnow
                self.message = "APDU=%s" %apdu
                try:
                    cnx = HTTPConnection(self.mtmoIp,self.mtmoPort)
                    get = "%s?user=vivo&msisdn=%s&client_reference_id=%s&type=G348&tar=%s&spi=%s" \
                    %(self.mtmoUrl,self.msisdn,self.tid,self.tar, self.spi)
                    get += "&counter=%s&kic=%s&kid=%s&kickey=%s&kidkey=%s" %(counter,kic,kid,kickey, kidkey)
                    if (self.rpack==1):
                        get+="&rpack=1"
                    if (self.waitmo==1):
                        get+="&waitmo=1"
                    get += "&sms=%s" %(apdu)
                    cnx.request("GET",get)
                    answer = cnx.getresponse().read()
                    LOG.info("ANSWER %s" %answer)
                except:
                    LOG.error("mtmoSrv not reachable")
            else:
                try:
                    ################################################################
                    #TODO: most of this block should be hidden in a module
                    ###############################################################
                    sdapi = minimal_import.sdApiClientConnect(LOG)

                    # Check that MSISDN exists
                    t = datetime.datetime.now()
                    subscriber = subsFactory.getSubscriber(self.msisdn)
                    diff = datetime.datetime.now() - t
                    isMsisdnExisting = bool(subscriber.isMsisdnExisting())
                    LOG.info("subsFactory.getSubscriber took %d:%06d (subscriber exists: %s)" % (diff.seconds, diff.microseconds, isMsisdnExisting))
                    if isMsisdnExisting == False:
                        LOG.info("msisdn: [%s]"%(self.msisdn))
                        st = alm.admErr_UNKNOWN_MSISDN
                    else:
                        # Authorize sending the APDU
                        t = datetime.datetime.now()

                        # get OTA keys for this subcriber
                        keySetData = subscriber.getKeysetValue(self.keySetVersion)

                        # get Send Apdu parameters to get appi id and secu desc
                        sendApduParams = subscriber.getSendApduSecurityParameters()

                        # Granularity is 1/10 s
			counter_value = "%10d" % ((int(time.time()) - 1301577593) * 10)

                        # build g348 parameters suitable for EnhancedSendAPDU
                        g0348Params = _0_ci.SdGsm0348Params( '', #cmd_set
                                             self.spi,
                                             _0_ci.SdGsm0348Counter(keySetData.counterId, counter_value),
                                             int(self.kicHex, 16),
                                             int(self.kidHex, 16),       
                                             len(keySetData.kicValue.strip()),
                                             keySetData.kicValue.strip(),
                                             len(keySetData.kidValue.strip()),
                                             keySetData.kidValue.strip(),
                                             '', # kicdiv
                                             '', # kiddiv
                                             self.tar,
                                             _0_ci.algo_3des_2key,
                                             _0_ci.algo_3des_2key,
                                             sendApduParams.smsAppTypeId,
                                             sendApduParams.smsSecurity.securityDesc)

                        # build operation parameters suitable for EnhancedSendAPDU
                        op = _0_ci.SdOperationParams( _0_ci.SecuFlag_Default,
                                        _0_ci.SdSecuParams(False, "00", # pid unset. retrieved by the ASP at runtime
                                                   True, 5  # taken from _config.getMessageVP()
                                                  ),
                                        _0_ci.Media_SMS,
                                        _0_ci.SdUnitarySched("0000/00/00", "00:00:00", _0_ci.AppliPrio_HIGH, 3),
                                        self.vp)

                        status = sdapi.EnhancedSendAPDU(self.msisdn, self.almLogin, apdu, int(self.tid), g0348Params, op)
                        st = 0
                        if status < 0:
                            st = abs(status)

                        msg = alm.almStatusToString(st)

                        diff = datetime.datetime.now()  - t
                        LOG.info("validatedEnhancedSendAPDU took %d:%06d. st=<%d> msg=<%s>" % (diff.seconds, diff.microseconds, st, msg))

                    self.code = almToOTAShlrStatus(st)
                    self.message = shlr_statusToString(self.code)
                    LOG.info("%s" %(self.displayResponse()))
                    return self.formatResponse()
                except Exception, e:
#                   import traceback
#                   trace = open('/tmp/stack', 'w+')
#                   traceback.print_stack(file = trace)
#                   trace.close()
                    LOG.error("OtaRequest exception: [%s]"%str(e))
                    self.code = shlr_ota_error
                    self.message = shlr_statusToString(self.code)
                    LOG.info("%s" %(self.displayResponse()))
                    return self.formatResponse()
        LOG.info("%s" %(self.displayResponse()))
        return self.formatResponse()

class SelectItemRequest(OtaRequest):
    def __init__(self, orchId, msisdnFake, tid, config, title, listItems):
        OtaRequest.__init__(self, "SELECT_ITEM", orchId, msisdnFake, tid)
        self.config = config
        self.title = title
        self.listItems = listItems
    def __str__(self):
        s = "%s config <%s> title <%s> items <%s>" %(OtaRequest.__str__(self), self.config, self.title, self.listItems)
        return s
    def check(self):
        OtaRequest.check(self)
        if (self.code!=2000):
            return
        if (self.title == ""):
            self.code=2002
            self.message = "Mandatory title"
        if (self.listItems == []):
            self.code=2002
            self.message = "Mandatory items"
    def getApdu(self):
        tmp=""
        tmp +="8103012400"
        tmp +="82028182"
        tmp += "05%02X" %(len(self.title))
        tmp +=convertSOAPStringToHexaCoded8BitData(self.title)
        i=0;
        while (i<len(self.listItems)):
            tmp += "8F%02X%02X" %(len(self.listItems[i])+1,i+1)
            tmp +=convertSOAPStringToHexaCoded8BitData(self.listItems[i])
            i+=1
        if ( (len(tmp)/2) > 127):
            proActiveCommand = "D081%02X%s"%(len(tmp)/2,tmp)
        else:
            proActiveCommand = "D0%02X%s"%(len(tmp)/2,tmp)
        if ( (len(proActiveCommand)/2)>127):
            s = "%sC202%sC381%02X%s"%(OtaRequest.getApdu(self),self.config,len(proActiveCommand)/2,proActiveCommand)
        else:
            s = "%sC202%sC3%02X%s"%(OtaRequest.getApdu(self),self.config,len(proActiveCommand)/2,proActiveCommand)
        return s
    def execute(self, LOG):
        return OtaRequest.executeOtaCommand(self, LOG)

class GetInputRequest(OtaRequest):
    def __init__(self, orchId=-1, msisdnFake="", tid=-1, config="", title="", type=-1, min=-1, max=-1):
        OtaRequest.__init__(self, "GET_INPUT", orchId, msisdnFake, tid)
        self.config = config
        self.title = title
        self.type = type
        self.min= min
        self.max = max
    def __str__(self):
        s = "%s config <%s> title <%s> type <%s> size [%d,%d]" \
        %(OtaRequest.__str__(self), self.config, self.title, self.type, self.min, self.max)
        return s
    def check(self):
        OtaRequest.check(self)
        if (self.code!=2000):
            return
        if (self.title == ""):
            self.code=2002
            self.message = "Mandatory title"
        if (self.type == -1):
            self.code=2002
            self.message = "Mandatory type"
        if (self.type > 1):
            self.code=2001
            self.message = "Bad type"
    def getApdu(self):
        tmp=""
        tmp +="8103012300"
        tmp +="82028182"
        tmp += "0D%02XF4" %(len(self.title)+1)
        tmp +=convertSOAPStringToHexaCoded8BitData(self.title)
        tmp += "9102%02X%02X" %(self.min, self.max)
        if ( (len(tmp)/2) > 127):
            proActiveCommand = "D081%02X%s"%(len(tmp)/2,tmp)
        else:
            proActiveCommand = "D0%02X%s"%(len(tmp)/2,tmp)
        if ( (len(proActiveCommand)/2)>127):
            s = "%sC202%sC381%02X%s"%(OtaRequest.getApdu(self),self.config,len(proActiveCommand)/2,proActiveCommand)
        else:
            s = "%sC202%sC3%02X%s"%(OtaRequest.getApdu(self),self.config,len(proActiveCommand)/2,proActiveCommand)
        return s
    def execute(self, LOG):
        return OtaRequest.executeOtaCommand(self, LOG)

class DisplayText(OtaRequest):
    def __init__(self, orchId=-1, msisdnFake="", tid=-1, config="", title=""):
        OtaRequest.__init__(self, "DISPLAY_TEXT", orchId, msisdnFake, tid)
        self.config = config
        self.title = title
    def __str__(self):
        s = "%s config <%s> title <%s> " %(OtaRequest.__str__(self), self.config, self.title)
        return s
    def check(self):
        OtaRequest.check(self)
        if (self.code!=2000):
            return
        if (self.title == ""):
            self.code=2002
            self.message = "Mandatory title"
    def getApdu(self):
        tmp=""
        if (self.config[0]=="8"):
            # text with user confirmation - qualifier 80
            tmp +="0103012180"
        else:
            tmp +="0103012100"
        tmp +="02028102"
        if ( (len(self.title)+1) > 127):
            tmp += "0D81%02XF4" %(len(self.title)+1)
        else:
            tmp += "0D%02XF4" %(len(self.title)+1)
        tmp +=convertSOAPStringToHexaCoded8BitData(self.title)
        if ( (len(tmp)/2) > 127):
            proActiveCommand = "D081%02X%s"%(len(tmp)/2,tmp)
        else:
            proActiveCommand = "D0%02X%s"%(len(tmp)/2,tmp)
        if ( (len(proActiveCommand)/2)>127):
            s = "%sC202%sC381%02X%s"%(OtaRequest.getApdu(self),self.config,len(proActiveCommand)/2,proActiveCommand)
        else:
            s = "%sC202%sC3%02X%s"%(OtaRequest.getApdu(self),self.config,len(proActiveCommand)/2,proActiveCommand)
        return s
    def execute(self, LOG):
        return OtaRequest.executeOtaCommand(self, LOG)
        
class ProvideLocalInfo(OtaRequest):
    def __init__(self, orchId=-1, msisdnFake="", tid=-1, config="", title=""):
        OtaRequest.__init__(self, "PROVIDE_LOCAL_INFO", orchId, msisdnFake, tid)
        self.config = config
        # TODO: fix the log infrastructure
#       if config!="0000":
#           LOG.warning("Config isn't 0000 for PROVIDE_LOCAL_INFO but %s"%config)
 
    def __str__(self):
        s = "%s config <%s>" %(OtaRequest.__str__(self), self.config)
        return s
    def check(self):
        OtaRequest.check(self)
        if (self.code!=2000):
            return
    def getApdu(self):
        tmp="810301260182028182"
#       tmp="810301010402028281"    #for refresh command
        if ( (len(tmp)/2) > 127):
            proActiveCommand = "D081%02X%s"%(len(tmp)/2,tmp)
        else:
            proActiveCommand = "D0%02X%s"%(len(tmp)/2,tmp)
        if ( (len(proActiveCommand)/2)>127):
            s = "%sC202%sC381%02X%s"%(OtaRequest.getApdu(self),self.config,len(proActiveCommand)/2,proActiveCommand)
        else:
            s = "%sC202%sC3%02X%s"%(OtaRequest.getApdu(self),self.config,len(proActiveCommand)/2,proActiveCommand)
        return s
    def execute(self, LOG):
        return OtaRequest.executeOtaCommand(self, LOG)
        
class ActivateImeiTracking(OtaRequest):
    def __init__(self, orchId="", msisdnFake="", tid=-1, smspReal=""):
        OtaRequest.__init__(self, "ACTIVATE_IMEI_TRACKING", orchId, msisdnFake, tid)
        self.smspReal = smspReal
    def __str__(self):
        s = "%s smspReal <%s>" %(OtaRequest.__str__(self), self.smspReal)
        return s
    def check(self):
        OtaRequest.check(self)
        if (self.code!=2000):
            return
        if (self.smspReal == ""):
            self.code=2002
            self.message = "Mandatory smspReal"
    def getApdu(self):
        smsp = convertMSISDN(self.smspReal)
        tmp=""
        tmp +="80E60C005608A0000000184106010FA00000001841060100000001494D450FA00000001841060100000001494D450100"
        tmp +="29EF14C8020000C7020000CA0A01000101010100000700C91100"
        tmp += "%02X%s" %(len(smsp)/2,smsp)
        tmp += "05815300F002040300"
        return tmp
    def execute(self, LOG):
        return OtaRequest.executeOtaCommand(self, LOG)

class ActivateSIMchronize(OtaRequest):
    def __init__(self, orchId="", msisdnFake="", tid=-1, smspReal=""):
        OtaRequest.__init__(self, "ACTIVATE_SIMCHRONIZE", orchId, msisdnFake, tid)
        self.smspReal = smspReal
    def __str__(self):
        s = "%s smspReal <%s>" %(OtaRequest.__str__(self), self.smspReal)
        return s
    def check(self):
        OtaRequest.check(self)
        if (self.code!=2000):
            return
        if (self.smspReal == ""):
            self.code=2002
            self.message = "Mandatory smspReal"
    def getApdu(self):
        smsp = convertMSISDN(self.smspReal)
        apdu = "0100800A0010%02X%s" %(len(smsp)/2,smsp)
        return apdu
    def execute(self, LOG):
        return OtaRequest.executeOtaCommand(self, LOG)

class ActivateHRS(OtaRequest):
    def __init__(self, orchId="", msisdnFake="", tid=-1, config="", imsiReal="", imsiRoaming="", smspReal="", msisdnReal="" ):
        OtaRequest.__init__(self, "ACTIVATE_HRS", orchId, msisdnFake, tid)
        self.config = config
        self.imsiReal = imsiReal
        self.imsiRoaming = imsiRoaming
        self.smspReal = smspReal
        self.msisdnReal = msisdnReal
    def __str__(self):
        s = "%s config <%s> imsiReal <%s> imsiRoaming <%s> smspReal <%s> msisdnReal <%s>" \
        %(OtaRequest.__str__(self), self.config, self.imsiReal, self.imsiRoaming, self.smspReal, self.msisdnReal)
        return s
    def check(self):
        OtaRequest.check(self)
        if (self.code!=2000):
            return
        if (self.smspReal == ""):
            self.code=2002
            self.message = "Mandatory smspReal"
    def getApdu(self):
        smsp = convertMSISDN(self.smspReal)
        smsp0B = smsp
        i = len(smsp)
        while (i < 22):
            smsp0B += "F"
            i+=1
        msisdn = convertMSISDN(self.msisdnReal)
        msisdn0B = msisdn
        i = len(msisdn)
        while (i < 22):
            msisdn0B += "F"
            i+=1
        imsiReal = convertIMSI(self.imsiReal)
        mnc = self.imsiReal[4]+self.imsiReal[3]
        tmp=""
        tmp+="C60C%02X%s"%(len(smsp)/2,smsp0B)
        tmp+="C7077F106F42001C01C7077F108F42001C01"
        tmp+="C609%02X%s"%(len(imsiReal)/2, imsiReal)
        tmp+="C7067F206F070000C7067F208F070000"
        if (self.imsiRoaming!=""):
            imsiRoaming = convertIMSI(self.imsiRoaming)
            tmp+="C609%02X%s"%(len(imsiRoaming)/2, imsiRoaming)
            tmp+="C7067F209F070000"
        tmp+="C60BFFFFFFFF27F4%s0000FF01C7067F206F7E0000C7067F208F7E0000"%mnc
        tmp+="C60EFFFFFFFFFFFFFF27F4%s0000FF01C7067F206F530000C7067F208F530000"%mnc
        tmp+="C60C%02X%s"%(len(smsp)/2,smsp0B)
        tmp+="C7077F456F42000D01C7077F456F42000D02"
        tmp+="C60C%02X%s"%(len(msisdn)/2,msisdn0B)
        tmp+="c7077f106f40000F01"
        apdu = "%sC405%sC5%02X%s"%(OtaRequest.getApdu(self),self.config,len(tmp)/2,tmp)
        return apdu

    def execute(self, LOG):
        return OtaRequest.executeOtaCommand(self, LOG)

class ActivateHRS_LTE(OtaRequest):
    def __init__(self,
            orchId      = "",
            msisdnFake  = "",
            labelCommand    = "",
            tid     = -1,
            config      = "",
            imsiReal    = "",
            smspReal    = "",
            msisdnReal  = ""):
        OtaRequest.__init__(self, "ACTIVATE_HRS_LTE", orchId, msisdnFake, tid)
        self.labelCommand = labelCommand
        self.config = config
        self.imsiReal = imsiReal
        self.smspReal = smspReal
        self.msisdnReal = msisdnReal

    def __str__(self):
        s = "%s labelCommand <%s> config <%s> imsiReal <%s> smspReal <%s> msisdnReal <%s>" \
            % (OtaRequest.__str__(self),
                self.labelCommand,
                self.config,
                self.imsiReal,
                self.smspReal,
                self.msisdnReal)
        return s

    def check(self):
        OtaRequest.check(self)

        if (self.code != 2000):
            return

        if (self.smspReal == ""):
            self.code = 2002
            self.message = "Mandatory smspReal"

    def getApdu(self):
        apdu = None

        try:
            generator = ApduGenerator(self.labelCommand)
            apdu = generator.getApdu(self)
        except:
            apdu = None

        return apdu

    def getStaticApdu(self):
        """
        This is a deprecated version of getApdu that has been used
        to create a static, or hardcoded, APDU.
        
        It is preserved in order to allow for rolling back
        if any error occurs during testing of the automatic APDU generation.
        """

        smsp = convertMSISDN(self.smspReal)
        smsp0B = smsp
        i = len(smsp)

        while (i < 22):
            smsp0B += "F"
            i += 1

        msisdn = convertMSISDN(self.msisdnReal)
        msisdn0B = msisdn
        i = len(msisdn)

        while (i < 22):
            msisdn0B += "F"
            i+=1

        imsiReal = convertIMSI(self.imsiReal)
        mnc = self.imsiReal[4] + self.imsiReal[3]

        tmp = ""
        tmp += "C60C%02X%s" % (len(smsp) / 2, smsp0B)           # Length + SMSP
        tmp += "C7077F106F42001C01C7077F108F42001C01"           # SMSP
        tmp += "C60101C7063F005F490000"                 # IMEI
        tmp += "C609%02X%s" % (len(imsiReal) / 2, imsiReal)     # Length + IMSI
        tmp += "C7067F206F070000"                   # ???
        tmp += "C60BFFFFFFFF27F4%s0000FF01C7067F206F7E0000" % mnc   # LOCI
        tmp += "C60EFFFFFFFFFFFFFF27F4%s0000FF01C7067F206F530000" % mnc # LOCI GPRS
        tmp += "C60C%02X%s" % (len(msisdn) / 2, msisdn0B)       # MSISDN
        tmp += "c7077f106f40000F01"                 # ???

        apdu = "%sC405%sC5%02X%s" % (OtaRequest.getApdu(self), self.config, len(tmp) / 2, tmp)

        return str.upper(apdu)

    def execute(self, LOG):
        return OtaRequest.executeOtaCommand(self, LOG)


def convertIMSI(imsiInitial):
    imsi=""
    s="9%s"%imsiInitial
    i = len(s)
    while (i < 16):
        s += "F"
        i+=1
    i=0
    while (i < 16):
        imsi += s[i+1]+s[i]
        i+=2
    return imsi
 
def convertMSISDN(msisdnInitial):
    l=len(msisdnInitial)
    s=""
    msisdn=msisdnInitial
    if (msisdn[0]=="+"):
        s+="91"
        l=l-1
        msisdn=msisdnInitial[1:]
    else:
        s+="81"
    if ((len(msisdn)%2)==1):
        msisdn+="F"
    i=0
    while (i < l):
        s += msisdn[i+1]+ msisdn[i]
        i = i + 2
    return s
