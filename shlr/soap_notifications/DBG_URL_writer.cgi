#!/usr/bin/env python
#******************************************************************************
# file: $Id: DBG_URL_writer.cgi,v 1.2 2011-04-04 10:39:24 ndt Exp $
# author:  Benjamin Portier
#*****************************************************************************/
import os
import sys
import time

try:
        length = int(os.environ['CONTENT_LENGTH'])
        soapNotif = sys.stdin.read ()
except:
        soapNotif = 'Exception inside cgi to read notif'

hour=time.strftime("%d/%m/%y %H:%M:%S", time.localtime())
f=open('./shlr_dbg_notif.log','a')
f.write(hour + "\n")
f.write(soapNotif + "\n")
f.close()

print """Status: 200 OK
Content-Type: text/xml;

<toto><toto>"""
