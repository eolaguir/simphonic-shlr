#!/bin/bash

export SIMphonIC=/export/home/sfovivo/exe
export PATH=$PATH:/export/home/sfovivo/bin

MMILOG=/export/home/sfovivo/exe/asp/shlvivo/mmi/shlr/log/`date +%m%d`/vivo.log
CORBA_ERROR_TIME=/export/home/sfovivo/scripts/error_time.log

TIME_WAITING_AFTER_ERROR=120

# Create MMILOG if not exist
if [ ! -e $MMILOG ]; then
	mkdir /export/home/sfovivo/exe/asp/shlvivo/mmi/shlr/log/`date +%m%d`
	touch /export/home/sfovivo/exe/asp/shlvivo/mmi/shlr/log/`date +%m%d`/vivo.log
fi

# Start monitoring
while true
do
	reboot=0

	tail -f $MMILOG | while read line
	do
		if [ $reboot -eq "0" ]; then
			echo $line | grep 'INTERNAL ERROR'
			if [ "$?" = "0" ]; then
				date >> $CORBA_ERROR_TIME
       				#scl shutdown sys admin_alm
       				#sleep 1
				#OMNIORB_CONFIG=/opt/omniORB/etc/omniORB-admin_alm.cfg scl boot sys admin_alm	
				echo "Check FE02" | mailx -s "FE02 - SP1 with INTERNAL ERROR" l.marui@oberthur.com a.huart@oberthur.com y.silva@oberthur.com e.ramos2@oberthur.com m.diniz@oberthur.com
				reboot=1
			fi
		else
			break
		fi
	done

	sleep $TIME_WAITING_AFTER_ERROR
done
