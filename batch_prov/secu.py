#!/bin/env python
#******************************************************************************
# file: $Id: secu.py,v 1.4 2012-07-02 15:02:31 chriscus Exp $
# author:  Saad Amrani
#*****************************************************************************/
import Cenum
import _secu
import ctypes 
import threading 
from prov_config import LOG_DIRECTORY

for enum in (  "crAlgo", "crMacAlgo", "crDivAlgo",  "crKeyArea", "crStatus"):
	Cenum.enumLoad(_secu.SECU_LIB, enum, target = globals())
_secu.SECU_LIB.logInit(LOG_DIRECTORY, 'prov_secu', 5)

# get_KIs is not thread safe. We need to lock before calling it
#lockSecu = threading.Lock()

def getKi(_alm_algo_endecrypt, alm_acces_key, masterkey, iccid):
	_axs_key_name = ctypes.c_char_p(alm_acces_key)
	iccid_hex = ctypes.create_string_buffer(16)
	masterkey = ctypes.c_char_p(masterkey)
	iccid_hex = iccid 
	KIC = ctypes.create_string_buffer(33)
	KID = ctypes.create_string_buffer(33)
	KIK = ctypes.create_string_buffer(33)
#	lockSecu.acquire()
        cr = _secu.SECU_LIB.get_KIs(iccid_hex, KIC, KID, KIK, _alm_algo_endecrypt, _axs_key_name, masterkey) 	
#	lockSecu.release()
	return cr, KIC.value, KID.value, KIK.value

	
def call_getKi(masterkey, iccid):
	alm_acces_key = "AlmAccess"
	_alm_algo_endecrypt = cr_DES_ENC_CBC 
	return getKi(_alm_algo_endecrypt, alm_acces_key, masterkey, iccid)
if __name__ =='__main__':
	_alm_algo_endecrypt = cr_DES_ENC_CBC 
	l_masterkey = "OT"
	l_iccid = "00112233445566778899"
	alm_acces_key = "AlmAccess" 
  	getKis(_alm_algo_endecrypt, alm_acces_key, l_masterkey, l_iccid[len(l_iccid)-16:])

