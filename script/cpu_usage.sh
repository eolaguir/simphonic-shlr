#!/bin/bash

day=`date +%Y-%m-%d`
hour=`date +%H:%M`
data=$day" "$hour

#cpu=`ps -eo pcpu | grep -v %CPU | grep -v - | awk '{sum +=$1}; END {print sum}'`
cpu=`e=$(vmstat 1 5|awk '{print $22}'|tail -1) && echo 100 - $e|bc`

echo "$data|CPU Usage: %$cpu" >> /export/home/sfovivo/scripts/cpu_usage.txt
