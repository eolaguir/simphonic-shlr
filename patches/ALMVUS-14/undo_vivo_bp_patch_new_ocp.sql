/*
 * File:        undo_vivo_bp_patch_new_ocp.sql
 * Author:      Oleg Usoltsev <o.usoltsev@external.oberthur.com>
 * Description: The script rolls back changes made by vivo_bp_patch_new_ocp.sql
 *
 * Created:     05 Nov 2013
 * Verified:    05 Nov 2013
 */

DECLARE
  TYPE ocp_name_t IS VARRAY(2) OF origin_card_profile.name%TYPE;
  TYPE cmm_id_t IS VARRAY(2) OF cmm_profile.id%TYPE;
  
  -- The list of OCP-s to be deleted.
  v_ocp_list ocp_name_t := ocp_name_t('VIVO_19', 'VIVO_21');
  
BEGIN
  FORALL i IN v_ocp_list.FIRST .. v_ocp_list.LAST
    DELETE FROM ocp_card_profile
    WHERE ocp_id IN
      (
        SELECT id FROM origin_card_profile WHERE name = v_ocp_list(i)
      );

  FORALL i IN v_ocp_list.FIRST .. v_ocp_list.LAST
    DELETE FROM card_profile_security
    WHERE card_profile_id = 
      (
        SELECT id FROM card_profile WHERE description = v_ocp_list(i)
      );

  FORALL i IN v_ocp_list.FIRST .. v_ocp_list.LAST
    DELETE FROM cmm_profile WHERE id =
      (
        SELECT id FROM card_profile WHERE description = v_ocp_list(i)
      );

  FORALL i IN v_ocp_list.FIRST .. v_ocp_list.LAST
    DELETE FROM card_profile WHERE description = v_ocp_list(i);

  FORALL i IN v_ocp_list.FIRST .. v_ocp_list.LAST
    DELETE FROM origin_card_profile WHERE name = v_ocp_list(i);

  COMMIT;
  
  dbms_output.put_line('Changes have been rolled back');
  
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    dbms_output.put_line('ERROR: ' || SQLERRM);
END;
/