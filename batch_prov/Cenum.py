#!/usr/bin/env python
import ctypes
import sys
import warnings

def _isValidData(f_enumString, f_enumFromString, data):
	# f_enumString and f_enumFromString are bijective for valid data
	if type(data) == type(''):
		# empty labels are invalid
		return f_enumString(f_enumFromString(data)) == data and len(data) > 0, f_enumFromString(data)
	else:
	 	return f_enumFromString(f_enumString(data)) == data, data

def enumLoad(dynlib, enum, start = 0, end = None, invalid = -1, target = globals()):
	"""
	loads the content of the C enum from the dynamic library dynlib

	dynlib  -- ctypes.CDLLpath object or path to a dynamic library providing
	           the APIs needed to load the enum
	enum  -- enum name

	Keyword argument
	start -- starting indice(s) for the enum (default: 0)
		 int, string or list of mixed int/strings
	end -- last entry of the enum (int or string, default: None)
	invalid -- value used for invalid data (int or string, default: -1)
	target -- target dictionary to store the enum content
		  (default: Cenum.globals() dict)

	set appropriate start parameter when handling enum not starting from 0
	and/or non sequential enum values

	if end is set, all the values between entries in start and end are scanned

	enumLoad will successfully load a C enum only if the dynamic lib
	provides the following two functions:
		- <enum>String (enum):
		     	returns the string label from the enum int value
		- <enum>FromString (string):
		     	returns the enum int value from the string label
		     	returns -1 when the string label does not exist

	enumLoad defines:
		- a dictionnary associating labels to int values
		- a variable for each label of the enum set to the corresponding
		  int value 

	if enumLoad is called several times for the same enum, data is appended
	and/or replaced
	"""

	if type(dynlib) == type(''):
		try:
			lib = ctypes.CDLL(dynlib)
		except:
			warnings.warn("could not open %s" % dynlib)
			return
	else:
		lib = dynlib
	try:
		# API giving the int value for the enum label
		f_enumFromString = eval('lib.%sFromString' % enum)

		# API giving the enum label from the int value
		f_enumString = eval('lib.%sString' % enum)
		f_enumString.restype = ctypes.c_char_p
	except AttributeError:
		# at least one API is missing
		warnings.warn("enum <%s> not loaded. (%sString and %sFromString missing from %s)" % (enum, enum, enum, dynlib))
		return

	# if start is a single item, make it a list
	if not hasattr(start, '__iter__'):
		l_start= (start,)
	else: l_start = start

	if end is not None:
		# check if end condition is a valid value
		valid, end = _isValidData(f_enumString, f_enumFromString, end)
		if not valid:
			# an invalid end label is as good as no end point
			if type(end) == type(''):
				end = None

	dummy, inv = _isValidData(f_enumString, f_enumFromString, invalid)

	# if dict already exists, data will be appended
	if target.has_key(enum):
		d = target[enum]
	else:
		d = {}

	# store enum name for further use
	_enum = enum

	# build a type derived from int for the enum values
	tpl = """class %s(int):
	pass""" % enum
	exec tpl
	# get a convenient handler to this new type
	enum = eval("%s" % enum)

	# attach type definition to the target dict
	enum.__module__ = target.get('__name__', '__main__')

	# going through all the starting points
	for strt in l_start:
		# check if strt is a valid enum value, and retrieve its id if ok
		valid, idx = _isValidData(f_enumString, f_enumFromString, strt)
		if not valid:
			# invalid label, or invalid start index with no stop condition
			if type(strt) == type('') or end is None:
				continue	
			else:
				# strt is an invalid index. Will still scan from there to end
				idx = strt

		# check end will eventually be reached
		if end is not None and end < idx: continue

		while 1:
			# idx for invalid data is skipped as it's a reserved value for f_enumFromString
			if idx == inv:
				idx += 1
			# end has been reached
			if end is not None and idx > end:
				break

			# check if we are dealing with a valid enum value
			valid, dummy = _isValidData(f_enumString, f_enumFromString, idx)
			if not valid:
				if end is None:
					# all the sequential enum values retrieved
					break
			else:
				# store idx -> label association in d
				d[enum(idx)] = f_enumString(idx)
				# store label = idx in target dict
				target[d[enum(idx)]] = enum(idx)
			idx += 1
	if len(d) == 0:
		warnings.warn("could not load enum <%s>" % _enum)
		return

	# store the dict associating int values to enum labels (overwriting enum type by the way)
	target[_enum] = d
