#!/bin/env python
#******************************************************************************
# file: $Id: shlr_req_process.py,v 1.16 2011-12-08 21:14:52 gabritou Exp $
# author:  Nathalie Desbonnet
#*****************************************************************************/
import os
from shlr_req_definitions import ActivateSIMchronize
from shlr_req_definitions import ActivateImeiTracking
from shlr_tools import  Cfg
from shlr_req_definitions import *

cfgPath=os.environ.get("SHLR_CFGFILE")
CFG_SHLR=Cfg(cfgPath,0)
alm_host = CFG_SHLR.getvalue("ALM_HOST") 
alm_port = CFG_SHLR.getvalue("ALM_PORT") 
key_set  = CFG_SHLR.getvalue("KEY_SET") 
tsm_ref  = CFG_SHLR.getvalue("TSM_REF")

def processSelectItem(orchId, msisdn, tid, config, title, items, LOG):
        LOG.setMsisdn(msisdn)
        req = SelectItemRequest(orchId, msisdn, tid, config, title, items)
        LOG.info("%s" %req)
        result = req.execute(LOG)
        return result

def processGetInput(orchId, msisdn, tid, config, title, type, min, max, LOG):
        LOG.setMsisdn(msisdn)
        req = GetInputRequest(orchId, msisdn, tid, config, title, type, min, max)
        LOG.info("%s" %req)
        result = req.execute(LOG)
        return result

def processDisplayText(orchId, msisdn, tid, config, title, LOG):
        LOG.setMsisdn(msisdn)
        req = DisplayText(orchId, msisdn, tid, config, title)
        LOG.info("%s" %req)
        result = req.execute(LOG)
        return result

def processProvideLocalInfo(orchId, msisdn, tid, config, LOG):
        LOG.setMsisdn(msisdn)
        req = ProvideLocalInfo(orchId, msisdn, tid, config)
        LOG.info("%s" %req)
        result = req.execute(LOG)
        return result

def processActivateImeiTracking(orchId, msisdn, tid, smspReal, LOG):
        LOG.setMsisdn(msisdn)
        req = ActivateImeiTracking(orchId, msisdn, tid, smspReal)
        LOG.info("%s" %req)
        result = req.execute(LOG)
        return result

def processActivateSIMchronize(orchId, msisdn, tid, smspReal, LOG):
        LOG.setMsisdn(msisdn)
        req = ActivateSIMchronize(orchId, msisdn, tid, smspReal)
        LOG.info("%s" %req)
        result = req.execute(LOG)
        return result

def processActivateHRS(orchId, msisdn, tid, config, imsiReal, imsiRoaming, smspReal, msisdnReal, LOG):
        LOG.setMsisdn(msisdn)
        req = ActivateHRS(orchId, msisdn, tid, config, imsiReal, imsiRoaming, smspReal, msisdnReal)
        LOG.info("%s" %req)
        result = req.execute(LOG)
        return result

def processActivateHRS_LTE(orchId, msisdn, labelCommand, tid, config, imsiReal, smspReal, msisdnReal, LOG):
        LOG.setMsisdn(msisdn)
        req = ActivateHRS_LTE(orchId, msisdn, labelCommand, tid, config, imsiReal, smspReal, msisdnReal)
        LOG.info("%s" % req)
        result = req.execute(LOG)
        return result

def getSmmId():
    return int(CFG_SHLR.getvalue("SMM_ID"))
