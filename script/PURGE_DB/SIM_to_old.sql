spool &2
set heading off
set trimout off
set termout off
set verify off
set echo off
set feed off
set linesize 500
set trimspool ON
set newpage NONE
SELECT A.iccid, A.imsi, B.msisdn, C.name, E.kic_value, E.kid_value, E.kik_value,
TO_CHAR(A.import_date,'YYYY/MM/DD hh24:mi:ss') ,A.activity_date
FROM sim_card A, subscriber B, origin_card_profile C, cmm_sim_card D, cmm_sim_keyset_counter E
WHERE (TO_DATE(A.activity_date,'YYYY/MM/DD hh24:mi:ss') !=A.import_date)
and ((SYSDATE-TO_DATE(A.activity_date, 'YYYY/MM/DD hh24:mi:ss'))>&1)
and A.id=B.sim_card_id
and A.card_profile_id=C.id
and D.iccid=A.iccid
and D.id=E.sim_card_id;
spool off
exit
