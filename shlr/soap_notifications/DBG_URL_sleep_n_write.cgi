#!/usr/bin/env python
#******************************************************************************
# file: $Id: DBG_URL_sleep_n_write.cgi,v 1.1 2011-04-20 09:21:43 portier Exp $
# author:  Benjamin Portier
#*****************************************************************************/
import os
import sys
import time

try:
        length = int(os.environ['CONTENT_LENGTH'])
        soapNotif = sys.stdin.read ()
	time.sleep(100)
except:
        soapNotif = 'Exception inside cgi to read notif'

hour=time.strftime("%d/%m/%y %H:%M:%S", time.localtime())
f=open('./shlr_dbg_notif.log','a')
f.write(hour + "\n")
f.write(soapNotif + "\n")
f.close()

print """Status: 200 OK
Content-Type: text/xml;

<toto><toto>"""
