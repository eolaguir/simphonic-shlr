
#include <event_alarm.h> 
#include <log.h>
#include <com.h>

evStatus sendAlarm(char* modName, evClass  alarm_class,  evCode code, evComment comment)
{
	evStatus cr;  
	comStatus com_rc;
	com_rc = comReinit((const qName *) modName, 0);

	if (com_rc != comOK) {
		fprintf(stderr, "comReinit() failed: %s\n",
		comError(com_rc));
		return evErr_INTERFACE;
	}

	
	cr = evInit(modName, evModType_SYS); 
	cr = evNotify( alarm_class ,
			code,
			evModType_SYS,
			(char *) modName,
			comment);

	if (cr != evOK) 
		fprintf(stderr, "evNotify failed to initialise the dialog with the evServer, Reason: %s\n", evError(cr));

	comEnd();
	return cr;
}
