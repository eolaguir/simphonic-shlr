/*-------------------------------------------------------------------------
-- File:	$RCSfile: Pkg_CardState_body.sql,v $
-- Project:	ALM
-- modify by:	$Author: portier $ ($Date: 2011-06-23 13:31:53 $)
-------------------------------------------------------------------------
creation       :       v1.0, 12/18/2002
version        :       $Revision: 1.1 $
-------------------------------------------------------------------------*/


CREATE OR REPLACE PACKAGE BODY pkg_card_state AS




----------------------------------------------------------------------------
--
--	Update card activity (A = Available, R = Running, P = Pending)
--
PROCEDURE updateActivity(
        p_id IN sim_card.id%TYPE,
        p_activity IN sim_card.activity%TYPE,
        p_nb_rows_updated OUT NUMBER
) AS
	remote_update EXCEPTION;
        PRAGMA EXCEPTION_INIT(remote_update, -20102);
BEGIN
	BEGIN
		UPDATE sim_card
		SET activity = p_activity, change_date = SYSDATE
		WHERE id = p_id;
		p_nb_rows_updated := SQL%ROWCOUNT ;
		COMMIT ;
	EXCEPTION
		WHEN remote_update THEN
			p_nb_rows_updated:=0;
	END;


END;

------------------------------------------------------------------------------
--
-- SetFreeEeprom
-- For a sim card (msisdn) sets the new available memory value (eeprom)
--
PROCEDURE SetFreeEeprom(
		p_msisdn IN subscriber.msisdn%TYPE,
		p_eeprom IN sim_card.eeprom%TYPE
) AS
	v_simCardId	sim_card.id%TYPE;
	v_mSimCardId	m_sim_card.id%TYPE;
	v_ocp_id	sim_card.ocp_id%TYPE;
	v_m_ocp_id	m_ocp.id%TYPE;

	v_package_name 	package.name%TYPE;
	v_package_id	package.id%TYPE;

	v_isDualCardFl	CHAR(1);
	v_isSharedEepromFl CHAR(1);

BEGIN

	tools_pack.getSimCardId(p_msisdn, v_simCardId);

	v_isDualCardFl := tools_pack.isDualCard(v_simCardId);

	IF v_isDualCardFl = tools_pack.FALSE THEN
	-- NOT DUAL CARD

		UPDATE 
			sim_card
		SET 
			eeprom = p_eeprom
		WHERE 
			id = v_simCardId;
	ELSE
	-- DUAL CARD
		tools_pack.getOcpId(v_simCardId, v_ocp_id);

		tools_pack.getMOcpId(v_ocp_id,v_m_ocp_id);

		v_isSharedEepromFl := tools_pack.isSharedEeprom(v_m_ocp_id);

		IF v_isSharedEepromFl = tools_pack.TRUE THEN
			tools_pack.getMSimCardId(
				v_simCardId,
				v_mSimCardId);

			IF tools_pack.crGetSimCardId%ISOPEN THEN
				CLOSE tools_pack.crGetSimCardId;
			END IF;
		
			FOR curSimCardId IN tools_pack.crGetSimCardId(v_mSimCardId) LOOP

				UPDATE 
					sim_card
				SET 
					eeprom = p_eeprom
				WHERE 
					id = curSimCardId.sim_card_id;

			END LOOP;
		ELSE

			UPDATE 
				sim_card
			SET 
				eeprom = p_eeprom
			WHERE 
				id = v_simCardId;
			
		END IF;
	END IF;

	COMMIT ;

END;

-----------------------------------------------------------------------------
--
-- SetCounter
-- For a sim card (msisdn) sets the new counter value (counter)
--
PROCEDURE SetCounter(
                p_msisdn IN subscriber.msisdn%TYPE,
                p_counter IN sim_card.cntr%TYPE
) AS
	v_simCardId	sim_card.id%TYPE;
	v_mSimCardId	m_sim_card.id%TYPE;
	v_ocp_id	sim_card.ocp_id%TYPE;
	v_m_ocp_id	m_ocp.id%TYPE;

	v_package_name 	package.name%TYPE;
	v_package_id	package.id%TYPE;

	v_isDualCardFl	CHAR(1);
	v_isSharedCounterFl CHAR(1);

BEGIN

	tools_pack.getSimCardId(p_msisdn, v_simCardId);

	v_isDualCardFl := tools_pack.isDualCard(v_simCardId);

	IF v_isDualCardFl = tools_pack.FALSE THEN
	-- NOT DUAL CARD

		UPDATE 
			sim_card
		SET 
			cntr  = p_counter
		WHERE 
			id = v_simCardId;
	ELSE
	-- DUAL CARD
		tools_pack.getOcpId(v_simCardId, v_ocp_id);

		tools_pack.getMOcpId(v_ocp_id,v_m_ocp_id);

		v_isSharedCounterFl := tools_pack.isSharedCounter(v_m_ocp_id);

		IF v_isSharedCounterFl = tools_pack.TRUE THEN
			tools_pack.getMSimCardId(
				v_simCardId,
				v_mSimCardId);

			IF tools_pack.crGetSimCardId%ISOPEN THEN
				CLOSE tools_pack.crGetSimCardId;
			END IF;
		
			FOR curSimCardId IN tools_pack.crGetSimCardId(v_mSimCardId) LOOP

				UPDATE 
					sim_card
				SET 
					cntr  = p_counter
				WHERE 
					id = curSimCardId.sim_card_id;

			END LOOP;
		ELSE

			UPDATE 
				sim_card
			SET 
				cntr  = p_counter
			WHERE 
				id = v_simCardId;
			
		END IF;
	END IF;

	COMMIT ;

END;

-----------------------------------------------------------------------------
--
-- SetCounterWithId
-- For a counter id, set the new value
--
PROCEDURE SetCounterWithId(
                p_id IN counter.id%TYPE,
                p_value IN counter.value%TYPE
) AS

BEGIN
	UPDATE counter
	SET value = p_value
	WHERE id = p_id;

	COMMIT;

END;

-------------------------------------------------------------------------------

END pkg_card_state;
/


