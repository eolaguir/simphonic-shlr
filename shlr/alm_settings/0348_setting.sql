insert into security values ('100','VIVO RAM','2','f','cc','1','1','f','0','1','1','1','1');			-- SPI:1221 KIC:25 KID:25
insert into security values ('101','VIVO SIMchronize','0','f','no','1','1','f','0','1','0','1','0'); 		-- SPI:0021 KIC:01 KID:01
insert into security values ('102','VIVO HRS interact no POR)','2','f','cc','0','1','f','0','1','1','1','1');	-- SPI:1220 KIC:25 KID:25

-- copy paste into cmm_security with kik_algo=kik_des_mode=1
insert into cmm_security values(100, 'VIVO RAM','2','f','cc','1','1','f','0','1','1','1','1','1','1',sysdate, sysdate);
insert into cmm_security values ('101','VIVO SIMchronize','0','f','no','1','1','f','0','1','0','1','0','1','1', sysdate, sysdate);
insert into cmm_security values ('102','VIVO HRS interact no POR)','2','f','cc','0','1','f','0','1','1','1','1','1','1', sysdate, sysdate);

insert into application_type values(101, 'GSM_MG', '524144', '2G', 'A0');	-- SIMchronize
INSERT INTO files_application_type VALUES ('ADN',101);

insert into application_type values(102, 'USIM1_MG', '53484C', '3G', '00'); 	-- HRS
INSERT INTO files_application_type VALUES ('3G_ADNG',102);
