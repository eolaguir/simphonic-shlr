#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#******************************************************************************
# file: $Id: gsm_alphabet_lib.py,v 1.3 2011-04-05 11:40:03 ndt Exp $
# author:  Benjamin Portier
#*****************************************************************************/

DICT_GSM_TO_ASCII=\
{ 	"00":	'@',
	"02":	'$',
	"04":	"�",
	"05":	"�",
	"06":	"�",
	"07":	chr(0xEC), # i accent grave
	"09":	chr(0xC7), # grand c c�dille
	"11":	"_",
	"40":	chr(0xA1), # point d exclamation espagnol
	"5D":	chr(0xD1), # n espagnol majuscule
	"5E":	chr(0xDC), # u trema majuscul
	"60":	chr(0xBF), # point d interrogation espagnol
	"7B":	chr(0xE4), # a trema minuscule
	"7C":	chr(0xF6), # o trema minuscule
	"7D":	chr(0xF1), # n espagnol minuscule
	"7E":	chr(0xFC), # u trema minuscule
	"7F":	"�",
	"15":	"y",	   # Omega recycled for yes (organs field value), conversion in 1 way only
	"16":	"n"	   # Pi recycled for no (organs field value), conversion in 1 way only
}

DICT_ASCII_TO_GSM=\
{ 	"@":		'00',
	"$":		'02',
	"�":		"04",
	"�":		"05",
	"�":		"06",
	chr(0xEC):	"07", # i accent grave
	chr(0xC7):	"09", # grand c c�dille
	"_":		"11",
	chr(0xA1):	"40", # point d exclamation espagnol
	chr(0xD1):	"5D", # n espagnol majuscule
	chr(0xDC):	"5E", # u trema majuscul
	chr(0xBF):	"60", # point d interrogation espagnol
	chr(0xE4):	"7B", # a trema minuscule
	chr(0xF6):	"7C", # o trema minuscule
	chr(0xF1):	"7D", # n espagnol minuscule
	chr(0xFC):	"7E", # u trema minuscule
	"�":		"7F",
}

def convertHexaGSMToDBChar(two_digits):
	if (len(two_digits)!=2):
		# return copyright
		return chr(0xA9)
	int_value = int(two_digits, 16)
	if ( (0x20 <= int_value and int_value <= 0x23) or (0x25 <= int_value and int_value <= 0x3F)\
	or (0x41 <= int_value and int_value <= 0x5A) or (0x61 <= int_value and int_value <= 0x7A) ):
		return chr(int_value)
	else:
		return DICT_GSM_TO_ASCII.get(two_digits, chr(0xA9))

def convertDBCharToHexaGSM(one_char, tag):
	if (len(one_char)!=1):
		# return @
		return "00"
	# Special case for organs
	if (tag=="24"):
		if one_char=='y':
			return "15"
		if one_char=='n':
			return "16"
	int_value = ord(one_char)
	if ( (0x20 <= int_value and int_value <= 0x23) or (0x25 <= int_value and int_value <= 0x3F)\
	or (0x41 <= int_value and int_value <= 0x5A) or (0x61 <= int_value and int_value <= 0x7A) ):
		return "%02X"%int_value
	else:
		return DICT_ASCII_TO_GSM.get(one_char, "00")

# To be used:
def convertHexaCodedGSMToSOAPString(ma_chaine):
	i=0
	out=""
	while (i < len(ma_chaine)-1):
		out = out + convertHexaGSMToDBChar(ma_chaine[i:i+2])
		i = i + 2
	return out

# To be used
def convertSOAPStringToHexaCodedGSM(ma_chaine, tag):
	i=0
	out=""
	while (i < len(ma_chaine)):
		out = out + convertDBCharToHexaGSM(ma_chaine[i], tag)
		i = i + 1
	return out
	

"""
s2 = "6368656D696E206465206C61206772616E64652070656C6F757365203738313130204C6520560573696E6574"
print convertHexaGSMToDBString(s2)
"""
