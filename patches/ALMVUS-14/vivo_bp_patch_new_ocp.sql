/*
 * File:        vivo_bp_patch_new_ocp.sql
 * Author:      Oleg Usoltsev <o.usoltsev@external.oberthur.com>
 * Description: The script creates new VIVO OCP-s.
 *
 * Created:     04 Nov 2013
 * Verified:    05 Nov 2013
 */

-- The name and CMM ID of the source OCP that will be cloned.
DEFINE SRC_OCP_NAME = 'VIVO_18'

DECLARE
  TYPE ocp_name_t IS VARRAY(2) OF origin_card_profile.name%TYPE;
  TYPE cp_keys_t IS TABLE OF card_profile_security%ROWTYPE;
  
  -- The list of new OCP-s and their CMM ID-s.
  v_ocp_list ocp_name_t := ocp_name_t('VIVO_19', 'VIVO_21');

  v_cp card_profile%ROWTYPE;
  v_ocp origin_card_profile%ROWTYPE;
  v_cmm cmm_profile%ROWTYPE;
  v_cp_keys cp_keys_t;
  
  -- Fetch the data of the original OCP (to be cloned).
  PROCEDURE fetch_ocp IS
  BEGIN
    SELECT adn_mode,
           cntr,
           parent_id,
           cattp_parameters_set_id,
           post_file_perso_eeprom,
           ro_available_eeprom
    INTO v_ocp.adn_mode,
         v_ocp.cntr,
         v_ocp.parent_id,
         v_ocp.cattp_parameters_set_id,
         v_ocp.post_file_perso_eeprom,
         v_ocp.ro_available_eeprom
    FROM origin_card_profile
    WHERE name = '&SRC_OCP_NAME';
  
  EXCEPTION
    WHEN no_data_found THEN
      dbms_output.put_line('ERROR: OCP not found.');
      RAISE;
  END fetch_ocp;
  
  -- Fetch the data of the original CP (to be cloned).
  PROCEDURE fetch_cp IS
  BEGIN
    SELECT supplier_id,
           chip_ref,
           mask_version,
           mask_date,
           vm_release,
           dl_protocol,
           ram,
           eeprom,
           applet_ram,
           applet_eeprom,
           cattp_compat,
           package_tag,
           push_sms_secu,
           sms_concatenated
    INTO v_cp.supplier_id,
         v_cp.chip_ref,
         v_cp.mask_version,
         v_cp.mask_date,
         v_cp.vm_release,
         v_cp.dl_protocol,
         v_cp.ram,
         v_cp.eeprom,
         v_cp.applet_ram,
         v_cp.applet_eeprom,
         v_cp.cattp_compat,
         v_cp.package_tag,
         v_cp.push_sms_secu,
         v_cp.sms_concatenated
    FROM card_profile
    WHERE description = '&SRC_OCP_NAME';

  EXCEPTION
    WHEN no_data_found THEN
      dbms_output.put_line('ERROR: Card profile not found.');
      RAISE;
  END fetch_cp;
  
  -- Fetch security keys of the original CP (to be cloned).
  PROCEDURE fetch_cp_keys IS
  BEGIN
    SELECT *
    BULK COLLECT INTO v_cp_keys
    FROM card_profile_security
    WHERE card_profile_id =
      (
        SELECT id
        FROM card_profile
        WHERE description = '&SRC_OCP_NAME'
      );
  
  EXCEPTION
    WHEN no_data_found THEN
      dbms_output.put_line('ERROR: Security keys not found.');
      RAISE;
  END fetch_cp_keys;
  
  -- Fetch CMM profile of the original OCP (to be cloned).
  PROCEDURE fetch_cmm IS
  BEGIN
    SELECT dl_protocol, r6_flag
    INTO v_cmm.dl_protocol, v_cmm.r6_flag
    FROM cmm_profile
    WHERE id =
      (
        SELECT id FROM card_profile WHERE description = '&SRC_OCP_NAME'
      );
  
  EXCEPTION
    WHEN no_data_found THEN
      dbms_output.put_line('ERROR: CMM profile of &SRC_OCP_NAME not found.');
      RAISE;
  END fetch_cmm;

  -- Check if the given OCP exists.
  FUNCTION ocp_exists(p_name IN origin_card_profile.name%TYPE) RETURN BOOLEAN
  IS
    v_id origin_card_profile.id%TYPE;
  BEGIN
    SELECT id INTO v_id FROM origin_card_profile WHERE name = p_name;

    RETURN TRUE;
  
  EXCEPTION
    WHEN no_data_found THEN
      RETURN FALSE; -- OK, that's what we need.
  END ocp_exists;
  
  -- Check if the given CP exists.
  FUNCTION cp_exists(p_description IN card_profile.description%TYPE) RETURN BOOLEAN
  IS
    v_id card_profile.id%TYPE;
  BEGIN
    SELECT id INTO v_id FROM card_profile WHERE description = p_description;
    
    RETURN TRUE;
  
  EXCEPTION
    WHEN no_data_found THEN
      RETURN FALSE; -- OK, that's what we need.
  END cp_exists;
  
  -- Check if the given CP has security keys.
  FUNCTION cp_keys_exist(p_id IN card_profile.id%TYPE) RETURN BOOLEAN
  IS
    v_id card_profile.id%TYPE;
  BEGIN
    SELECT DISTINCT card_profile_id INTO v_id
    FROM card_profile_security
    WHERE card_profile_id = p_id;
    
    RETURN TRUE;
  
  EXCEPTION
    WHEN no_data_found THEN
      RETURN FALSE; -- OK, that's what we need.
  END cp_keys_exist;
  
  -- Check if CMM profile exists.
  FUNCTION cmm_profile_exists(p_name IN card_profile.description%TYPE)
    RETURN BOOLEAN
  IS
    v_id cmm_profile.id%TYPE;
  BEGIN
    SELECT id INTO v_id FROM cmm_profile WHERE id = 
      (
        SELECT id FROM card_profile WHERE description = p_name
      );
    
    RETURN TRUE;
  
  EXCEPTION
    WHEN no_data_found THEN
      RETURN FALSE; -- OK, that's what we need.
  END cmm_profile_exists;
  
  -- Create a new OCP.
  PROCEDURE create_ocp IS
  BEGIN
    INSERT INTO origin_card_profile(
        id,
        name,
        adn_mode,
        cntr,
        import_date,
        parent_id,
        cattp_parameters_set_id,
        post_file_perso_eeprom,
        ro_available_eeprom)
      VALUES (
        v_ocp.id,
        v_ocp.name,
        v_ocp.adn_mode,
        '0000000000',
        SYSDATE,
        v_ocp.parent_id,
        v_ocp.cattp_parameters_set_id,
        v_ocp.post_file_perso_eeprom,
        v_ocp.ro_available_eeprom);
  END create_ocp;
  
  -- Create a new CP.
  PROCEDURE create_cp IS
  BEGIN
    INSERT INTO card_profile(
        id,
        description,
        supplier_id,
        chip_ref,
        mask_version,
        mask_date,
        vm_release,
        dl_protocol,
        ram,
        eeprom,
        applet_ram,
        applet_eeprom,
        import_date,
        cattp_compat,
        package_tag,
        push_sms_secu,
        sms_concatenated)
      VALUES (
        v_cp.id,
        v_cp.description,
        v_cp.supplier_id,
        v_cp.chip_ref,
        v_cp.mask_version,
        v_cp.mask_date,
        v_cp.vm_release,
        v_cp.dl_protocol,
        v_cp.ram,
        v_cp.eeprom,
        v_cp.applet_ram,
        v_cp.applet_eeprom,
        SYSDATE,
        v_cp.cattp_compat,
        v_cp.package_tag,
        v_cp.push_sms_secu,
        v_cp.sms_concatenated);
  
    INSERT INTO ocp_card_profile(ocp_id, cp_id)
    VALUES (v_ocp.id, v_cp.id);
  END create_cp;
  
  -- Create a new security keyset for the given card profile.
  PROCEDURE create_cp_keys(p_id IN card_profile.id%TYPE) IS
  BEGIN
    FOR i IN v_cp_keys.FIRST .. v_cp_keys.LAST
    LOOP
      v_cp_keys(i).card_profile_id := p_id;
    END LOOP;
  
    FORALL i IN v_cp_keys.FIRST .. v_cp_keys.LAST
      INSERT INTO card_profile_security VALUES v_cp_keys(i);
  END create_cp_keys;
  
  -- Create a new CMM profile.
  PROCEDURE create_cmm_profile IS
  BEGIN
    INSERT INTO cmm_profile(
      id,
      dl_protocol,
      r6_flag,
      creation_date,
      last_mod_date)
    VALUES (
      v_cmm.id,
      v_cmm.dl_protocol,
      v_cmm.r6_flag,
      SYSDATE,
      SYSDATE);
  END create_cmm_profile;

-- Let's rock-n-roll!
BEGIN
  SELECT MAX(id) INTO v_ocp.id FROM origin_card_profile;
  SELECT MAX(id) INTO v_cp.id FROM card_profile;

  fetch_ocp();
  fetch_cp();
  fetch_cp_keys();
  fetch_cmm();

  dbms_output.put_line('Cloning &SRC_OCP_NAME');
  dbms_output.put_line('');
  
  FOR i IN 1 .. v_ocp_list.COUNT
  LOOP
  
    -- Cloning OCP.
    IF NOT ocp_exists(v_ocp_list(i)) THEN
      v_ocp.id := v_ocp.id + 1;
      v_ocp.name := v_ocp_list(i);
      
      create_ocp();
      dbms_output.put_line('OCP ' || v_ocp_list(i) || ' has been created: ID = ' || v_ocp.id);
    ELSE
      dbms_output.put_line('OCP ' || v_ocp_list(i) || ' already exists.');
    END IF;
    
    -- Cloning CP.
    IF NOT cp_exists(v_ocp_list(i)) THEN
      v_cp.id := v_cp.id + 1;
      v_cp.description := v_ocp_list(i);
    
      create_cp();
      dbms_output.put_line('CP ' || v_ocp_list(i) || ' has been created: ID = ' || v_cp.id);
    ELSE
      dbms_output.put_line('CP ' || v_ocp_list(i) || ' already exists.');
    END IF;
    
    -- Cloning CP security keys.
    IF NOT cp_keys_exist(v_cp.id) THEN
      create_cp_keys(v_cp.id);
      dbms_output.put_line('CP security keyset ' || v_ocp_list(i) ||
                           ' has been created: ' || v_cp_keys.COUNT || ' record(s)');
    ELSE
      dbms_output.put_line('CP security keyset ' || v_ocp_list(i) || ' already exists.');
    END IF;
    
    -- Cloning CMM profile.
    IF NOT cmm_profile_exists(v_ocp_list(i)) THEN
      SELECT id INTO v_cmm.id
      FROM card_profile
      WHERE description = v_ocp_list(i);
    
      create_cmm_profile();
      dbms_output.put_line('CMM profile has been created: ID = ' || v_cmm.id);
    ELSE
      dbms_output.put_line('CMM profile for ' || v_ocp_list(i) || ' already exists.');
    END IF;
    
    dbms_output.put_line('');
  END LOOP;
  
  COMMIT;
  
EXCEPTION
  WHEN no_data_found THEN
    dbms_output.put_line('ERROR: &SRC_OCP_NAME data is incomplete.');

  WHEN OTHERS THEN
    ROLLBACK;
    dbms_output.put_line('ERROR: ' || SQLERRM);
END;
/