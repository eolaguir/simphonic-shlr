#!/usr/bin/env python
import copy
import re
import time
import os
import os.path
import string
import sys
import prov_sql
from prov_sql import prov_card

import threading
import shutil
from prov_config import NB_THREADS
import datetime 
from  delete_sql import delete_card, get_imsi,get_id,get_kic,get_kid,get_kik,get_profile_id,get_id_cmm,get_keys,get_ids,update_dates,get_activity_date,get_import_date
global TH_ID
TH_ID=0
lockT = threading.Lock()
lock_total = threading.Lock()
global tmp_output_file 
global file_type

#total, total_ok = 0, 0
y, _m, _d, _h, _min, _sec, p,q,r = time.localtime()
#name_file = "sim_%d%d%d_%d%d%d_LLLLLLL.dat" % (y, _m, _d, _h, _min, _sec)
today =  '%d%d%d_%02d%02d%02d' % (y, _m, _d, _h, _min, _sec)




def GetInHMS(seconds):
	hours = seconds / 3600
	seconds -= 3600*hours
	minutes = seconds / 60
	seconds -= 60*minutes
	print("hours: %02d"%hours)
	print("minutes: %02d"%minutes)
	print("seconds: %02d"%seconds)
	return "%02d%02d%02d" % (hours, minutes, seconds)


if __name__=='__main__':
	filename                    = sys.argv[1]
	#filename='tt.txt'
	#format = '%s|\n'
	#f = open(filename, 'w')
	#prefix = 1000000333167
	#for i in range(0,5):
		#var = prefix + i
		#f.write(format % (str(var)))
	#f.close()
	l_con = prov_sql.getDBConnection()
	cursor = l_con.cursor()
	infile = open(filename,"r")
	start_time =  time.time()
	print("starting the scrippt")
	#print("start_time: <%s>"%start_time) 
	for line in infile.readlines():
		l_data = line.split('|')
		iccid   = l_data[0]
		print "test iccid: <%s>"%iccid
		#imsi,profile_id,import_date,activity_date=get_ids(l_con, iccid)
		imsi=get_imsi(l_con, iccid)
		profile_id=get_profile_id(l_con, iccid)
		activity_date=get_activity_date(l_con, iccid)
		import_date=get_import_date(l_con, iccid)

		#print ("test imsi: <%s>"%imsi)
		#print ("test import_date: <%s>"%import_date) 
		#print ("test activity_date: <%s>"%activity_date)
		
		id_cmm=get_id_cmm(l_con, iccid)

		
		#print ("test id_cmm: <%d>"%id_cmm) 
		kic,kid,kik=get_keys(l_con, id_cmm)
		
		
		
		#print ("test kic: <%s>"%kic)
		
		#print ("test kid: <%s>"%kid)
		
		#print ("test kik: <%s>"%kik)
		msisdn = "+5570" + imsi[7:]
		#print ("test msisdn: <%s>"%msisdn)
		
		#print ("test profile_id: <%d>"%profile_id) 
	
		try :
			message1 = delete_card(cursor, iccid, imsi, msisdn)
			#l_con.commit()
		except  Exception, e:
			print ("delete_card  exception : <%s>"%(e.__str__()))
		l_con.commit()
		try :
			message2 = prov_sql.prov_card(cursor, iccid, imsi, msisdn, '1', kic,kid,kik)
			#l_con.commit()
		except  Exception, e:
			print ("prov_card exception : <%s>"%(e.__str__()))
		try :
			message3 = update_dates(l_con, iccid, import_date, activity_date)
			#l_con.commit()
		except  Exception, e:
			print ("update exception : <%s>"%(e.__str__()))
		
		line = infile.readline()
		
	infile.close()
	stop_time =  time.time()
	print("stop_time: <%s>"%stop_time)
	duration = int(round(stop_time - start_time))
	duration =      GetInHMS(duration)

	cursor.close()
	prov_sql.closeCon(l_con)


	


	





