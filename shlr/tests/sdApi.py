import os
import sys
import alm
import soap
import traceback
from zsiTools import *
from ALM_server import *
from alm_tools import *
from shlr_tools import Log

import sdApiHelper
import ALM_server
import ALM_helper

# -- pyClient 
from pyClient import *
from dao_idl import _0_ci
from ci import *

LOG = Log()

def RegisterTSMNotificationUrl(ps):
    logInfo("Calling function RegisterTSMNotificationUrl")
    req = ps.Parse(RegisterTSMNotificationUrlRequest)
    logInfo("Request object is " + str(req))

    username = req._username
    aid = req._aid
    urlxsi = req._urlxsi

    logInfo("RegisterTSMNotificationUrl (%s/%s)"%(username, urlxsi))
    resp = sdApiHelper.registerTSMNotificationUrl(username, aid, urlxsi)
    logInfo("sending response")
    response = RegisterTSMNotificationUrlResponse()
    response._code = resp.get("code")
    response._message = resp.get("message")
    return response

def logSendAPDUSecurityParams(sendApduParams):
	logInfo('------------------------------')
	logInfo("*** DB SendAPDUSecurityParams ***")
	logInfo("useCattpSecurityForPush <%d>"%(int(sendApduParams.useCattpSecurityForPush)))
	logInfo("smsAppTypeId <%d>"%(sendApduParams.smsAppTypeId))
	logInfo("smsTar <%s>"%(sendApduParams.smsTar))
	logInfo("smsClassByte <%s>"%(sendApduParams.smsClassByte))
	logInfo("smsSecurity.securityDesc <%s>"%(sendApduParams.smsSecurity.securityDesc))
	logInfo("smsSecurity.kicIndex <%d>"%(sendApduParams.smsSecurity.kicIndex))
	logInfo("smsSecurity.kidIndex <%d>"%(sendApduParams.smsSecurity.kidIndex))
	logInfo("cattpAppTypeId <%d>"%(sendApduParams.cattpAppTypeId))
	logInfo("cattpTar <%s>"%(sendApduParams.cattpTar))
	logInfo("cattpClassByte <%s>"%(sendApduParams.cattpClassByte))
	logInfo("cattpSecurity.securityDesc <%s>"%(sendApduParams.cattpSecurity.securityDesc))
	logInfo("cattpSecurity.kicIndex <%d>"%(sendApduParams.cattpSecurity.kicIndex))
	logInfo("cattpSecurity.kidIndex <%d>"%(sendApduParams.cattpSecurity.kidIndex))
	logInfo("counter <%s>"%(sendApduParams.counter))
	logInfo("ciphering <%s>"%(sendApduParams.ciphering))
	logInfo("crypto <%s>"%(sendApduParams.crypto))
	logInfo("por <%s>"%(sendApduParams.por))
	logInfo("por_response <%s>"%(sendApduParams.por_response))
	logInfo("por_ciphering <%s>"%(sendApduParams.por_ciphering))
	logInfo("por_security <%s>"%(sendApduParams.por_security))
	logInfo("kic_algo <%s>"%(sendApduParams.kic_algo))
	logInfo("kic_des_mode <%s>"%(sendApduParams.kic_des_mode))
	logInfo("kid_algo <%s>"%(sendApduParams.kid_algo))
	logInfo("kid_des_mode <%s>"%(sendApduParams.kid_des_mode))
	logInfo("kik_algo <%s>"%(sendApduParams.kik_algo))
	logInfo("kik_des_mode <%s>"%(sendApduParams.kik_des_mode))
	logInfo("smsKeyType <%s>"%(sendApduParams.smsKeyType))
	logInfo("cattpKeyType <%s>"%(sendApduParams.cattpKeyType))
	logInfo('------------------------------')
	
def validateEnhancedSendAPDU(fields):

	# Variable initialization
	code = 0
	message = 'OK'
	tsmReferenceId = 0
	requestID = 0

	MSISDN_PROV = 'MSISDN_PROVISIONED'
	MSISDN_NPROV = 'MSISDN_NOT_PROVISIONED'
	TAR_PROV = 'TAR_PROVISIONED'
	TAR_NPROV = 'MSISDN_NOT_PROVISIONED'
	GET_APDU_DATA_INIT = 'APDU_INITIALIZED'
	GET_APDU_DATA_OK = 'GET_APDU_OK'
	GET_APDU_DATA_NOK = 'GET_APDU_NOT_OK'
	GET_KEYSET_DATA_INIT = 'KEYSET_INITIALIZED'
	GET_KEYSET_DATA_OK = 'KEYSET_DATA_OK'
	GET_KEYSET_DATA_NOK = 'KEYSET_DATA_NOT_OK'

	msisdnErr = 'This is mandatory since MSISDN is not provisioned'
	tarErr = 'This is mandatory since TAR is not provisioned'
	dbErr = 'This is mandatory since no default data retrieved from DB'

	fieldName = ''
	missingFlag = 0
	dbAppTypeId = 0
	dbSecurityDesc = ''
	counterId = 0
	
	# Extract fields from dictionary
	sessionID = fields['sessionID']
	userName = fields['userName']
	tsmReferenceId = fields['tsmReferenceId']
	bearer = getBearer(fields['bearer'])
	targetType = getTargetType(fields['targetType'])
	target = fields['target']
	media = getMedia(fields['media'])
	pid = fields['pid']
	validityPeriod = fields['validityPeriod']
	tar = fields['tar']
	apdu = fields['apdu'] # char len/280 must not exceed to 30 chars
	
	secu = fields['secu']
	
	spiHex = secu['spi']
	kicHex = secu['kic']
	kidHex = secu['kid']
	keyType = secu['keyType']
	
	keyset = fields['keyset']
	keySetVersion = keyset['keySetVersion']
	kicValue = keyset['kicValue']
	kidValue = keyset['kidValue']
	kikValue = keyset['kikValue']
	counter = keyset['counter']

	# MSISDN checking: to display the associated msisdn based from ICCID if existing in db, empty if not
	if targetType == 0: # targetTypeMsisdn
		msisdn = target
	elif targetType == 1: # targetTypeIccid
		try:
			dbmsisdn = subsFactory.getSubscriber("").getMSISDN(target)
			if dbmsisdn == " ": # No MSISDN linked to ICCID
				dbmsisdn = ""
			msisdn = dbmsisdn
		except SubscriberException, subsExcept:
			msisdn = ''
		except Exception, e:
			msisdn = ''
	else: # No targetType input yet
		msisdn = ''
	
	# Session validation
	st = getAlmReInitAPI(sessionID)
	if st != 0:
		return st, 'getAlmReInitAPI failed', msisdn, tsmReferenceId

	# Username validation
	if userName == '':
		code = alm.almErr_INVALID_FIELD
		return code, 'userName', msisdn, tsmReferenceId

	else: #userName != '':
		st, UserParamInfoMMI = alm.almGetUser(userName)
		if st != 0:
			code = alm.almProvision_USER_NAME_NOT_EXIST
			return code, 'USER_NAME_NOT_EXIST', msisdn, tsmReferenceId

	# TSM Reference ID validation
	if tsmReferenceId <= 0:
		code = alm.almErr_INVALID_TSM_REF_ID
		return code, 'INVALID_TSM_REF_ID', msisdn, tsmReferenceId

	# Bearer validation
	if bearer == _0_ci.Media_SMSCATTP or bearer == alm.almErr_INVALID_ENUM_VALUE:
		code = alm.almErr_INVALID_ENUM_VALUE
		return code, 'INVALID_BEARER', msisdn, tsmReferenceId

	# Target type validation
	if targetType == alm.almErr_INVALID_ENUM_VALUE:
		code = alm.almErr_INVALID_ENUM_VALUE
		return code, 'Target Type', msisdn, tsmReferenceId
		
	# MSISDN validation
	if target == '':
		code = alm.almErr_INVALID_FIELD
		return code, 'target', msisdn, tsmReferenceId
	
	msisdnFlag = MSISDN_PROV
	if targetType == 0: # targetTypeMsisdn
		try:
			isMsisdnProv = bool(subsFactory.getSubscriber(msisdn).isMsisdnExisting())
		except SubscriberException, subsExcept:
			message = "Subscriber Exception: %s"% (subsExcept.errorMessage)
		except Exception, e:
			message = "CORBA Exception: %s"%(e)
		
		if message != 'OK':
			code = alm.almErr_CORBA_DAO_RELATED
			return code, message, msisdn, tsmReferenceId
		
		if isMsisdnProv == False:
			msisdnFlag = MSISDN_NPROV
			code = alm.almProvision_REFERENCED_MSISDN_NOT_EXIST
			return code, 'REFERENCED_MSISDN_NOT_EXIST', msisdn, tsmReferenceId
			
	if targetType == 1: # targetTypeIccid
		try:
			dbmsisdn = subsFactory.getSubscriber("").getMSISDN(target)
		except SubscriberException, subsExcept:
			message = "Subscriber Exception: %s"% (subsExcept.errorMessage)
		except Exception, e:
			message = "CORBA Exception: %s"%(e)
			
		if message != 'OK':
			code = alm.almErr_CORBA_DAO_RELATED
			return code, message, msisdn, tsmReferenceId
			
		if dbmsisdn == " ": # No MSISDN linked to ICCID
			dbmsisdn = ""
			code = alm.almProvision_REFERENCED_ICCID_NOT_EXIST
			return code, "Iccid not exist", msisdn, tsmReferenceId
		else:
			target = dbmsisdn
			
	# Media validation
	if media == alm.almErr_INVALID_ENUM_VALUE:
		code = alm.almErr_INVALID_ENUM_VALUE
		return code, message, msisdn, tsmReferenceId
		
	# PID validation
	if pid == '':
		pid = '7F'
	
	if pid.upper() != '7F':
		code = alm.almErr_INVALID_FIELD
		return code, 'Bad pid', msisdn, tsmReferenceId
		
	# Validity period validation
	if validityPeriod < 0:
		code = alm.almErr_INVALID_FIELD
		return code, 'Wrong validityPeriod', msisdn, tsmReferenceId
		
	# TAR validation
	# tarFlag = TAR_PROV
	if validateTAR(tar) == NOT_OK:
		code = alm.almErr_TAR_MUST_BE_IN_HEXA_FORMAT
		return code, 'Bad TAR value', msisdn, tsmReferenceId

	# APDU validation
	if validateAPDU(apdu) == NOT_OK:
		code = alm.almErr_APDU_MUST_BE_IN_HEXA_FORMAT
		return code, 'APDU not in hexa', msisdn, tsmReferenceId


	apduDataFlag = GET_APDU_DATA_INIT # MSISDN is not yet provisioned
	if msisdnFlag == MSISDN_PROV: # MSISDN has been provisioned
		try:
			# Call to DAO to retrieve values from DB
			sendApduParams = _0_ci.SendApduSecurityParameters
			sendApduParams = subsFactory.getSubscriber(target).getSendApduSecurityParameters()
			logSendAPDUSecurityParams(sendApduParams)
			apduDataFlag = GET_APDU_DATA_OK # MSISDN is provisioned and there is an apdu data fetched
		except SubscriberException, subsExcept:
			apduDataFlag = GET_APDU_DATA_NOK # MSISDN is provisioned and apdu data is no apdu data fetched
			code = alm.almErr_APDU_MUST_BE_IN_HEXA_FORMAT
			message= 'getSendApduSecurityParameters failed'
			return code, message, msisdn, tsmReferenceId
		except Exception, e:
			code = alm.almErr_CORBA_DAO_RELATED
			message = "CORBA Exception: %s"%(e)
			return code, message, msisdn, tsmReferenceId

		if apduDataFlag == GET_APDU_DATA_OK:
			if bearer == _0_ci.Media_SMS:
				dbAppTypeId = sendApduParams.smsAppTypeId
				dbTar = sendApduParams.smsTar
				dbClassByte = sendApduParams.smsClassByte
				dbSecurityDesc = sendApduParams.smsSecurity.securityDesc
				dbKicIndex = sendApduParams.smsSecurity.kicIndex
				dbKidIndex = sendApduParams.smsSecurity.kidIndex
				dbKeyType = sendApduParams.smsKeyType
			
			if bearer ==_0_ci.Media_CATTP:
				dbAppTypeId = sendApduParams.cattpAppTypeId
				dbTar = sendApduParams.cattpTar
				dbClassByte = sendApduParams.cattpClassByte
				dbSecurityDesc = sendApduParams.cattpSecurity.securityDesc
				dbKicIndex = sendApduParams.cattpSecurity.kicIndex
				dbKidIndex = sendApduParams.cattpSecurity.kidIndex
				dbKeyType = sendApduParams.cattpKeyType
	
	keysetDataFlag = GET_KEYSET_DATA_INIT
	if msisdnFlag == MSISDN_PROV:
		try:
			# logInfo("getting keyset data")
			keySetData = _0_ci.KeysetData
			keySetData = subsFactory.getSubscriber(target).getKeysetValue(keySetVersion)
			keysetDataFlag = GET_KEYSET_DATA_OK
			counterId = keySetData.counterId
			logInfo('------------------------------')
			logInfo('*** DB Keyset Data Params ***')
			logInfo("keySetVersion <%s>"%(keySetVersion))
			logInfo("kicValue <%s>"%(keySetData.kicValue))
			logInfo("kidValue <%s>"%(keySetData.kidValue))
			logInfo("counterId <%d>"%(keySetData.counterId))
			logInfo("counterValue <%s>"%(keySetData.counterValue))
			logInfo('------------------------------')
		except SubscriberException, subsExcept:
			keysetDataFlag = GET_KEYSET_DATA_NOK
			code = alm.almErr_CORBA_DAO_RELATED
			message = "%s Subscriber Exception: %s"%(alm.almStatusToString(code), subsExcept.errorMessage)
			return code, message, msisdn, tsmReferenceId
		except Exception, e:
			code = alm.almErr_CORBA_DAO_RELATED
			message = "CORBA Exception: %s"%(e)
			return code, message, msisdn, tsmReferenceId

	if counter == '' or counter == None:
		if msisdnFlag == MSISDN_NPROV or keysetDataFlag == GET_KEYSET_DATA_NOK or keysetDataFlag == GET_KEYSET_DATA_INIT:
			missingFlag = 1
			fieldName = 'counter'
		else:
			counter = keySetData.counterValue
	
	if kidValue == '' or kidValue == None:
		if msisdnFlag == MSISDN_NPROV or keysetDataFlag == GET_KEYSET_DATA_NOK or keysetDataFlag == GET_KEYSET_DATA_INIT:
			missingFlag = 1
			fieldName = 'kidValue'
		else:
			kidValue = keySetData.kidValue
	
	if kicValue == '' or kicValue == None:
		if msisdnFlag == MSISDN_NPROV or keysetDataFlag == GET_KEYSET_DATA_NOK or keysetDataFlag == GET_KEYSET_DATA_INIT:
			missingFlag = 1
			fieldName = 'kicValue'
		else:
			kicValue = keySetData.kicValue
	
	logInfo('------------------------------')
	logInfo('keySetVersion: %s' % (keySetVersion))
	logInfo('kicValue: %s' % (kicValue))
	logInfo('kidValue: %s' % (kidValue))
	logInfo('kikValue: %s' % (kikValue))
	logInfo('counter: %s' % (counter))

	if missingFlag == 1:
		code = alm.almErr_INVALID_FIELD
		
		if msisdnFlag == MSISDN_NPROV:
			message = 'Keyset %s: %s' % (fieldName, msisdnErr)
		# elif tarFlag == TAR_NPROV:
			# message = 'Keyset %s: %s' % (fieldName, tarErr)
		elif keysetDataFlag == GET_KEYSET_DATA_NOK or keysetDataFlag == GET_KEYSET_DATA_INIT:
			message = 'Keyset %s: %s' % (fieldName, dbErr)

		return code, message, msisdn, tsmReferenceId

	secu = {
		'spi': spiHex,
		'kic': int(kicHex,16),
		'kid': int(kidHex,16),
		'keyType': keyType
	}
	
	keySet = {
		'keySetVersion': keySetVersion,
		'kicValue': kicValue,
		'kidValue': kidValue,
		'kikValue': kikValue,
		'counterId': counterId,
		'counter': counter
	}
	
	fields = {
	'sessionID': sessionID,
	'userName': userName,
	'tsmReferenceId': tsmReferenceId,
	'bearer': bearer,
	'targetType': targetType,
	'target': target,
	'media': media,
	'pid': pid,
	'validityPeriod': validityPeriod,
	'tar': tar,
	'appTypeId': dbAppTypeId,
	'secuDesc': dbSecurityDesc,
	'apdu': apdu,
	'secu': secu,
	'keyset': keySet,
	'kic_desMode': '01',
	'kid_desMode': '01'
	}
	response = sdApiHelper.enhancedSendAPDU(fields)
	
	if response['code'] == 0:
		logSoapExecutionTime("EnhancedSendAPDU", target, tsmReferenceId)
	elif response['code'] == alm.almErr_CARD_ENGAGED:
		try:
			activity = 'A'
			subscriber = subsFactory.getSubscriber(msisdn).changeCardState(activity)
		except SubscriberException, subsExcept:
			message = "Subscriber Exception: %s"% (subsExcept.errorMessage)
		except Exception, e:
			message = "CORBA Exception: %s"%(e)

		if message != 'OK':
			code = alm.almErr_CORBA_DAO_RELATED
			return code, message, msisdn, tsmReferenceId
	
	return response['code'], response['message'], response['msisdn'], response['tsmReferenceId']
	
def EnhancedSendAPDU(ps):
	logInfo("Calling function EnhancedSendAPDU")

	try:
		#request = ps.Parse(EnhancedSendAPDURequest)
		request, response = ALM_helper.ALMHelper(ALM_server.ALM).soap_EnhancedSendAPDU(ps)
		logInfo("Request object is " + str(request))
	except:
	        try:
                        logInfo("******************************TRACEBACK*****************************")
                        traceback.print_exc(file=open(environ['SIMphonIC']+'/asp/'+ environ['ASP_NAME']+'/mmi/alm/soap/log/almzsi.log', 'a+b'))
                        logInfo("**************************END TRACEBACK*****************************")
                except:
                        logInfo("Failed to write traceback")

		response = EnhancedSendAPDUResponse()
		response._code = alm.almErr_ENHANCED_SEND_APDU_REQUEST
		response._message = alm.almStatusToString(alm.almErr_ENHANCED_SEND_APDU_REQUEST)
		response._msisdn = ''
		response._tsmReferenceId = 0
		response._requestID = 0
		#raise Error(alm.almErr_ENHANCED_SEND_APDU_REQUEST, alm.almStatusToString(alm.almErr_ENHANCED_SEND_APDU_REQUEST))

	return response
