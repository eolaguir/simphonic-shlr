## File:        ApduGenerator.py
## Description: Generates APDU based on a template from the configuration file.
## Author:      Oleg Usoltsev <o.usoltsev@external.oberthur.com>
## Created:     12 August 2013
## Verified:    15 August 2013

import os

from shlr_tools import Cfg

# Set to True to turn off debugging messages.
NDEBUG = True

class ApduGenerator:
    """
    APDU generator uses APDU template stored in the configuration file to generate APDU
    using supplied values.
    
    Prerequisites:
        1. SHLR_APDU_DEF_CFG environment variable must be set to point to a directory
           where the configuration file with templates has been placed.
    
    The format of template parameter:
        <Parameter>[_<Modifier>_<Fill Character>_<Length>]
    where:
        1. Parameter is a mandatory name of the template parameter, e.g. MSISDN.
        2. Modifier is an optional attribute that indicates modification that must be
           applied to the input value.
        3. Fill Character is a mandatory attribute if LEFT or RIGHT alignment modifier
           is used. It specifies a hex character (e.g. 'F') that must be used to fill
           the field if the length of the input value is less than given one.
        4. Length is the maximum length of the field.
    
    Sample template parameter:
        MSISDN_REAL_CONVERTED_LEFT_F_22
        Parameter = MSISDN
        Modifiers = REAL, CONVERTED, LEFT
        Fill Char = F
        Length    = 22
    
    How to use APDU generator: see simple unit test at the end.
    """

    # Sections in the configuration file.
    HRS_LTE_SECTION = "HRS_LTE"

    # Template parameters.
    RID     = "RID"         # Request ID.
    TID     = "TID"         # Transaction ID.
    CONFIG  = "CONFIG"
    MSISDN  = "MSISDN"
    MNC     = "MNC"
    SMSP    = "SMSP"
    IMSI    = "IMSI"
    
    # Modifiers that can be used with a template parameter.
    MOD_ALIGN_LEFT  = "LEFT"
    MOD_ALIGN_RIGHT = "RIGHT"
    MOD_REAL        = "REAL"
    MOD_FAKE        = "FAKE"
    MOD_CONVERTED   = "CONVERTED"
    MOD_ROAMING     = "ROAMING"

    # Lengths that can be computed for a template parameter.
    APDU_LENGTH_PARAMS = (
        "LENGTH_BYTE",
        "LENGTH_DIGIT")

        # Characters used to designate LENGTH_* parameters.
    TMPL_LENGTH_SEPARATOR = "[]"
    
    # Template parameter separator.
    TMPL_PARAM_SEPARATOR = "_"
    
    # The fill character used by default.
    DEFAULT_FILL_CHAR = "F"
    
    # The TID length is constant, in bytes.
    DEFAULT_TID_LENGTH = 4
    
    def __init__(self, command):
    
        # Dynamic binding.
        self.__processors = {
                ApduGenerator.RID       : self._processRequestId,
                ApduGenerator.MSISDN    : self._processMsisdn,
                ApduGenerator.IMSI      : self._processImsi,
                ApduGenerator.MNC       : self._processMnc,
                ApduGenerator.SMSP      : self._processSmsp,
                ApduGenerator.TID       : self._processTid,
                ApduGenerator.CONFIG    : self._processConfig }

        self.__modifiers = {
                ApduGenerator.MOD_ALIGN_LEFT    : self._modAlignLeft,
                ApduGenerator.MOD_ALIGN_RIGHT   : self._modAlignRight }
                
        # May throw an exception.
        cfgFile = os.environ.get("SHLR_APDU_DEF_CFG")
        config  = Cfg(cfgpath = cfgFile, raiseIOError = 1)
        self.__template = config.getvalue(label = command, default = [])

    def getApdu(self, request):
    
        # Parse the template.
        if not NDEBUG:
            print "==========================================="
            print "Template: ", self.__template
            print "-------------------------------------------"

        # Generate APDU.
        apdu = []
        for tmplParam in self.__template:
            processed = False # True, if a template parameter has been processed.
        
            for apduParam in self.__processors.keys():

                # Parameter can be complex.
                if apduParam not in tmplParam.upper():
                    continue
    
                if not NDEBUG:
                    print "Processing [%s ; %s]" % (apduParam, tmplParam)
                    
                nibbles = self.__processors.get(apduParam)(tmplParam, request)
                if nibbles is not None:
                    apdu.append(nibbles)
                
                # No need to go further.
                processed = True
                break
        
            # If template parameter has not been processed, it's not a template.
            # So we simply add the parameter as is.
            if not processed:
                if not NDEBUG:
                    print "Adding as is [%s]" % tmplParam
            
                apdu.append(tmplParam)

        return (''.join(apdu)).upper()

    def _processRequestId(self, request):
        pass

    def _processMsisdn(self, tmpl, request):
        nibbles = []
        
        if self._hasLength(tmpl):
            items = tmpl.replace(ApduGenerator.TMPL_LENGTH_SEPARATOR[0], " ") \
                        .replace(ApduGenerator.TMPL_LENGTH_SEPARATOR[1], " ") \
                        .split()
            
            if not NDEBUG:
                print "MSISDN items:", items
            
            for item in items:
                if item in ApduGenerator.APDU_LENGTH_PARAMS:
                    
                    # Length must be computed after conversion, if required,
                    # but before any modifier is applied.
                    pass
                elif ApduGenerator.MSISDN in item:
                    msisdn = self._processMsisdn(item, request)
                    nibbles.append(str(msisdn))
        else:
            msisdn = None
            items  = tmpl.split(ApduGenerator.TMPL_PARAM_SEPARATOR)
            
            if not NDEBUG:
                print "MSISDN items:", items
            
            # Process template parameters according to the prescribed format.
            # The first item must be a parameter name.
            if len(items) == 1:
                nibbles.append(str(msisdn))
            else:
                if ApduGenerator.MOD_REAL in items:
                    msisdn = request.msisdnReal
                    items.remove(ApduGenerator.MOD_REAL)

                    if not NDEBUG:
                        print "Real MSISDN:", msisdn
                        print "MSISDN items:", items
                
                if ApduGenerator.MOD_CONVERTED in items:
                    msisdn = self._convertMsisdn(msisdn)
                    items.remove(ApduGenerator.MOD_CONVERTED)
                    
                    if not NDEBUG:
                        print "Converted MSISDN:", msisdn
                        print "MSISDN items:", items
                
                length = len(msisdn) / 2
                nibbles.append("%02X" % length)

                if not NDEBUG:
                    print "MSISDN length: %02X" % length
                
                # Check if there are any modifier remains.
                if len(items) > 1:
                    msisdn = self.__modifiers.get(items[1])(items[2:], msisdn)
                
                nibbles.append(str(msisdn))

        nibbles = ''.join(nibbles)
        
        if not NDEBUG:
            print "Returning processed MSISDN:", nibbles
                
        return nibbles

    def _processMnc(self, tmpl, request):
        
        # MNC computed using the real IMSI.
        mnc = request.imsiReal[4] + request.imsiReal[3]
        
        if not NDEBUG:
            print "Returning MNC:", mnc
        
        return mnc

    def _processImsi(self, tmpl, request):
        nibbles = []
        
        if self._hasLength(tmpl):
            items = tmpl.replace(ApduGenerator.TMPL_LENGTH_SEPARATOR[0], " ") \
                        .replace(ApduGenerator.TMPL_LENGTH_SEPARATOR[1], " ") \
                        .split()
            
            if not NDEBUG:
                print "IMSI items:", items
            
            for item in items:
                if item in ApduGenerator.APDU_LENGTH_PARAMS:
                    
                    # Length must be computed after conversion, if required,
                    # but before any modifier is applied.
                    pass
                elif ApduGenerator.IMSI in item:
                    imsi = self._processImsi(item, request)
                    nibbles.append(str(imsi))
        else:
            imsi  = None
            items = tmpl.split(ApduGenerator.TMPL_PARAM_SEPARATOR)
            
            if not NDEBUG:
                print "IMSI items:", items
            
            # Process template parameters according to the prescribed format.
            # The first item must be a parameter name.
            if len(items) == 1:
                nibbles.append(str(imsi))
            else:
                if ApduGenerator.MOD_REAL in items:
                    imsi = request.imsiReal
                    items.remove(ApduGenerator.MOD_REAL)

                    if not NDEBUG:
                        print "Real IMSI:", imsi
                        print "IMSI items:", items
                
                if ApduGenerator.MOD_CONVERTED in items:
                    imsi = self._convertImsi(imsi)
                    items.remove(ApduGenerator.MOD_CONVERTED)
                    
                    if not NDEBUG:
                        print "Converted IMSI:", imsi
                        print "IMSI items:", items
                
                length = len(imsi) / 2
                nibbles.append("%02X" % length)

                if not NDEBUG:
                    print "IMSI length: %02X" % length
                
                # Check if there are any modifier remains.
                if len(items) > 1:
                    imsi = self.__modifiers.get(items[1])(items[2:], imsi)
                
                nibbles.append(str(imsi))

        nibbles = ''.join(nibbles)
        
        if not NDEBUG:
            print "Returning processed IMSI:", nibbles
                
        return nibbles
        
    def _processSmsp(self, tmpl, request):
        nibbles = []
        
        if self._hasLength(tmpl):
            items = tmpl.replace(ApduGenerator.TMPL_LENGTH_SEPARATOR[0], " ") \
                        .replace(ApduGenerator.TMPL_LENGTH_SEPARATOR[1], " ") \
                        .split()
            
            if not NDEBUG:
                print "SMSP items:", items
            
            for item in items:
                if item in ApduGenerator.APDU_LENGTH_PARAMS:
                    
                    # Length must be computed after conversion, if required,
                    # but before any modifier is applied.
                    pass
                elif ApduGenerator.SMSP in item:
                    smsp = self._processSmsp(item, request)
                    nibbles.append(str(smsp))
        else:
            smsp  = request.smspReal
            items = tmpl.split(ApduGenerator.TMPL_PARAM_SEPARATOR)
            
            if not NDEBUG:
                print "SMSP items:", items
            
            # Process template parameters according to the prescribed format.
            # The first item must be a parameter name.
            if len(items) == 1:
                nibbles.append(str(smsp))
            else:
                if ApduGenerator.MOD_CONVERTED in items:
                    smsp = self._convertMsisdn(request.smspReal)
                    items.remove(ApduGenerator.MOD_CONVERTED)
                    
                    if not NDEBUG:
                        print "Converted SMSP:", smsp
                        print "SMSP items:", items
                
                length = len(smsp) / 2
                nibbles.append("%02X" % length)

                if not NDEBUG:
                    print "SMSP length: %02X" % length
                
                # Check if there are any modifier remains.
                if len(items) > 1:
                    smsp = self.__modifiers.get(items[1])(items[2:], smsp)
                    nibbles.append(str(smsp))

        nibbles = ''.join(nibbles)
        
        if not NDEBUG:
            print "Returning processed SMSP:", nibbles
                
        return nibbles
    
    def _processTid(self, tmpl, request):
        nibbles = []
        lenRequired = False
        
        if self._hasLength(tmpl):
            items = tmpl.replace(ApduGenerator.TMPL_LENGTH_SEPARATOR[0], " ") \
                        .replace(ApduGenerator.TMPL_LENGTH_SEPARATOR[1], " ") \
                        .split()
            
            if not NDEBUG:
                print "TID items:", items
            
            for item in items:
                if item in ApduGenerator.APDU_LENGTH_PARAMS:
                
                    # Length is constant. No specific behaviour.
                    pass
                elif ApduGenerator.TID in item:
                    tid = self._processTid(item, request)
                    nibbles.append(str(tid))
        else:
            tid   = request.tid
            items = tmpl.split(ApduGenerator.TMPL_PARAM_SEPARATOR)
            
            if not NDEBUG:
                print "TID items:", items
            
            nibbles.append("%02X" % ApduGenerator.DEFAULT_TID_LENGTH)
            
            # Process template parameters according to the prescribed format.
            # The first item must be a parameter name.
            if len(items) == 1:
                nibbles.append(str(tid))
            else:
                tid = self.__modifiers.get(items[1])(items[2:], tid)
                nibbles.append(str(tid))

        nibbles = ''.join(nibbles)
        
        if not NDEBUG:
            print "Returning processed TID:", nibbles
                
        return nibbles
        
    def _processConfig(self, tmpl, request):
        nibbles = []
        
        if self._hasLength(tmpl):
            items = tmpl.replace(ApduGenerator.TMPL_LENGTH_SEPARATOR[0], " ") \
                        .replace(ApduGenerator.TMPL_LENGTH_SEPARATOR[1], " ") \
                        .split()
            
            if not NDEBUG:
                print "CONFIG items:", items
            
            for item in items:
                if item in ApduGenerator.APDU_LENGTH_PARAMS:
                    pass
                elif ApduGenerator.CONFIG in item:
                    config = self._processConfig(item, request)
                    nibbles.append(str(config))
        else:
            config = request.config
            items  = tmpl.split(ApduGenerator.TMPL_PARAM_SEPARATOR)
            
            if not NDEBUG:
                print "CONFIG items:", items
            
            # Process template parameters according to the prescribed format.
            # The first item must be a parameter name.
            if len(items) == 1:
                nibbles.append(str(config))
            else:
                config = self.__modifiers.get(items[1])(items[2:], config)
                nibbles.append(str(config))

        nibbles = ''.join(nibbles)
        
        if not NDEBUG:
            print "Returning processed CONFIG:", nibbles
                
        return nibbles

    def _hasLength(self, tmpl):
        for param in ApduGenerator.APDU_LENGTH_PARAMS:
            if param in tmpl:
                return True
        
        return False

    def _modAlignLeft(self, *params):
        fillChar = params[0][0]
        maxLen   = int(params[0][1])
        value    = str(params[1])
    
        if not NDEBUG:
            print "Applying left-align modifier using", params
        
        count = maxLen - len(value) # Number of fill characters to append.
        modValue = [value]
        modValue.append(count * fillChar)
        modValue = ''.join(modValue)
        
        if not NDEBUG:
            print "Left-aligned %s: %s" % (value, modValue)
        
        return modValue
    
    def _modAlignRight(self, *params):
        fillChar = params[0][0]
        maxLen   = int(params[0][1])
        value    = str(params[1])
    
        if not NDEBUG:
            print "Applying right-align modifier using", params
        
        count = maxLen - len(value) # Number of characters to prepend.
        modValue = []
        modValue.append(count * fillChar)
        modValue.append(value)
        modValue = ''.join(modValue)
        
        if not NDEBUG:
            print "Right-aligned %s: %s" % (value, modValue)
        
        return modValue
    
    def _convertImsi(self, original):
        imsi = []
        nibbles = []
        nibbles.append("9%s" % original)

        count = 16 - len(nibbles[0]) # Total number of fill characters to add.
        nibbles.append(count * ApduGenerator.DEFAULT_FILL_CHAR)
        nibbles = ''.join(nibbles)
        
        i = 0
        while (i < 16):
            imsi.append(nibbles[i + 1] + nibbles[i])
            i += 2
        
        imsi = ''.join(imsi)
        
        return imsi
     
    def _convertMsisdn(self, original):
        origLen = len(original)
        nibbles = []
        
        msisdn = original
        if (msisdn.startswith("+")):
            nibbles.append("91")
            origLen -= 1
            msisdn = original[1:]
        else:
            nibbles.append("81")
        
        if ((len(msisdn) % 2) == 1):
            msisdn += "F"
        
        i = 0
        while (i < origLen):
            nibbles.append(msisdn[i + 1] + msisdn[i])
            i = i + 2
        
        nibbles = ''.join(nibbles)
        
        return nibbles

# Simple unit test.
if __name__ == "__main__":
    class ActivateHRS_LTE: pass

    SAMPLE_APDU_1 = """c10400000016c4050400000015c582c60c0791551010010201ffffffff\
c7077f106f42001c01c7077f108f42001c01c60101c7063F005F490000c609087942019180845651\
c7067f206f070000c60bffffffff27f4010000ff01c7067f206f7e0000c60effffffffffffff27f4\
010000ff01c7067f206f530000c60c0791557169523002ffffffffc7077f106f40000f01"""

    SAMPLE_APDU_2 = """c10400000004c4050400000015c582c60c0791551010010201ffffffff\
c7077f106f42001c01c7077f108f42001c01c60101c7063F005F490000c609087942018180683194\
c7067f206f070000c60bffffffff27f4010000ff01c7067f206f7e0000c60effffffffffffff27f4\
010000ff01c7067f206f530000c60c0891551179213484f3ffffffc7077f106f40000f01"""
    
    request                 = ActivateHRS_LTE()
    request.msisdn          = "+557055018910"
    request.labelCommand    = "Activate_HRS_LTE"
    request.tid             = 16
    request.config          = "0400000015"
    request.imsiReal        = "724101908486515"
    request.smspReal        = "+550101102010"
    request.msisdnReal      = "+551796250320"
    
    try:
        gen = ApduGenerator(request.labelCommand);
        apdu = gen.getApdu(request)
    except Exception, e:
        print "FAILED:", e
    
    print "-- TC 1 -------------------------------------------"
    print "APDU:", apdu
    assert(apdu == SAMPLE_APDU_1.upper())

    request.msisdn          = "+557055494878"
    request.labelCommand    = "Activate_HRS_LTE"
    request.tid             = 4
    request.config          = "0400000015"
    request.imsiReal        = "724101808861349"
    request.smspReal        = "+550101102010"
    request.msisdnReal      = "+5511971243483"
    
    try:
       gen = ApduGenerator(request.labelCommand);
       apdu = gen.getApdu(request)
    except Exception, e:
        print "FAILED:", e
    
    print "-- TC 2 -------------------------------------------"
    print "APDU:", apdu
    assert(apdu == SAMPLE_APDU_2.upper())
