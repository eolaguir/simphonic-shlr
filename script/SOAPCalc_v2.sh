#!/bin/sh

nawk '
BEGIN{
	for (i=0; i < 24; i++){
		req_perhour[i]=0;
		max_time_perhour[i]=0;
		total_time_perhour[i]=0;
		aver_time_perhour[i]=0;
		sec[i]=0;
		gt10s[i]=0;
	}
}

{
	time=substr($1,1,2);
	if (time < 10)
		hh=substr($1,2,1)
	else
		hh=time;
	
}

$3 ~ "REQUEST" { 
	req_perhour[hh]+=1; 
}

$6 ~ "sec" { 
	total_time_perhour[hh]+=$5;
	sec[hh]+=1;
	if (max_time_perhour[hh]<$5){
		max_time_perhour[hh]=$5;
	}
	if ($5 > 30){
		gt10s[hh]+=1;
	}
}

END{ 
	printf("[hour]\t\t[req]\t\t[aver_time](s)\t[max_time](s)\t\t[30+ secs]\n");
	for (i=0; i < 24 ; i++){ 
		if (sec[i]!=0){
			aver_time_perhour[i]=total_time_perhour[i]/sec[i];
		}
		printf("%02d:00\t\t%s\t\t%.2f\t\t(%.2f)\t\t\t%d\n",i,req_perhour[i],aver_time_perhour[i],max_time_perhour[i],gt10s[i]);
	}
}
' $1
