#!/bin/bash

export SIMphonIC=/export/home/sfovivo/exe
export PATH=$PATH:/export/home/sfovivo/bin

day=`date +%Y-%m-%d`
hour=`date +%H:%M`
data=$day" "$hour

alm2_run=`scl state | head -24 | tail -1 | awk '{print $4}'`
mtmo=`scl state | head -31 | tail -1|awk '{print $3}'`
alm2=`ps -ef | grep -v grep | grep sh_alm2 | wc -l | sed "s/ //g"`
alm2mem=`ps -eo pmem,args | grep -v grep | grep sh_alm2 | awk '{sum +=$1}; END {print sum}'`

mmi=`ps -eo pmem,args | grep -v grep | grep mmi_admin_alm | wc -l | sed "s/ //g"`
mmimem=`ps -eo pmem,args | grep -v grep | grep mmi_admin_alm | awk '{sum +=$1}; END {print sum}'`

cgi=`ps -eo pmem,args | grep -v grep | grep vivo_service.cgi | wc -l | sed "s/ //g"`
cgimem=`ps -eo pmem,args | grep -v grep | grep vivo_service.cgi | awk '{sum +=$1}; END {print sum}'`

proc=`ps -eo pmem | grep -v %MEM | grep -v - | wc -l | sed "s/ //g"`
mem=`ps -eo pmem | grep -v %MEM | grep -v - | awk '{sum +=$1}; END {print sum}'`

swapuse=`/usr/local/bin/top -b | grep Memory | sed "s/G//g" | awk -e '{print $8 - $11}'` 

echo "$data||alm2:$alm2|%$alm2mem|sh_alm2-active:$alm2_run||sh_mtmoSrv-active:$mtmo||mmi:$mmi|%$mmimem||cgi:$cgi|%$cgimem||tot:$proc|%$mem||swap-use:$swapuse"GB"" >> /export/home/sfovivo/scripts/mem_usage_new.txt

