#!/bin/env python
#******************************************************************************
# file: $Id: _secu.py,v 1.2 2011-04-26 14:49:00 saadamra Exp $
# author: Saad Amrani 
#*****************************************************************************/
import ctypes

PATH_LIB ='./libsecukis.so'
SECU_LIB = ctypes.CDLL(PATH_LIB)

class Structure(ctypes.Structure):
	def __init__(self, arg = None, **kwargs):
		if len(kwargs) != 0:
			ctypes.Structure.__init__(self, **kwargs)
			return
		if arg is not None:
			for field in self._fields_:
				self.__setattr__(field[0], arg.__getattribute__(field[0]))

	def __setattr__(self, attr, value):
		if attr == 'u_data':
			# u_len is implicit in the high level u_data
			value = binascii.unhexlify(value)
			ctypes.Structure.__setattr__(self, 'u_len', len(value))
		try:
			for field in [ fld[0] for fld in self.__getattribute__(attr)._fields_]:
				try:
					self.__getattribute__(attr).__setattr__(field, value.__getattribute__(field))
				except AttributeError:
					pass
		except AttributeError:
			ctypes.Structure.__setattr__(self, attr, value)

	def __getattribute__(self, attr):
		value = ctypes.Structure.__getattribute__(self, attr)
		if attr == 'u_data':
			value = binascii.hexlify(value)
		return value

class crKeyData(Structure):
	_fields_ = [	
			("len", ctypes.c_int),
			("data", ctypes.c_ubyte * 8)
	]


class crKeyData(Structure):
	_fields_ = [	
			("len", ctypes.c_int),
			("data", ctypes.c_char_p)
]


