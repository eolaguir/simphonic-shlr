from zsiTools import *
from mmi_tools import *
from omniORB import CORBA
import CosNaming
import sdapi_idl
import alm
import alm
from sdapi_idl import _0_ci
from ALM_server import RemoveSecurityDomainResponse
from sdApiConfig import SdApiConfigurator
import os
import fcntl
_config = SdApiConfigurator(os.environ['SIMphonIC']+'/asp/'+os.environ['ASP_NAME']+'/mmi/alm/soap/services/sdapi.cfg') 
_config.readConfig()

class SdApiException:
    def __init__(self, message):
        self._message = message

    def getErrorMessage(self):
        return self._message


def getCorbaConnectionDetails():
    if _config.isConfigOk == False:
        raise SdApiException("Bad config file, check logs for details")
        
    root = _config.getRootName()
    system = _config.getSystemName()
    service = _config.getServiceName()
    nb_child = _config.getNbChild()
    logInfo("nb_child = %s" % str(nb_child))
    return root, system, service, nb_child
    
def deleteSD(msisdn, aid):
    code = 1
    message = "Failed to send Remove Security Domain Request, check parameters"

    root, system, service, nb_child = getCorbaConnectionDetails()

    sdapi = sdApiClientConnect(root, system, service, nb_child)
    op = getDefaultOperationParams()
    logInfo('deleting security domain with aid ' + aid + ' for msisdn ' + msisdn)
    reqID = sdapi.deleteSD(str(msisdn), str(aid), op)
    if (reqID >= 0):
        code = 0
        message = "OK"

    return {"code": code, "message": message, "requestID": reqID}

def createSD(msisdn, aid):
    code = 1
    message = "Failed to send Create Security Domain Request, check parameters"

    root, system, service, nb_child = getCorbaConnectionDetails()

    sdapi = sdApiClientConnect(root, system, service, nb_child)
    op = getDefaultOperationParams()
    logInfo('creating security domain with aid ' + aid + ' for msisdn ' + msisdn)
    reqID = sdapi.createSD(str(msisdn), str(aid), op)
    if (reqID >= 0):
        code = 0
        message = "OK"

    return {"code": code, "message": message, "requestID": reqID}

def downloadApplication(msisdn, aidApplication, aidApplet, aidPackage, installParam, appProp, tokenFlag, token):
    code = 1
    message = "Failed to send Download Application Request, check parameters"

    root, system, service, nb_child = getCorbaConnectionDetails()

    sdapi = sdApiClientConnect(root, system, service, nb_child)
    op = getDefaultOperationParams()
    logInfo('downloading application with aid ' + aidApplication + ' for msisdn ' + msisdn)
    reqID = sdapi.downloadApplication(str(msisdn), str(aidApplication), str(aidApplet), str(aidPackage), str(installParam), str(appProp), tokenFlag, str(token), op)
    if (reqID >= 0):
        code = 0
        message = "OK"

    return {"code": code, "message": message, "requestID": reqID}

def downloadOnSD(msisdn, aidApplication, sourceSD, associatedSD, tokenFlag, token):
    code = 1
    message = "Failed to send Download on Security Domain Request, check parameters"

    root, system, service, nb_child = getCorbaConnectionDetails()

    sdapi = sdApiClientConnect(root, system, service, nb_child)
    op = getDefaultOperationParams()
    logInfo('downloading application with aid ' + aidApplication + ' and associated SD ' + associatedSD + ' for msisdn ' + msisdn)
    reqID = sdapi.downloadOnSD(str(msisdn), str(aidApplication), str(sourceSD), str(associatedSD), tokenFlag, str(token), op)
    if (reqID >= 0):
        code = 0
        message = "OK"

    return {"code": code, "message": message, "requestID": reqID}

def getInstallToken(msisdn, packageAid, appletAid, instanceAid, privilege, installParams):
    code = 1
    message = "Failed to send Get Install Token Request, check parameters"

    root, system, service, nb_child = getCorbaConnectionDetails()
    sdapi = sdApiClientConnect(root, system, service, nb_child)

    logInfo('getting install token with aid ' + appletAid + ' for msisdn ' + msisdn)
    reqID, installToken = sdapi.getInstallToken(str(msisdn), str(packageAid), str(appletAid), str(instanceAid), str(privilege), str(installParams))

    installToken = str(installToken.encode('hex'))
    logInfo("installToken: %s" % installToken)

    if (reqID >= 0):
        code = 0
        message = "OK"

    return {"code": code, "message": message, "requestID": reqID, "installToken": installToken}

def registerTSMNotificationUrl(username, aid, urlxsi):
    code = 1
    message = "Failed to send Register TSM Notification Request, check parameters"

    root, system, service, nb_child = getCorbaConnectionDetails()

    sdapi = sdApiClientConnect(root, system, service, nb_child)
    logInfo('registering TSM notification URL with aid ' + aid + ' and URL ' + urlxsi + ' for username ' + username)

    aidList = []
    aidList.append(str(aid))

    status = sdapi.registerTSMNotificationURL(str(username), aidList, str(urlxsi))

    if (status == _0_ci.ADMCMD_SUCCESS):
        code = 0
        message = "OK"

    return {"code": code, "message": message}

def extraditeApplet(msisdn, aidApplet, aidSD, tokenFlag, token):
    code = 1
    message = "Failed to send Extradite Applet Request, check parameters"

    root, system, service, nb_child = getCorbaConnectionDetails()

    sdapi = sdApiClientConnect(root, system, service, nb_child)
    op = getDefaultOperationParams()
    logInfo('extradition of applet with aid ' + aidApplet + 'to security domain with aid ' + aidSD + ' for msisdn ' + msisdn)

    reqID = sdapi.extraditeApplication(str(msisdn), str(aidApplet), str(aidSD), tokenFlag, str(token), op)

    if (reqID >= 0):
        code = 0
        message = "OK"

    return {"code": code, "message": message, "requestID": reqID}


def sdApiClientConnect(root, system, service, nb_child):

    filelock=0

    orb = CORBA.ORB_init([], CORBA.ORB_ID)
    obj = orb.resolve_initial_references("NameService")
    ctx = obj._narrow(CosNaming.NamingContext)

    #try to obtain lock
    fda=open("mmi.current","a")
    try:
        fcntl.flock(fda.fileno(), fcntl.LOCK_EX)
        filelock=1
    except: 
        #in case the file is locked, we will continue anyway 
        logInfo("WARNING: file lock error, check mmi.current")

    try:
        fdr=open("mmi.current","r")
        mmi_child=int(fdr.read())
        fdr.close()
    except:
         mmi_child = 0
         logInfo("defaulting to MMI child 0")

    mmi_child=int(mmi_child)+1;
    if (mmi_child >= nb_child):
        mmi_child=0;
    logInfo('mmi_child = %d'% int(mmi_child))

    if filelock == 1:
        fdw=open("mmi.current","w")
        fdw.write(str(mmi_child))
        fdw.close()

    fda.close()

    rootName = ('%03d_%s' % (mmi_child, root['name']))
    systemName = ('%03d_%s' % (mmi_child, system['name']))
    serviceName = ('%03d_%s' % (mmi_child, service['name']))

    logInfo('force round robin')
    logInfo(rootName)
    logInfo(systemName)
    logInfo(serviceName)

    obj = ctx.resolve([
           CosNaming.NameComponent(rootName, root['kind']),
              CosNaming.NameComponent(systemName, system['kind']),
              CosNaming.NameComponent(serviceName, service['kind'])
           ])

    ctx = obj._narrow(CosNaming.NamingContext)

    obj = ctx.resolve([CosNaming.NameComponent("sd_api","")])
    sd = obj._narrow(_0_ci.SdApiManager)

    return sd

def getDefaultOperationParams():
    sp = _0_ci.SdSecuParams
    sp.pid_flag = False
    sp.pid = "00"
    sp.vp_flag = True
    sp.vp = _config.getMessageVP()

    sc = _0_ci.SdUnitarySched
    sc.date = "0000/00/00"
    sc.hour = "00:00:00"
    sc.prio = _0_ci.AppliPrio_HIGH
    sc.cattpRetry = 3

    sf = _0_ci.SecuFlag_Default

    op = _0_ci.SdOperationParams    
    op.flag_340 = sf
    op.p0340 = sp

    if (_config.getMedia() == "CATTPSMS"):
        op.mediaFlag =  _0_ci.Media_SMSCATTP
    elif (_config.getMedia() == "CATTP"):
        op.mediaFlag =  _0_ci.Media_CATTP
    elif (_config.getMedia() == "CARD_READER"):
        op.mediaFlag =  _0_ci.Media_CARD_READER        
    else:
        op.mediaFlag =  _0_ci.Media_SMS

    
    op.schedule = sc

    
    return op

def unitaryExtraditeOperation(msisdn, userName, aidApplet, aidSD, bearer, tsmRefId):
    code = 1
    message = "Failed to send Unitary Extradite Request, check parameters"

    root, system, service, nb_child = getCorbaConnectionDetails()

    sdapi = sdApiClientConnect(root, system, service, nb_child)
    op = getDefaultOperationParams()
    op.mediaFlag =  getBearer(bearer)

    logInfo('unitary extradition of application with aid ' + aidApplet + 'to security domain with aid ' + aidSD + ' for msisdn ' + msisdn)

    reqID = sdapi.unitaryExtradite(str(msisdn), str(userName), str(aidApplet), str(aidSD), tsmRefId, op)

    tsmReferenceId = tsmRefId
    if (reqID >= 0):
       code = 0
       message = "OK"
    elif (reqID < 0):
       logInfo('reqId %d'  %(reqID))
       code = -reqID
       reqID = -1
       message = "%s" % (alm.almStatusToString(code))

    return {"code": code, "message": message, "requestID": reqID, "tsmReferenceId": tsmReferenceId}
    
def unitaryInstallSelectableOperation(msisdn, userName, aid, installAid, installTar, installParam, pkgAid, bearer, optInstall, tsmRefId):
    code = 1
    message = "Failed to send Unitary Install Selectable Request, check parameters"

    root, system, service, nb_child = getCorbaConnectionDetails()


    sdapi = sdApiClientConnect(root, system, service, nb_child)
    op = getDefaultOperationParams()
    op.mediaFlag =  getBearer(bearer)
    logInfo('optInstall ' + optInstall)
    installOption = getInstallOption(optInstall)
    logInfo('installOption <%d>'%(installOption))

    logInfo('unitary install selectable of application with aid ' + aid + ' for msisdn ' + msisdn)
    reqID = sdapi.installSelectable (str(msisdn), str(userName), str(aid), str(installAid), str(installTar), str(installParam), str(pkgAid), tsmRefId, installOption, op)

    tsmReferenceId = tsmRefId
    if (reqID >= 0):
       code = 0
       message = "OK"
    elif (reqID == -(alm.almErr_APPLICATION_NOT_YET_INSTALLED)):
       logInfo('reqId %d'  %(reqID))
       code = -reqID
       reqID = -1
       message = "Application must be installed before making it selectable"
    elif (reqID < 0):
       logInfo('reqId %d'  %(reqID))
       code = -reqID
       reqID = -1
       message = "%s" % (alm.almStatusToString(code))

    return {"code": code, "message": message, "requestID": reqID, "tsmReferenceId": tsmReferenceId}

def unitaryInstallOperation(msisdn, userName, aid, installAid, installTar, installParam, pkgAid, bearer, tsmRefId):
    code = 1
    message = "Failed to send Unitary Install Request, check parameters"

    root, system, service = getCorbaConnectionDetails()


    sdapi = sdApiClientConnect(root, system, service, nb_child)
    op = getDefaultOperationParams()
    op.mediaFlag =  getBearer(bearer)

    logInfo('unitary install of application with aid ' + aid + ' for msisdn ' + msisdn)
    reqID = sdapi.install4installApplication (str(msisdn), str(userName), str(aid), str(installAid), str(installTar), str(installParam), str(pkgAid), tsmRefId, op)

    tsmReferenceId = tsmRefId
    if (reqID >= 0):
       code = 0
       message = "OK"
    elif (reqID < 0):
       logInfo('reqId %d'  %(reqID))
       code = -reqID
       reqID = -1
       message = "%s" % (alm.almStatusToString(code))

    return {"code": code, "message": message, "requestID": reqID, "tsmReferenceId": tsmReferenceId}

def lockApplication(msisdn, userName, aid, bearer, tsmRefId):
    code = 1
    message = "Failed to send LockApplication request, check parameters"

    root, system, service, nb_child = getCorbaConnectionDetails()

    sdapi = sdApiClientConnect(root, system, service, nb_child)
    op = getDefaultOperationParams()
	
    op.mediaFlag =  getBearer(bearer)
    	
    if ((bearer.upper() != "SMS") and (bearer.upper() != "CATTP") and (bearer.upper() != "CATTP_FALLBACK_TO_SMS")):
	return {"code": alm.almErr_INVALID_ENUM_VALUE, "message": "%s (bearer)" % (alm.almStatusToString(alm.almErr_INVALID_ENUM_VALUE)), "tsmReferenceId": tsmRefId}
		
    logInfo('locking application with aid ' + aid + ' for msisdn ' + msisdn)
  
    reqID = sdapi.lockApplication(str(msisdn), str(userName), str(aid), tsmRefId, op)
    tsmReferenceId = tsmRefId
    if (reqID >= 0):
       code = 0
       message = "OK"
    elif (reqID < 0):
       logInfo('reqId %d'  %(reqID))
       code = -reqID
       reqID = -1
       message = "%s" % (alm.almStatusToString(code))

    return {"code": code, "message": message, "requestID": reqID, "tsmReferenceId": tsmReferenceId}

def unlockApplication(msisdn, userName, aid, bearer, tsmRefId):
    code = 1
    message = "Failed to send UnLockApplication request, check parameters"

    root, system, service, nb_child = getCorbaConnectionDetails()

    sdapi = sdApiClientConnect(root, system, service, nb_child)
    op = getDefaultOperationParams()
	
    op.mediaFlag =  getBearer(bearer)
    	
    if ((bearer.upper() != "SMS") and (bearer.upper() != "CATTP") and (bearer.upper() != "CATTP_FALLBACK_TO_SMS")):
	return {"code": alm.almErr_INVALID_ENUM_VALUE, "message": "%s (bearer)" % (alm.almStatusToString(alm.almErr_INVALID_ENUM_VALUE)), "tsmReferenceId": tsmRefId}
   
    logInfo('creating unlock operation for aid ' + aid + ' for msisdn ' + msisdn) 
 
    reqID = sdapi.unlockApplication(str(msisdn), str(userName), str(aid), tsmRefId, op)
    tsmReferenceId = tsmRefId
    if (reqID >= 0):
       code = 0
       message = "OK"
    elif (reqID < 0):
       logInfo('reqId %d'  %(reqID))
       code = -reqID
       reqID = -1
       message = "%s" % (alm.almStatusToString(code))

    return {"code": code, "message": message, "requestID": reqID, "tsmReferenceId": tsmReferenceId}

def fullcardaudit(msisdn, userName, bearer, tsmRefId):
    code = 1
    message = "Failed to send FullCardAudit request, check parameters"

    root, system, service, nb_child = getCorbaConnectionDetails()

    sdapi = sdApiClientConnect(root, system, service, nb_child)
    op = getDefaultOperationParams()
    op.mediaFlag =  getBearer(bearer)

    logInfo('creating full card audit for msisdn ' + msisdn)
  
    reqID = sdapi.fullcardaudit(str(msisdn), str(userName), tsmRefId, op)
    tsmReferenceId = tsmRefId
    if (reqID >= 0):
       code = 0
       message = "OK"
    elif (reqID < 0):
       logInfo('reqId %d'  %(reqID))
       code = -reqID
       reqID = -1
       message = "%s" % (alm.almStatusToString(code))
    
    return {"code": code, "message": message, "requestID": reqID, "tsmReferenceId": tsmReferenceId}
	
def unitaryRemoveApplication(msisdn, userName, aid, bearer, tsmRefId):
    code = 1
    message = "Failed to send unitaryRemoveApplication request, check parameters"

    root, system, service, nb_child = getCorbaConnectionDetails()

    sdapi = sdApiClientConnect(root, system, service, nb_child)
    op = getDefaultOperationParams()
	
    op.mediaFlag =  getBearer(bearer)
    	
    if ((bearer.upper() != "SMS") and (bearer.upper() != "CATTP") and (bearer.upper() != "CATTP_FALLBACK_TO_SMS")):
	return {"code": alm.almErr_INVALID_ENUM_VALUE, "message": "%s (bearer)" % (alm.almStatusToString(alm.almErr_INVALID_ENUM_VALUE)), "tsmReferenceId": tsmRefId}
	
    logInfo('remove application with aid ' + aid + ' for msisdn ' + msisdn)
  
    reqID = sdapi.unitaryRemoveApplication(str(msisdn), str(userName), str(aid), tsmRefId, op)
    tsmReferenceId = tsmRefId
    if (reqID >= 0):
       code = 0
       message = "OK"
    elif (reqID < 0):
       logInfo('reqId %d'  %(reqID))
       code = -reqID
       reqID = -1
       message = "%s" % (alm.almStatusToString(code))

    return {"code": code, "message": message, "requestID": reqID, "tsmReferenceId": tsmReferenceId}

def unitaryCreateSdOperation(msisdn, userName, aid, installAid, installTar, installParam, keyData, bearer, tsmRefId):
    code = 1
    message = "Failed to send Unitary Create SD Request, check parameters"

    root, system, service, nb_child = getCorbaConnectionDetails()

    sdapi = sdApiClientConnect(root, system, service, nb_child)
    op = getDefaultOperationParams()
    op.mediaFlag =  getBearer(bearer)

    logInfo('unitary create SD of application with aid ' + aid + ' for msisdn ' + msisdn)

    reqID = sdapi.unitaryCreateSd (str(msisdn), str(userName), str(aid), str(installAid), str(installTar), str(installParam), str(keyData), tsmRefId, op)

    tsmReferenceId = tsmRefId
    if (reqID >= 0):
       code = 0
       message = "OK"
    elif (reqID < 0):
       logInfo('reqId %d'  %(reqID))
       code = -reqID
       reqID = -1
       message = "%s" % (alm.almStatusToString(code))

    return {"code": code, "message": message, "requestID": reqID, "tsmReferenceId": tsmReferenceId}

def unitaryRemoveSSD(msisdn, userName, aid, bearer, tsmRefId):
    code = 1
    message = "Failed to send unitaryRemoveSSD request, check parameters"

    root, system, service, nb_child = getCorbaConnectionDetails()

    sdapi = sdApiClientConnect(root, system, service, nb_child)
    op = getDefaultOperationParams()
	
    op.mediaFlag =  getBearer(bearer)
    	
    if ((bearer.upper() != "SMS") and (bearer.upper() != "CATTP") and (bearer.upper() != "CATTP_FALLBACK_TO_SMS")):
	return {"code": alm.almErr_INVALID_ENUM_VALUE, "message": "%s (bearer)" % (alm.almStatusToString(alm.almErr_INVALID_ENUM_VALUE)), "tsmReferenceId": tsmRefId}
	     
    reqID = sdapi.unitaryRemoveSSD(str(msisdn), str(userName), str(aid), tsmRefId, op)
    tsmReferenceId = tsmRefId
    if (reqID >= 0):
       code = 0
       message = "OK"
    elif (reqID < 0):
       logInfo('reqId %d'  %(reqID))
       code = -reqID
       reqID = -1
       message = "%s" % (alm.almStatusToString(code))

    return {"code": code, "message": message, "requestID": reqID, "tsmReferenceId": tsmReferenceId}

def unitaryLoadPackage(msisdn, userName, aid, bearer, tsmRefId):
    code = 1
    message = "Failed to send Unitary Load Package Request, check parameters"

    root, system, service, nb_child = getCorbaConnectionDetails()

    sdapi = sdApiClientConnect(root, system, service, nb_child)
    op = getDefaultOperationParams()
    logInfo('unitary load package with aid ' + aid + ', msisdn ' + msisdn)
	
    media = getBearer(bearer)
    
    op.mediaFlag =  media

    reqID = sdapi.unitaryLoadPackage(str(msisdn), str(userName), str(aid), tsmRefId, op)

    tsmReferenceId = tsmRefId
    if (reqID >= 0):
		code = 0
		message = "OK"
    elif (reqID < 0):
		logInfo('reqId %d'  %(reqID))
		code = -reqID
		reqID = -1
		message = "%s" % (alm.almStatusToString(code))

    return {"code": code, "message": message, "requestID": reqID, "tsmReferenceId": tsmReferenceId}
    
def unitaryExtraditePkgOperation(msisdn, userName, pkgAid, aidSD, bearer, tsmRefId):
    code = 1
    message = "Failed to send Unitary Extradite Request, check parameters"

    root, system, service, nb_child = getCorbaConnectionDetails()

    sdapi = sdApiClientConnect(root, system, service, nb_child)
    op = getDefaultOperationParams()
    op.mediaFlag =  getBearer(bearer)

    logInfo('unitary extradition of package with aid ' + pkgAid + ' to security domain with aid ' + aidSD + ' for msisdn ' + msisdn)

    reqID = sdapi.unitaryExtraditePkg(str(msisdn), str(userName), str(pkgAid), str(aidSD), tsmRefId, op)

    tsmReferenceId = tsmRefId
    if (reqID >= 0):
       code = 0
       message = "OK"
    elif (reqID < 0):
       logInfo('reqId %d'  %(reqID))
       code = -reqID
       reqID = -1
       message = "%s" % (alm.almStatusToString(code))

    return {"code": code, "message": message, "requestID": reqID, "tsmReferenceId": tsmReferenceId}
	
def unitaryRemovePackage(msisdn, userName, aid, bearer, tsmRefId, remAppli):
    code = 1
    message = "Failed to send Unitary Remove Package Request, check parameters"

    root, system, service, nb_child = getCorbaConnectionDetails()

    sdapi = sdApiClientConnect(root, system, service, nb_child)
    op = getDefaultOperationParams()
    logInfo('unitary remove package with aid ' + aid + ', msisdn ' + msisdn)
	
    media = getBearer(bearer)
    logInfo('unitary remove package with aid ' + aid + ', msisdn ' + msisdn)
    op.mediaFlag =  media
	
    reqID = sdapi.unitaryRemovePackage(str(msisdn), str(userName), str(aid), bool(remAppli), tsmRefId, op)

    tsmReferenceId = tsmRefId
    if (reqID >= 0):
		code = 0
		message = "OK"
    elif (reqID < 0):
		logInfo('reqId %d'  %(reqID))
		code = -reqID
		reqID = -1
		message = "%s" % (alm.almStatusToString(code))

    return {"code": code, "message": message, "requestID": reqID, "tsmReferenceId": tsmReferenceId}
    
def sendTextMessageOperation(msisdn, userName, dcs, pid, vp, msg, tsmReferenceId, bearer):
	code = 0
	requestID = 0

	root, system, service, nb_child = getCorbaConnectionDetails()

	sdapi = sdApiClientConnect(root, system, service, nb_child)
	op = getDefaultOperationParams()
	op.mediaFlag =  getBearer(bearer)
	
	logInfo('----------------------------------------')
	logInfo('*** TSM Send Text Message Parameters ***')
	logInfo('msisdn: %s' % (msisdn))
	logInfo('userName: %s' % (userName))
	logInfo('dcs: %s' % (dcs))
	logInfo('pid: %d' % (len(pid)))
	logInfo('vp: %s' % (vp))
	logInfo('msg: %s' % (msg))
	logInfo('-----------------------------------------')
	
	requestID = sdapi.sendTextMessage(str(msisdn), str(userName), str(dcs), str(pid), str(msg), vp, tsmReferenceId, op)
	
	if (requestID < 0):
		code = -requestID
		requestID = -1
		message = "%s" % (alm.almStatusToString(code))

	return status(TSMSTM, code, msisdn, tsmReferenceId, requestID)

def getGsmG0348DESMode(mode):
	if (mode == '01'): # TRIPLE_DES2
		return _0_ci.algo_3des_2key
	elif (mode == '10'): # TRIPLE_DES3
		return _0_ci.algo_3des_3key
	elif (mode == '11'): # DES_ECB
		return _0_ci.algo_1des_ecb
	elif (mode == '00'): # DES_CBC
		return _0_ci.algo_1des_cbc
	else:
		return _0_ci.algo_no

def getGsmG0348Params(spi, counterId, counter, kic, kid, kicValue, kidValue, tar, appTypeId, secuDesc, kic_desMode, kid_desMode):
	g0348Counter = _0_ci.SdGsm0348Counter
	g0348Counter.id = counterId
	g0348Counter.value = counter
	
	g0348 = _0_ci.SdGsm0348Params
	g0348.cmd_set = ''
	g0348.spi = spi
	g0348.cntr = g0348Counter
	g0348.kic = kic
	g0348.kid = kid
	g0348.key_kic = kicValue.strip()
	g0348.l_key_kic = len(g0348.key_kic)
	g0348.key_kid = kidValue.strip()
	g0348.l_key_kid = len(g0348.key_kid)
	g0348.kicdiv = ''
	g0348.kiddiv = ''
	g0348.tar = tar
	g0348.kic_implicit_algo = getGsmG0348DESMode(kic_desMode)
	g0348.kid_implicit_algo = getGsmG0348DESMode(kid_desMode)
	g0348.appTypeId = appTypeId
	g0348.secudesc = secuDesc
	
	return g0348

def enhancedSendAPDU(fields):

	code = 0
	tsmReferenceId = 0
	requestID = 0

	# Extract fields from dictionary
	sessionID = fields['sessionID']
	userName = fields['userName']
	tsmReferenceId = fields['tsmReferenceId']
	bearer = fields['bearer']
	targetType = fields['targetType']
	target = fields['target']
	media = fields['media']
	pid = fields['pid']
	validityPeriod = fields['validityPeriod']
	tar = fields['tar']
	appTypeId = fields['appTypeId']
	secuDesc = fields['secuDesc']
	apdu = fields['apdu'] # char len/280 must not exceed to 30 chars
	apdu = string.replace(apdu, '\n', '')
	
	secu = fields['secu']
	spi = secu['spi']
	kic = secu['kic']
	kid = secu['kid']
	keyType = secu['keyType']
	
	keyset = fields['keyset']
	keySetVersion = keyset['keySetVersion']
	kicValue = keyset['kicValue']
	kidValue = keyset['kidValue']
	kikValue = keyset['kikValue']
	counterId = keyset['counterId']
	counter = keyset['counter']
	
	kic_desMode = fields['kic_desMode']
	kid_desMode = fields['kid_desMode']
	
	root, system, service, nb_child = getCorbaConnectionDetails()
	sdapi = sdApiClientConnect(root, system, service, nb_child)
	op = getDefaultOperationParams()
	op.mediaFlag = bearer
	op.vp = validityPeriod
		

	gsmG0348Params = getGsmG0348Params(spi, counterId, counter, kic, kid, kicValue, kidValue, tar, appTypeId, secuDesc, kic_desMode, kid_desMode)

	logInfo('------------------------------')
	logInfo('*** SDAPI EnhancedSendAPDU Parameters ***')
	logInfo('msisdn: %s' % (target))
	logInfo('userName: %s' % (userName))
	logInfo('apdu: %s' % (apdu))
	logInfo('apdu length: %d' % (len(apdu)))
	logInfo('tsmReferenceId: %s' % (tsmReferenceId))
	logInfo('------------------------------')
	logInfo('------------------------------')
	logInfo('*** G0348 Parameters ***')
	logInfo('cmd_set: %s' % (gsmG0348Params.cmd_set))
	logInfo('spi: %s' % (gsmG0348Params.spi))
	logInfo('counterId: %s' % (gsmG0348Params.cntr.id))
	logInfo('counter: %s' % (gsmG0348Params.cntr.value))
	logInfo('kic length: %s' % (gsmG0348Params.l_key_kic))
	logInfo('kid length: %s' % (gsmG0348Params.l_key_kid))
	logInfo('kic: %s' % (gsmG0348Params.kic))
	logInfo('kid: %s' % (gsmG0348Params.kid))
	logInfo('kicValue: %s' % (gsmG0348Params.key_kic))
	logInfo('kidValue: %s' % (gsmG0348Params.key_kid))
	logInfo('kikValue: %s' % (kikValue))
	logInfo('kicdiv: %s' % (gsmG0348Params.kicdiv))
	logInfo('kiddiv: %s' % (gsmG0348Params.kiddiv))
	logInfo('tar: %s' % (gsmG0348Params.tar))
	logInfo('kic_desMode: %s' % (gsmG0348Params.kic_implicit_algo))
	logInfo('kid_desMode: %s' % (gsmG0348Params.kid_implicit_algo))
	logInfo('appTypeId: %s' % (gsmG0348Params.appTypeId))
	logInfo('secudesc: %s' % (gsmG0348Params.secudesc))
	logInfo('------------------------------')
	logInfo('------------------------------')
	logInfo('*** Operation Parameters ***')
	logInfo('mediaFlag: %s' % (op.mediaFlag))
	logInfo('validityPeriod: %s' % (op.vp))
	logInfo('pid: %s' % (pid.upper()))
	logInfo('------------------------------')

	
	requestID = sdapi.EnhancedSendAPDU(target, userName, apdu, tsmReferenceId, gsmG0348Params, op)

	if (requestID < 0):
		logInfo('requestID %d' % (requestID))
		code = -requestID
		requestID = -1
		message = "%s" % (alm.almStatusToString(code))

	return status(ESAPDU, code, target, tsmReferenceId, requestID)

