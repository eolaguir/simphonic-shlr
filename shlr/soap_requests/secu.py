#!/bin/env python
#******************************************************************************
# file: $Id: secu.py,v 1.6 2011-04-29 13:28:21 oracle Exp $
# author:  Saad Amrani
#*****************************************************************************/
import sys
import Cenum
import _secu
import array 
import ctypes 
import binascii

VL = 0x00020001
for enum in (  "crAlgo", "crMacAlgo", "crDivAlgo",  "crKeyArea", "crStatus"):
	Cenum.enumLoad(_secu.SECU_LIB, enum, target = globals())
#status for not found key 
#5
#crERR_KEYSTORE_KEY_NOT_FOUND 


def getKi(_alm_algo_endecrypt, alm_acces_key, masterkey, iccid):
	_axs_key_name = ctypes.c_char_p(alm_acces_key)
	iccid_hex = ctypes.create_string_buffer(16)
	masterkey = ctypes.c_char_p(masterkey)
	iccid_hex = iccid 
	KIC = ctypes.create_string_buffer(33)
	KID = ctypes.create_string_buffer(33)
	KIK = ctypes.create_string_buffer(33)
        cr = _secu.SECU_LIB.get_KIs(iccid_hex, KIC, KID, KIK, _alm_algo_endecrypt, _axs_key_name, masterkey) 	
	return cr, KIC.value, KID.value, KIK.value

	

if __name__ =='__main__':
	_alm_algo_endecrypt = cr_DES_ENC_CBC 
	l_masterkey = "OT"
	l_iccid = "00112233445566778899"
	alm_acces_key = "AlmAccess" 
  	getKis(_alm_algo_endecrypt, alm_acces_key, l_masterkey, l_iccid[len(l_iccid)-16:])

