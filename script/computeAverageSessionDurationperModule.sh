show_help() {
        echo "Usage: $0 -m module saff_files"
        echo "Eg: $0 -m shlvivo saff.*"
}

OPTIND=1
if [ -z "$1" ]
then
        show_help;
        exit;
fi

if [ $1 == "-m" ];then
        module="$2"
        shift
        shift
fi
## END OF M.DINIZ EDIT

#This script parses saff files provided in argument and computes :
#   - the total number of sessions per file (and average per sec on one hour)
#   - the average and max session duration per file
#   - the average and max number of sessions per sec (seconds with no session are ignored, measures peaks of traffic)
# author: EPO
# date: 08/10/2011
# table column names
echo "filename                              nb session (per sec)             duration (max)          peak tps (max)"
# for each file provide in input
# we're going to use nawk since awk for solaris is a little annoying with their errors
for i in $@;do
nawk -v mod="$module" -F'|' '{
if ($15==mod) {
     if ($2=="CREA") {
                        if ($1!=prev) {
                                if (tps > maxtps) { maxtps=tps; maxsess=sess }
                                gtps+=tps; prev=$1; tps=1; nsec+=1;
                                # beware only seconds with transaction are counted => it gives average peak of tps
                        }
                        else  { tps +=1 };
                }
                # compute session duration (in field #12 of lines 'STOP'
                if ($2=="STOP") {
                        s+=$12; n+=1;sess=$4
                        if ($12 > maxdur) {maxdur=$12;maxsess=$4}
                }
		if ($2=="ABRT") {
                       s+=$12; n+=1;sess=$4
                        if ($12 > maxdur) {maxdur=$12;maxsess=$4}
		}


}
}
        END {
               if (s!=0) {
                                        print FILENAME, "\t", n, "\t(", substr(n/3600,0,4), ")\t\t", substr(s/n,0,4) , "\t(", maxdur, ") [S: ", maxsess, "]\t\t", substr(gtps/nsec,0,4), "\t(", maxtps, ")";
              }
                else {

                                        print FILENAME, "\t", n, "\t(", substr(n,0,4), ")\t\t\t", substr(s,0,4) , "\t(", maxdur, ")\t\t\t", substr(gtps,0,4), "(", maxtps, ")";
                                }
       }' $i
done



