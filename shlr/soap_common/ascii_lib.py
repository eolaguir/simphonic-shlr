#!/usr/bin/env python
#******************************************************************************
# file: $Id: ascii_lib.py,v 1.2 2011-04-04 10:40:57 ndt Exp $
# author:  Benjamin Portier
#*****************************************************************************/
# -*- coding: iso-8859-15 -*-
# To be used for MO:
def convertHexaCoded8BitDataToSOAPString(ma_chaine):
	i=0
	out=""
	while (i < len(ma_chaine)-1):
		out = out + chr(int(ma_chaine[i:i+2], 16))
		i = i + 2
	return out

# To be used for MT
def convertSOAPStringToHexaCoded8BitData(ma_chaine):
        i=0
        out=""
        while (i < len(ma_chaine)):
                out += "%02X"%(ord(ma_chaine[i]))
                i = i + 1
        return out
	
"""
s2 = "6368656D696E206465206C61206772616E64652070656C6F757365203738313130204C6520560573696E6574"
print "%s\n"%convertHexaCoded8BitDataToSOAPString(s2)
"""
