#!/bin/env python
import cx_Oracle
import threading
#from sswap_#LOG import *
import secu
from prov_config import DB_SID, DB_PASS, DB_LOGIN, OCP_MAPPING
lockA = threading.Lock()
#-- Oracle connection--
lockDelete = threading.Lock()

# ota
shlr_ota_ok             =       2000
shlr_ota_wrong_param    =       2001
shlr_miss_param         =       2002
shlr_unknow_msisdn      =       2003
shlr_susb_engaged       =       2004
shlr_ota_error          =       2020
# UFO
shlr_por_nok_secu       =       3001
shlr_por_nok_count      =       3002

shlr_prov_ok            =       1000
shlr_prov_wrong_param   =       1001
shlr_imsi_exists        =       1002
shlr_msisdn_exists      =       1003
shlr_iccid_exists       =       1004
shlr_key_error          =       1005
shlr_key_not_found      =       1006
shlr_prov_error         =       1020

shlr_dict_status        = {\
shlr_ota_ok             : "OTA: SENT",
shlr_ota_wrong_param    : "OTA: WRONG FORMAT OF PARAMETER",
shlr_miss_param         : "OTA: MISSING PARAMETER",
shlr_unknow_msisdn      : "OTA: UNKNOWN MSISDN",
shlr_susb_engaged       : "OTA: SUBSCRIBER ALREADY ENGAGED",
shlr_ota_error          : "OTA: INTERNAL ERROR",
shlr_por_nok_secu       : "POR NOK Secu",
shlr_por_nok_count      : "POR NOK Counter",
shlr_prov_ok            : "PROV: OK",
shlr_prov_wrong_param   : "PROV: WRONG PARAMETER",
shlr_imsi_exists        : "PROV: IMSI ALREADY EXISTS",
shlr_msisdn_exists      : "PROV: MSISDN ALREADY EXISTS",
shlr_iccid_exists       : "PROV: ICCID ALREADY EXISTS",
shlr_key_error          : "PROV: KEY DIVERSIFICATION ERROR",
shlr_key_not_found      : "PROV: MASTER KEY NOT FOUND",
shlr_prov_error         : "PROV: INTERNAL ERROR"
}
ICCID_EXIST = 'almProvision_REFERENCED_ICCID_ALREADY_EXIST'
IMSI_EXIST  = 'almProvision_REFERENCED_IMSI_ALREADY_EXIST'
SUBS_EXIST  = 'almProvision_SIM_CARD_ALREADY_EXIST_IN_USERS_TABLE'

crERR_KEYSTORE_KEY_NOT_FOUND = 5

def getDBConnection():
    try:

        connection = cx_Oracle.Connection(DB_LOGIN,
                        DB_PASS,
                        DB_SID,
                        threaded = True
        )
        cursor = connection.cursor()
        cursor.callproc("DBMS_REPUTIL.REPLICATION_OFF", [])
        cursor.close()


    #conc =  connection
        return connection
    except  Exception, e:
        #LOG.error("SQL Connection failed")
        print "connection failed %s" % e.__str__()
        return None
def create_SIM_KeySet(cursor, p_ocp_name, p_imsi, p_iccid, p_isc1, p_key_set_nb, p_key_set_index, p_kic_values_tab, p_kid_values_tab, p_kik_values_tab):

    #LOG.debug("Enters in create_SIM_KeySet")
    #LOG.info("ocp_name: %s p_imsi: %s p_iccid:"%(p_ocp_name, p_imsi, p_iccid))
    try:

        cursor.callproc('provsimCard.create_SIM_KeySet',
        [p_ocp_name, p_imsi, p_iccid, p_isc1, p_key_set_nb, p_key_set_index, p_kic_values_tab, p_kid_values_tab, p_kik_values_tab])
        return shlr_prov_ok ,shlr_dict_status[shlr_prov_ok]
    except Exception, e:
        pass
        if ICCID_EXIST in str(e.__str__()):
            return shlr_iccid_exists, shlr_dict_status[shlr_iccid_exists]
        if IMSI_EXIST in str(e.__str__()):
            return shlr_imsi_exists, shlr_dict_status[shlr_imsi_exists]
        print ("create_SIM_KeySet imsi :%s Exception: <%s>"%(str(p_imsi),  e.__str__()))
        return shlr_prov_error, shlr_dict_status[shlr_prov_error]
        #LOG.error("create_SIM_KeySet Exception: <%s>"%(e.__str__()))
    return



def delete_SIM(cursor,  imsi):
    try:
        cursor.callproc('provsimCard.delete_SIM',
        [imsi])
        return shlr_prov_ok ,shlr_dict_status[shlr_prov_ok]
    except Exception, e:
        pass
        print "delete_SIM  Exception: <%s>"%(e.__str__())
        return shlr_prov_error, shlr_dict_status[shlr_prov_error]


def delete_CMM_SIM(cursor, iccid):
    try:
        cursor.callproc('CmmSimCardProv.deleteSimTarCounters',
        [iccid])
        cursor.callproc('CmmSimCardProv.deleteKeySets',
                [iccid])
        cursor.callproc('CmmSimCardProv.deleteSimCard',
                [iccid])
        cursor.execute("commit")
        return shlr_prov_ok ,shlr_dict_status[shlr_prov_ok]
    except Exception, e:
        pass
        print "delete_CCM_SIM  Exception: <%s>"%(e.__str__())
        return shlr_prov_error, shlr_dict_status[shlr_prov_error]

def delete_Subscriber(cursor, msisdn):
    try:
        cursor.callproc('provusers.delete_Subscriber',
        [msisdn, 1])
        return shlr_prov_ok ,shlr_dict_status[shlr_prov_ok]
    except Exception, e:
        pass
        print "delete_Subscriber  Exception: <%s>"%(e.__str__())
        return shlr_prov_error, shlr_dict_status[shlr_prov_error]

def CMMcreateSimCard(cursor, p_sim_data, p_app_counter, p_app_keyset_counter, imsi):
    #LOG.debug("Enters in CMM createSimCard")
    #LOG.info("ocp_name: %s p_imsi: %s p_iccid:"%(p_ocp_name, p_imsi, p_iccid))
    try:
        cursor.callproc('cmmProvisioning_script.createSimCard_script',
        [p_sim_data, p_app_counter, p_app_keyset_counter])
        return shlr_prov_ok ,shlr_dict_status[shlr_prov_ok]
    except Exception, e:
        l_st, l_message = delete_SIM(cursor,  imsi)
        print "statue de  delete_SIM:  st " + str(l_st) + "message" +str(l_message)
        pass
        print "CMM createSimCard Exception: <%s>"%(e.__str__())
        return shlr_prov_error, shlr_dict_status[shlr_prov_error]
        #LOG.error("CMM createSimCard Exception: <%s>"%(e.__str__()))

def create_Subscriber(cursor, imsi, iccid,  p_subscriber_name, p_msisdn, p_user_profile_desc, p_user_Parent_name, p_group_desc_nb, p_group_desc,  commit_flag):
    #LOG.debug("Enters in create_SIM_KeySet")
    #LOG.info("ocp_name: %s p_imsi: %s p_iccid:"%(p_ocp_name, p_imsi, p_iccid))
    try:
        cursor.callproc('provusers.create_Subscriber',
        [imsi,  p_subscriber_name, p_msisdn, p_user_profile_desc, p_user_Parent_name, p_group_desc_nb, p_group_desc ,  commit_flag])
        return shlr_prov_ok ,shlr_dict_status[shlr_prov_ok]
    except Exception, e:
        l_st, l_message = delete_SIM(cursor,  imsi)
        print "statue de  delete_SIM:  st " + str(l_st) + "message" +str(l_message)
        lockDelete.acquire()
        l_st, l_message = delete_CMM_SIM(cursor, iccid)
        print "statue de  delete_CMM_SIM:  st " + str(l_st) + "message" +str(l_message)
        lockDelete.release()
        if SUBS_EXIST in str(e.__str__()):
            return shlr_msisdn_exists, shlr_dict_status[shlr_msisdn_exists]
        print ("provusers.create_Subscriber: %s  Exception: <%s>"%( str(p_msisdn), e.__str__()))
        return shlr_prov_error, shlr_dict_status[shlr_prov_error]
        #LOG.error("provusers.create_Subscriber Exception: <%s>"%(e.__str__()))

def get_profileId(con, profile_name):
    try:
        cursor = con.cursor()
        cursor.execute("SELECT id  FROM card_profile where DESCRIPTION='%s'" % profile_name)
        id = cursor.fetchall()[0][0]
        cursor.close()
        return id
    except Exception, e:
        print ("get_profileId exception : <%s>"%(e.__str__()))
        pass
        #LOG.error("provusers.create_Subscriber Exception: <%s>"%(e.__str__()))
        return -1

conc = getDBConnection()
simTarCounterList =  []
cursor = conc.cursor()
simTarCounterList.append('101|0000000000')
simTarCounterList.append('2|0000000000')
p_simTarCounterList = cursor.arrayvar(cx_Oracle.STRING, 30)
p_simTarCounterList.setvalue(0, simTarCounterList)

def provision_card(cursor, iccid, imsi, msisdn, id_profile,MK):
    try:
        ocp_id = OCP_MAPPING[id_profile]
        profile_id = get_profileId(conc, 'VIVO_' + ocp_id)

        if profile_id == -1:
            return shlr_prov_error,shlr_dict_status[shlr_prov_error]
        KEY1, KEY2, KEY3 = [], [], []
        lockA.acquire()
        rc , l_KEY1, l_KEY2,  l_KEY3 =  secu.call_getKi(MK, iccid[len(iccid)-16:])
        lockA.release()
        if rc != 0:
            #LOG.info("get KIs failed  %d " % rc);
            if rc == crERR_KEYSTORE_KEY_NOT_FOUND:
                print "key %s not found for the imsi %s" % (str(MK), str(imsi))
                return  shlr_key_not_found, shlr_dict_status[shlr_key_not_found]
            else:
                print "key %s generation error for the imsi %s (crError %d)" % (str(MK), str(imsi), rc)
                return  shlr_key_error, shlr_dict_status[shlr_key_error]
        
        KEY1.append(l_KEY1)
        KEY2.append(l_KEY2)
        KEY3.append(l_KEY3)
        p_key_set_index  = cursor.arrayvar(cx_Oracle.NUMBER, 10)
        p_KIC, p_KID, p_KIK , p_KIL = cursor.arrayvar(cx_Oracle.STRING, 100), cursor.arrayvar(cx_Oracle.STRING, 100), cursor.arrayvar(cx_Oracle.STRING, 100), cursor.arrayvar(cx_Oracle.STRING, 100)
        p_KIC.setvalue(0,KEY1)
        p_KID.setvalue(0,KEY2)
        p_KIK.setvalue(0,KEY3)
        l_set = []
        l_set.append(2)
        p_key_set_index.setvalue(0, l_set)
        st, message = create_SIM_KeySet (cursor, 'VIVO_'+ocp_id , imsi, iccid, "888888", 1, p_key_set_index, p_KIC, p_KID, p_KIK)
        if st != shlr_prov_ok :
            return st, message

        simCardRec, simKeysetList = [], []
        simKeysetList.append('2|2|NO|NO|NO|' + str(KEY1[0]) + '|' + str(KEY2[0]) + '|' + str(KEY3[0]) + '|0000000000')

        simCardRec.append(  iccid + '|' + str(profile_id) + '|1|0000000000')
        p_simCardRec, p_simKeysetList = cursor.arrayvar(cx_Oracle.STRING, 64), cursor.arrayvar(cx_Oracle.STRING, 256)
        p_simKeysetList.setvalue(0, simKeysetList)
        p_simCardRec.setvalue(0,simCardRec)
        st, message = CMMcreateSimCard(cursor, p_simCardRec, p_simTarCounterList ,  p_simKeysetList, imsi)
        if st != shlr_prov_ok :
            return st, message
        group_list = []
        group_list = cursor.arrayvar(cx_Oracle.STRING, 5)
        st, message = create_Subscriber(cursor, imsi,  iccid, "vivo_subscriber",msisdn, "" , "", 0 , group_list,  1)
        if st != 0 :
            return st, message

            return shlr_prov_ok ,shlr_dict_status[shlr_prov_ok]
    except  Exception, e:
        print ("provision_card exception : <%s> for the imsi %s "%(e.__str__(), str(imsi)))
        return shlr_prov_error,shlr_dict_status[shlr_prov_error]


def closeCon(con):
    con.close()
