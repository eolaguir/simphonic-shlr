#!/bin/env python
#******************************************************************************
# file: $Id: shlr_notif_definitions.py,v 1.16 2011-06-27 18:15:00 portier Exp $
# author:  Benjamin Portier
#*****************************************************************************/

TYPE_SELECT_ITEM=0x24
TYPE_GET_INPUT=0x23
TYPE_DISPLAY_TEXT=0x21
TYPE_PROVIDE_LOCAL_INFO=0x26

from shlr_tools import FormatOutputMsisdn

class Notification:
	def __init__(self, timeout, smmId):
		self.timeout = timeout
		self.smmId = smmId
		self.dispMsisdn=""
		#self.timeout = float(CFG.getvalue("HTTP_TIMEOUT"))
		#self.smmId = CFG.getvalue("SMM_ID")
		self.header =\
"""<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:orc="ORC">
   <soapenv:Header/>
   <soapenv:Body>"""
		self.footer =\
"""   </soapenv:Body>
</soapenv:Envelope>"""

	def soapNotif(self):
		return ""

# ------------------------------
# InteractUser
class InteractUserNotification(Notification):
	def __init__(self, msisdnFake, tid, code, timeout, smmId):
		Notification.__init__(self, timeout, smmId)
		if msisdnFake is None:
			self.msisdnFake = ""
		else:
			self.msisdnFake = msisdnFake
			self.dispMsisdn = self.msisdnFake
		if tid is None:
			self.tid = ""
		else:
			self.tid = tid
		if code is None:
			self.code = -1
			self.msg = "UNKNOWN"
		else:
			self.code = code
			self.msg = interactCode2Msg(code)
		self.cmdType=0
		self.sItemValue=-1
		self.gInputValue=""

	def soapNotif(self):
		soapSitem=""
		soapGinput=""
		if self.cmdType==TYPE_SELECT_ITEM and self.code==0:
			soapSitem="%d"%self.sItemValue
		elif self.cmdType==TYPE_GET_INPUT and self.code==0:
			soapGinput=self.gInputValue
		s="""\
%(HEADER)s
      <orc:InteractUserAnswer>
         <smmId>%(SMM)s</smmId>
         <msisdnFake>%(MSISDN)s</msisdnFake>
         <transactionId>%(TID)s</transactionId>
         <cmdType>%(CMD_TYPE)s</cmdType>
         <generalResultCode>%(CODE)s</generalResultCode>
         <generalResultMsg>%(MSG)s</generalResultMsg>
	 <choice>
	   <sItemValue>
	     <index>%(SOAP_SITEM)s</index>
	   </sItemValue>
	   <gInputValue>
	     <text>%(SOAP_GINPUT)s</text>
	   </gInputValue>
	 </choice>	 
      </orc:InteractUserAnswer>
%(FOOTER)s	"""\
		%{\
		'HEADER'	: self.header,
		'SMM'		: self.smmId,
		'MSISDN'	: FormatOutputMsisdn(self.msisdnFake),
		'TID'		: self.tid,
		'CMD_TYPE'	: cmdType2SOAP(self.cmdType),
		'CODE'		: "%02X" %self.code,
		'MSG'		: interactCode2Msg(self.code),
		'SOAP_SITEM'	: soapSitem,
		'SOAP_GINPUT'	: soapGinput,
		'FOOTER'	: self.footer}
		return s

# ------------------------------
# GetInput
class GetInputAnswer(InteractUserNotification):
	def __init__(self, msisdnFake, tid, code, text, timeout, smmId):
		InteractUserNotification.__init__(self, msisdnFake, tid, code, timeout, smmId)
		self.cmdType = TYPE_GET_INPUT
		if text is None:
			self.gInputValue = -1
		else:
			self.gInputValue = text

	def __str__(self):
		s = "[GETINPUT] msisdn:[%s] tid:[%s] code:[%d] text: [%s]"\
		%(self.msisdnFake, self.tid, self.code, self.gInputValue)
		return s

# ------------------------------
# SelectItem
class SelectItemAnswer(InteractUserNotification):
	def __init__(self, msisdnFake, tid, code, index, timeout, smmId):
		InteractUserNotification.__init__(self, msisdnFake, tid, code, timeout, smmId)
		self.cmdType = TYPE_SELECT_ITEM
		if index is None:
			self.sItemValue = -1
		else:
			self.sItemValue = index
	def __str__(self):
		s = "[SELECTITEM] msisdn:[%s] tid:[%s] code:[%d] item: [%d]"\
		%(self.msisdnFake, self.tid, self.code, self.sItemValue)
		return s

# ------------------------------
# DisplayText
class DisplayTextAnswer(InteractUserNotification):
	def __init__(self, msisdnFake, tid, code, timeout, smmId):
		InteractUserNotification.__init__(self, msisdnFake, tid, code, timeout, smmId)
		self.cmdType = TYPE_DISPLAY_TEXT

	def __str__(self):
		s = "[DISPLAYTEXT] msisdn:[%s] tid:[%s] code:[%d]"\
		%(self.msisdnFake, self.tid, self.code)
		return s

# ------------------------------
# DisplayText
class ProvideLocalInfoAnswer(InteractUserNotification):
	def __init__(self, msisdnFake, tid, code, data="", timeout=10, smmId="1"):
		InteractUserNotification.__init__(self, msisdnFake, tid, code, timeout, smmId)
		self.data = data
		self.cmdType = TYPE_PROVIDE_LOCAL_INFO

	def __str__(self):
		s = "[PROVIDE_LOCAL_INFO] msisdn:[%s] tid:[%s] code:[%d] data:[%s]"\
		%(self.msisdnFake, self.tid, self.code, self.data)
		return s
# ------------------------------
# InteractUserErrorAnswer
class InteractUserErrorAnswer(InteractUserNotification):
	def __init__(self, msisdnFake, tid, code, type, timeout, smmId):
		InteractUserNotification.__init__(self, msisdnFake, tid, code, timeout, smmId)
		self.cmdType = type

	def __str__(self):
		s = "[%s] msisdn:[%s] tid:[%s] code:[%d]"\
		%(cmdType2SOAP(self.cmdType),self.msisdnFake, self.tid, self.code)
		return s

# ------------------------------
# Activation		
class ActivationAnswer(Notification):
	def __init__(self, msisdnReal, tid, iccid, timeout, smmId):
		Notification.__init__(self, timeout, smmId)
		if msisdnReal is None:
			self.msisdnReal = ""
		else:
			self.msisdnReal = msisdnReal
			self.dispMsisdn = self.msisdnReal
		if tid is None:
			self.tid = ""
		else:
			self.tid = tid
		if iccid is None:
			self.iccid = ""
		else:
			self.iccid = iccid
	def __str__(self):
		s = "[ACTIVATION_ANS] msisdn:[%s] tid:[%s] iccid:[%s]"\
		%(self.msisdnReal, self.tid, self.iccid)
		return s

	def soapNotif(self):
		s="""\
%(HEADER)s
      <orc:ActivationAnswer>
         <smmId>%(SMM)s</smmId>
         <msisdnReal>%(MSISDN)s</msisdnReal>
         <transactionId>%(TID)s</transactionId>
         <iccid>%(ICCID)s</iccid>
      </orc:ActivationAnswer>
%(FOOTER)s	"""\
		%{\
		'HEADER': self.header,
		'SMM'	: self.smmId,
		'MSISDN': FormatOutputMsisdn(self.msisdnReal),
		'TID'	: self.tid,
		'ICCID'	: self.iccid,
		'FOOTER': self.footer}
		return s

# ------------------------------
# InitialSMS
class InitialSMS(Notification):
	def __init__(self, msisdnFake, imei, loci, timeout, smmId):
		Notification.__init__(self, timeout, smmId)
		if imei is None:
			self.imei = ""
		else:
			self.imei = imei
		if loci is None:
			self.loci = ""
		else:
			self.loci = loci
		if msisdnFake is None:
			self.msisdnFake = ""
		else:
			self.msisdnFake = msisdnFake
			self.dispMsisdn = self.msisdnFake

	def __str__(self):
		s = "[INITIALSMS] msisdn:[%s] imei:[%s] loci:[%s]"\
		%(self.msisdnFake, self.imei, self.loci)
		return s

	def soapNotif(self):
		s="""\
%(HEADER)s
      <orc:InitialSMS>
         <smmId>%(SMM)s</smmId>
         <msisdnFake>%(MSISDN)s</msisdnFake>
         <imei>%(IMEI)s</imei>
         <loci>%(LOCI)s</loci>
      </orc:InitialSMS>
%(FOOTER)s	"""\
		%{\
		'HEADER': self.header,
		'SMM'	: self.smmId,
		'MSISDN': FormatOutputMsisdn(self.msisdnFake),
		'IMEI'	: self.imei,
		'LOCI'	: self.loci,
		'FOOTER': self.footer}
		return s

# ------------------------------
# AsyncACK
class AsyncACK(Notification):
	def __init__(self, msisdn="", transactionId="", timeout=10, smmId="1"):
		Notification.__init__(self, timeout, smmId)
		self.msisdn = msisdn
		self.dispMsisdn = self.msisdn
		self.transactionId = transactionId
		self.deliveryStatus = ""
		self.message = ""
		self.porData = ""
	
	def setDeliveryStatus(self, dlv_st):
		self.deliveryStatus = dlv_st

	def setMessage(self, message):
		self.message = message

	def setPorData(self, por_data):
		self.porData = por_data

	def __str__(self):
		s = "[AsyncACK] msisdn:[%s] tid:[%s] delivStatus:[%s] message:[%s] porData:[%s]"\
		%(self.msisdn, self.transactionId, self.deliveryStatus, self.message, self.porData)
		return s

	def soapNotif(self):
		s="""\
%(HEADER)s
      <orc:AsyncACK>
         <smmId>%(SMM)s</smmId>
         <msisdn>%(MSISDN)s</msisdn>
         <transactionId>%(TID)s</transactionId>
         <deliveryStatus>%(DLV_ST)s</deliveryStatus>
         <message>%(MSG)s</message>
         <porData>%(POR)s</porData>
      </orc:AsyncACK>
%(FOOTER)s	"""\
		%{\
		'HEADER': self.header,
		'SMM'	: self.smmId,
		'MSISDN': FormatOutputMsisdn(self.msisdn),
		'TID'	: self.transactionId,
		'DLV_ST': self.deliveryStatus,
		'MSG'   : self.message,
		'POR'	: self.porData,
		'FOOTER': self.footer}
		return s

# ------------------------------
def interactCode2Msg(code):
	if code==0x00:
		return "CMD PERFORMED SUCCESSFULLY"
	elif code==0x01:
		return "CMD PERFORMED WITH PARTIAL COMPREHENSION"
	elif code==0x02:
		return "CMD PERFORMED, WITH MISSING INFO"
	elif code==0x04:
		return "CMD PERFORMED SUCCESSFULLY, BUG REQUESTED ICON COULD NOT BE DISPLAYED"
	elif code==0x10:
		return "PROACTIVE SIM SESSION TERMINATED BY USER"
	elif code==0x11:
		return "BACKWARD MOVE IN THE PROACTIVE SIM SESSION REQUESTED BY THE USER"
	elif code==0x12:
		return "NO RESPONSE FROM USER"
	elif code==0x13:
		return "HELP INFORMATION REQUIRED BY USER"
	elif code==0x20:
		return "ME CURRENTLY ENABLE TO PROCESS COMMAND"
	elif code==0x21:
		return "NETWORK CURRENTLY UNABLE TO PROCESS COMMAND"
	elif code==0x30:
		return "CMD BEYOND ME CAPABILITIES"
	elif code==0x31:
		return "CMD TYPE NOT UNDERSTOOD BY ME"
	elif code==0x32:
		return "CMD DATA NOT UNDERSTOOD BY ME"
	elif code==0x33:
		return "CMD NUMBER NOT KNOWN BY ME"
	elif code==0x35:
		return "SMS RPERROR"
	elif code==0x36:
		return "ERROR, REQUIRED VALUES ARE MISSING"
	elif code==0x39:
		return "INTERACTION WITH CALL CONTROL BY SIM OR MO SM CONTROL BY SIM, PERMANENT PROBLEM"
	return "UNKNOWN"



def cmdType2SOAP(code):
	if code == TYPE_SELECT_ITEM:
		return "SELECT_ITEM"
	elif code == TYPE_GET_INPUT:
		return "GET_INPUT"
	elif code == TYPE_DISPLAY_TEXT:
		return "DISPLAY_TEXT"
	elif code == TYPE_PROVIDE_LOCAL_INFO:
		return "PROVIDE_LOCAL_INFO"
	else:
		return "UNKNOWN"

def validIP(address):
	parts = address.split(".")
	if len(parts) != 4:
		return False
	for item in parts:
		if not 0 <= int(item) <= 255:
			return False
	return True
