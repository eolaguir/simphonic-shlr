## File:        test_ApduGenerator_perf.py
## Description: Simple test of the performance of ApduGenerator
## Author:      Oleg Usoltsev <o.usoltsev@external.oberthur.com>
## Created:     16 August 2013

import unittest
import threading
import time
import sys
import bisect

sys.path.append("../../soap_common")
from mock_shlr_req_process import processActivateHRS_LTE

# Set to True to disable debugging messages.
NDEBUG = True

# Set to True to dump captured time stats to a file.
CREATE_DUMP_FILE = True

class TestVariables:
    """
    Stores input parameters used to generate APDU-s.
    """
    orchId          = 1
    msisdn          = "+557055018910"
    labelCommand    = "Activate_HRS_LTE"
    tid             = 16
    config          = "0400000015"
    imsiReal        = "724101908486515"
    smspReal        = "+550101102010"
    msisdnReal      = "+551796250320"

class Launcher(threading.Thread):
    """
    Executor of a single run of the performance test.
    The launcher accepts the number of requests to send.
    """

    def __init__(self, numOfMessages, collectStats = True):
        threading.Thread.__init__(self)
        
        self.__numOfMessages = numOfMessages
        self.__collectStats  = collectStats
    
    def run(self):
        if not NDEBUG:
            print "Launcher %s is running a test" % self
        
        self.__stats = []
        start  = 0
        finish = 0
    
        for i in range(self.__numOfMessages):
            if self.__collectStats:
                start = time.clock()
            
            # START of measured unit.
            req = processActivateHRS_LTE(
                    TestVariables.orchId,
                    TestVariables.msisdn,
                    TestVariables.labelCommand,
                    TestVariables.tid,
                    TestVariables.config,
                    TestVariables.imsiReal,
                    TestVariables.smspReal,
                    TestVariables.msisdnReal)

            apdu = req.getApdu()
            # END of measured unit.

            if self.__collectStats:
                finish = time.clock()
                self.__stats.append(finish - start)
            
            del apdu
    
        if not NDEBUG:
            print "Launcher %s has finished a test" % self
        
    def getTimeStatistics(self):
        return self.__stats

class ApduGeneratorTestCase(unittest.TestCase):
    NUM_OF_TEST_THREADS     = 100
    NUM_OF_TEST_REQUESTS    = 10000
    STATS_DUMP_FILE         = "stats.txt"

    def setUp(self):
        """
        Warm up the engine. Start 2 threads sending 100 requests each.
        """
        print "Warming up the engine"

        if not NDEBUG:
            print "running %d threads sending %d requests each" % \
                    (ApduGeneratorTestCase.NUM_OF_TEST_THREADS,
                    ApduGeneratorTestCase.NUM_OF_TEST_REQUESTS)
    
        self.__launchers = []
        for i in range(ApduGeneratorTestCase.NUM_OF_TEST_THREADS):
            launcher = Launcher(ApduGeneratorTestCase.NUM_OF_TEST_REQUESTS, False)
            launcher.start()
            self.__launchers.append(launcher)
    
        print "Waiting for the threads to stop"
        for launcher in self.__launchers:
            if launcher.isAlive():
                launcher.join()

            if not NDEBUG:
                print "Launcher %s is stopped" % launcher
        
        del self.__launchers[0:]
        print "Finished warming-up"
    
    def tearDown(self):
        """
        Aggregate the statistics and compute an avarage KPI
        using the simple average method.
        """
        totals  = []
        
        print "Computing time statistics"
        for launcher in self.__launchers:
            totals.extend(launcher.getTimeStatistics())

        # Compute min, max and average time.
        totals  = sorted(totals)
        minTime = min(totals)
        maxTime = max(totals)
        total   = sum(totals)
        
        numOfRequests = ApduGeneratorTestCase.NUM_OF_TEST_THREADS \
                        * ApduGeneratorTestCase.NUM_OF_TEST_REQUESTS
        avgTime = total / numOfRequests

        # Compute the number of requests whose processing time is greater
        # than the average time.
        index = bisect.bisect_left(totals, avgTime)
        numOfRequestsOverAvg = len(totals[index:])
        numOfRequestsOverAvgPct = (numOfRequestsOverAvg / float(numOfRequests)) * 100.0

        # Compute average time for 25th, 50th and 75th percentile.
        quartile = int(0.25 * len(totals))
        q1AvgTime = sum(totals[:quartile]) / float(quartile)
        
        quartile = int(0.50 * len(totals))
        q2AvgTime = sum(totals[:quartile]) / float(quartile)
        
        quartile = int(0.75 * len(totals))
        q3AvgTime = sum(totals[quartile:]) / float(len(totals[quartile:]))
        
        print
        print "== STATISTICS ==================================================="
        print "  Total of requests                    :", numOfRequests
        print "  Min time (seconds)                   :", minTime
        print "  Max time (seconds)                   :", maxTime
        print "  Average time (seconds)               :", avgTime
        print "  Num of requests with (t) > average   :", numOfRequestsOverAvg
        print "    as % of total                      :", numOfRequestsOverAvgPct, "%"
        print "  Average time of 1st quartile (< 25%) :", q1AvgTime
        print "  Average time of 2nd quartile (< 50%) :", q2AvgTime
        print "  Average time of 3rd quartile (> 75%) :", q3AvgTime
        print
        
        if CREATE_DUMP_FILE:
            fh = open(ApduGeneratorTestCase.STATS_DUMP_FILE, 'w')
            value = '\n'.join(str(item) for item in totals)
            
            try:
                fh.write(value)
            except Exception, e:
                print "Failed to save stats to", ApduGeneratorTestCase.STATS_DUMP_FILE
                print e
            finally:
                fh.close()
        
        del self.__launchers[0:]

    def test_performance(self):
        """
        Start N threads sending M requests each. N and M are configurable
        via NUM_OF_THREADS and NUM_OF_REQUESTS constants.
        """
        print "Ready to punch it"
        
        for i in range(ApduGeneratorTestCase.NUM_OF_TEST_THREADS):
            launcher = Launcher(ApduGeneratorTestCase.NUM_OF_TEST_REQUESTS)
            launcher.start()
            self.__launchers.append(launcher)
    
        print "Waiting for the threads to stop"
        for launcher in self.__launchers:
            if launcher.isAlive():
                launcher.join()

            if not NDEBUG:
                print "Launcher %s is stopped" % launcher
    
        print "Finished testing"
    
if __name__ == "__main__":
    unittest.main()
