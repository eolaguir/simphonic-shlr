#!/bin/bash

if [ ! $# -eq 2 ] && [ ! $# -eq 1 ]; then
	echo ""
	echo "Usage: ./transaction_per_msisdn_v2.sh msisdn [date]

	Ex: $0 557000193641
	    $0 557000193641 1221
	"
	exit
fi

if [ $# -eq 2 ]; then
	MMI_LOG="/export/home/sfovivo/exe/asp/shlvivo/mmi/shlr/log/$2/vivo.log"
	SHL_LOG="/export/home/sfovivo/exe/sys/shlr2ats/log/$2/vivo.log"
else
	MMI_LOG="/export/home/sfovivo/exe/asp/shlvivo/mmi/shlr/log/`date +%m%d`/vivo.log"
	SHL_LOG="/export/home/sfovivo/exe/sys/shlr2ats/log/`date +%m%d`/vivo.log"
fi

server[1]=10.11.190.145
server[2]=10.11.190.146
server[3]=10.26.42.143
server[4]=10.26.42.144

for i in 1 2 3 4; do
	rsh ${server[i]} grep $1 $MMI_LOG | grep -v '|D|' | egrep '_REQUEST|_RESULT|sec' >> $1.tmp
	rsh ${server[i]} grep $1 $SHL_LOG | grep -v 'Tries sending with' | grep -v 'MO to be appended' >> $1.tmp
done

sort -u -t':' -n -k 1 -k 2 -k 3 -k 4 $1.tmp > $1.ord

while read line; do
	echo $line
	echo ""
done < $1.ord

rm $1.tmp
rm $1.ord
