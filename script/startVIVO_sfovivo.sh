

echo "******************* Starting SYS and MD modules... *******************"
scl boot sys event
scl boot sys archiver
scl boot sys gsm0348
scl boot sys router
scl boot sys transArc
scl boot sys transArc_relay
scl boot sys notif_connector
scl boot sys shlr2ats
scl boot sys alm_archiver
scl boot sys admin_alm
scl boot md smpp34_0
scl boot md smpp34_0_MO_only
scl boot md smpp34_1
scl boot md smpp34_1_MO_only
scl boot md smpp34_2
scl boot md smpp34_2_MO_only
scl boot md smpp34_3
scl boot md smpp34_3_MO_only

echo "******************* Starting DAO and CMM... *******************"
jbl start all

echo "******************* Starting SECU... *******************"
/export/home/sfovivo/secu/install_secu/scripts/start_secu
