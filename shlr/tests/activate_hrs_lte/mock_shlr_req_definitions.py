from ApduGenerator import ApduGenerator

class CommonRequest:
    def __init__(self, tid=-1, msisdn="", code=-1, message=""):
        self.type = -1
        self.tid = tid
        self.msisdn = msisdn
        self.code = code
        self.message = message
        self.smmId = 0

    def __str__(self):
        s = "smmId <%s> tid <%s> msisdn <%s> code <%s> message <%s>" \
            % (str(self.smmId), str(self.tid), self.msisdn, str(self.code), self.message)

        return s

    def formatResponse(self):
        return (self.smmId, self.tid, self.msisdn, self.code, self.message)

class OtaRequest(CommonRequest):
    def __init__(self, request="", orchId=-1, msisdn="", tid=-1):
        CommonRequest.__init__(self, tid, msisdn, 2000, "OK")
        self.type = 1
        self.request = request
        self.sessionId = ""
        self.kicHex='25'
        self.kidHex='25'
        self.keyType='KEY_SET'
        self.keySetVersion=2
        self.pid='7F'
        self.counterId=2
        self.vp = 60 # in seconds 
        self.tar = "53484C"
        self.spi = "1222"
        self.rpack = 1
        self.waitmo = 1 
        self.orchId = orchId

    def __str__(self):
        s = "[%s_REQUEST] orchId <%d> msisdn <%s> tid <%d>" % (self.request, self.orchId, self.msisdn, int(self.tid))
        return s

    def displayResponse(self):
        s = "[%s_RESULT] orchId <%d> msisdn <%s> tid <%d> code <%d> message <%s>" \
            % (self.request, self.orchId, self.msisdn, int(self.tid), int(self.code), self.message)

        return s

    def check(self):
        if (self.tid == -1):
            self.code=2002
            self.message = "Mandatory transaction id"

        if (self.msisdn == ""):
            self.code=2002
            self.message = "Mandatory MSISDN"

    def getApdu(self):
        s = "C104%08d" %(self.tid)
        return s
            
class ActivateHRS_LTE(OtaRequest):
    def __init__(self,
            orchId      = "",
            msisdnFake  = "",
            labelCommand    = "",
            tid     = -1,
            config      = "",
            imsiReal    = "",
            smspReal    = "",
            msisdnReal  = ""):
        OtaRequest.__init__(self, "ACTIVATE_HRS_LTE", orchId, msisdnFake, tid)
        self.labelCommand = labelCommand
        self.config = config
        self.imsiReal = imsiReal
        self.smspReal = smspReal
        self.msisdnReal = msisdnReal

    def __str__(self):
        s = "%s labelCommand <%s> config <%s> imsiReal <%s> smspReal <%s> msisdnReal <%s>" \
            % (OtaRequest.__str__(self),
                self.labelCommand,
                self.config,
                self.imsiReal,
                self.smspReal,
                self.msisdnReal)
        return s

    def check(self):
        OtaRequest.check(self)

        if (self.code != 2000):
            return

        if (self.smspReal == ""):
            self.code = 2002
            self.message = "Mandatory smspReal"

    def getApdu(self):
        apdu = None

        try:
            generator = ApduGenerator(self.labelCommand)
            apdu = generator.getApdu(self)
        except:
            apdu = None

        return apdu