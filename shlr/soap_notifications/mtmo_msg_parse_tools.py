#!/usr/bin/env python
#******************************************************************************
# file: $Id: mtmo_msg_parse_tools.py,v 1.2 2011-06-27 18:16:33 portier Exp $
# author:  Benjamin Portier
#*****************************************************************************/
import os, re
from shlr_notif_mo_parsing import *

"""
initialSMS="C00110C8080102030405060708C90711121314151617CA09212223242526272829"
# 010301 <type>
selectItemResponse="C00110C10400000001CB0F010301240002028281030100100103"
getInputResponse="C00110C10400000002CB210103012300020282810301000D1304313233343536373839303132333435363738"
getInputResponse="C00110C10400000002CB210103012300020282810301000D1304414233343536373839303132333435363738"
displayTextResponse="C00110C10400000003CB0B010301210002028281030111"
confirmationSMS="C00110C10400000004CC102143658709214365870921436587FF"

url_string="http://demo1.com/client/sms?type=MO&id=11/01/12:09:15:47.975_+33608223728:0&msisdn=+33608223728&msisdn_port=0&sme=11520&sme_port=0&pid=00&dcs=08&encoding=binary&udhi=true&scts=2011/01/25 14:45:36&smsc=popup&sms=%s"%getInputResponse

#http://172.25.31.45:9908/cgi-bin/shlr_applet_mo_server.cgi?type=MO&id=11/01/12:09:15:47.975_+33608223728:0&msisdn=+33608223728&msisdn_port=0&sme=11520&sme_port=0&pid=00&dcs=08&encoding=binary&udhi=true&scts=2011/01/25 14:45:36&smsc=popup&sms=C00110C10400000004CC102143658709214365870921436587FF
"""


class PreParseResponse:
	def __init__(self):
		self.httpRespMsg=""
		self.notif=None
		

def preParse(get_url_string, log, httpTimeOut, smmID, hrsSme):
	out = PreParseResponse()

	fields = re.split("[&=]", get_url_string)
	log.debug(str(fields))

	fieldList = []
	i = 0
	while i < len(fields)-1:
		fieldList.append([fields[i], fields[i+1]])
		i = i + 2

	# Check HTTP request is linked with valid MO reception, destinated to applet
	# Extract SMS data from this request
	moFound = 0
	porFound = 0
	msisdnFound = 0
	smeOK=0
	data=None
	rpackFound=0
	status=""
	messageError=""
	tid=""

	log.debug(fieldList.__str__())

	for elem in fieldList:
		if string.lower(elem[0])=='type' and string.upper(elem[1])=='MO':
			moFound = 1
		if string.lower(elem[0])=='type' and string.upper(elem[1])=='RPACK':
			rpackFound = 1
		elif string.lower(elem[0])=='msisdn':
			msisdnFound = 1
			msisdn = elem[1].split(":")[0]
		elif string.lower(elem[0])=='sme' and elem[1].split(":")[0]==hrsSme:
			smeOK=1
		elif string.lower(elem[0])=='sms':
			data = elem[1]
		if string.lower(elem[0])=='client_reference_id':
			tid = elem[1].split("_")[0]
		if string.lower(elem[0])=='status':
			status = elem[1]
			if status!="OK":
				status="TO_RETRY"
		elif string.lower(elem[0])=='message':
			messageError = elem[1]

	if msisdnFound==0:
		log.warning("IncomingRequest not including source MSISDN")
		out.httpRespMsg="UNSUPPORTED_GET_REQUEST : no MSISDN"
		return out
	elif ((moFound==0) and (rpackFound==0)):
		log.warning("IncomingRequest not linked with a MO")
		out.httpRespMsg="UNSUPPORTED_GET_REQUEST : only RPACK and MO supported"
		return out
	elif (moFound==1) and (smeOK==0):
		log.warning("MO not destinated to expected SME")
		out.httpRespMsg="UNSUPPORTED_GET_REQUEST : bad SME"
		return out
	elif (moFound==1) and data is None:
		log.warning("MO data empty")
		out.httpRespMsg="UNSUPPORTED_GET_REQUEST : no data found"
		return out
	elif ((rpackFound==1) and (status=="") and (tid=="")):
		log.warning("RPACK without status and/or transaction ID")
		out.httpRespMsg="UNSUPPORTED_GET_REQUEST : bad RPACK"
		return out

	if moFound and data[:4]=="0271":
		moFound=0
		porFound=1

	if moFound:
		notif = parseMO(msisdn, data, log, httpTimeOut, smmID)
		if notif is None:
			log.warning("MO not matching any predefined cmd type: "+data)
			out.httpRespMsg="UNSUPPORTED_GET_REQUEST"
			return out
		else:
			out.httpRespMsg="OK"
			out.notif = notif
			log.info("MO to be appended to queue: "+data)
			return out
