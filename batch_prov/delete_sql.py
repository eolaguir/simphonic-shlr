#!/bin/env python
import threading

from prov_sql import  getDBConnection, delete_SIM, delete_CMM_SIM, delete_Subscriber, shlr_prov_ok, shlr_prov_error

def get_imsi(con, iccid):
	try:
		cursor = con.cursor()
		cursor.execute("SELECT  imsi FROM sim_card where iccid='%s'" % iccid)
		imsi = cursor.fetchall()[0][0]
		cursor.close()
		return imsi
	except Exception, e:
                print ("get_imsi exception : <%s>"%(e.__str__()))
                pass
                #LOG.error("provusers.create_Subscriber Exception: <%s>"%(e.__str__()))
        	return -1
	


def delete_card(cursor, iccid, imsi,  msisdn):
	st, message = delete_SIM(cursor,  imsi)
	if st != shlr_prov_ok:	
		return st, message
	try:	
		st, message = delete_CMM_SIM(cursor,  iccid)
	except  Exception, e:
		print "connection failed %s" % e.__str__()
		lock_Delete.release()
		return shlr_prov_error, "Delete KO"
	if st != shlr_prov_ok:	
		return st, message
	return shlr_prov_ok, 'Delete OK'
