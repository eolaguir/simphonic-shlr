-------------------------------------------------------------------------------
--File          :     $Id: cmmProvisioningBody_script.sql,v 1.1 2011-11-16 02:29:13 saadamra Exp $
--Project       :     CMM
--Author        :     Eric Baquiran
-----------------------------------------------------------------------------
--Version       :     $Revision: 1.1 $
-----------------------------------------------------------------------------
--Description   :     PL/SQL package for provisioning interface functions
-------------------------------------------------------------------------------
CREATE OR REPLACE PACKAGE BODY cmmProvisioning_script AS


  PROCEDURE createSimCard_script (
    p_sim_data            IN   CharArray64_index,
    p_app_counter         IN   CharArray30_index,
    p_app_keyset_counter  IN   CharArray256_index
  ) IS 
	
    l_p_sim_data  CharArray64;
    l_p_app_counter CharArray30;
    l_p_app_keyset_counter CharArray256;
    BEGIN
    l_p_sim_data := CharArray64();
    l_p_sim_data.extend(p_sim_data.count);
    for i in 1..p_sim_data.count loop
		l_p_sim_data(i) := p_sim_data(i);
	END LOOP;
    
    l_p_app_counter := CharArray30();
    l_p_app_counter.extend(p_app_counter.count);	
    for i in 1..p_app_counter.count loop
                l_p_app_counter(i) := p_app_counter(i);
        END LOOP;
    
    l_p_app_keyset_counter := CharArray256();	
    l_p_app_keyset_counter.extend(p_app_keyset_counter.count); 		
    for i in 1..p_app_keyset_counter.count loop
                l_p_app_keyset_counter(i) := p_app_keyset_counter(i);
        END LOOP;
    cmmProvisioning.createSimCard(l_p_sim_data,
		l_p_app_counter,
		l_p_app_keyset_counter);
    END createSimCard_script;		


  
END;
/

show errors;
