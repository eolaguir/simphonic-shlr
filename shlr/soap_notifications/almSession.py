import os
import sys
import alm
import soap
from shlr_tools import Log
# -- for userFactory interface to be accessible
from pyClient import *
#from dao_idl   import _0_ci
from ci import *

LOG=Log()

def OpenSession(login, password, tsmReferenceId):
	code = 0
	message = 'OK'
	tsmReferenceId = 0
	sessionID = ""
	try:
		LOG.debug('OpenSession (%s/%s)'%(login, password))
		soap.soapLoadConfig('../../alm/soap/services/almcfg.cfg')
		cfg = soap.soapGetConfig()
		try:
			attemptNumber = userFactory.getUser(login).getAttempNumber()
			if attemptNumber == -1: #username does not exists
				pass
			elif attemptNumber == 3: #user has been blocked
				code = alm.almProvision_USER_BLOCKED
				message = alm.almStatusToString(code)
		except UserException, userExcept:
			code = alm.almErr_CORBA_DAO_RELATED
			message = "%s User Exception: %s"%(alm.almStatusToString(code), userExcept.errorMessage)
		except Exception, e:
			code = alm.almErr_CORBA_DAO_RELATED 
			message = "%s CORBA Exception: %s"%(alm.almStatusToString(code), e)
		if code != 0:
			return code, message, sessionID, tsmReferenceId
		status, resp = soap.ALM__OpenSession(login, password)
		if resp.code == alm.admErr_INVALID_LOGIN:
			status, resp = soap.ALM__OpenSession(login, password)
		if resp.code == 0:
			sessionID = resp.sessionID
		return resp.code, resp.message, sessionID, tsmReferenceId
	except:
		code = alm.almErr_OPEN_SESSION_REQUEST
		message = alm.almStatusToString(code)
		return code, message, sessionID, tsmReferenceId

def CloseSession(sessionID):
	code = 0
	message = 'OK'
	try:
		LOG.debug('CloseSession (%s)'%(sessionID))
		status, resp = soap.ALM__CloseSession(sessionID)	
		return resp.code, resp.message
	except:
		code = alm.almErr_CLOSE_SESSION_REQUEST
		return code, alm.almStatusToString(code)
