#!/bin/env python

DB_LOGIN = 'smalm_qa'
DB_PASS  = 'smalm_qa'
DB_SID 	 = 'SMDB10'
NB_THREADS = 30
LOG_DIRECTORY = '.'

# The profile mapping:
# The first value is the original profile ID (as it is in the input file).
# The second value is an OCP ID that must be used instead of the original OCP ID.
# No system-specific name is required as a prefix.
OCP_MAPPING =\
{
    "18": "18",
    "19": "18",
    "21": "21"
}
