set serveroutput on;
DECLARE
  P_MSISDN VARCHAR2(20);
  P_CARD_STATE VARCHAR2(1);
BEGIN
  P_MSISDN := '+551100000011';
  P_CARD_STATE := 'A';

  DAO_PROC.CHANGECARDSTATE(
    P_MSISDN => P_MSISDN,
    P_CARD_STATE => P_CARD_STATE
  );
END;
/
