

echo "******************* Stopping SYS and MD modules... *******************"

su - sfovivo -c "\
	scl shutdown md smpp34_3_MO_only; \
	scl shutdown md smpp34_3; \
	scl shutdown md smpp34_2_MO_only; \
	scl shutdown md smpp34_2; \
	scl shutdown md smpp34_1_MO_only; \
	scl shutdown md smpp34_1; \
	scl shutdown md smpp34_0_MO_only; \
	scl shutdown md smpp34_0; \
	scl shutdown sys admin_alm; \
	scl shutdown sys alm_archiver; \
	scl shutdown sys notif_connector; \
	scl shutdown sys transArc_relay; \
	scl shutdown sys transArc; \
	scl shutdown sys shlr2ats; \
	scl shutdown sys router; \
	scl shutdown sys gsm0348; \
	scl shutdown sys archiver; \
	scl shutdown sys event;
"

echo "******************* Stopping ASP ALM... *******************"
su - shlvivo -c "scl shutdown asp shlvivo"

echo "******************* Stopping ASP mtmo... *******************"
su - shlvivo2 -c "scl shutdown asp shlvivo2"


echo "******************* Stopping DAO and CMM... *******************"
su - sfovivo -c "jbl stop all"

echo "******************* Stoping SECU... *******************"
su - sfovivo -c "/export/home/sfovivo/secu/install_secu/scripts/stop_secu"
