#!/bin/env python
from datetime import datetime
import threading
import time
import httplib2

class Log:
	def __init__(self):
		self.f=open("./log_client","w")
	def debug(self, text):
		try:
			class_time = datetime.now()
			m, d, h, min, _sec, _microsec = class_time.month, class_time.day, class_time.hour, class_time.minute, class_time.second,  class_time.microsecond
			# re-upadte the name of the log file
			todisp="%02d:%02d:%02d:%d|"%(h,min, _sec,_microsec)
			todisp = todisp + text
			self.f.write("%s \n"%todisp)
		except Exception, e:
			print "Exception: "+e.__str__()

LOG=Log()

def sendHTTPGetRequest(msisdn):
	t0 = time.time()
	
	urlPath= """/soap/vivo_notif.cgi?type=MO&id=11/01/12:09:15:47.975_+33608223728:0&msisdn=%s&msisdn_port=0&sme=60245&sme_port=0&pid=00&dcs=08&encoding=binary&udhi=true&scts=2011/01/25_2014:45:36&smsc=popup&sms=C00110C10400000004CC102143658709214365870921436587FF"""%(msisdn)
	LOG.debug("about to send: "+urlPath)
	con = None
	try:
		#con = httplib2.HTTPConnectionWithTimeout("172.25.31.191", 9091, timeout=1)
		con = httplib2.HTTPConnectionWithTimeout("10.4.3.181", 9091, timeout=10)
		con.putrequest("GET", urlPath)
		con.endheaders()
		#con.send("")
		con.close()
	except Exception, e:
		LOG.debug("ERROR http: "+e.__str__())
		return
	tend = time.time()
	duration = tend - t0
	if (duration < 2):
		time.sleep(2-duration)
	else:
		LOG.debug("WARNING: sendHTTP took more than 2 seconds")
	return


class HttpSender(threading.Thread):
        def __init__(self, prefix):
                threading.Thread.__init__(self)
                self._timer = threading.Event()
		self.prefix = prefix
        def Exit(self):
                pass
        def EndThread(self):
                self._timer.set()
        def run(self):
		for i in range(0,100):
                        if self._timer.isSet():
                                LOG.debug("Stopping the ATS notifier thread attached to notif_connector")
                                return
			sendHTTPGetRequest("%s%05d"%(self.prefix, i))

"""
l_HttpSender = HttpSender("+3333")
l_HttpSender.start()
"""


"""
# 10 per second
for i in range(0,20):
	l_HttpSender = HttpSender("+50%02d"%i)
	l_HttpSender.start()
	time.sleep(0.1)

"""

# 40 per second
for i in range(0,40):
	l_HttpSender = HttpSender("+50%02d"%i)
	l_HttpSender.start()
	time.sleep(0.025)
