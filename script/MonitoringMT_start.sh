#!/bin/bash

export SIMphonIC=/export/home/sfovivo/exe
export PATH=$PATH:/export/home/sfovivo/bin

#
# Platform parameters - TO BE UPDATED FOR EACH SERVER
#
SERVER="FE02"
SITE="SP"
IP="10.11.190.145"
MD="smpp34_0"

#MD_LOG="/export/home/sfovivo/exe/md/$MD/log/`date +%m%d`/md_smpp34"
#MMI_LOG="/export/home/sfovivo/exe/asp/shlvivo/mmi/shlr/log/`date +%m%d`/vivo.log"
#LOG="/export/home/sfovivo/scripts/MonitoringMT/monitoring_MD.log"
#FLUX_LOG="/export/home/sfovivo/exe/md/$MD/arc/`date +%m%d`/flux"

TIME_WAITING=120

#
# Send mail parameters
#
SENDER="plataforma_hrs@vivo.com.br"
RECPT1="l.marui@oberthur.com"
RECPT2="a.huart@oberthur.com"
RECPT3="y.silva@oberthur.com"
RECPT4="p.sherratt@oberthur.com"
RECPT5="a.lecorche@oberthur.com"
RECPT6="s.amrani@oberthur.com"
RECPT7="e.ramos2@oberthur.com"
RECPT8="m.diniz@oberthur.com"
SUBJECT="$SERVER-$SITE $IP - MT's not sent"
MESSAGE="$SERVER-$SITE $IP: No MT's sent since last $TIME_WAITING seconds!!"

sendmail(){

        (sleep 1;
        echo "helo";
        sleep 1;
        echo "mail from:$SENDER";
        sleep 1;
        echo "rcpt to:$RECPT1";
        sleep 1;
        echo "rcpt to:$RECPT2";
        sleep 1;
        echo "rcpt to:$RECPT3";
        sleep 1;
        echo "rcpt to:$RECPT4";
        sleep 1;
        echo "rcpt to:$RECPT5";
        sleep 1;
        echo "rcpt to:$RECPT6";
        sleep 1;
        echo "rcpt to:$RECPT7";
        sleep 1;
        echo "rcpt to:$RECPT8";
        sleep 1;
        echo "data";
        sleep 1;
        echo "Subject:$SUBJECT";
        sleep 1;
        echo "";
        sleep 1;
        echo "Details:$MESSAGE"
        sleep 1;
        echo ".";) | telnet 10.128.3.67 25

} > /dev/null 2>&1

# Start monitoring
while true
do

	MD_LOG="/export/home/sfovivo/exe/md/$MD/log/`date +%m%d`/md_smpp34"
	MMI_LOG="/export/home/sfovivo/exe/asp/shlvivo/mmi/shlr/log/`date +%m%d`/vivo.log"
	LOG="/export/home/sfovivo/scripts/MonitoringMT/monitoring_MD.log"
	FLUX_LOG="/export/home/sfovivo/exe/md/$MD/arc/`date +%m%d`/flux"

	#nb_of_SH_sessions=`scl state | grep 'sh:' | tail -1 | awk '{print $3}'`
	nb_of_SH_sessions=`scl state asp shlvivo | grep 'sh:' | awk '{print $4}'`
	
	if [ ! $nb_of_SH_sessions -eq "0" ]; then
		
		nb_REQUEST_before=`grep -c 'REQUEST' $MMI_LOG`	

		sleep $TIME_WAITING

		nb_REQUEST_after=`grep -c 'REQUEST' $MMI_LOG`

		nb_REQUEST_delta=$(($nb_REQUEST_after-$nb_REQUEST_before))

		echo "`date`: SOAP REQUESTS last $TIME_WAITING seconds = $nb_REQUEST_delta" >> $LOG

		if [ $nb_REQUEST_delta -gt "10" ]; then
			#MTs_SENT=`tail -1 $FLUX_LOG | awk '{print $2}'`
			MTs_SENT=`tail -2 $FLUX_LOG | awk '{sum +=$2}; END {print sum}'`
			echo "`date`: MTs_SENT = $MTs_SENT" >> $LOG
			if [ $MTs_SENT -eq "0" ]; then
				echo "`date`: NO MT's sen't since last $TIME_WAITING seconds!" >> $LOG
				sendmail
			fi
		fi
	fi

	sleep 3

done
