#! /usr/bin/env python
#******************************************************************************
# file: $Id: vivo_service.cgi,v 1.12 2011-12-17 21:46:54 gabritou Exp $
# author:  Saad Amrani
#*****************************************************************************/
# This script is charge of implementing the alm api in python
# it is based on ZSI library, and is used as a soap server

# import standard libs
import os
import sys
import string
import time
import datetime
import threading

# generci SOAP tools
import ZSI

# shlr specific SOAP handlers
import shlr_req_interface
import shlr_tools

# SOAP_TRANS needed for "old" python version.
# In newer version, string.translate accepts None as a translation table
SOAP_TRANS = string.maketrans('', '')

SOAP_HANDLERS = {
    'InteractUser'      : shlr_req_interface.InteractUser,
    'ActivateSIMchronize'   : shlr_req_interface.ActivateSIMchronize,
    'ActivateIMEITracking'  : shlr_req_interface.ActivateIMEITracking,
    'ActivateHRS'       : shlr_req_interface.ActivateHRS,
    'ActivateHRS_LTE'   : shlr_req_interface.ActivateHRS_LTE
}

def HTTP_anwser(start_response, start_time, status, LOG, output = ""):
    response_headers = [('Content-type', 'text/xml; charset="utf-8"'), ('Content-Length', str(len(output)))]
    start_response(status, response_headers)
    diff = datetime.datetime.now() - start_time
    LOG.info("%s in %02u.%06u sec" % (status, diff.seconds, diff.microseconds))
    LOG.debug("Answering: %s" % (output))
    return [output]

def application(environ, start_response):
    LOG = shlr_tools.Log()

    LOG.info("SOAP REQUEST entry")
    start_time = datetime.datetime.now()
    status = '200 OK'

    soapRequest = environ['wsgi.input'].read(int(environ['CONTENT_LENGTH']))
    LOG.debug("soapRequest %s" % soapRequest.translate(SOAP_TRANS, '\n\r'))

    try:
        try:
            # parse SOAP request
            ps = ZSI.ParsedSoap (soapRequest)
        except:
            status = "400 Bad Request"
            return HTTP_anwser(start_response, start_time, status, LOG)
        try:
            # select SAOP handler
            handler = SOAP_HANDLERS[ps.body_root.localName]
        except KeyError:
            LOG.error("Unsupported SOAP interface: %s" % ps.body_root.localName)
            status = '501 Not Implemented'
            return HTTP_anwser(start_response, start_time, status, LOG)

        # call SOAP handler
        result = handler(ps, LOG)

        # format SOAP response
        tc = ZSI.TC.Any(pname = str(ps.body_root.localName) + 'Response')
        sw = ZSI.SoapWriter(nsdict = {'SMM':'SMM'})
        sw.serialize(result, tc)

        return HTTP_anwser(start_response, start_time, status, LOG, str(sw))

    except:
        import traceback
        status = "500 Internal Server Error"
        tb = sys.exc_info()[2]
        for name, line, func, text in traceback.extract_tb(tb):
            LOG.error("'%s:%d:%s" % (name, line, text))
        LOG.error("%s: %s " % (sys.exc_info()[0], sys.exc_info()[1]))
        output = ""
        return HTTP_anwser(start_response, start_time, status, LOG, output)
