#!/bin/env python
#******************************************************************************
# file: $Id: shlr_notif_mo_parsing.py,v 1.21 2011-07-05 13:08:47 ndt Exp $
# author:  Benjamin Portier
#*****************************************************************************/

import string, sys
from shlr_notif_definitions import TYPE_SELECT_ITEM, TYPE_GET_INPUT, TYPE_DISPLAY_TEXT, TYPE_PROVIDE_LOCAL_INFO
from shlr_notif_definitions import GetInputAnswer, SelectItemAnswer, DisplayTextAnswer, ActivationAnswer, InitialSMS, InteractUserErrorAnswer, ProvideLocalInfoAnswer
from gsm_alphabet_lib import convertHexaCodedGSMToSOAPString
from ascii_lib import convertHexaCoded8BitDataToSOAPString

TAG_APPLET_VERSION=0x40
TAG_TID=0x41
TAG_IMEI=0x48
TAG_LOCI=0x49
TAG_PROACT_CMD=0x4B
TAG_ICCID=0x4C
TAG_TEXT_INPUT=0x0D
TAG_ITEM_SELECTION=0x10
TAG_PROVIDE_LOCAL_INFO=0x14

# Value in MO must match the constant tags, whatever value of 1st bit (1 or 0)
def valueMatchesTag(value, tag):
	v = value & 0x7F
	if v==tag:
		return 1
	return 0
		
		
def parsePOR(data, log):
	log.debug("data %s"%data)
	dataLen = len(data)
	offset=0
	status="OK"
	message="POR OK"
	try:
		if (dataLen==0):
			# Matches with NetworkACK positive on Patch 13
			log.warning("No PoR has been received")
			return "OK", "SENT",""
		elif (dataLen<32):
			log.error("BAD_DATA_LEN")
			return "FAILED","POR_PARSING_ERROR",data
		headerLen = int(data[offset:offset+2], 16)
		if (headerLen<2):
			log.error("BAD_HEADER_LEN")
			return "FAILED","POR_PARSING_ERROR",data
		offset += 2
		headerTag = int(data[offset:offset+2], 16)
		if (headerTag!=0x71):
			log.error("IS_NOT_POR")
			return "FAILED","POR_PARSING_ERROR",data
		offset += 4
		lenPor = int(data[offset:offset+2], 16)*256 + int(data[offset+2:offset+4], 16)
		offset += 4
		lenSecu = int(data[offset:offset+2], 16)
		if ((lenPor-lenSecu)<0):
			log.error("BAD_SIZE")
			return "FAILED","POR_PARSING_ERROR",data
		tar = data[offset+2:offset+8]
		offset = (lenSecu+5)*2
		if (offset>len(data)):
			log.error("BAD_OFFSET")
			return "FAILED","POR_PARSING_ERROR",data
		status348 = int(data[offset:offset+2], 16)
		if (status348==1):
			return "FAILED","G348_RC/CC/DS_FAILED",data
		elif (status348==2):
			return "TO_RETRY","G348_COUNTER_LOW",data
		elif (status348==3):
			return "FAILED","G348_COUNTER_HIGH",data
		elif (status348==4):
			return "FAILED","G348_COUNTER_BLOCKED",data
		elif (status348==5):
			return "FAILED","G348_CYPHERING_ERROR",data
		elif (status348==6):
			return "FAILED","G348_SECURITY_ERROR",data
		elif (status348==7):
			return "FAILED","G348_INSUFFICIENT_MEMORY",data
		elif (status348==9):
			return "FAILED","G348_BAD_TAR",data
		elif (status348==10):
			return "FAILED","G348_UNDER_MINIMUM_SECURITY_LEVEL",data
		elif (status348!=0) and (status348!=255):
			return "FAILED","G348_BAD_STATUS_%02X"%status348,data
		offset += 4
		if ((offset+4)>dataLen):
			return "OK","POR_OK",data
		sw1sw2 = int(data[offset:offset+2], 16)*256 + int(data[offset+2:offset+4], 16)
		if (status348==255):
			if   (sw1sw2== 0):
				return "OK","NetworkAckOK",""
			elif (sw1sw2== 1):
				return "TO_RETRY","NetworkDiagErr_UNKNOWN_SUBSCRIBER",""
			elif (sw1sw2== 2):
				return "TO_RETRY","NetworkDiagErr_TELESERVICE_NOT_PROVISIONED",""
			elif (sw1sw2== 3):
				return "TO_RETRY","NetworkDiagErr_CALL_BARRED",""
			elif (sw1sw2== 4):
				return "TO_RETRY","NetworkDiagErr_FACILITY_NOT_SUPPORTED",""
			elif (sw1sw2== 5):
				return "TO_RETRY","NetworkDiagErr_ABSENT_SUBSCRIBER",""
			elif (sw1sw2== 6):
				return "TO_RETRY","NetworkDiagErr_MT_BUSY_FOR_MT_SMS",""
			elif (sw1sw2== 7):
				return "TO_RETRY","NetworkDiagErr_SMS_LOWER_LAYERS_CAPABILITIES_NOT_PROVISIONED",""
			elif (sw1sw2== 8):
				return "TO_RETRY","NetworkDiagErr_ERROR_IN_MS",""
			elif (sw1sw2== 9):
				return "TO_RETRY","NetworkDiagErr_ILLEGAL_SUBSCRIBER",""
			elif (sw1sw2==10):
				return "TO_RETRY","NetworkDiagErr_ILLEGAL_EQUIPMENT",""
			elif (sw1sw2==11):
				return "TO_RETRY","NetworkDiagErr_SYSTEM_FAILURE",""
			elif (sw1sw2==12):
				return "TO_RETRY","NetworkDiagErr_MEMORY_CAPACITY_EXCEEDED",""
			elif (sw1sw2==13):
				return "TO_RETRY","NetworkDiagErr_SIM_SMS_STORAGE_FULL",""
			elif (sw1sw2==14):
				return "TO_RETRY","NetworkDiagErr_SM_EXPIRED",""
			elif (sw1sw2==15):
				return "TO_RETRY","NetworkDiagErr_SM_DELETED",""
			elif (sw1sw2==16):
				return "TO_RETRY","NetworkDiagErr_SM_UNDELIVERABLE",""
			elif (sw1sw2==17):
				return "TO_RETRY","NetworkDiagErr_NO_ACK",""
			elif (sw1sw2==18):
				return "TO_RETRY","NetworkDiagErr_CONGESTION_MD",""
			else:
				return "TO_RETRY","NetworkDiagErr_SM_UNDELIVERABLE",""

		if (sw1sw2==0x6101):
			return "OK","POR_OK",data
		elif (sw1sw2==0x9000):
			if(tar=="53484C"):
				log.warning("Simulate POR OK has been received")
				return "OK", "SENT",""
			else:
				return "OK","POR_OK",data
		elif (sw1sw2==0x6985):
			return "OK","ALREADY_PERFORMED",data
		return "OK","POR_OK",data

	except Exception, e:
		log.error("[%s] raised exception: %s"%(data, e.__str__()))
		return None, None, data
	
def parseMO(msisdn, text, log, httpTimeOut, smmID):
	offset = 0
	try:
		tag1 = int(text[offset:offset+2], 16)
		if valueMatchesTag(tag1, TAG_APPLET_VERSION)==0:
			lVersion = int(text[offset:offset+2], 16)
			if (lVersion==1):
				log.error("tag C0 not found, C0 added")
				text = "C0"+text
			else:
				log.error("tag C0 not found, C00110 added")
				text = "C00110"+text
		log.debug("Tag version passed!")
		offset = offset + 2
		lVersion = int(text[offset:offset+2], 16)
		offset = offset+2
		version = text[offset:offset+2*lVersion]
		log.debug("Applet version recognized: [%s]"%version)
		offset = offset + 2*lVersion
		tag2 = int(text[offset:offset+2], 16)
		
		if valueMatchesTag(tag2, TAG_TID)==1:
			# TID -> To be parsed deeper to get type
			offset = offset + 2
			lTid = int(text[offset:offset+2], 16)
			offset = offset +2
			tid = text[offset:offset+2*lTid]
			log.debug("TID: %s"%tid)
			offset = offset + 2*lTid
			tag3=int(text[offset:offset+2], 16)
			if valueMatchesTag(tag3, TAG_PROACT_CMD)==1:
				# InteractUserAnswer
				offset = offset + 2
				# Skip length
				log.debug("proact length is: %d"%(int(text[offset:offset+2], 16)))
				offset = offset + 2
				type_of_cmd = int(text[offset+6:offset+8], 16)
				# Skip fixed length command details (see 11.14)
				offset=offset + 10
				# Skip fixed length device identities (see 11.14)
				offset = offset + 8
				# Result after fix block: 0301
				code = int(text[offset+4:offset+6], 16)
				log.debug("CODE: %d"%code)
				if code!=0:
					o = InteractUserErrorAnswer(msisdn, tid, code, type_of_cmd, httpTimeOut, smmID)
					return o
				if type_of_cmd==TYPE_DISPLAY_TEXT:
					log.debug("before DispText constr")
					o = DisplayTextAnswer(msisdn, tid, code, httpTimeOut, smmID)
					return o
				elif type_of_cmd==TYPE_PROVIDE_LOCAL_INFO:
					offset = offset + 6
					next_tag = int(text[offset:offset+2], 16)
					if valueMatchesTag(next_tag, TAG_PROVIDE_LOCAL_INFO)==0:
						log.error("MO discarded (ProvideLocalInfo: Text tag missing) %s"%text)
						return None
					offset = offset + 2
					lPprovideLIResponse = int(text[offset:offset+2], 16)
					offset = offset + 2
					userResponse=text[offset:offset+2*(lPprovideLIResponse)]
					o = ProvideLocalInfoAnswer(msisdn, tid, code, userResponse, httpTimeOut, smmID)
					log.debug("MO recognized as ProvideLocalInfo")
					return o
				elif type_of_cmd==TYPE_GET_INPUT:
					offset = offset + 6
					next_tag = int(text[offset:offset+2], 16)
					if valueMatchesTag(next_tag, TAG_TEXT_INPUT)==0:
						log.error("MO discarded (GetInput: Text tag missing) %s"%text)
						return None
					offset = offset + 2
					lUserResponse = int(text[offset:offset+2], 16)
					offset = offset + 2
					dcs = int(text[offset:offset+2], 16)
					charsetMask = dcs&12
					offset = offset + 2
					userResponse=""
					if charsetMask==0:
						log.debug("DCS: GSM alphabet")
						userResponse = \
						convertHexaCodedGSMToSOAPString(\
						text[offset:offset+2*(lUserResponse-1)])
					elif charsetMask==4:
						log.debug("DCS: 8 bit data")
						userResponse = \
						convertHexaCoded8BitDataToSOAPString(\
						text[offset:offset+2*(lUserResponse-1)])
					else:
						log.warning("DCS: unknown")
						log.error("MO discarded (GetInput: Text tag missing): %s"%text)
						return None
					log.debug("MO recognized as GetInputAnswer")
					o = GetInputAnswer(msisdn, tid, code, userResponse, httpTimeOut, smmID)
                                        return o
				elif type_of_cmd==TYPE_SELECT_ITEM:
					offset = offset + 6
					next_tag = int(text[offset:offset+2], 16)
					if valueMatchesTag(next_tag, TAG_ITEM_SELECTION)==0:
						log.error("MO discarded (SelectItem: Item Selection tag missing) %s"\
						%text)
						return None
					offset = offset + 4
					itemSel = int(text[offset:offset+2], 16)
					log.debug("ITEM: %d"%itemSel)
					o = SelectItemAnswer(msisdn, tid, code, itemSel, httpTimeOut, smmID)
					log.debug("MO recognized as SelectItemAnswer")
                                        return o
				else:
					log.error("MO discarded (Unsupported proactive cmd) %s"%text)
					return None
				
			elif valueMatchesTag(tag3, TAG_ICCID)==1:
				# ActivationAnswer
				offset = offset + 2
				lICCID = int(text[offset:offset+2], 16)
				offset = offset + 2
				simIccid = text[offset:offset+2*lICCID]
				iccid = ""
				i = 0
				while i < lICCID:
					if string.upper(simIccid[2*i+1])=='F':
						if (string.upper(simIccid[2*i])!='F'):
							iccid = iccid + simIccid[2*i]
						break
					iccid = iccid + simIccid[2*i+1]
					iccid = iccid + simIccid[2*i]
					i = i + 1
				o = ActivationAnswer(msisdn, tid, iccid, httpTimeOut, smmID)
				return o

		elif valueMatchesTag(tag2, TAG_IMEI)==1:
			# IMEI -> InitialSMS
			log.debug("IMEI tag detected")
			offset = offset +2
			lImei = int(text[offset:offset+2], 16)
			offset = offset + 2
			imei = text[offset:offset + 2*lImei]
			log.debug("IMEI: %s"%imei)
			offset = offset + 2*lImei
			tag3 = int(text[offset:offset+2], 16)
			if valueMatchesTag (tag3, TAG_LOCI)==0:
				log.error("MO discarded (LOCI tag not found)")
				return None
			offset = offset + 2
			lLoci = int(text[offset:offset+2], 16)
			offset = offset + 2
			loci = text[offset:offset+2*lLoci]
			log.debug("LOCI: %s"%loci)
			log.debug("All info OK for InitialSMS")
			o = InitialSMS(msisdn, imei, loci, httpTimeOut, smmID)
			return o
		else:
			log.error("MO discarded (2nd tag unsupported): %s"%text)
			return None
			
	except Exception, e:
		log.error("[%s] raised exception: %s"%(text, e.__str__()))
		return None


"""
initialSMS="C00110C8080102030405060708C90711121314151617CA09212223242526272829"
# 010301 <type>
selectItemResponse="C00110C10400000001CB0F010301240002028281030100100103"
getInputResponse="C00110C10400000002CB210103012300020282810301000D1304313233343536373839303132333435363738"
displayTextResponse="C00110C10400000003CB0B010301210002028281030111"
confirmationSMS="C00110C10400000004CC1021436587092143658709214365870921"


msisdn = '+401234'
log.setMsisdn(msisdn)
notif = parseMO(msisdn, getInputResponse)
#notif = parseMO(msisdn, selectItemResponse)
#notif = parseMO(msisdn, initialSMS)
#notif = parseMO(msisdn, displayTextResponse)
#notif = parseMO(msisdn, confirmationSMS)

log.debug("after parse")
if notif is None:
	log.warning("No notif can be sent")
	sys.exit(1)
log.info(notif.__str__())
log.info("\n%s"%(notif.soapNotif()))
for i in range(1, 4+1):
	url = CFG.getvalue('ATS_URL%d'%i)
	log.info("Tries sending with url: [%s]"%url)
	st, resp = notif.send2SpecificURL(url)
	if st==0:
		log.info("Sending notif is OK, received from ATS: [%s]"%resp)
		break
	else:
		if i == 4:
			log.error("Sending failed with URL [%s], error: [%s] -> FAILURE"%(url, resp))
			break			
		else:
			log.warning("Sending failed with URL [%s], error: [%s] -> RETRY with next URL"%(url, resp))
			continue
"""
