--------------------------------------------------------------------------------
--  Title:       dao_proc.sql
--  Created:     27/04/2007
--  Author:      E. Baquiran
--  Description: PL/SQL package for DAO stored procs
--  File:        $Id: dao_proc_body.sql,v 1.1 2011-06-23 13:31:54 portier Exp $
-------------------------------------------------------------------------
create or replace PACKAGE BODY Dao_Proc AS

	CURSOR crCheckSimCardCPId(p_msisdn IN SUBSCRIBER.msisdn%TYPE) IS
		SELECT a.sim_card_id, b.card_profile_id
			FROM SUBSCRIBER a, SIM_CARD b
			WHERE b.ID = a.sim_card_id
				AND a.msisdn = p_msisdn;


	CURSOR crCheckAppAid(p_aid IN APPLICATION.aid%TYPE) IS
		SELECT app.ID, app.APPLET_ID
		FROM APPLICATION app
		WHERE app.AID = p_aid;

	CURSOR crCheckCPAppliLink(p_card_profile_id IN CARD_PROFILE.ID%TYPE,
														p_application_id IN APPLICATION.ID%TYPE) IS
		SELECT 1
		FROM CARD_PROFILE_APPLICATION
		WHERE CARD_PROFILE_ID = p_card_profile_id
			AND APPLICATION_ID = p_application_id;

	CURSOR crCheckSimCardApplication(installAid IN SIM_CARD_APPLICATION.AID%TYPE,
																		p_sim_card_id IN SUBSCRIBER.SIM_CARD_ID%TYPE) IS
		SELECT 1
		FROM SIM_CARD_APPLICATION
		WHERE sim_card_id = p_sim_card_id
    AND aid = installAid;
		--AND application_id = p_application_id;

	CURSOR crGetAppliAid(p_application_id IN APPLICATION.ID%TYPE,
																		p_sim_card_id IN SUBSCRIBER.SIM_CARD_ID%TYPE) IS
		SELECT a.aid
		FROM SIM_CARD_APPLICATION scp, APPLICATION a
		WHERE scp.application_id = p_application_id
		AND scp.sim_card_id = p_sim_card_id;
		
	CURSOR crGetPackageId(p_package_aid IN PACKAGE.AID%TYPE) IS
		SELECT ID
		FROM PACKAGE
		WHERE AID = p_package_aid;

  --
  -- Return the list of packages that have to be deleted from a particular
  -- sim card, such that none of the packages is no longer necessary for
  -- any of other applications installed on the card
  --
 PROCEDURE getPackageToDelete (
    p_msisdn IN SUBSCRIBER.msisdn%TYPE,
    p_iaid IN APPLICATION.aid%TYPE,
    p_nb_rows_max IN NUMBER,
    p_nb_rows_read OUT NUMBER,
    p_more_data_to_read OUT NUMBER,
    p_package_id OUT NUMBERARRAY,
    p_supplier_id OUT NUMBERARRAY,
    p_aid OUT CHARARRAY32,
    p_name OUT CHARARRAY30,
    p_version OUT CHARARRAY10,
    p_property OUT CHARARRAY01,
    p_installed_size OUT NUMBERARRAY,
    p_java_vm OUT CHARARRAY10,
    p_dap_size OUT NUMBERARRAY,
    p_cap_size OUT NUMBERARRAY,
    p_load_file OUT CHARARRAY30,
    p_status OUT CHARARRAY01,
    p_import_date OUT DATEARRAY,
	p_is_contained OUT CHARARRAY01,
	p_install_parameters OUT CHARARRAY256
    ) IS
    v_package_id      Out_Proc.NUMBERARRAY;
    v_supplier_id     Out_Proc.NUMBERARRAY;
    v_aid             Out_Proc.CHARARRAY32;
    v_name            Out_Proc.CHARARRAY30;
    v_version         Out_Proc.CHARARRAY10;
    v_property        Out_Proc.CHARARRAY;
    v_installed_size  Out_Proc.NUMBERARRAY;
    v_java_vm         Out_Proc.CHARARRAY10;
    v_dap_size        Out_Proc.NUMBERARRAY;
    v_cap_size        Out_Proc.NUMBERARRAY;
    v_load_file       Out_Proc.CHARARRAY30;
    v_status          Out_Proc.CHARARRAY;
    v_import_date     Out_Proc.DATEARRAY;
	v_is_contained    Out_Proc.CHARARRAY;
	v_install_parameters Out_Proc.CHARARRAY256;
  BEGIN
    Out_Proc.getPackageToDelete(
      p_msisdn,
      p_iaid,
      p_nb_rows_max,
      p_nb_rows_read,
      p_more_data_to_read,
      v_package_id,
      v_supplier_id,
      v_aid,
      v_name,
      v_version,
      v_property,
      v_installed_size,
      v_java_vm,
      v_dap_size,
      v_cap_size,
      v_load_file,
      v_status,
      v_import_date,
	  v_is_contained,
	  v_install_parameters
    );

    -- convert PL/SQL type out params to SQL type out params
	-- p_package_id := NUMBERARRAY(v_package_id.COUNT);
    p_package_id := NUMBERARRAY();    p_package_id.EXTEND(v_package_id.COUNT);
    p_supplier_id := NUMBERARRAY();    p_supplier_id.EXTEND(v_package_id.COUNT);
    p_aid := CHARARRAY32();    p_aid.EXTEND(v_package_id.COUNT);
    p_name := CHARARRAY30();    p_name.EXTEND(v_package_id.COUNT);
    p_version := CHARARRAY10();    p_version.EXTEND(v_package_id.COUNT);
    p_property := CHARARRAY01();    p_property.EXTEND(v_package_id.COUNT);
    p_installed_size := NUMBERARRAY();    p_installed_size.EXTEND(v_package_id.COUNT);
    p_java_vm := CHARARRAY10();    p_java_vm.EXTEND(v_package_id.COUNT);
    p_dap_size := NUMBERARRAY();    p_dap_size.EXTEND(v_package_id.COUNT);
    p_cap_size := NUMBERARRAY();    p_cap_size.EXTEND(v_package_id.COUNT);
    p_load_file := CHARARRAY30();    p_load_file.EXTEND(v_package_id.COUNT);
    p_status := CHARARRAY01();    p_status.EXTEND(v_package_id.COUNT);
    p_import_date := DATEARRAY();    p_import_date.EXTEND(v_package_id.COUNT);
	p_is_contained:=CHARARRAY01();    p_is_contained.EXTEND(v_is_contained.COUNT);
	p_install_parameters:=CHARARRAY256();     p_install_parameters.EXTEND(v_install_parameters.COUNT);

    IF (p_nb_rows_read <> 0) THEN
      FOR i IN 1..v_package_id.COUNT LOOP
        p_package_id(i) := v_package_id(i);
        p_supplier_id(i) := v_supplier_id(i);
        p_aid(i) := v_aid(i);
        p_name(i) := v_name(i);
        p_version(i) := v_version(i);
        p_property(i) := v_property(i);
        p_installed_size(i) := v_installed_size(i);
        p_java_vm(i) := v_java_vm(i);
        p_dap_size(i) := v_dap_size(i);
        p_cap_size(i) := v_cap_size(i);
        p_load_file(i) := v_load_file(i);
        p_status(i) := v_status(i);
        p_import_date(i) := v_import_date(i);
		p_is_contained(i) :=v_is_contained(i);
		p_install_parameters(i) := v_install_parameters(i);
      END LOOP;
    END IF;

  END getPackageToDelete;

  --
  -- Return the list of packages that have to be deleted from a particular
  -- sim card, such that none of the packages is no longer necessary for
  -- any of other applications installed on the card,a nd application that is
  -- going to replace the old one
  --
  PROCEDURE getPackageToDeleteReplace (
    p_msisdn IN SUBSCRIBER.msisdn%TYPE,
    p_iaid_old IN APPLICATION.aid%TYPE,
    p_iaid_new IN APPLICATION.aid%TYPE,
    p_nb_rows_max IN NUMBER,
    p_nb_rows_read OUT NUMBER,
    p_more_data_to_read OUT NUMBER,
    p_package_id OUT NUMBERARRAY,
    p_supplier_id OUT NUMBERARRAY,
    p_aid OUT CHARARRAY32,
    p_name OUT CHARARRAY30,
    p_version OUT CHARARRAY10,
    p_property OUT CHARARRAY01,
    p_installed_size OUT NUMBERARRAY,
    p_java_vm OUT CHARARRAY10,
    p_dap_size OUT NUMBERARRAY,
    p_cap_size OUT NUMBERARRAY,
    p_load_file OUT CHARARRAY30,
    p_status OUT CHARARRAY01,
    p_import_date OUT DATEARRAY
    ) IS
    v_package_id      Out_Proc.NUMBERARRAY;
    v_supplier_id     Out_Proc.NUMBERARRAY;
    v_aid             Out_Proc.CHARARRAY32;
    v_name            Out_Proc.CHARARRAY30;
    v_version         Out_Proc.CHARARRAY10;
    v_property        Out_Proc.CHARARRAY;
    v_installed_size  Out_Proc.NUMBERARRAY;
    v_java_vm         Out_Proc.CHARARRAY10;
    v_dap_size        Out_Proc.NUMBERARRAY;
    v_cap_size        Out_Proc.NUMBERARRAY;
    v_load_file       Out_Proc.CHARARRAY30;
    v_status          Out_Proc.CHARARRAY;
    v_import_date     Out_Proc.DATEARRAY;
  BEGIN
    Out_Proc.getPackageToDeleteReplace(
      p_msisdn,
      p_iaid_old,
      p_iaid_new,
      p_nb_rows_max,
      p_nb_rows_read,
      p_more_data_to_read,
      v_package_id,
      v_supplier_id,
      v_aid,
      v_name,
      v_version,
      v_property,
      v_installed_size,
      v_java_vm,
      v_dap_size,
      v_cap_size,
      v_load_file,
      v_status,
      v_import_date
    );

    -- convert PL/SQL type out params to SQL type out params
    p_package_id := NUMBERARRAY(); p_package_id.EXTEND(v_package_id.COUNT);
    p_supplier_id := NUMBERARRAY();     p_supplier_id.EXTEND(v_package_id.COUNT);
    p_aid := CHARARRAY32();     p_aid.EXTEND(v_package_id.COUNT);
    p_name := CHARARRAY30();     p_name.EXTEND(v_package_id.COUNT);
    p_version := CHARARRAY10();     p_version.EXTEND(v_package_id.COUNT);
    p_property := CHARARRAY01();     p_property.EXTEND(v_package_id.COUNT);
    p_installed_size := NUMBERARRAY();     p_installed_size.EXTEND(v_package_id.COUNT);
    p_java_vm := CHARARRAY10();     p_java_vm.EXTEND(v_package_id.COUNT);
    p_dap_size := NUMBERARRAY();     p_dap_size.EXTEND(v_package_id.COUNT);
    p_cap_size := NUMBERARRAY();     p_cap_size.EXTEND(v_package_id.COUNT);
    p_load_file := CHARARRAY30();     p_load_file.EXTEND(v_package_id.COUNT);
    p_status := CHARARRAY01();     p_status.EXTEND(v_package_id.COUNT);
    p_import_date := DATEARRAY();     p_import_date.EXTEND(v_package_id.COUNT);

    IF (p_nb_rows_read <> 0) THEN
      FOR i IN 1..v_package_id.COUNT LOOP
        p_package_id(i) := v_package_id(i);
        p_supplier_id(i) := v_supplier_id(i);
        p_aid(i) := v_aid(i);
        p_name(i) := v_name(i);
        p_version(i) := v_version(i);
        p_property(i) := v_property(i);
        p_installed_size(i) := v_installed_size(i);
        p_java_vm(i) := v_java_vm(i);
        p_dap_size(i) := v_dap_size(i);
        p_cap_size(i) := v_cap_size(i);
        p_load_file(i) := v_load_file(i);
        p_status(i) := v_status(i);
        p_import_date(i) := v_import_date(i);
      END LOOP;
    END IF;

  END getPackageToDeleteReplace;

  --
  -- Return the list of Packages and their security, that need to be downloaded
  -- onto a particular sim_card, such that none of each package is already
  -- on the card
  --

  PROCEDURE getPackageForDownload (
    p_msisdn IN SUBSCRIBER.msisdn%TYPE,
    p_iaid IN APPLICATION.aid%TYPE,
    p_nb_rows_max IN NUMBER,
    p_nb_rows_read OUT NUMBER,
    p_more_data_to_read OUT NUMBER,
    p_package_id OUT NUMBERARRAY,
    p_supplier_id OUT NUMBERARRAY,
    p_aid OUT CHARARRAY32,
    p_name OUT CHARARRAY30,
    p_version OUT CHARARRAY10,
    p_property OUT CHARARRAY01,
    p_installed_size OUT NUMBERARRAY,
    p_java_vm OUT CHARARRAY10,
    p_dap_size OUT NUMBERARRAY,
    p_cap_size OUT NUMBERARRAY,
    p_load_file OUT CHARARRAY48,
    p_status OUT CHARARRAY01,
    p_import_date OUT DATEARRAY,
  	p_is_contained OUT CHARARRAY01,
  	p_install_parameters OUT CHARARRAY256
  ) IS
    v_package_id      Out_Proc.NUMBERARRAY;
    v_supplier_id     Out_Proc.NUMBERARRAY;
    v_aid             Out_Proc.CHARARRAY32;
    v_name            Out_Proc.CHARARRAY30;
    v_version         Out_Proc.CHARARRAY10;
    v_property        Out_Proc.CHARARRAY;
    v_installed_size  Out_Proc.NUMBERARRAY;
    v_java_vm         Out_Proc.CHARARRAY10;
    v_dap_size        Out_Proc.NUMBERARRAY;
    v_cap_size        Out_Proc.NUMBERARRAY;
    v_load_file       Out_Proc.CHARARRAY48;
    v_status          Out_Proc.CHARARRAY;
    v_import_date     Out_Proc.DATEARRAY;
  	v_is_contained Out_Proc.CharArray;
  	v_install_parameters Out_Proc.CharArray256;

  BEGIN
    Out_Proc.getPackageForDownload(
      p_msisdn,
      p_iaid,
      p_nb_rows_max,
      p_nb_rows_read,
      p_more_data_to_read,
      v_package_id,
      v_supplier_id,
      v_aid,
      v_name,
      v_version,
      v_property,
      v_installed_size,
      v_java_vm,
      v_dap_size,
      v_cap_size,
      v_load_file,
      v_status,
      v_import_date,
  	  v_is_contained,
  	  v_install_parameters

    );

    -- convert PL/SQL type out params to SQL type out params
    p_package_id := NUMBERARRAY(); p_package_id.EXTEND(v_package_id.COUNT);
    p_supplier_id := NUMBERARRAY();     p_supplier_id.EXTEND(v_package_id.COUNT);
    p_aid := CHARARRAY32();     p_aid.EXTEND(v_package_id.COUNT);
    p_name := CHARARRAY30();     p_name.EXTEND(v_package_id.COUNT);
    p_version := CHARARRAY10();     p_version.EXTEND(v_package_id.COUNT);
    p_property := CHARARRAY01();     p_property.EXTEND(v_package_id.COUNT);
    p_installed_size := NUMBERARRAY();     p_installed_size.EXTEND(v_package_id.COUNT);
    p_java_vm := CHARARRAY10();     p_java_vm.EXTEND(v_package_id.COUNT);
    p_dap_size := NUMBERARRAY();     p_dap_size.EXTEND(v_package_id.COUNT);
    p_cap_size := NUMBERARRAY();     p_cap_size.EXTEND(v_package_id.COUNT);
    p_load_file := CHARARRAY48();     p_load_file.EXTEND(v_package_id.COUNT);
    p_status := CHARARRAY01();     p_status.EXTEND(v_package_id.COUNT);
    p_import_date := DATEARRAY();     p_import_date.EXTEND(v_package_id.COUNT);
	p_is_contained:=CHARARRAY01();    p_is_contained.EXTEND(v_is_contained.COUNT);
	p_install_parameters:=CHARARRAY256(); p_install_parameters.EXTEND(v_install_parameters.COUNT);

    IF (p_nb_rows_read <> 0) THEN
      FOR i IN 1..v_package_id.COUNT LOOP
        p_package_id(i) := v_package_id(i);
        p_supplier_id(i) := v_supplier_id(i);
        p_aid(i) := v_aid(i);
        p_name(i) := v_name(i);
        p_version(i) := v_version(i);
        p_property(i) := v_property(i);
        p_installed_size(i) := v_installed_size(i);
        p_java_vm(i) := v_java_vm(i);
        p_dap_size(i) := v_dap_size(i);
        p_cap_size(i) := v_cap_size(i);
        p_load_file(i) := v_load_file(i);
        p_status(i) := v_status(i);
        p_import_date(i) := v_import_date(i);
		p_is_contained(i) :=v_is_contained(i);
		p_install_parameters(i) :=v_install_parameters(i);
      END LOOP;
    END IF;
  END getPackageForDownload;

  --
  -- get packages information related to one particular application
  --
  PROCEDURE getPackagesOfAppli (
    p_aid IN APPLICATION.aid%TYPE,
    p_nb_rows_max IN NUMBER,
    p_nb_rows_read OUT NUMBER,
    p_more_data_to_read OUT NUMBER,
    p_paid OUT CHARARRAY32,
    p_name OUT CHARARRAY30,
    p_version OUT CHARARRAY10,
    p_property OUT CHARARRAY01,
    p_installed_size OUT NUMBERARRAY,
    p_java_vm OUT CHARARRAY10,
    p_dap_size OUT NUMBERARRAY,
    p_cap_size OUT NUMBERARRAY,
    p_load_file OUT CHARARRAY30,
    p_status OUT CHARARRAY01,
    p_import_date OUT DATEARRAY,
    p_is_contained OUT CHARARRAY01
    ) IS
    v_paid            Admin_Pack.CHARARRAY32;
    v_name            Admin_Pack.CHARARRAY30;
    v_version         Admin_Pack.CHARARRAY10;
    v_property        Admin_Pack.CHARARRAY;
    v_installed_size  Admin_Pack.NUMBERARRAY;
    v_java_vm         Admin_Pack.CHARARRAY10;
    v_dap_size        Admin_Pack.NUMBERARRAY;
    v_cap_size        Admin_Pack.NUMBERARRAY;
    v_load_file       Admin_Pack.CHARARRAY30;
    v_status          Admin_Pack.CHARARRAY;
    v_import_date     Admin_Pack.DATEARRAY;
    v_is_contained    Admin_Pack.CHARARRAY;
  BEGIN
    Admin_Pack.getPackagesOfAppli(
      p_aid,
      p_nb_rows_max,
      p_nb_rows_read,
      p_more_data_to_read,
      v_paid,
      v_name,
      v_version,
      v_property,
      v_installed_size,
      v_java_vm,
      v_dap_size,
      v_cap_size,
      v_load_file,
      v_status,
      v_import_date,
      v_is_contained
    );

    -- convert PL/SQL type out params to SQL type out params
    p_paid := CHARARRAY32(); p_paid.EXTEND(v_paid.COUNT);
    p_name := CHARARRAY30();     p_name.EXTEND(v_paid.COUNT);
    p_version := CHARARRAY10();     p_version.EXTEND(v_paid.COUNT);
    p_property := CHARARRAY01();     p_property.EXTEND(v_paid.COUNT);
    p_installed_size := NUMBERARRAY();     p_installed_size.EXTEND(v_paid.COUNT);
    p_java_vm := CHARARRAY10();     p_java_vm.EXTEND(v_paid.COUNT);
    p_dap_size := NUMBERARRAY();     p_dap_size.EXTEND(v_paid.COUNT);
    p_cap_size := NUMBERARRAY();     p_cap_size.EXTEND(v_paid.COUNT);
    p_load_file := CHARARRAY30();     p_load_file.EXTEND(v_paid.COUNT);
    p_status := CHARARRAY01();     p_status.EXTEND(v_paid.COUNT);
    p_import_date := DATEARRAY();     p_import_date.EXTEND(v_paid.COUNT);
    p_is_contained := CHARARRAY01();     p_is_contained.EXTEND(v_paid.COUNT);

    IF (p_nb_rows_read <> 0) THEN
      FOR i IN 1..v_paid.COUNT LOOP
        p_paid(i) := v_paid(i);
        p_name(i) := v_name(i);
        p_version(i) := v_version(i);
        p_property(i) := v_property(i);
        p_installed_size(i) := v_installed_size(i);
        p_java_vm(i) := v_java_vm(i);
        p_dap_size(i) := v_dap_size(i);
        p_cap_size(i) := v_cap_size(i);
        p_load_file(i) := v_load_file(i);
        p_status(i) := v_status(i);
        p_import_date(i) := v_import_date(i);
        p_is_contained(i) := v_is_contained(i);
      END LOOP;
    END IF;
  END getPackagesOfAppli;

  --
  -- Return data from the 'sim_card_application' class of the ALM database
  -- thanks to the sim card msisdn and the application aid
  --
  PROCEDURE getLkSimCardApplicationAid (
    p_msisdn IN SUBSCRIBER.msisdn%TYPE,
    p_iaid IN APPLICATION.aid%TYPE,
    p_nb_rows_max IN NUMBER,
    p_nb_rows_read OUT NUMBER,
    p_more_data_to_read OUT NUMBER,
    p_sim_card_id OUT NUMBERARRAY,
    p_application_id OUT NUMBERARRAY,
    --p_instance_date OUT DATEARRAY,
    p_instance_date OUT CHARARRAY32,
    p_locked OUT CHARARRAY01,
    p_status OUT CHARARRAY01,
    p_aid OUT CHARARRAY32,
    p_name OUT CHARARRAY30,
    p_type OUT CHARARRAY01,
    p_appli_status OUT CHARARRAY01,
    p_version OUT CHARARRAY10,
    p_security_domain_id OUT NUMBERARRAY,
    p_usable OUT NUMBERARRAY,
    p_is_security_domain OUT NUMBERARRAY,
    p_security_domain_name OUT CHARARRAY30
    ) IS
    v_sim_card_id         Out_Proc.NUMBERARRAY;
    v_application_id      Out_Proc.NUMBERARRAY;
    v_instance_date       Out_Proc.CHARARRAY32;
    v_locked              Out_Proc.CHARARRAY;
    v_status              Out_Proc.CHARARRAY;
    v_aid                 Out_Proc.CHARARRAY32;
    v_name                Out_Proc.CHARARRAY30;
    v_type                Out_Proc.CHARARRAY;
    v_appli_status        Out_Proc.CHARARRAY;
    v_version             Out_Proc.CHARARRAY10;
    v_security_domain_id  Out_Proc.NUMBERARRAY;
    v_usable              Out_Proc.NUMBERARRAY;
    v_is_security_domain  Out_Proc.NUMBERARRAY;
    v_security_domain_name Out_Proc.CHARARRAY30;

  BEGIN
    Out_Proc.getLkSimCardApplicationAid (
      p_msisdn,
      p_iaid,
      p_nb_rows_max,
      p_nb_rows_read,
      p_more_data_to_read,
      v_sim_card_id,
      v_application_id,
      v_instance_date,
      v_locked,
      v_status,
      v_aid,
      v_name,
      v_type,
      v_appli_status,
      v_version,
      v_security_domain_id,
      v_usable,
      v_is_security_domain,
      v_security_domain_name
    );

    -- convert PL/SQL type out params to SQL type out params
    p_sim_card_id := NUMBERARRAY(); p_sim_card_id.EXTEND(v_sim_card_id.COUNT);
    p_application_id := NUMBERARRAY();     p_application_id.EXTEND(v_sim_card_id.COUNT);
    p_instance_date := CHARARRAY32();     p_instance_date.EXTEND(v_sim_card_id.COUNT);
    p_locked := CHARARRAY01();     p_locked.EXTEND(v_sim_card_id.COUNT);
    p_status := CHARARRAY01();     p_status.EXTEND(v_sim_card_id.COUNT);
    p_aid := CHARARRAY32();     p_aid.EXTEND(v_sim_card_id.COUNT);
    p_name := CHARARRAY30();     p_name.EXTEND(v_sim_card_id.COUNT);
    p_type := CHARARRAY01();     p_type.EXTEND(v_sim_card_id.COUNT);
    p_appli_status := CHARARRAY01();     p_appli_status.EXTEND(v_sim_card_id.COUNT);
    p_version := CHARARRAY10();     p_version.EXTEND(v_sim_card_id.COUNT);
    p_security_domain_id := NUMBERARRAY();  p_security_domain_id.EXTEND(v_sim_card_id.COUNT);
    p_usable := NUMBERARRAY();  p_usable.EXTEND(v_sim_card_id.COUNT);
    p_is_security_domain := NUMBERARRAY();  p_is_security_domain.EXTEND(v_sim_card_id.COUNT);
    p_security_domain_name := CHARARRAY30();  p_security_domain_name.EXTEND(v_sim_card_id.COUNT);

    IF (p_nb_rows_read <> 0) THEN
      FOR i IN 1..v_sim_card_id.COUNT LOOP
        p_sim_card_id(i) := v_sim_card_id(i);
        p_application_id(i) := v_application_id(i);
        p_instance_date(i) := v_instance_date(i);
        p_locked(i) := v_locked(i);
        p_status(i) := v_status(i);
        p_aid(i) := v_aid(i);
        p_name(i) := v_name(i);
        p_type(i) := v_type(i);
        p_appli_status(i) := v_appli_status(i);
        p_version(i) := v_version(i);
        p_security_domain_id(i) := v_security_domain_id(i);
        p_usable(i) := v_usable(i);
        p_is_security_domain(i) := v_is_security_domain(i);
        p_security_domain_name(i) := v_security_domain_name(i);
      END LOOP;
    END IF;
  END getLkSimCardApplicationAid;

  -------------------------------------------------------------------------------
  --
  -- Return data from the 'sim_card_application' class of the ALM database
  -- thanks to the sim card msisdn
  --
  -------------------------------------------------------------------------------
  PROCEDURE getLkSimCardApplication (
    p_msisdn IN SUBSCRIBER.msisdn%TYPE,
    p_nb_rows_max IN NUMBER,
    p_nb_rows_read OUT NUMBER,
    p_more_data_to_read OUT NUMBER,
    p_sim_card_id OUT NUMBERARRAY,
    p_application_id OUT NUMBERARRAY,
    p_instance_date OUT CHARARRAY32,
    p_locked OUT CHARARRAY01,
    p_status OUT CHARARRAY01,
    p_aid OUT CHARARRAY32,
    p_name OUT CHARARRAY30,
    p_type OUT CHARARRAY01,
    p_appli_status OUT CHARARRAY01,
    p_version OUT CHARARRAY10,
    p_security_domain_id OUT NUMBERARRAY,
    p_usable OUT NUMBERARRAY,
    p_is_security_domain OUT NUMBERARRAY,
    p_security_domain_name OUT CHARARRAY30
  ) IS

    v_sim_card_id         Application_Pack.NUMBERARRAY;
    v_application_id      Application_Pack.NUMBERARRAY;
    v_instance_date       Application_Pack.CHARARRAY32;
    v_locked              Application_Pack.CHARARRAY;
    v_status              Application_Pack.CHARARRAY;
    v_aid                 Application_Pack.CHARARRAY32;
    v_name                Application_Pack.CHARARRAY30;
    v_type                Application_Pack.CHARARRAY;
    v_appli_status        Application_Pack.CHARARRAY;
    v_version             Application_Pack.CHARARRAY10;
    v_security_domain_id  Application_Pack.NUMBERARRAY;
    v_usable              Application_Pack.NUMBERARRAY;
    v_is_security_domain  Application_Pack.NUMBERARRAY;
    v_security_domain_name Application_Pack.CHARARRAY30;

  BEGIN
    DBMS_OUTPUT.PUT_LINE('dao_proc.getLkSimCardApplication <-- start -->');

    Application_Pack.getLkSimCardApplication (
      p_msisdn,
      p_nb_rows_max,
      p_nb_rows_read,
      p_more_data_to_read,
      v_sim_card_id,
      v_application_id,
      v_instance_date,
      v_locked,
      v_status,
      v_aid,
      v_name,
      v_type,
      v_appli_status,
      v_version,
      v_security_domain_id,
      v_usable,
      v_is_security_domain,
      v_security_domain_name
    );

    -- convert PL/SQL type out params to SQL type out params
    p_sim_card_id := NUMBERARRAY(); p_sim_card_id.EXTEND(v_sim_card_id.COUNT);
    p_application_id := NUMBERARRAY();     p_application_id.EXTEND(v_sim_card_id.COUNT);
    p_instance_date := CHARARRAY32();     p_instance_date.EXTEND(v_sim_card_id.COUNT);
    p_locked := CHARARRAY01();     p_locked.EXTEND(v_sim_card_id.COUNT);
    p_status := CHARARRAY01();     p_status.EXTEND(v_sim_card_id.COUNT);
    p_aid := CHARARRAY32();     p_aid.EXTEND(v_sim_card_id.COUNT);
    p_name := CHARARRAY30();     p_name.EXTEND(v_sim_card_id.COUNT);
    p_type := CHARARRAY01();     p_type.EXTEND(v_sim_card_id.COUNT);
    p_appli_status := CHARARRAY01();     p_appli_status.EXTEND(v_sim_card_id.COUNT);
    p_version := CHARARRAY10();     p_version.EXTEND(v_sim_card_id.COUNT);
    p_security_domain_id := NUMBERARRAY();  p_security_domain_id.EXTEND(v_sim_card_id.COUNT);
    p_usable := NUMBERARRAY();  p_usable.EXTEND(v_sim_card_id.COUNT);
    p_is_security_domain := NUMBERARRAY();  p_is_security_domain.EXTEND(v_sim_card_id.COUNT);
    p_security_domain_name := CHARARRAY30();  p_security_domain_name.EXTEND(v_sim_card_id.COUNT);

    IF (p_nb_rows_read <> 0) THEN
      FOR i IN 1..v_sim_card_id.COUNT LOOP
        p_sim_card_id(i) := v_sim_card_id(i);
        p_application_id(i) := v_application_id(i);
        p_instance_date(i) := v_instance_date(i);
        p_locked(i) := v_locked(i);
        p_status(i) := v_status(i);
        p_aid(i) := v_aid(i);
        p_name(i) := v_name(i);
        p_type(i) := v_type(i);
        p_appli_status(i) := v_appli_status(i);
        p_version(i) := v_version(i);
        p_security_domain_id(i) := v_security_domain_id(i);
        p_usable(i) := v_usable(i);
        p_is_security_domain(i) := v_is_security_domain(i);
        p_security_domain_name(i) := v_security_domain_name(i);
      END LOOP;
    END IF;
  END getLkSimCardApplication;

  -------------------------------------------------------------------------------
  --
  -- Return data from the 'sim_card_package' class of the ALM database
  -- thanks to the sim card msisdn
  --
  -------------------------------------------------------------------------------
  PROCEDURE getLkSimCardPackage (
    p_msisdn IN SUBSCRIBER.msisdn%TYPE,
    p_nb_rows_max IN NUMBER,
    p_nb_rows_read OUT NUMBER,
    p_more_data_to_read OUT NUMBER,
    p_sim_card_id OUT NUMBERARRAY,
    p_package_id OUT NUMBERARRAY,
    p_aid OUT CHARARRAY32,
    p_name OUT CHARARRAY30,
    p_version OUT CHARARRAY10
  ) IS
    v_sim_card_id   Application_Pack.NUMBERARRAY;
    v_package_id    Application_Pack.NUMBERARRAY;
    v_aid           Application_Pack.CHARARRAY32;
    v_name          Application_Pack.CHARARRAY30;
    v_version       Application_Pack.CHARARRAY10;
  BEGIN
    Application_Pack.getLkSimCardPackage (
      p_msisdn,
      p_nb_rows_max,
      p_nb_rows_read,
      p_more_data_to_read,
      v_sim_card_id,
      v_package_id,
      v_aid,
      v_name,
      v_version
    );

    -- convert PL/SQL type out params to SQL type out params
    p_sim_card_id := NUMBERARRAY(); p_sim_card_id.EXTEND(v_sim_card_id.COUNT);
    p_package_id := NUMBERARRAY();     p_package_id.EXTEND(v_sim_card_id.COUNT);
    p_aid := CHARARRAY32();     p_aid.EXTEND(v_sim_card_id.COUNT);
    p_name := CHARARRAY30();     p_name.EXTEND(v_sim_card_id.COUNT);
    p_version := CHARARRAY10();     p_version.EXTEND(v_sim_card_id.COUNT);

    IF (p_nb_rows_read <> 0) THEN
      FOR i IN 1..v_sim_card_id.COUNT LOOP
        p_sim_card_id(i) := v_sim_card_id(i);
        p_package_id(i) := v_package_id(i);
        p_aid(i) := v_aid(i);
        p_name(i) := v_name(i);
        p_version(i) := v_version(i);
      END LOOP;
    END IF;
  END getLkSimCardPackage;

  --
  -- Return the gsmfiles fields of the 'gsm_files' class of the ALM database
  -- thanks to the sim card msisdn
  --
  PROCEDURE getSimCardGsmfilesInfo (
    p_msisdn IN SUBSCRIBER.msisdn%TYPE,
    p_ef IN SIM_CARD_GSMFILES.gsmfiles_ef_name%TYPE,
    p_nb_rows_max IN NUMBER,
    p_nb_rows_read OUT NUMBER,
    p_more_data_to_read OUT NUMBER,
    p_adn_mode OUT CHARARRAY01,
    p_file_reference OUT CHARARRAY30,
    p_tag_length OUT NUMBERARRAY,
    p_rec_length OUT NUMBERARRAY,
    p_file_size OUT NUMBERARRAY,
    p_EF_type OUT CHARARRAY01,
    p_EF_id OUT CHARARRAY20
    ) IS
    v_adn_mode        Out_Proc.CHARARRAY;
    v_file_reference  Out_Proc.CHARARRAY30;
    v_tag_length      Out_Proc.NUMBERARRAY;
    v_rec_length      Out_Proc.NUMBERARRAY;
    v_file_size       Out_Proc.NUMBERARRAY;
    v_EF_type         Out_Proc.CHARARRAY;
    v_EF_id           Out_Proc.CHARARRAY20;
  BEGIN
    Out_Proc.getSimCardGsmfilesInfo(
      p_msisdn,
      p_ef,
      p_nb_rows_max,
      p_nb_rows_read,
      p_more_data_to_read,
      v_adn_mode,
      v_file_reference,
      v_tag_length,
      v_rec_length,
      v_file_size,
      v_EF_type,
      v_EF_id
    );

    -- convert PL/SQL type out params to SQL type out params
    p_adn_mode := CHARARRAY01(); p_adn_mode.EXTEND(v_adn_mode.COUNT);
    p_file_reference := CHARARRAY30();     p_file_reference.EXTEND(v_adn_mode.COUNT);
    p_tag_length := NUMBERARRAY();     p_tag_length.EXTEND(v_adn_mode.COUNT);
    p_rec_length := NUMBERARRAY();     p_rec_length.EXTEND(v_adn_mode.COUNT);
    p_file_size := NUMBERARRAY();     p_file_size.EXTEND(v_adn_mode.COUNT);
    p_EF_type := CHARARRAY01();     p_EF_type.EXTEND(v_adn_mode.COUNT);
    p_EF_id := CHARARRAY20();     p_EF_id.EXTEND(v_adn_mode.COUNT);

    IF (p_nb_rows_read <> 0) THEN
      FOR i IN 1..p_adn_mode.COUNT LOOP
        p_adn_mode(i) := v_adn_mode(i);
        p_file_reference(i) := v_file_reference(i);
        p_tag_length(i) := v_tag_length(i);
        p_rec_length(i) := v_rec_length(i);
        p_file_size(i) := v_file_size(i);
        p_EF_type(i) := v_EF_type(i);
        p_EF_id(i) := v_EF_id(i);
      END LOOP;
    END IF;

  END getSimCardGsmfilesInfo;

  --
  -- Return data from the application, applet, package, and card profile tables of the ALM database
  -- thanks to the application aid
  --
  PROCEDURE getApplicationRecord (
  	p_aid	IN	APPLICATION.aid%TYPE,
  	p_applet_id OUT APPLICATION.applet_id%TYPE,
  	p_name OUT APPLICATION.NAME%TYPE,
  	p_type OUT APPLICATION.TYPE%TYPE,
  	p_status OUT APPLICATION.status%TYPE,
  	p_app_class OUT WINDAPPCLASS.application_class%TYPE,
  	p_instance_size OUT APPLICATION.instance_size%TYPE,
  	p_required_ram OUT APPLICATION.required_ram%TYPE,
  	p_install_parameters OUT APPLICATION.install_parameters%TYPE,
  	p_privilege OUT APPLICATION.PRIVILEGE%TYPE,
  	p_allowed_media OUT APPLICATION.allowed_media%TYPE,
  	p_import_date OUT VARCHAR2,
  	p_volume_name OUT VOLUME.volume_name%TYPE,
  	p_supplier_id OUT SUPPLIER.ID%TYPE,
  	p_supplier_name OUT SUPPLIER.NAME%TYPE,
  	p_applet_aid OUT APPLET.AID%TYPE,
  	p_applet_name OUT APPLET.NAME%TYPE,
  	p_applet_version OUT APPLET.VERSION%TYPE,
  	p_applet_release_date OUT VARCHAR2,
  	p_applet_status OUT APPLET.STATUS%TYPE,
  	p_elf_aid OUT PACKAGE.aid%TYPE,
  	p_package_aids OUT CHARARRAY32,
  	p_card_profile_desc OUT CHARARRAY30,
  	p_security_domain_id OUT NUMBER,
  	p_usable OUT NUMBER,
	p_token_flag OUT APPLICATION.TOKEN_FLAG%TYPE,
    p_tsm_user_id   OUT tsm_application.tsm_user_id%TYPE,
	p_tar OUT application.tar%TYPE
  ) IS
  	-- local cursors
  	CURSOR crGetAppliData(p_aid IN APPLICATION.aid%TYPE) IS
	  	SELECT a.APPLET_ID, a.NAME, a.TYPE, a.STATUS, a.INSTANCE_SIZE, a.REQUIRED_RAM,
		   a.INSTALL_PARAMETERS, a.PRIVILEGE, a.ALLOWED_MEDIA, TO_CHAR(a.IMPORT_DATE,'MM/DD/YYYY HH:MI:SS PM'), d.VOLUME_NAME,
		   c.ID AS SUPPLIER_ID, c.NAME AS SUPPLIER_NAME,
		   b.AID AS APPLET_AID, b.NAME AS APPLET_NAME, b.VERSION AS APPLET_VERSION,
		   b.RELEASE_DATE, b.STATUS AS APPLET_STATUS, a.TOKEN_FLAG AS TOKEN_FLAG, NVL(a.TAR, ' ') AS APP_TAR
		FROM APPLICATION a, APPLET b, SUPPLIER c
    --, SIM_CARD_APPLICATION d
    , VOLUME d
		WHERE b.ID = a.applet_id
		AND c.ID = b.supplier_id
		--AND a.id = d.application_id
		AND d.VOLUME_ID = a.VOLUME_ID
		AND a.aid = p_aid;

	CURSOR crGetAppliClass(p_aid IN APPLICATION.aid%TYPE) IS
		SELECT b.application_class
		FROM APPLICATION a, WINDAPPCLASS b
		WHERE b.APPLICATION_ID = a.ID
		AND a.aid = p_aid;
		
	CURSOR crGetAppliPackageData(p_aid IN APPLICATION.aid%TYPE) IS
		SELECT d.aid, c.IS_CONTAINED
		FROM APPLICATION a, APPLET b, APPLET_PACKAGE c, PACKAGE d
		WHERE d.ID = c.PACKAGE_ID
		AND b.ID = a.applet_id
		AND c.APPLET_ID = b.ID
		AND a.aid = p_aid;

	CURSOR crGetAppliCPDescData(p_aid IN APPLICATION.aid%TYPE) IS
		SELECT c.DESCRIPTION
		FROM APPLICATION a, CARD_PROFILE_APPLICATION b, CARD_PROFILE c
		WHERE c.ID = b.CARD_PROFILE_ID
		AND b.APPLICATION_ID = a.ID
		AND a.aid = p_aid;

 	CURSOR crGetAppliSD(p_aid IN APPLICATION.aid%TYPE) IS
	  SELECT b.SECURITY_DOMAIN_ID, b.USABLE
		FROM APPLICATION a, SIM_CARD_APPLICATION b
		WHERE a.ID = b.application_id
		AND a.aid = p_aid;

  CURSOR crGetTsmUserId IS
    SELECT tsm_user_id FROM application a, tsm_application b
      WHERE b.application_id = a.id AND a.aid = p_aid;

	i INTEGER := 1;
	v_iscontained CHAR(1) := 'F';

  BEGIN
	IF crGetAppliData%ISOPEN THEN
		CLOSE crGetAppliData;
	END IF;

	OPEN crGetAppliData(p_aid);

	FETCH crGetAppliData INTO p_applet_id,
	  	p_name,
	  	p_type,
	  	p_status,
	  	p_instance_size,
	  	p_required_ram,
	  	p_install_parameters,
	  	p_privilege,
	  	p_allowed_media,
	  	p_import_date,
	  	p_volume_name,
	  	p_supplier_id,
	  	p_supplier_name,
	  	p_applet_aid,
	  	p_applet_name,
	  	p_applet_version,
	  	p_applet_release_date,
	  	p_applet_status,
		  p_token_flag,
      p_tar;


	-- handle not found data
	IF crGetAppliData%NOTFOUND THEN
		RAISE_APPLICATION_ERROR(-20001, 'No application found with AID ' || p_aid);
	END IF;

	IF crGetAppliData%ISOPEN THEN
		CLOSE crGetAppliData;
	END IF;
	
	IF crGetAppliClass%ISOPEN THEN
		CLOSE crGetAppliClass;
	END IF;

	OPEN crGetAppliClass(p_aid);
	
	FETCH crGetAppliClass into p_app_class;
	
	IF crGetAppliClass%NOTFOUND THEN
		p_app_class := 1;
	END IF;
	
	IF crGetAppliClass%ISOPEN THEN
		CLOSE crGetAppliClass;
	END IF;

	OPEN crGetAppliSD(p_aid);

	FETCH crGetAppliSD INTO
    p_security_domain_id,
    p_usable;

  IF crGetAppliSD%NOTFOUND THEN
    CLOSE crGetAppliSD;
    p_security_domain_id := 0;
    p_usable := 0;
  END IF;

	IF crGetAppliData%ISOPEN THEN
		CLOSE crGetAppliData;
	END IF;

	IF crGetAppliPackageData%ISOPEN THEN
		CLOSE crGetAppliPackageData;
	END IF;

	OPEN crGetAppliPackageData(p_aid);

	p_package_aids := CharArray32();

	-- loop to copy package aids and find elfAID
	LOOP
		p_package_aids.EXTEND();
		FETCH crGetAppliPackageData INTO p_package_aids(i), v_iscontained;
		EXIT WHEN crGetAppliPackageData%NOTFOUND;
		IF (UPPER(v_iscontained) = 'T') THEN
			p_elf_aid := p_package_aids(i);
			v_iscontained := 'f';
		END IF;
		i := i + 1;
	END LOOP;

	IF i > 1 THEN
	   p_package_aids.trim();
	END IF;

	IF crGetAppliPackageData%ISOPEN THEN
		CLOSE crGetAppliPackageData;
	END IF;

	IF crGetAppliCPDescData%ISOPEN THEN
		CLOSE crGetAppliCPDescData;
	END IF;

	OPEN crGetAppliCPDescData(p_aid);

	FETCH crGetAppliCPDescData BULK COLLECT INTO p_card_profile_desc;

	IF crGetAppliCPDescData%ISOPEN THEN
		CLOSE crGetAppliCPDescData;
	END IF;

  IF crGetTsmUserId%ISOPEN THEN
    CLOSE crGetTsmUserId;
  END IF;

  OPEN crGetTsmUserId;
  FETCH crGetTsmUserId INTO p_tsm_user_id;

  IF crGetTsmUserId%NOTFOUND THEN
    p_tsm_user_id := 0;
  END IF;
  CLOSE crGetTsmUserId;

  END getApplicationRecord;



  PROCEDURE create_Package (
  	p_supplier_name IN SUPPLIER.NAME%TYPE,
  	p_package_aid IN PACKAGE.aid%TYPE,
  	p_package_name IN PACKAGE.NAME%TYPE,
  	p_package_version IN PACKAGE.VERSION%TYPE,
  	p_package_type IN PACKAGE.property%TYPE,
  	p_package_installSize IN PACKAGE.installed_size%TYPE,
  	p_package_javamv IN PACKAGE.java_vm%TYPE,
  	p_package_dapsize IN PACKAGE.dap_size%TYPE,
  	p_package_capsize IN PACKAGE.cap_size%TYPE,
  	p_package_load_file IN PACKAGE.load_file%TYPE,
  	p_package_status IN PACKAGE.status%TYPE,
  	p_nbElements IN NUMBER,
  	p_card_profile_desc_tab IN CHARARRAY30,
  	p_volume_name IN VOLUME.volume_name%TYPE,
  	p_install_parameters IN PACKAGE.INSTALL_PARAMETERS%TYPE
	)IS

	v_card_profile_desc_tab  Provappli.CHARARRAY30;
  BEGIN

 	  FOR i IN 1..p_nbElements LOOP
 	    v_card_profile_desc_tab(i) := p_card_profile_desc_tab(i);
 	  END LOOP;

	  Provappli.create_Package (
	    p_supplier_name,
  		p_package_aid,
  		p_package_name,
  		p_package_version,
  		p_package_type,
  		p_package_installSize,
  		p_package_javamv,
  		p_package_dapsize,
  		p_package_capsize,
  		p_package_load_file,
  		p_package_status,
  		0,
  		v_card_profile_desc_tab,
  		p_volume_name,
  		p_install_parameters
	  );
  END create_Package;

  PROCEDURE modify_Package (
	p_package_aid 			IN PACKAGE.aid%TYPE,
	p_package_name 			IN PACKAGE.NAME%TYPE,
	p_package_status 		IN PACKAGE.status%TYPE,
	p_nbElements			IN NUMBER,
	p_Card_profile_desc_tab IN CHARARRAY30,
	p_volume_name			IN VOLUME.volume_name%TYPE,
	p_supplier_name 		IN SUPPLIER.NAME%TYPE,
	p_package_version 		IN PACKAGE.VERSION%TYPE,
	p_package_type 			IN PACKAGE.property%TYPE,
	p_package_installSize 	IN PACKAGE.installed_size%TYPE,
	p_package_javavm 		IN PACKAGE.java_vm%TYPE,
	p_package_dapsize 		IN PACKAGE.dap_size%TYPE,
	p_package_capsize 		IN PACKAGE.cap_size%TYPE,
	p_package_load_file 	IN PACKAGE.load_file%TYPE,
	p_install_parameters 	IN PACKAGE.INSTALL_PARAMETERS%TYPE
	)IS

	v_card_profile_desc_tab  Provappli.CHARARRAY30;
  BEGIN

 	  FOR i IN 1..p_nbElements LOOP
 	    v_card_profile_desc_tab(i) := p_card_profile_desc_tab(i);
 	  END LOOP;

	  Provappli.modify_Package (
  		p_package_aid,
  		p_package_name,
  		p_package_status,
  		p_nbElements,
  		v_Card_profile_desc_tab,
  		p_volume_name,
  	  p_supplier_name,
  		p_package_version,
  		p_package_type,
  		p_package_installSize,
  		p_package_javavm,
  		p_package_dapsize,
  		p_package_capsize,
  		p_package_load_file,
  		p_install_parameters
  	);
  END modify_Package;

  PROCEDURE createSimCardApplication (
  	p_msisdn IN SUBSCRIBER.msisdn%TYPE,
  	p_aid IN APPLICATION.aid%TYPE,
  	p_package_aids	IN CHARARRAY32,
  	p_isDownloaded IN NUMBER
  ) IS
  BEGIN

  	-- insert application in sim_card_application
  	--IF p_isDownloaded = 1 THEN
  	 Application_Pack.insLkSimCardApplication (
  		p_aid,
  		p_msisdn,
  		'f', -- unlocked
  		's' -- selectable
  	 );

  	 -- insert packages of application in sim_card_packages
  	 FOR i IN p_package_aids.FIRST..p_package_aids.LAST LOOP
  		Application_Pack.insLkSimCardPackage (
  			p_package_aids(i),
  			p_msisdn
  		);
  	 END LOOP;
  	--END IF;
  END createSimCardApplication;

  PROCEDURE installSimCardApplication (
    p_msisdn IN SUBSCRIBER.msisdn%TYPE,
    p_application_aid IN APPLICATION.aid%TYPE,
    p_install_aid IN SIM_CARD_APPLICATION.aid%TYPE,
    p_install_tar IN SIM_CARD_APPLICATION.tar%TYPE,
    p_install_param IN SIM_CARD_APPLICATION.install_params%TYPE
  ) IS

  v_appli_id NUMBER;
  v_sim_card_id NUMBER;

  err_app_exists EXCEPTION ;
  i NUMBER := 1;
  recCount NUMBER := 0;
  installApp NUMBER := 0;
  BEGIN

    DBMS_OUTPUT.PUT_LINE('p_install_aid ' || p_install_aid);
    DBMS_OUTPUT.PUT_LINE('inserting new record');

    SELECT a.id, s.sim_card_id into v_appli_id, v_sim_card_id
    FROM application a, subscriber s
    WHERE a.aid = p_application_aid AND s.msisdn = p_msisdn;

    INSERT INTO SIM_CARD_APPLICATION
    (SIM_CARD_ID, APPLICATION_ID, INSTANCE_DATE, LOCKED, STATUS, AID, TAR, INSTALL_PARAMS)
    VALUES
    (v_sim_card_id, v_appli_id, sysdate, 'f', 's', p_install_aid, p_install_tar, p_install_param);
    commit;
  END installSimCardApplication;

  PROCEDURE unitaryInstall (
  	p_msisdn IN SUBSCRIBER.msisdn%TYPE,
  	p_aid IN APPLICATION.aid%TYPE

  ) IS
  BEGIN

  	 Application_Pack.insLkSimCardApplication (
  		p_aid,
  		p_msisdn,
  		'f', -- unlocked
  		's' -- selectable
  	 );

  END unitaryInstall;

  ---------------------------------------------------------------
  --Create Application
  ---------------------------------------------------------------
  PROCEDURE create_Application (
  	p_applet_aid 		IN applet.aid%TYPE,
  	p_application_aid 	IN application.aid%TYPE,
  	p_application_name 	IN application.name%TYPE,
  	p_application_type 	IN application.type%TYPE,
  	p_application_status 	IN application.status%TYPE,
  	p_application_class	IN windappclass.application_class%TYPE,
  	p_application_instanceSize 	IN application.instance_size%TYPE,
  	p_ram_size 		IN application.required_ram%TYPE,
  	p_install_param 	IN application.install_parameters%TYPE,
  	p_privileges 		IN application.privilege%TYPE,
  	p_allowed_media 	IN application.allowed_media%TYPE,
  	p_nb_Cp			IN NUMBER,
  	p_cpDesc		IN CharArray30,
  	p_volume_name	IN volume.volume_name%TYPE,
  	p_token_flag		IN application.token_flag%TYPE,
  	p_tsm_user_id   IN tsm_application.tsm_user_id%TYPE,
  	p_supplier_name 	IN supplier.name%TYPE,
  	p_applet_name 		IN applet.name%TYPE,
  	p_applet_version 	IN applet.version%TYPE,
   	--p_applet_release_date 	IN applet.release_date%TYPE,
  	p_applet_status 	IN applet.status%TYPE,
  	p_nbpackages 		IN NUMBER,
  	p_package_aid 		IN CharArray32,
  	p_constrained 		IN NumberArray,
  	p_tar               IN application.tar%TYPE
  ) IS
  	v_card_profile_desc_tab		Provappli.CHARARRAY30;
  	v_package_aid_tab		Provappli.CHARARRAY32;
	v_constrained_tab		Provappli.NUMBERARRAY;

  BEGIN
	FOR i IN 1..p_nb_Cp LOOP
		v_card_profile_desc_tab(i) := p_cpDesc(i);
	END LOOP;

	FOR i IN 1..p_nbpackages LOOP
		v_package_aid_tab(i) := p_package_aid(i);
	END LOOP;

	FOR i IN 1..p_nbpackages LOOP
		v_constrained_tab(i) := p_constrained(i);
	END LOOP;

	Begin

	Provappli.create_Applet (
			p_supplier_name,
			p_applet_aid,
			p_applet_name,
			p_applet_version,
 			--p_applet_release_date,
			p_applet_status,
			p_nbpackages,
			v_package_aid_tab,
			v_constrained_tab
	);

	Exception
			When Others Then
			RAISE_APPLICATION_ERROR(-20000,'almProvision_CREATE_APPLET_ERROR');
	End;

	Begin

	Provappli.create_Application (
		p_applet_aid,
		p_application_aid,
		p_application_name,
		p_application_type,
		p_application_status,
		p_application_class,
		p_application_instanceSize,
		p_ram_size,
		p_install_param,
		p_privileges,
		p_allowed_media,
		p_nb_Cp,
		v_card_profile_desc_tab,
 	  p_volume_name,
		p_token_flag,
		p_tsm_user_id,
		p_tar
	);

	EXCEPTION
		When Others Then
			 provappli.delete_applet(p_applet_name,1);
		 	 RAISE_APPLICATION_ERROR(-20000,'almProvision_CREATE_APPLICATION_ERROR');

	END;

  END create_Application;

  ---------------------------------------------------------------

PROCEDURE create_SecurityDomain (
  p_applet_aid 		IN applet.aid%TYPE,
	p_application_aid 	IN application.aid%TYPE,
	p_application_name 	IN application.name%TYPE,
	p_application_type 	IN application.type%TYPE,
	p_application_status 	IN application.status%TYPE,
	p_application_class	IN windappclass.application_class%TYPE,
	p_application_instanceSize 	IN application.instance_size%TYPE,
	p_ram_size 		IN application.required_ram%TYPE,
	p_install_param 	IN application.install_parameters%TYPE,
	p_privileges 		IN application.privilege%TYPE,
	p_allowed_media 	IN application.allowed_media%TYPE,
	p_nb_Cp			IN NUMBER,
	p_cpDesc		IN CharArray30,
	p_volume_name	IN volume.volume_name%TYPE,
	p_token_flag		IN application.token_flag%TYPE,
	p_tsm_user_id   IN tsm_application.tsm_user_id%TYPE,
	p_supplier_name 	IN supplier.name%TYPE,
	p_applet_name 		IN applet.name%TYPE,
	p_applet_version 	IN applet.version%TYPE,
	p_applet_status 	IN applet.status%TYPE,
	p_nbpackages 		IN NUMBER,
	p_package_aid 		IN CharArray32,
	p_constrained 		IN NumberArray,
	p_tar               IN application.tar%TYPE,
	p_counter IN security_domain_msl.counter%TYPE,
	p_ciphering IN security_domain_msl.ciphering%TYPE,
	p_crypto IN security_domain_msl.crypto%TYPE,
	p_por IN security_domain_msl.por%TYPE,
	p_por_response IN security_domain_msl.por_response%TYPE,
	p_por_ciphering IN security_domain_msl.por_ciphering%TYPE,
	p_por_security IN security_domain_msl.por_security%TYPE,
	p_kic_algo IN security_domain_msl.kic_algo%TYPE,
	p_kic_des_mode IN security_domain_msl.kic_des_mode%TYPE,
	p_kid_algo IN security_domain_msl.kid_algo%TYPE,
	p_kid_des_mode IN security_domain_msl.kid_des_mode%TYPE
)IS
  v_card_profile_desc_tab		Provappli.CHARARRAY30;
  v_package_aid_tab		Provappli.CHARARRAY32;
	v_constrained_tab		Provappli.NUMBERARRAY;

  BEGIN
	FOR i IN 1..p_nb_Cp LOOP
		v_card_profile_desc_tab(i) := p_cpDesc(i);
	END LOOP;

	FOR i IN 1..p_nbpackages LOOP
		v_package_aid_tab(i) := p_package_aid(i);
	END LOOP;

	FOR i IN 1..p_nbpackages LOOP
		v_constrained_tab(i) := p_constrained(i);
	END LOOP;

	Begin

	Provappli.create_Applet (
			p_supplier_name,
			p_applet_aid,
			p_applet_name,
			p_applet_version,
 			--p_applet_release_date,
			p_applet_status,
			p_nbpackages,
			v_package_aid_tab,
			v_constrained_tab
			);

	Exception
			When Others Then
			RAISE_APPLICATION_ERROR(-20000,'almProvision_CREATE_APPLET_ERROR');
	End;

	Begin

	Provappli.create_SecurityDomain (
		p_applet_aid,
		p_application_aid,
		p_application_name,
		p_application_type,
		p_application_status,
		p_application_class,
		p_application_instanceSize,
		p_ram_size,
		p_install_param,
		p_privileges,
		p_allowed_media,
		p_nb_Cp,
		v_card_profile_desc_tab,
 	  p_volume_name,
		p_token_flag,
		p_tsm_user_id,
    p_tar,
    p_counter,
    p_ciphering,
	  p_crypto,
	  p_por,
	  p_por_response,
	  p_por_ciphering,
	  p_por_security,
	  p_kic_algo,
	  p_kic_des_mode,
	  p_kid_algo,
	  p_kid_des_mode
	);

	EXCEPTION
		When Others Then
			 provappli.delete_applet(p_applet_name,1);
		 	 RAISE_APPLICATION_ERROR(-20000,'almProvision_CREATE_SECURITY_DOMAIN_ERROR');

	END;

  END create_SecurityDomain;
  PROCEDURE modify_Application (
  	p_application_aid	IN APPLICATION.aid%TYPE,
  	p_application_name	IN APPLICATION.NAME%TYPE,
  	p_application_install_param IN APPLICATION.install_parameters%TYPE,
  	p_application_privileges	IN APPLICATION.PRIVILEGE%TYPE,
  	p_application_status	IN APPLICATION.status%TYPE,
  	p_application_type 	IN APPLICATION.TYPE%TYPE,
  	p_application_class IN WINDAPPCLASS.application_class%TYPE,
  	p_nb_Cp				IN NUMBER,
  	p_cpDesc			IN CharArray30,
  	p_volume_name	IN volume.volume_name%TYPE,
  	p_elf_aid			IN PACKAGE.aid%TYPE,
  	p_applet_aid		IN APPLET.AID%TYPE,
  	p_applet_version	IN APPLET.VERSION%TYPE,
  	p_applet_supplier	IN SUPPLIER.NAME%TYPE,
  	p_nb_PackAid		IN NUMBER,
  	p_packAid			IN CharArray32,
  	p_security_domain_id IN SIM_CARD_APPLICATION.security_domain_id%TYPE,
  	p_usable IN SIM_CARD_APPLICATION.usable%TYPE,
  	p_token_flag		IN application.token_flag%TYPE,
  	p_tsm_user_id   IN tsm_application.tsm_user_id%TYPE,
  	p_tar IN application.tar%TYPE
  ) IS
  	v_card_profile_desc_tab	Provappli.CHARARRAY30;
  	v_package_aid_tab		Provappli.CHARARRAY32;
  BEGIN
	FOR i IN 1..p_nb_Cp LOOP
		v_card_profile_desc_tab(i) := p_cpDesc(i);
	END LOOP;

	FOR i IN 1..p_nb_PackAid LOOP
		v_package_aid_tab(i) := p_packAid(i);
	END LOOP;

	Provappli.modify_Application (
		p_application_aid,
		p_application_name,
		p_application_install_param,
		p_application_privileges,
		p_application_status,
		p_application_type,
		p_application_class,
		p_nb_Cp,
		v_card_profile_desc_tab,
		p_volume_name,
		p_elf_aid,
		p_applet_aid,
		p_applet_version,
		p_applet_supplier,
		p_nb_PackAid,
		v_package_aid_tab,
		p_security_domain_id,
		p_usable,
		p_token_flag,
		p_tsm_user_id,
		p_tar
	);
  END modify_Application;

  /******************************************************************************
	   NAME:       extraditeApplication
	   PURPOSE:    use to associate application to its security domain
	******************************************************************************/

  PROCEDURE extraditeApplication (
    p_msisdn IN SUBSCRIBER.msisdn%TYPE,
  	p_aid	IN APPLICATION.aid%TYPE,
  	p_destinationSdAid IN APPLICATION.aid%TYPE

  ) IS

  v_SD_id SIM_CARD_APPLICATION.SECURITY_DOMAIN_ID%TYPE;
  v_sim_id SUBSCRIBER.SIM_CARD_ID%TYPE;

  BEGIN
         BEGIN
           SELECT sim_card_id INTO  v_sim_id FROM SUBSCRIBER
           WHERE msisdn = p_msisdn;

         EXCEPTION
           WHEN NO_DATA_FOUND THEN
           RAISE_APPLICATION_ERROR(-20005, 'Subscriber not found');
         END;

 	 IF (p_destinationSdAid <> 'Card Domain') THEN
             
             BEGIN 
     	       SELECT b.ID INTO v_SD_id FROM APPLICATION b
    	       WHERE b.aid = p_destinationSdAid;

             EXCEPTION
               WHEN NO_DATA_FOUND THEN
               RAISE_APPLICATION_ERROR(-20005, 'Security Domain not found');
             END;

       	     UPDATE SIM_CARD_APPLICATION
    	     SET security_domain_id = v_SD_id
    	     WHERE aid = p_aid
    	     AND sim_card_id = v_sim_id;

	 ELSE

	     UPDATE SIM_CARD_APPLICATION
    	     SET security_domain_id = 0
    	     WHERE aid = p_aid
    	     AND sim_card_id = v_sim_id;

	 END IF;

         COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20005, 'Error in updating sim_card_application table');
  END extraditeApplication;

   /******************************************************************************
	   NAME:       getCampaignStatistics
	   PURPOSE:    retrieve the records of a certain camapaign_id
		 						 from campaign statistics table
	******************************************************************************/

  PROCEDURE getCampaignStatistics(
    p_campaignId IN CAMPAIGN_STATISTIC.campaign_id%TYPE,
    attempt OUT NUMBERARRAY,
    transientFailure OUT NUMBERARRAY,
    success OUT NUMBERARRAY,
    permanentFailure OUT NUMBERARRAY,
    addressedCard OUT NUMBERARRAY
  ) IS
  i NUMBER := 1;
  BEGIN
  	attempt := NUMBERARRAY();
  	transientFailure := NUMBERARRAY();
  	success := NUMBERARRAY();
  	permanentFailure := NUMBERARRAY();
	  addressedCard := NUMBERARRAY();
	FOR rec IN (SELECT * FROM CAMPAIGN_STATISTIC WHERE campaign_id= p_campaignId ORDER BY attempt ASC) LOOP
    	attempt.EXTEND();
    	transientFailure.EXTEND();
    	success.EXTEND();
    	permanentFailure.EXTEND();
		  addressedCard.EXTEND();
      attempt(i) := rec.attempt;
      transientFailure(i) := rec.nb_transient_failure;
      success(i) := rec.nb_success;
      permanentFailure(i) := rec.nb_perm_failure;
	    addressedCard(i) := rec.nb_cards_addressed;
	    i:= i + 1;
    END LOOP;
  END getCampaignStatistics;

  PROCEDURE createFailedCardsGroup(
  p_campaignId IN CAMPAIGN.ID%TYPE
  ) IS

	v_name CAMPAIGN.NAME%TYPE;
	v_parentID CAMPAIGN.parent_id%TYPE;
	v_id GROUP_REF.ID%TYPE;

  BEGIN

  SELECT NAME, parent_id INTO v_name, v_parentID FROM CAMPAIGN WHERE ID = p_campaignId;
  SELECT MAX(ID) INTO v_id FROM GROUP_REF;
  v_name := v_name || '_failed';

  INSERT INTO GROUP_REF VALUES (v_id+1, v_name, v_parentID);
  COMMIT;

	FOR rec IN (SELECT sim_card_id FROM CAMPAIGN_SIM_CARD WHERE campaign_id = p_campaignId AND (state = 3 OR state = 4)) LOOP
		INSERT INTO GROUP_SIM_CARD VALUES (v_id+1, rec.sim_card_id);
	END LOOP;

  END createFailedCardsGroup;


  /******************************************************************************
	   NAME:       splitCampaign
	   PURPOSE:    split a group into subgroups and create duplicate campaign(s)
	   			   addressing each subgroup created
	******************************************************************************/
	PROCEDURE splitCampaign (
	   campaignId IN CAMPAIGN.ID%TYPE,
 	   subsPerGroup IN PLS_INTEGER,
 	   childCampaignIds OUT NUMBERARRAY
	)
	IS
		campaign_data CAMPAIGN%ROWTYPE;
		campaign_time_slots Pkg_Campaign.tab_time_slot;
		campaign_op_type OPERATION_TYPE.ID%TYPE;
		param_vals Pkg_Campaign.tab_parameter;
		new_campaign_id CAMPAIGN.ID%TYPE;
		old_campaign_name CAMPAIGN.NAME%TYPE;
		new_campaign_name CAMPAIGN.NAME%TYPE;

		group_name GROUP_REF.NAME%TYPE;
		group_parent_id GROUP_REF.PARENT_ID%TYPE;
		new_group_id GROUP_REF.ID%TYPE;
		group_ids NUMBERARRAY;
		new_group_name GROUP_REF.NAME%TYPE;

		group_size PLS_INTEGER;
		num_groups_to_create PLS_INTEGER := 1;

		max_campaign_name_length CONSTANT PLS_INTEGER := 30;
		max_group_name_length CONSTANT PLS_INTEGER := 30;

		err_no_campaign_found EXCEPTION;
		err_no_campaign_params_found EXCEPTION;
		err_no_cmp_time_slots_found EXCEPTION;
	BEGIN

		childCampaignIds := NUMBERARRAY();

	   /* retrieve existing (base) campaign, time slots and parameters. */
	  BEGIN
	    SELECT * INTO campaign_data FROM CAMPAIGN WHERE ID = campaignId;

	    old_campaign_name := campaign_data.NAME;

	    SELECT OPERATION_TYPE_ID INTO campaign_op_type FROM OPERATION WHERE ID = campaign_data.operation_id;
	  EXCEPTION
	    WHEN NO_DATA_FOUND THEN
		    RAISE err_no_campaign_found;
		END;

	  BEGIN
	    SELECT VALUE BULK COLLECT INTO param_vals FROM PARAMETER WHERE operation_id = campaign_data.operation_id ORDER BY position ASC;
	  EXCEPTION
	    WHEN NO_DATA_FOUND THEN
	      RAISE err_no_campaign_params_found;
	  END;

	  BEGIN
	  	SELECT * BULK COLLECT INTO campaign_time_slots FROM CAMPAIGN_TIME_SLOT WHERE campaign_id = campaignId;
	  EXCEPTION
	  	WHEN NO_DATA_FOUND THEN
	  		RAISE err_no_cmp_time_slots_found;
	  END;

	  /* retrieve existing (base) group, count # of members. */
	  SELECT NAME, PARENT_ID, COUNT(sim_card_id) INTO
	         group_name, group_parent_id, group_size
	    FROM GROUP_REF a, GROUP_SIM_CARD b
	    WHERE a.ID = campaign_data.group_id
	      AND b.group_id = a.ID
	    GROUP BY NAME, PARENT_ID;

	  /* calculate # of groups/campaigns to create. */
	  num_groups_to_create := CEIL(group_size/subsPerGroup);

	  group_ids := NUMBERARRAY();

	  group_ids.EXTEND(num_groups_to_create);

	  /* create subgroups from the base group
	      (group name should follow the format <base_group_name>_1, <base_group_name>_2, etc) */
	  FOR i IN 1..num_groups_to_create LOOP
	    SELECT NVL(MAX(ID), 0)+1 INTO new_group_id FROM GROUP_REF;

	    /* make sure the name fits in the column */
	    new_group_name := group_name||'_'||LTRIM(TO_CHAR(i, '9999'));
	    IF LENGTH(new_group_name) > max_group_name_length THEN
	      new_group_name := SUBSTR(
	                          group_name,
	                          0,
	                          LENGTH(group_name) -
	                          (LENGTH(new_group_name) - max_group_name_length)
	                        ) || '_' || LTRIM(TO_CHAR(i, '9999'));
	    END IF;

	    INSERT INTO GROUP_REF (ID, NAME, PARENT_ID) VALUES (
	      new_group_id,
	      new_group_name,
	      group_parent_id
	      );

	    /* create group_sim_card records using paginated query */
	    INSERT INTO GROUP_SIM_CARD
	      (SELECT new_group_id, SIM_CARD_ID
	         FROM (SELECT /*+ FIRST_ROWS(N) */ a.*, ROWNUM rnum FROM
	         (SELECT * FROM GROUP_SIM_CARD WHERE group_id = campaign_data.group_id ORDER BY sim_card_id ASC) a
	            WHERE ROWNUM <= (i)*subsPerGroup)
	         WHERE rnum >= ((i-1)*subsPerGroup)+1);

	    group_ids(i) := new_group_id;
	  END LOOP;

	  /* create new campaigns from the base campaign using pkg_campaign.build
	      (campaign name should follow the format <campaign_name>_1, <campaign_name>_2, etc), using the new subgroups created. */
	  FOR i IN group_ids.FIRST..group_ids.LAST LOOP
	    campaign_data.group_id := group_ids(i);
	    new_campaign_name := old_campaign_name || '_' || LTRIM(TO_CHAR(i, '9999'));
	    IF LENGTH(new_campaign_name) > max_campaign_name_length THEN
	      new_campaign_name := SUBSTR(
	                             old_campaign_name,
	                             0,
	                             LENGTH(old_campaign_name) -
	                             (LENGTH(new_campaign_name) - max_campaign_name_length)
	                           ) || '_' || LTRIM(TO_CHAR(i, '9999'));
	    END IF;

	    campaign_data.NAME := new_campaign_name;

	    Pkg_Campaign.BUILD(campaign_data, campaign_time_slots, campaign_op_type, param_vals, new_campaign_id);

	    childCampaignIds.EXTEND();
	    childCampaignIds(i) := new_campaign_id;
	  END LOOP;

	  /* delete the original campaign and any records linked to it */
	  DELETE FROM CAMPAIGN_PROCESSING_SIM_CARD WHERE campaign_id = campaignId;

	  DELETE FROM CAMPAIGN_PROGRESS WHERE campaign_id = campaignId;

	  DELETE FROM CAMPAIGN_SIM_CARD WHERE campaign_id = campaignId;

	  DELETE FROM CAMPAIGN_TIME_SLOT WHERE campaign_id = campaignId;

	  DELETE FROM CAMPAIGN WHERE ID = campaignId;

	  COMMIT;

	  EXCEPTION
	    WHEN OTHERS THEN
	      -- Consider logging the error and then re-raise
	      RAISE;
	END splitCampaign;

	/******************************************************************************
	   NAME:       setCampaignValidityPeriod
	   PURPOSE:    set new validity period for campaign
	******************************************************************************/

	PROCEDURE setCampaignValidityPeriod (
  	p_campaignId IN CAMPAIGN.ID%TYPE,
  	p_validityPeriod IN NUMBER
  ) IS
  	v_operation_id 	CAMPAIGN.OPERATION_ID%TYPE;
  BEGIN

		SELECT operation_id INTO v_operation_id FROM CAMPAIGN WHERE ID = p_campaignId;

		UPDATE PARAMETER SET VALUE = TO_CHAR(p_validityPeriod)
			WHERE operation_id = v_operation_id AND position = 1;

  END setCampaignValidityPeriod;

	PROCEDURE auditUnknownApplication (
		p_sim_card_id IN SIM_CARD.ID%TYPE,
		p_card_profile_id IN CARD_PROFILE.ID%TYPE,
		p_app_aid IN APPLICATION.aid%TYPE,
		p_app_type IN APPLICATION.TYPE%TYPE,
		p_app_status IN APPLICATION.status%TYPE,
		p_app_priv IN APPLICATION.PRIVILEGE%TYPE,
		p_packageAid IN PACKAGE.AID%TYPE,
    p_installAid IN SIM_CARD_APPLICATION.AID%TYPE,
    p_tar IN SIM_CARD_APPLICATION.TAR%TYPE,
    p_params IN SIM_CARD_APPLICATION.INSTALL_PARAMS%TYPE
	) IS
		the_id APPLICATION.ID%TYPE;
		the_name APPLICATION.NAME%TYPE;
		v_package_id PACKAGE.ID%TYPE;
		the_priv APPLICATION.PRIVILEGE%TYPE;
		v_applet_id APPLET.ID%TYPE;
		the_usable PLS_INTEGER;
    v_appli_aid APPLICATION.AID%TYPE;
    v_app_locked SIM_CARD_APPLICATION.LOCKED%TYPE;

	BEGIN
    v_app_locked := 'f';

    IF (p_app_status = 'l') THEN
      v_app_locked := 't';
    END IF;

		-- retrieve package ID given AID
		IF crGetPackageId%ISOPEN THEN 
			CLOSE crGetPackageId; 
		END IF;
		
		OPEN crGetPackageId(p_packageAid);
		
		FETCH crGetPackageId INTO v_package_id;
		
		CLOSE crGetPackageId;

		-- APPLET:
		SELECT NVL((MAX(ID) + 1),1)
		INTO the_id
		FROM APPLET;

		the_name := CONCAT('unknown_', the_id);

		INSERT INTO APPLET
			(ID, SUPPLIER_ID, AID, NAME, VERSION, RELEASE_DATE, STATUS, IMPORT_DATE)
		VALUES
			(the_id, 3, p_app_aid, the_name, 1.0, SYSDATE, p_app_status, SYSDATE);

		-- link package to applet
		INSERT INTO	APPLET_PACKAGE
			(APPLET_ID, PACKAGE_ID, IS_CONTAINED)
		VALUES
			(the_id, v_package_id, 't');

		v_applet_id := the_id;

		--APPLICATION:
		SELECT NVL((MAX(ID) + 1),1)
		INTO the_id
		FROM APPLICATION;

		the_name := CONCAT('unknown_', the_id);
		the_usable := 0;

		INSERT INTO APPLICATION
			(ID, APPLET_ID, AID, NAME, TYPE, STATUS, INSTANCE_SIZE, REQUIRED_RAM,
			INSTALL_PARAMETERS, PRIVILEGE, ALLOWED_MEDIA, IMPORT_DATE, VOLUME_ID)
		VALUES
			(the_id, v_applet_id, p_app_aid, the_name, p_app_type, 'i', 1, 1,
			 '00', p_app_priv, 'OP', SYSDATE, 1);

			 
		-- define default application class
		INSERT INTO WINDAPPCLASS (APPLICATION_ID, APPLICATION_CLASS) 
		VALUES
		(the_id, 1);
		
		SELECT PRIVILEGE
		INTO the_priv
		FROM APPLICATION
		WHERE ID = the_id;

    IF (crGetAppliAid%ISOPEN) THEN
        CLOSE crGetAppliAid;
    END IF;

    OPEN crGetAppliAid(the_id, p_sim_card_id);

    FETCH crGetAppliAid INTO
       v_appli_aid;

    CLOSE crGetAppliAid;

			IF the_priv = '80' THEN
				INSERT INTO
					   SIM_CARD_APPLICATION
					   (SIM_CARD_ID, APPLICATION_ID, INSTANCE_DATE, LOCKED, STATUS, SECURITY_DOMAIN_ID, USABLE, AID, TAR, INSTALL_PARAMS)
			    VALUES
				 	   (p_sim_card_id, the_id, SYSDATE, v_app_locked, p_app_status, 0, 1, p_installAid, p_tar, p_params);
			ELSE
				INSERT INTO
					   SIM_CARD_APPLICATION
					   (SIM_CARD_ID, APPLICATION_ID, INSTANCE_DATE, LOCKED, STATUS, SECURITY_DOMAIN_ID, USABLE, aid, install_params)
			    VALUES
				 	   (p_sim_card_id, the_id, SYSDATE, v_app_locked, p_app_status, -1, the_usable, p_app_aid, '00');
	   END IF;

		-- associate to card profile of the audited SIM
		INSERT INTO	CARD_PROFILE_APPLICATION
			(CARD_PROFILE_ID, APPLICATION_ID)
		VALUES
			(p_card_profile_id, the_id);

	END auditUnknownApplication;

	PROCEDURE auditKnownApplication (
		p_sim_card_id IN SIM_CARD.ID%TYPE,
		p_card_profile_id IN CARD_PROFILE.ID%TYPE,
		p_app_aid IN APPLICATION.aid%TYPE,
		p_app_type IN APPLICATION.TYPE%TYPE,
		p_app_status IN APPLICATION.status%TYPE,
		p_privilege IN APPLICATION.PRIVILEGE%TYPE,
    p_installAid IN SIM_CARD_APPLICATION.AID%TYPE,
    p_tar IN SIM_CARD_APPLICATION.TAR%TYPE,
    p_params IN SIM_CARD_APPLICATION.INSTALL_PARAMS%TYPE,
    p_aidExists IN NUMBER
	) IS
		v_application_id	APPLICATION.ID%TYPE;
    v_appli_aid	APPLICATION.AID%TYPE;
		v_applet_id	APPLICATION.applet_id%TYPE;
		v_is_linked	PLS_INTEGER;
		the_usable	PLS_INTEGER;
		the_sd	APPLICATION.ID%TYPE;
		the_name	APPLICATION.NAME%TYPE;
		the_priv APPLICATION.PRIVILEGE%TYPE;
		v_app_locked SIM_CARD_APPLICATION.LOCKED%TYPE;

	BEGIN
    v_app_locked := 'f';

		IF crCheckAppAid%ISOPEN THEN
			CLOSE crCheckAppAid;
		END IF;

		OPEN crCheckAppAid(p_app_aid);
			FETCH crCheckAppAid INTO v_application_id, v_applet_id;

--		UPDATE APPLICATION SET
--			status = p_app_status,
--			PRIVILEGE = p_privilege
--		WHERE
--			ID = v_application_id;

		--check card profile/application association and create if necessary
		IF crCheckCPAppliLink%ISOPEN THEN
			CLOSE crCheckCPAppliLink;
		END IF;

		OPEN crCheckCPAppliLink(p_card_profile_id, v_application_id);
			FETCH crCheckCPAppliLink INTO v_is_linked;

		IF crCheckCPAppliLink%NOTFOUND THEN
			INSERT INTO CARD_PROFILE_APPLICATION
				(card_profile_id, application_id)
			VALUES
				(p_card_profile_id, v_application_id);
		END IF;

		CLOSE crCheckCPAppliLink;

		-- check sim card/package association and create if necessary
		INSERT INTO SIM_CARD_PACKAGE (sim_card_id, package_id)
				(SELECT p_sim_card_id, package_id FROM APPLET_PACKAGE AP
					WHERE applet_id = v_applet_id
					AND NOT EXISTS
						(SELECT 1 FROM SIM_CARD_PACKAGE WHERE sim_card_id = p_sim_card_id AND package_id = AP.package_id)
				);

		-- check sim card/application association and create if necessary
--		OPEN crCheckSimCardApplication(v_application_id, p_sim_card_id);
--		FETCH crCheckSimCardApplication INTO v_is_linked;

    IF (p_app_status = 'l') THEN
      v_app_locked := 't';
    END IF;

--		IF crCheckSimCardApplication%NOTFOUND THEN
    IF (p_aidExists = 0)  THEN --install aid not found in sim_card_aid
			  the_usable := 0;

				SELECT PRIVILEGE INTO the_priv FROM APPLICATION
				WHERE ID = v_application_id;

				IF the_priv = '80' THEN
					  INSERT INTO
					   SIM_CARD_APPLICATION
					   (SIM_CARD_ID, APPLICATION_ID, INSTANCE_DATE, LOCKED, STATUS, SECURITY_DOMAIN_ID, USABLE, AID, TAR, INSTALL_PARAMS)
	    	    VALUES
				 	   (p_sim_card_id, v_application_id, SYSDATE, v_app_locked, p_app_status, 0, 1, p_installAid, p_tar, p_params);
			  ELSE
						INSERT INTO
					   SIM_CARD_APPLICATION
					   (SIM_CARD_ID, APPLICATION_ID, INSTANCE_DATE, LOCKED, STATUS, SECURITY_DOMAIN_ID, USABLE, AID, TAR, INSTALL_PARAMS)
			    	VALUES
				 	   (p_sim_card_id, v_application_id, SYSDATE, v_app_locked, p_app_status, -1, the_usable, p_installAid, p_tar, p_params);
	 	    END IF;
	    ELSE

	      SELECT NAME, PRIVILEGE INTO the_name, the_priv FROM APPLICATION
	      WHERE ID = v_application_id;

	      IF the_priv = '80' THEN
	      	 the_usable := 1;
	      	 the_sd := 0;
	      ELSE
	      	 SELECT security_domain_id INTO the_sd FROM SIM_CARD_APPLICATION
	      	 WHERE sim_card_id = p_sim_card_id
	      	 AND aid = p_installAid;

	      	 the_name :=  SUBSTR(the_name, 1, 7);

	      	 IF (the_sd = -1 OR (the_name = 'unknown')) THEN
	         		the_usable := 0;
    			 ELSE
	         		the_usable := 1;
     			 END IF;
     	  END IF;

	      UPDATE SIM_CARD_APPLICATION
	      SET usable = the_usable,
	      		security_domain_id = the_sd,
            locked = v_app_locked,
            status = p_app_status
	      WHERE sim_card_id = p_sim_card_id
	      AND application_id = v_application_id;

		 	END IF;

	--	CLOSE crCheckSimCardApplication;

	END auditKnownApplication;

--===========
-- PACKAGE
--===========
	PROCEDURE auditPackage (
		p_sim_card_id IN SIM_CARD.ID%TYPE,
		p_card_profile_id IN CARD_PROFILE.ID%TYPE,
		p_aid IN PACKAGE.aid%TYPE,
		p_type IN APPLICATION.TYPE%TYPE,
		p_status IN PACKAGE.status%TYPE,
		p_privilege IN APPLICATION.PRIVILEGE%TYPE
	) IS

	v_aid PACKAGE.AID%TYPE;
	v_id PACKAGE.ID%TYPE;
	the_id PACKAGE.ID%TYPE;
	the_name PACKAGE.NAME%TYPE;
	the_sc_id SUBSCRIBER.SIM_CARD_ID%TYPE;
	the_applet_id APPLET.ID%TYPE;
	new_the_sc_id SIM_CARD_PACKAGE.sim_card_id%TYPE;

	CURSOR crCheckAid(p_aid IN PACKAGE.aid%TYPE)IS
		SELECT pack.ID
		FROM PACKAGE pack
		WHERE pack.AID = p_aid;

	CURSOR crCheckSimCardPackage(v_id PACKAGE.ID%TYPE, the_sc_id SUBSCRIBER.SIM_CARD_ID%TYPE) IS
		 SELECT SIM_CARD_ID
		 FROM SIM_CARD_PACKAGE
		 WHERE PACKAGE_ID = v_id AND SIM_CARD_ID = the_sc_id;

	BEGIN

		IF crCheckAid%ISOPEN THEN
			CLOSE crCheckAid;
		END IF;

		OPEN crCheckAid(p_aid);
			FETCH crCheckAid
		INTO v_id;

		IF crCheckAid%NOTFOUND THEN
			-- create package
			-- get id for package
			SELECT NVL((MAX(ID) + 1),1)I
				INTO the_id
			FROM PACKAGE;

			the_name := CONCAT('unknown_', the_id);

			-- create new package with default type/usage as utility/reference
			INSERT INTO PACKAGE
				(ID, SUPPLIER_ID, AID, NAME, VERSION, PROPERTY, INSTALLED_SIZE, JAVA_VM,
				 DAP_SIZE, CAP_SIZE, LOAD_FILE, STATUS, IMPORT_DATE, VOLUME_ID,
				 INSTALL_PARAMETERS)
			VALUES
				(the_id, 3, p_aid, the_name, 1.0, 'u', 1, 'JAVA',
				 0 , 0, the_name, 'R', SYSDATE, 1,
				 '00');

			INSERT INTO SIM_CARD_PACKAGE
				(SIM_CARD_ID, PACKAGE_ID)
			VALUES
				(p_sim_card_id, the_id);

		ELSE

			--UPDATE PACKAGE SET
			--	PACKAGE.STATUS = p_status
			--WHERE
			--	PACKAGE.ID = v_id;

			OPEN crCheckSimCardPackage(v_id, p_sim_card_id);
			FETCH crCheckSimCardPackage INTO new_the_sc_id;

			IF crCheckSimCardPackage%NOTFOUND THEN
				INSERT INTO SIM_CARD_PACKAGE
					(SIM_CARD_ID, PACKAGE_ID)
				VALUES
					(p_sim_card_id, v_id);
			END IF;

			CLOSE  crCheckSimCardPackage;

		END IF;

		CLOSE crCheckAid;

	END auditPackage;

	FUNCTION isKnownApplication(
		p_aid	IN APPLICATION.AID%TYPE
	) RETURN BOOLEAN
	IS
		v_id APPLICATION.ID%TYPE;

		CURSOR getAppliId(p_aid IN APPLICATION.AID%TYPE) IS
			SELECT ID FROM APPLICATION WHERE AID = p_aid;
	BEGIN
		IF getAppliId%ISOPEN THEN
			CLOSE getAppliId;
		END IF;

		OPEN getAppliId(p_aid);

		FETCH getAppliId INTO v_id;

		IF getAppliId%NOTFOUND THEN
			CLOSE getAppliId;
			RETURN FALSE;
		ELSE
			CLOSE getAppliId;
			RETURN TRUE;
		END IF;
	END isKnownApplication;

	FUNCTION isKnownPackage(
		p_aid	IN PACKAGE.AID%TYPE
	) RETURN BOOLEAN
	IS
		v_id PACKAGE.ID%TYPE;

		CURSOR getPackId(p_aid IN PACKAGE.AID%TYPE) IS
			SELECT ID FROM PACKAGE WHERE AID = p_aid;
	BEGIN
		IF getPackId%ISOPEN THEN
			CLOSE getPackId;
		END IF;

		OPEN getPackId(p_aid);

		FETCH getPackId INTO v_id;

		IF getPackId%NOTFOUND THEN
			CLOSE getPackId;
			RETURN FALSE;
		ELSE
			CLOSE getPackId;
			RETURN TRUE;
		END IF;
	END isKnownPackage;

	PROCEDURE updateSimCardApplications (
		p_msisdn IN SUBSCRIBER.msisdn%TYPE,
		p_app_aid IN CHARARRAY32,
		p_app_type IN CHARARRAY,
		p_app_status IN CHARARRAY,
		p_app_priv IN CHARARRAY06,
		p_pack_aid IN CHARARRAY32,
		p_pack_type IN CHARARRAY,
		p_pack_status IN CHARARRAY,
		p_pack_priv IN CHARARRAY06
	) IS
		i PLS_INTEGER;
		j PLS_INTEGER;

		v_sim_card_id SIM_CARD.ID%TYPE;
		v_card_profile_id CARD_PROFILE.ID%TYPE;
    v_install_params APPLICATION.INSTALL_PARAMETERS%TYPE;
    v_appli_id APPLICATION.ID%TYPE;
    v_installAid CHARARRAY32;
    v_appId NUMBERARRAY;
    aidIndex NUMBER := 1;
    n NUMBER := 1;
    installAidExists NUMBER := 0;

	BEGIN
		-- get sim_card id and card profile id
		IF crCheckSimCardCPId%ISOPEN THEN
			CLOSE crCheckSimCardCPId;
		END IF;

		OPEN crCheckSimCardCPId(p_msisdn);

		FETCH crCheckSimCardCPId INTO v_sim_card_id, v_card_profile_id;

		CLOSE crCheckSimCardCPId;


		v_installAid := CHARARRAY32();
		v_appId := NUMBERARRAY();

    -- get all installed applications on the card
		aidIndex := 1;
		FOR rec IN (SELECT application_id, aid FROM SIM_CARD_APPLICATION WHERE sim_card_id = v_sim_card_id ORDER BY application_id) LOOP
			v_installAid.EXTEND();
			v_appId.EXTEND();
			v_installAid(aidIndex) := rec.aid;
			v_appId(aidIndex) := rec.application_id;
			aidIndex := aidIndex + 1;
		END LOOP;

		j := p_pack_aid.FIRST;
		LOOP -- loop through package list, find first unknown package
			EXIT WHEN (j IS NULL) OR (isKnownPackage(p_pack_aid(j)) <> TRUE);
			j := p_pack_aid.NEXT(j);
		END LOOP;
		
		IF j IS NULL THEN -- no unknown package found
		  -- use 1st package as default package for unknown apps
		  j := p_pack_aid.FIRST;
		END IF;
		
		-- audit all packages first 
		i := p_pack_aid.FIRST;
		WHILE i IS NOT NULL LOOP
			-- audit packages as known/unknown
			auditPackage(v_sim_card_id, v_card_profile_id, p_pack_aid(i),
									 p_pack_type(i), p_pack_status(i), p_pack_priv(i));
			i := p_pack_aid.NEXT(i);
		END LOOP;
		
		-- at this point all unknown packages have been provisioned and linked to the card
				
		-- loop through all audited applications
		i := p_app_aid.FIRST;
		LOOP
			EXIT WHEN i IS NULL;
			-- check if application is unknown
			installAidExists := 0;
			n := 1;
			FOR n IN 1..aidIndex-1 LOOP
				IF (p_app_aid(i) = v_installAid(n)) THEN
					installAidExists := 1;
					EXIT WHEN (installAidExists = 1);
				END IF;
			END LOOP;

			IF ((installAidExists = 0) AND (isKnownApplication(p_app_aid(i)) <> TRUE)) THEN -- unknown application
				v_install_params := '';
				auditUnknownApplication(v_sim_card_id, v_card_profile_id, p_app_aid(i),
																p_app_type(i), p_app_status(i), p_app_priv(i),
																p_pack_aid(j), 
																p_app_aid(i), '000000',
																v_install_params);
			ELSE -- known application
				-- audit known application
				SELECT install_parameters INTO v_install_params FROM APPLICATION WHERE AID = p_app_aid(i);
				auditKnownApplication(v_sim_card_id, v_card_profile_id, p_app_aid(i),
															p_app_type(i), p_app_status(i), p_app_priv(i),
															p_app_aid(i), '000000',
															v_install_params, installAidExists);
			END IF;
			i := p_app_aid.NEXT(i);
		END LOOP;

	END updateSimCardApplications;


	PROCEDURE getRFMCATTPParams(
	  p_msisdn IN subscriber.msisdn% TYPE,
	  p_efname IN gsmfiles.ef_name% TYPE,
	  p_set_name OUT cattp_parameters_set.set_name%TYPE,
	  p_version OUT cattp_parameters_set.version%TYPE,
	  p_login OUT cattp_parameters_set.login%TYPE,
	  p_password OUT cattp_parameters_set.password%TYPE,
	  p_apn OUT cattp_parameters_set.apn%TYPE,
	  p_sdu_max_size OUT cattp_parameters_set.sdu_max_size%TYPE,
	  p_app_id OUT application_type.id%TYPE,
	  p_app_name OUT application_type.name%TYPE,
	  p_app_tar OUT application_type.tar%TYPE,
	  p_kic_index OUT card_profile_security.kic_index%TYPE,
	  p_kid_index OUT card_profile_security.kid_index%TYPE
	)

	IS
	--Local Variables
	  v_card_profile_id card_profile.id%TYPE;

	--Exception
	  err_app_type_name_is_APP_MG EXCEPTION;

	BEGIN

	  BEGIN
	    select b.card_profile_id into v_card_profile_id
	    from subscriber a, sim_card b
	    where a.sim_card_id = b.id and a.msisdn = p_msisdn;

	  EXCEPTION
	   WHEN TOO_MANY_ROWS THEN
	      RAISE_APPLICATION_ERROR (-20000, 'almDaoProc_CANNOT_GET_CARD_PROFILE (Returns more than one row)');
	   WHEN NO_DATA_FOUND THEN
	      RAISE_APPLICATION_ERROR (-20001, 'almDaoProc_NO_CARD_PROFILE_FOUND');
	   WHEN OTHERS THEN
	      RAISE;
	  END;

	  BEGIN
	    select d.set_name, d.version, d.login, d.password, d.apn, d.sdu_max_size into
	      p_set_name, p_version, p_login, p_password, p_apn, p_sdu_max_size
	      from subscriber a, sim_card b, origin_card_profile c, cattp_parameters_set d
	      where a.sim_card_id = b.id       
        and b.ocp_id = c.id        
        and c.cattp_parameters_set_id = d.id 
        and a.msisdn = p_msisdn;

	  EXCEPTION
	    WHEN TOO_MANY_ROWS THEN
	      RAISE_APPLICATION_ERROR (-20002, 'almDaoProc_CANNOT_GET_CATTP_PARAMETERS (Returns more than one row)');
	    WHEN NO_DATA_FOUND THEN
	      RAISE_APPLICATION_ERROR (-20003, 'almDaoProc_NO_CATTP_PARAMETERS_FOUND');
	    WHEN OTHERS THEN
	      RAISE;
	  END;

	  BEGIN
	      IF p_efname IS NOT NULL THEN
           SELECT c.ID, c.NAME, c.tar, d.kic_index, d.kid_index INTO
             p_app_id, p_app_name, p_app_tar, p_kic_index, p_kid_index
             FROM GSMFILES a, FILES_APPLICATION_TYPE b, APPLICATION_TYPE c, CARD_PROFILE_SECURITY d
             WHERE a.ef_name = b.ef_name AND b.application_type_id = c.ID AND c.ID = d.application_type_id AND d.bearer = 2
             AND a.ef_name = p_efname AND d.card_profile_id = v_card_profile_id;
        ELSE
           SELECT c.ID, c.NAME, c.tar, d.kic_index, d.kid_index INTO
             p_app_id, p_app_name, p_app_tar, p_kic_index, p_kid_index
             FROM APPLICATION_TYPE c, CARD_PROFILE_SECURITY d
             WHERE c.ID = d.application_type_id AND d.bearer = 2
             AND d.card_profile_id = v_card_profile_id AND (c.NAME = 'USIM1_MG' OR (c.Name = 'UICC1_MG' AND c.cmd_set='3G'));
        END IF;

	      IF p_app_name = 'APP_MG' THEN
	        RAISE err_app_type_name_is_APP_MG;
	      END IF;

	  EXCEPTION
	    WHEN TOO_MANY_ROWS THEN
	      RAISE_APPLICATION_ERROR (-20004, 'almDaoProc_CANNOT_GET_APP_TYPES (Returns more than one row)');
	    WHEN err_app_type_name_is_APP_MG THEN
	      RAISE_APPLICATION_ERROR (-20005, 'almDaoProc_CANNOT_RETURN_APP_TYPE_NAME_APP_MG');
	    WHEN NO_DATA_FOUND THEN
	      RAISE_APPLICATION_ERROR (-20006, 'almDaoProc_NO_APP_TYPES_FOUND');
	    WHEN OTHERS THEN
	      RAISE;
	  END;

	END getRFMCATTPParams;



	PROCEDURE getRAMCATTPParams(
	  p_msisdn IN subscriber.msisdn% TYPE,
	  p_set_name OUT cattp_parameters_set.set_name%TYPE,
	  p_version OUT cattp_parameters_set.version%TYPE,
	  p_login OUT cattp_parameters_set.login%TYPE,
	  p_password OUT cattp_parameters_set.password%TYPE,
	  p_apn OUT cattp_parameters_set.apn%TYPE,
	  p_sdu_max_size OUT cattp_parameters_set.sdu_max_size%TYPE,
	  p_app_id OUT application_type.id%TYPE,
	  p_app_name OUT application_type.name%TYPE,
	  p_app_tar OUT application_type.tar%TYPE,
	  p_kic_index OUT card_profile_security.kic_index%TYPE,
	  p_kid_index OUT card_profile_security.kid_index%TYPE
	)

	IS

	--Local Variables
	  v_card_profile_id card_profile.id%TYPE;

	BEGIN

	  BEGIN
	    select b.card_profile_id into v_card_profile_id
	    from subscriber a, sim_card b
	    where a.sim_card_id = b.id and a.msisdn = p_msisdn;

	  EXCEPTION
	   WHEN TOO_MANY_ROWS THEN
	      RAISE_APPLICATION_ERROR (-20007, 'almDaoProc_CANNOT_GET_CARD_PROFILE (Returns more than one row)');
	   WHEN NO_DATA_FOUND THEN
	      RAISE_APPLICATION_ERROR (-20008, 'almDaoProc_NO_CARD_PROFILE_FOUND');
	    WHEN OTHERS THEN
	      RAISE;
	  END;

	  BEGIN
	    select d.set_name, d.version, d.login, d.password, d.apn, d.sdu_max_size into
	      p_set_name, p_version, p_login, p_password, p_apn, p_sdu_max_size
	      from subscriber a, sim_card b, origin_card_profile c, cattp_parameters_set d
	      where a.sim_card_id = b.id       
        and b.ocp_id = c.id        
        and c.cattp_parameters_set_id = d.id 
        and a.msisdn = p_msisdn;

	  EXCEPTION
	    WHEN TOO_MANY_ROWS THEN
	      RAISE_APPLICATION_ERROR (-20009, 'almDaoProc_CANNOT_GET_CATTP_PARAMETERS (Returns more than one row)');
	    WHEN NO_DATA_FOUND THEN
	      RAISE_APPLICATION_ERROR (-20010, 'almDaoProc_NO_CATTP_PARAMETERS_FOUND');
	    WHEN OTHERS THEN
	      RAISE;
	  END;

	  BEGIN
	    SELECT c.ID, c.NAME, c.tar, d.kic_index, d.kid_index INTO
	      p_app_id, p_app_name, p_app_tar, p_kic_index, p_kid_index
	      FROM APPLICATION_TYPE c, CARD_PROFILE_SECURITY d
	      WHERE c.ID = d.application_type_id AND d.bearer = 2
	      AND c.NAME = 'APP_MG' AND d.card_profile_id = v_card_profile_id;

	  EXCEPTION
	    WHEN TOO_MANY_ROWS THEN
	      RAISE_APPLICATION_ERROR (-20011, 'almDaoProc_CANNOT_GET_APP_TYPES (Returns more than one row)');
	    WHEN NO_DATA_FOUND THEN
	      RAISE_APPLICATION_ERROR (-20012, 'almDaoProc_NO_APP_TYPES_FOUND');
	    WHEN OTHERS THEN
	      RAISE;
	  END;

	END getRAMCATTPParams;

	PROCEDURE getCardProfileSecurityParams (
		p_msisdn 					IN  subscriber.msisdn%TYPE,
		p_ef_name 				IN  gsmfiles.ef_name%TYPE DEFAULT NULL,
		p_push_sms_secu			OUT card_profile.push_sms_secu%TYPE,
		p_app_type_id 			OUT application_type.id%TYPE,
		p_tar								OUT application_type.tar%TYPE,
		p_classbyte					OUT application_type.class_byte%TYPE,
		p_secu_desc 				OUT security.description%TYPE,
		p_kic_index 				OUT card_profile_security.kic_index%TYPE,
		p_kid_index 				OUT card_profile_security.kid_index%TYPE,
		p_cattp_app_type_id	OUT application_type.id%TYPE,
		p_cattp_tar					OUT application_type.tar%TYPE,
		p_cattp_classbyte		OUT application_type.class_byte%TYPE,
		p_cattp_secu_desc		OUT security.description%TYPE,
		p_cattp_kic_index		OUT card_profile_security.kic_index%TYPE,
		p_cattp_kid_index		OUT card_profile_security.kid_index%TYPE
	) IS
		v_card_profile_id sim_card.card_profile_id%TYPE;
		v_sms_secu_found boolean := false;
		v_cattp_secu_found boolean := false;
	BEGIN
		-- fetch push sms secu flag and card profile id
		BEGIN
			select c.id, c.push_sms_secu into v_card_profile_id, p_push_sms_secu
			from card_profile c, sim_card sc, subscriber s
			where c.id = sc.card_profile_id
			and sc.id = s.sim_card_id
			and s.msisdn = p_msisdn;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				RAISE_APPLICATION_ERROR (-20013, 'almDaoProc_NO_CARD_PROFILE_FOUND');
		END;

		-- fetch app_type_id, secu_desc, kic/kid indices
		if p_ef_name is not null then -- RFM
			if p_ef_name <> 'CARD_AUDIT_OPERATION' then -- RFM with defined ef_name
				FOR rec in (
					select cps.application_type_id, at.tar, at.class_byte, s.description, cps.kic_index, cps.kid_index, cps.bearer
					from card_profile_security cps, files_application_type fat, security s, application_type at
					where s.id = cps.security_id
					and at.id = cps.application_type_id
					and cps.card_profile_id = v_card_profile_id
					and cps.application_type_id = fat.application_type_id
					and fat.ef_name = p_ef_name
					order by cps.bearer asc
				) LOOP
					if (rec.bearer = 0) then
						if not v_sms_secu_found then
							p_app_type_id := rec.application_type_id;
							p_tar := rec.tar;
							p_classbyte := rec.class_byte;
							p_secu_desc := rec.description;
							p_kic_index := rec.kic_index;
							p_kid_index := rec.kid_index;
							v_sms_secu_found := true;
						end if;
					elsif (rec.bearer = 2) then
						if not v_cattp_secu_found then
							p_cattp_app_type_id := rec.application_type_id;
							p_cattp_tar := rec.tar;
							p_cattp_classbyte := rec.class_byte;
							p_cattp_secu_desc := rec.description;
							p_cattp_kic_index := rec.kic_index;
							p_cattp_kid_index := rec.kid_index;
							v_cattp_secu_found := true;
						end if;
					end if;
				END LOOP;
			else -- card audit operation (RFM without any ef_name)
				-- TODO: support distinct RFM 2G/3G for each bearer (return 4 sets of app type/security/kic/kid)
				FOR rec in (
					SELECT DISTINCT cps.application_type_id, AT.tar, AT.name, AT.class_byte, s.description, cps.kic_index, cps.kid_index, cps.bearer
					FROM card_profile_security cps, SECURITY s, application_type AT
					WHERE s.ID = cps.security_id
					AND AT.ID = cps.application_type_id
					AND cps.card_profile_id = v_card_profile_id
					AND ((AT.name = 'USIM1_MG' OR AT.name = 'UICC1_MG') OR AT.name = 'GSM_MG')
					ORDER BY cps.bearer ASC, AT.name DESC
				) LOOP
					if (rec.bearer = 0) then
						if not v_sms_secu_found then
							p_app_type_id := rec.application_type_id;
							p_tar := rec.tar;
							p_classbyte := rec.class_byte;
							p_secu_desc := rec.description;
							p_kic_index := rec.kic_index;
							p_kid_index := rec.kid_index;
							v_sms_secu_found := true;
						end if;
					elsif (rec.bearer = 2) then
						if not v_cattp_secu_found then
							p_cattp_app_type_id := rec.application_type_id;
							p_cattp_tar := rec.tar;
							p_cattp_classbyte := rec.class_byte;
							p_cattp_secu_desc := rec.description;
							p_cattp_kic_index := rec.kic_index;
							p_cattp_kid_index := rec.kid_index;
							v_cattp_secu_found := true;
						end if;
					end if;
				END LOOP;
			end if;

			IF (not v_sms_secu_found) THEN
				RAISE_APPLICATION_ERROR (-20014, 'almDaoProc_NO_RFM_SMS_SECU_FOUND');
			END IF;

			IF (not v_cattp_secu_found) THEN
				-- use sms security for cattp
				p_cattp_app_type_id := p_app_type_id;
				p_cattp_tar := p_tar;
				p_cattp_classbyte := p_classbyte;
				p_cattp_secu_desc := p_secu_desc;
				p_cattp_kic_index := p_kic_index;
				p_cattp_kid_index := p_kid_index;
			END IF;

		else -- RAM
			FOR rec in (
				select cps.application_type_id, at.tar, at.class_byte, s.description, cps.kic_index, cps.kid_index, cps.bearer
				from card_profile_security cps, application_type at, security s
				where s.id = cps.security_id
				and cps.card_profile_id = v_card_profile_id
				and cps.application_type_id = at.id
				and at.name = 'APP_MG'
				order by cps.bearer asc
			) LOOP
				if (rec.bearer = 0) then
					p_app_type_id := rec.application_type_id;
					p_tar := rec.tar;
					p_classbyte := rec.class_byte;
					p_secu_desc := rec.description;
					p_kic_index := rec.kic_index;
					p_kid_index := rec.kid_index;
					v_sms_secu_found := true;
				elsif (rec.bearer = 2) then
					p_cattp_app_type_id := rec.application_type_id;
					p_cattp_tar := rec.tar;
					p_cattp_classbyte := rec.class_byte;
					p_cattp_secu_desc := rec.description;
					p_cattp_kic_index := rec.kic_index;
					p_cattp_kid_index := rec.kid_index;
					v_cattp_secu_found := true;
				end if;

			END LOOP;

			IF (not v_sms_secu_found) THEN
				RAISE_APPLICATION_ERROR (-20015, 'almDaoProc_NO_RAM_SMS_SECU_FOUND');
			END IF;

			IF (not v_cattp_secu_found) THEN
				-- use sms security for cattp
				p_cattp_app_type_id := p_app_type_id;
				p_cattp_tar := p_tar;
				p_cattp_classbyte := p_classbyte;
				p_cattp_secu_desc := p_secu_desc;
				p_cattp_kic_index := p_kic_index;
				p_cattp_kid_index := p_kid_index;
			END IF;

		END IF;

	END getCardProfileSecurityParams;

	PROCEDURE getFileAuditSecurityParams (
		p_msisdn 					IN  subscriber.msisdn%TYPE,
		p_push_sms_secu			OUT card_profile.push_sms_secu%TYPE,
		p_2g_app_type_id OUT application_type.id%TYPE,
		p_2g_tar						OUT application_type.tar%TYPE,
		p_2g_classbyte			OUT application_type.class_byte%TYPE,
		p_2g_secu_desc 			OUT security.description%TYPE,
		p_2g_kic_index 			OUT card_profile_security.kic_index%TYPE,
		p_2g_kid_index 			OUT card_profile_security.kid_index%TYPE,
		p_3g_app_type_id 		OUT application_type.id%TYPE,
		p_3g_tar						OUT application_type.tar%TYPE,
		p_3g_classbyte			OUT application_type.class_byte%TYPE,
		p_3g_secu_desc 			OUT security.description%TYPE,
		p_3g_kic_index 			OUT card_profile_security.kic_index%TYPE,
		p_3g_kid_index 			OUT card_profile_security.kid_index%TYPE,
		p_2g_cattp_app_type_id	OUT application_type.id%TYPE,
		p_2g_cattp_tar					OUT application_type.tar%TYPE,
		p_2g_cattp_classbyte		OUT application_type.class_byte%TYPE,
		p_2g_cattp_secu_desc		OUT security.description%TYPE,
		p_2g_cattp_kic_index		OUT card_profile_security.kic_index%TYPE,
		p_2g_cattp_kid_index		OUT card_profile_security.kid_index%TYPE,
		p_3g_cattp_app_type_id	OUT application_type.id%TYPE,
		p_3g_cattp_tar					OUT application_type.tar%TYPE,
		p_3g_cattp_classbyte		OUT application_type.class_byte%TYPE,
		p_3g_cattp_secu_desc		OUT security.description%TYPE,
		p_3g_cattp_kic_index		OUT card_profile_security.kic_index%TYPE,
		p_3g_cattp_kid_index		OUT card_profile_security.kid_index%TYPE
	) IS
		v_card_profile_id sim_card.card_profile_id%TYPE;
		v_sms_2g_secu_found boolean := false;
		v_sms_3g_secu_found boolean := false;
		v_cattp_2g_secu_found boolean := false;
		v_cattp_3g_secu_found boolean := false;
		v_secu_found boolean := false;
		v_sms_secu_found boolean := false;
		v_cattp_secu_found boolean := false;
	BEGIN
		-- fetch push sms secu flag and card profile id
		BEGIN
			select c.id, c.push_sms_secu into v_card_profile_id, p_push_sms_secu
			from card_profile c, sim_card sc, subscriber s
			where c.id = sc.card_profile_id
			and sc.id = s.sim_card_id
			and s.msisdn = p_msisdn;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				RAISE_APPLICATION_ERROR (-20013, 'almDaoProc_NO_CARD_PROFILE_FOUND');
		END;

		FOR rec in (
			SELECT DISTINCT cps.application_type_id, AT.tar, AT.name, AT.cmd_set, AT.class_byte, s.description, cps.kic_index, cps.kid_index, cps.bearer
			FROM card_profile_security cps, files_application_type fat, SECURITY s, application_type AT
			WHERE s.ID = cps.security_id
			AND AT.ID = cps.application_type_id
			AND cps.card_profile_id = v_card_profile_id
			AND cps.application_type_id = fat.application_type_id
			AND (AT.NAME NOT IN ('APP_MG', 'RESIZE_MG'))
			ORDER BY cps.bearer ASC, AT.cmd_set ASC
		) LOOP
			if not v_secu_found then v_secu_found := true; end if;
			if (rec.bearer = 0) then
				if not v_sms_secu_found then v_sms_secu_found := true; end if;
				case rec.cmd_set
					when '2G' then
						if not v_sms_2g_secu_found then
							p_2g_app_type_id := rec.application_type_id;
							p_2g_tar := rec.tar;
							p_2g_classbyte := rec.class_byte;
							p_2g_secu_desc := rec.description;
							p_2g_kic_index := rec.kic_index;
							p_2g_kid_index := rec.kid_index;
							v_sms_2g_secu_found := true;
						end if;
					when '3G' then
						if not v_sms_3g_secu_found then
							p_3g_app_type_id := rec.application_type_id;
							p_3g_tar := rec.tar;
							p_3g_classbyte := rec.class_byte;
							p_3g_secu_desc := rec.description;
							p_3g_kic_index := rec.kic_index;
							p_3g_kid_index := rec.kid_index;
							v_sms_3g_secu_found := true;
						end if;
				end case;
			elsif (rec.bearer = 2) then
				if not v_cattp_secu_found then v_cattp_secu_found := true; end if;
				case rec.cmd_set
					when '2G' then
						if not v_cattp_2g_secu_found then
							p_2g_cattp_app_type_id := rec.application_type_id;
							p_2g_cattp_tar := rec.tar;
							p_2g_cattp_classbyte := rec.class_byte;
							p_2g_cattp_secu_desc := rec.description;
							p_2g_cattp_kic_index := rec.kic_index;
							p_2g_cattp_kid_index := rec.kid_index;
							v_cattp_2g_secu_found := true;
						end if;
					when '3G' then
						if not v_cattp_3g_secu_found then
							p_3g_cattp_app_type_id := rec.application_type_id;
							p_3g_cattp_tar := rec.tar;
							p_3g_cattp_classbyte := rec.class_byte;
							p_3g_cattp_secu_desc := rec.description;
							p_3g_cattp_kic_index := rec.kic_index;
							p_3g_cattp_kid_index := rec.kid_index;
							v_cattp_3g_secu_found := true;
						end if;
				end case;
			end if;
		END LOOP;

		if not v_secu_found then
				RAISE_APPLICATION_ERROR (-20013, 'almDaoProc_NO_SECURITY_FOUND');
		end if;

		if not v_sms_secu_found then
			-- use cattp security params for sms
			if v_cattp_2g_secu_found then
				p_2g_app_type_id := p_2g_cattp_app_type_id;
				p_2g_tar := p_2g_cattp_tar;
				p_2g_classbyte := p_2g_cattp_classbyte;
				p_2g_secu_desc := p_2g_cattp_secu_desc;
				p_2g_kic_index := p_2g_cattp_kic_index;
				p_2g_kid_index := p_2g_cattp_kid_index;
				v_sms_2g_secu_found := true;
			end if;
			if v_cattp_3g_secu_found then
				p_3g_app_type_id := p_3g_cattp_app_type_id;
				p_3g_tar := p_3g_cattp_tar;
				p_3g_classbyte := p_3g_cattp_classbyte;
				p_3g_secu_desc := p_3g_cattp_secu_desc;
				p_3g_kic_index := p_3g_cattp_kic_index;
				p_3g_kid_index := p_3g_cattp_kid_index;
				v_sms_3g_secu_found := true;
			end if;
		end if;

		-- check 2g/3g sms params
		if not v_sms_2g_secu_found then
			p_2g_app_type_id := p_3g_app_type_id;
			p_2g_tar := p_3g_tar;
			p_2g_classbyte := p_3g_classbyte;
			p_2g_secu_desc := p_3g_secu_desc;
			p_2g_kic_index := p_3g_kic_index;
			p_2g_kid_index := p_3g_kid_index;
		elsif not v_sms_3g_secu_found then
			p_3g_app_type_id := p_2g_app_type_id;
			p_3g_tar := p_2g_tar;
			p_3g_classbyte := p_2g_classbyte;
			p_3g_secu_desc := p_2g_secu_desc;
			p_3g_kic_index := p_2g_kic_index;
			p_3g_kid_index := p_2g_kid_index;
		end if;

		if not v_cattp_secu_found then
			-- use sms security params for cattp
			if v_sms_2g_secu_found then
				p_2g_cattp_app_type_id := p_2g_app_type_id;
				p_2g_cattp_tar := p_2g_tar;
				p_2g_cattp_classbyte := p_2g_classbyte;
				p_2g_cattp_secu_desc := p_2g_secu_desc;
				p_2g_cattp_kic_index := p_2g_kic_index;
				p_2g_cattp_kid_index := p_2g_kid_index;
				v_cattp_2g_secu_found := true;
			end if;
			if v_sms_3g_secu_found then
				p_3g_cattp_app_type_id := p_3g_app_type_id;
				p_3g_cattp_tar := p_3g_tar;
				p_3g_cattp_classbyte := p_3g_classbyte;
				p_3g_cattp_secu_desc := p_3g_secu_desc;
				p_3g_cattp_kic_index := p_3g_kic_index;
				p_3g_cattp_kid_index := p_3g_kid_index;
				v_cattp_3g_secu_found := true;
			end if;
		end if;

		-- check 2g/3g cattp params
		if not v_cattp_2g_secu_found then
			p_2g_cattp_app_type_id := p_3g_cattp_app_type_id;
			p_2g_cattp_tar := p_3g_cattp_tar;
			p_2g_cattp_classbyte := p_3g_cattp_classbyte;
			p_2g_cattp_secu_desc := p_3g_cattp_secu_desc;
			p_2g_cattp_kic_index := p_3g_cattp_kic_index;
			p_2g_cattp_kid_index := p_3g_cattp_kid_index;
		elsif not v_cattp_3g_secu_found then
			p_3g_cattp_app_type_id := p_2g_cattp_app_type_id;
			p_3g_cattp_tar := p_2g_cattp_tar;
			p_3g_cattp_classbyte := p_2g_cattp_classbyte;
			p_3g_cattp_secu_desc := p_2g_cattp_secu_desc;
			p_3g_cattp_kic_index := p_2g_cattp_kic_index;
			p_3g_cattp_kid_index := p_2g_cattp_kid_index;
		end if;

	END getFileAuditSecurityParams;
	
	PROCEDURE getSendApduSecurityParams (
		p_msisdn 			IN  subscriber.msisdn%TYPE,
		p_push_sms_secu		OUT card_profile.push_sms_secu%TYPE,
		p_app_type_id 		OUT application_type.id%TYPE,
		p_tar				OUT application_type.tar%TYPE,
		p_classbyte			OUT application_type.class_byte%TYPE,
		p_secu_desc 		OUT security.description%TYPE,
		p_kic_index 		OUT card_profile_security.kic_index%TYPE,
		p_kid_index 		OUT card_profile_security.kid_index%TYPE,
		p_cattp_app_type_id	OUT application_type.id%TYPE,
		p_cattp_tar			OUT application_type.tar%TYPE,
		p_cattp_classbyte	OUT application_type.class_byte%TYPE,
		p_cattp_secu_desc	OUT security.description%TYPE,
		p_cattp_kic_index	OUT card_profile_security.kic_index%TYPE,
		p_cattp_kid_index	OUT card_profile_security.kid_index%TYPE,
		p_sms_key_type		OUT card_profile_security.key_type%TYPE,
		p_cattp_key_type	OUT card_profile_security.key_type%TYPE,
		p_counter           OUT NUMBER,
		p_ciphering         OUT NUMBER,
		p_crypto            OUT NUMBER,
		p_por               OUT NUMBER,
		p_por_response      OUT NUMBER,
		p_por_ciphering     OUT NUMBER,
		p_por_security      OUT NUMBER,
		p_kic_algo          OUT NUMBER,
		p_kic_des_mode      OUT NUMBER,
		p_kid_algo          OUT NUMBER,
		p_kid_des_mode      OUT NUMBER,
		p_kik_algo          OUT NUMBER,
		p_kik_des_mode      OUT NUMBER
	) IS
		v_card_profile_id sim_card.card_profile_id%TYPE;
	    v_app_type_id application_type.id%type;
	    v_bearer card_profile_security.bearer%type;
		v_sms_secu_found boolean := false;
		v_cattp_secu_found boolean := false;
		
		v_ciphering VARCHAR2(1);
		v_por_ciphering VARCHAR2(1);
		v_crypto VARCHAR2(2);
		
	BEGIN
		-- fetch push sms secu flag and card profile id
		BEGIN
			select c.id, c.push_sms_secu into v_card_profile_id, p_push_sms_secu
			from card_profile c, sim_card sc, subscriber s
			where c.id = sc.card_profile_id
			and sc.id = s.sim_card_id
			and s.msisdn = p_msisdn;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				RAISE_APPLICATION_ERROR (-20013, 'almDaoProc_NO_CARD_PROFILE_FOUND');
		END;

		-- fetch app_type_id, secu_desc, kic/kid indices
    FOR rec in (
      select cps.application_type_id, at.tar, at.class_byte, s.description, cps.kic_index, cps.kid_index, cps.bearer, cps.key_type
      from card_profile_security cps, security s, application_type at
      where s.id = cps.security_id
      and at.id = cps.application_type_id
      and cps.card_profile_id = v_card_profile_id
      and at.name = 'APP_MG'
      order by cps.bearer asc
    ) LOOP
      if (rec.bearer = 0) then
        if not v_sms_secu_found then
          p_app_type_id := rec.application_type_id;
          p_tar := rec.tar;
          p_classbyte := rec.class_byte;
          p_secu_desc := rec.description;
          p_kic_index := rec.kic_index;
          p_kid_index := rec.kid_index;
          v_app_type_id := p_app_type_id;
          p_sms_key_type := rec.key_type;
          v_bearer := 0;
          v_sms_secu_found := true;
        end if;
      elsif (rec.bearer = 2) then
        if not v_cattp_secu_found then
          p_cattp_app_type_id := rec.application_type_id;
          p_cattp_tar := rec.tar;
          p_cattp_classbyte := rec.class_byte;
          p_cattp_secu_desc := rec.description;
          p_cattp_kic_index := rec.kic_index;
          p_cattp_kid_index := rec.kid_index;
          v_app_type_id := p_cattp_app_type_id;
		  p_cattp_key_type := rec.key_type;
          v_bearer := 2;
          v_cattp_secu_found := true;
        end if;
      end if;
    END LOOP;

    IF (not v_sms_secu_found) THEN
      RAISE_APPLICATION_ERROR (-20014, 'almDaoProc_NO_RAM_SMS_SECU_FOUND');
    END IF;

    IF (not v_cattp_secu_found) THEN
      -- use sms security for cattp
      p_cattp_app_type_id := p_app_type_id;
      p_cattp_tar := p_tar;
      p_cattp_classbyte := p_classbyte;
      p_cattp_secu_desc := p_secu_desc;
      p_cattp_kic_index := p_kic_index;
      p_cattp_kid_index := p_kid_index;
      p_cattp_key_type := p_sms_key_type;
    END IF;

    BEGIN
		select cs.counter, cs.ciphering, cs.crypto, cs.por, cs.por_response, cs.por_ciphering,
	      cs.por_security, cs.kic_algo, cs.kic_des_mode, cs.kid_algo, cs.kid_des_mode,
	      NVL(cs.kik_algo, 0), NVL(cs.kik_des_mode, 0)
	      into p_counter, v_ciphering, v_crypto, p_por, p_por_response, v_por_ciphering, p_por_security,
	      p_kic_algo, p_kic_des_mode, p_kid_algo, p_kid_des_mode, p_kik_algo,p_kik_des_mode
	      from cmm_security cs, card_profile_security cps
	      where cps.card_profile_id = v_card_profile_id
	      and cps.security_id = cs.id
	      and cps.application_type_id = v_app_type_id
	      and cps.bearer = v_bearer;
      
      IF v_ciphering = 't' THEN
        p_ciphering := 1;
      ELSE
        p_ciphering := 0;
      END IF;

      IF v_por_ciphering = 't' THEN
        p_por_ciphering := 1;
      ELSE
        p_por_ciphering := 0;
      END IF;

      IF v_crypto = 'rc' THEN
        p_crypto := 1;
      ELSIF v_crypto = 'cc' THEN
        p_crypto := 2;
      ELSIF v_crypto = 'ds' THEN
        p_crypto := 3;
      ELSE
        p_crypto := 0;
      END IF;
	  
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				RAISE_APPLICATION_ERROR (-20013, 'almDaoProc_NO_CARD_PROFILE_FOUND');
		END;
 	END getSendApduSecurityParams;

	PROCEDURE getporstatistics(
            p_campaign_id IN CAMPAIGN_POR_STATISTICS.CAMPAIGN_ID%TYPE,
            p_por OUT CHARARRAY10,
            p_por_desc OUT CHARARRAY48,
            p_nb_por OUT NUMBERARRAY
  )IS
  i NUMBER := 1;
  BEGIN
   	p_por := CHARARRAY10();
  	p_por_desc := CHARARRAY48();
  	p_nb_por := NUMBERARRAY();
	  FOR rec IN (SELECT * from CAMPAIGN_POR_STATISTICS WHERE CAMPAIGN_ID = p_campaign_id ORDER BY nb_por ASC) LOOP
    	p_por.EXTEND();
    	p_por_desc.EXTEND();
    	p_nb_por.EXTEND();
     	 p_por(i)  := rec.por_value;
     	 p_por_desc(i)  := rec.por_desc;
		 p_nb_por(i)  := rec.nb_por;
		 i:= i + 1;
    END LOOP;
  END getporstatistics;



PROCEDURE updateporstatistics(
		p_campaign_id IN CAMPAIGN_POR_STATISTICS.CAMPAIGN_ID%TYPE,
		p_por		  IN CAMPAIGN_POR_STATISTICS.POR_VALUE%TYPE,
		p_por_desc	  IN CAMPAIGN_POR_STATISTICS.POR_DESC%TYPE
	)
IS

BEGIN

	UPDATE CAMPAIGN_POR_STATISTICS set NB_POR = NB_POR+1 WHERE CAMPAIGN_ID = p_campaign_id
		AND POR_VALUE = p_por;

	IF SQL%ROWCOUNT = 0 THEN

	INSERT INTO CAMPAIGN_POR_STATISTICS(CAMPAIGN_ID, POR_VALUE, POR_DESC, NB_POR) values
		(p_campaign_id, p_por, p_por_desc, 1);

	END IF;


COMMIT;

EXCEPTION
	WHEN OTHERS THEN
	ROLLBACK;
	RAISE;

END updateporstatistics;

  PROCEDURE setTSMUserFlag(
      username IN USERS.USER_NAME%TYPE,
      tsmUserFlag IN USERS.TSM_FLAG%TYPE
    ) IS

    --Local variables
      v_id USERS.USER_ID%TYPE;

    --Exception
      err_user_not_exist EXCEPTION;

      BEGIN

          BEGIN
            --check existence of username in users table
              SELECT user_id INTO v_id FROM users where USER_NAME = username;
          EXCEPTION
              WHEN NO_DATA_FOUND THEN
                RAISE err_user_not_exist;
          END;

          UPDATE users SET tsm_flag = tsmUserFlag WHERE user_id = v_id;
          IF tsmUserFlag <> 'T' THEN
              DELETE from tsm_application WHERE tsm_user_id = v_id;
          END IF;

        EXCEPTION
            WHEN err_user_not_exist THEN
              ROLLBACK;
              RAISE_APPLICATION_ERROR (-20001, 'USERNAME DOES NOT EXIST');
            WHEN OTHERS THEN
              ROLLBACK;
              RAISE;

  END setTSMUserFlag;


  FUNCTION getTSMUserFlag(
    username IN USERS.USER_NAME%TYPE
  )
  RETURN USERS.TSM_FLAG%TYPE
  IS
  v_tsm_flag USERS.TSM_FLAG%TYPE;
  v_id USERS.USER_ID%TYPE;

  --Cursors
  CURSOR getUserName(c_username IN USERS.USER_NAME%TYPE) IS
			SELECT user_id, tsm_flag FROM users WHERE user_name = c_username;

  --Exception
  err_user_not_exist EXCEPTION;

  BEGIN

      IF getUserName%ISOPEN THEN
        CLOSE getUserName;
      END IF;

      OPEN getUserName(username);

      IF getUserName%NOTFOUND THEN
        RAISE err_user_not_exist;
      ELSE
        FETCH getUserName INTO v_id, v_tsm_flag;
        RETURN v_tsm_flag;
      END IF;
      CLOSE getUserName;

  EXCEPTION
      WHEN err_user_not_exist THEN
         ROLLBACK;
         RAISE_APPLICATION_ERROR (-20001, 'USERNAME DOES NOT EXIST');
      WHEN OTHERS THEN
         ROLLBACK;
         RAISE;

  END getTSMUserFlag;

  PROCEDURE registerTSMURL (
  	p_username IN USERS.USER_NAME%TYPE,
  	p_aidList  IN CHARARRAY32,
  	p_url	   IN TSM_NOTIF_URL.URL%TYPE
  ) IS

   v_userid			USERS.USER_ID%TYPE;
   v_application_id	APPLICATION.ID%TYPE;
   v_tsm_userid		NUMBER;

  BEGIN

	BEGIN
		SELECT user_id INTO v_userid
		FROM users
		WHERE user_name = p_username;

	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RAISE_APPLICATION_ERROR(-20001, 'Username does not exist.');

	END;

  	FOR i IN p_aidList.FIRST..p_aidList.LAST LOOP

	  BEGIN
        SELECT id INTO v_application_id
        FROM application
        WHERE aid = p_aidList(i);
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
        v_application_id := 0;
      END;

      BEGIN
        SELECT tsm_user_id INTO v_tsm_userid
        FROM tsm_application WHERE tsm_user_id=v_userid AND application_id=v_application_id;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
        v_tsm_userid := 0;
      END;

      IF v_tsm_userid <> 0 THEN
	  	BEGIN
			INSERT INTO TSM_NOTIF_URL(aid,url) VALUES(p_aidList(i), p_url);
		EXCEPTION
		   WHEN OTHERS THEN
		   ROLLBACK;
		   RAISE_APPLICATION_ERROR(-20001, 'Cannot insert: url: ' || p_url || ' aid: ' || p_aidList(i));
		END;

      END IF;

  	 END LOOP;

  END registerTSMURL;


  FUNCTION notifyTSM
    RETURN BOOLEAN

    IS

    --Local variables
    v_notify_value VARCHAR2(3);
    b_notify_tsm BOOLEAN;

    BEGIN
      --SELECT decode(VALUE, 'YES', TRUE, FALSE) INTO b_notify_tsm
      --  FROM CONFIG WHERE PARAMETER_NAME='NOTIFY_TSM';
      SELECT value INTO v_notify_value
        FROM CONFIG WHERE PARAMETER_NAME='NOTIFY_TSM';

      IF v_notify_value = 'YES' THEN
         b_notify_tsm := TRUE;
      ELSE
         b_notify_tsm := FALSE;
      END IF;

      RETURN b_notify_tsm;

    EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
        RAISE;

    END notifyTSM;

  PROCEDURE addTSMNotification(
    p_msisdn IN SUBSCRIBER.MSISDN%TYPE,
    p_operation IN TSM_OPERATION.ID%TYPE,
    p_aidList IN APPLICATION_PACK.CHARARRAY32,
    p_oldIccid IN VARCHAR2 default null,
    p_newIccid IN VARCHAR2 default null,
    p_oldMsisdn IN VARCHAR2 default null,
    p_newMsisdn IN VARCHAR2 default null
  ) IS

   v_operation_id	tsm_operation.id%TYPE;
   paramstring		varchar2(2000);
   URLlist		CHARARRAY256;
   err_operation_id	EXCEPTION;
   v_notif_log_id	tsm_notif_log.id%TYPE;
   v_num_params		tsm_operation.num_params%TYPE;
   v_aidValues   APPLICATION_PACK.CHARARRAY32;
   v_sql			varchar2(2000);
   temp_url			varchar2(2000);

  BEGIN

	IF p_operation != tsm_operation_lock AND p_operation != tsm_operation_unlock AND
	   p_operation != tsm_operation_sim_change AND p_operation != tsm_operation_msisdn_change THEN
		RAISE err_operation_id;
	ELSE
		Select num_params into v_num_params from tsm_operation where id = p_operation;

	END IF;

	IF p_aidList.COUNT <> 0 THEN
		paramstring := '';
		FOR i in p_aidList.FIRST..p_aidList.LAST LOOP
			paramstring := paramstring || '''' || p_aidList(i) || '''';
			if i < p_aidList.LAST THEN
				paramstring := paramstring || ',';
			end if;
		END LOOP;

		v_sql := '(Select distinct url from TSM_NOTIF_URL ' ||
			   ' Where aid in (' || paramstring || '))';

		EXECUTE IMMEDIATE v_sql BULK Collect into URLlist;

		IF SQL%FOUND THEN
			FOR k in URLlist.FIRST..URLlist.LAST Loop
				temp_url := URLlist(k);
				select TSM_NOTIF_LOG_ID_SEQ.NEXTVAL into v_notif_log_id from dual;

				insert into TSM_NOTIF_LOG (ID, URL, MSISDN, operation_id) values
					(v_notif_log_id, temp_url, p_msisdn, p_operation);

				if v_num_params > 0 then
					if p_operation = 3  then
						insert into TSM_NOTIF_PARAMS values(v_notif_log_id,1,p_oldIccid);
						insert into TSM_NOTIF_PARAMS values(v_notif_log_id,2,p_newIccid);
					end if;
					if p_operation = 4  then
						insert into TSM_NOTIF_PARAMS values(v_notif_log_id,1,p_newMsisdn);
					end if;

				end if;

				v_sql := '(select aid from tsm_notif_url ' ||
					  	 ' where url = :url and aid in (' || paramstring || '))';

				EXECUTE IMMEDIATE v_sql BULK Collect into v_aidValues USING temp_url;

		    FOR i in v_aidValues.FIRST..v_aidValues.LAST LOOP
		      Insert into tsm_notif_aid values (v_notif_log_id, v_aidValues(i));
		    END LOOP;

		  END LOOP;
		
		END IF;
	END IF;
  EXCEPTION
	WHEN err_operation_id THEN
		RAISE_APPLICATION_ERROR (-20001, 'Operation does not exist: '||p_operation);
	WHEN OTHERS THEN
		 ROLLBACK;
    RAISE;

  END addTSMNotification;

	PROCEDURE updateNotifyLog(
		p_id IN TSM_NOTIF_LOG.ID%TYPE,
		p_notified IN TSM_NOTIF_LOG.NOTIFIED%TYPE,
		p_interval IN NUMBER DEFAULT 1 -- # of days to wait before retrying notification
	)
	IS

	--Local variables
	v_num_retries TSM_NOTIF_LOG.NUM_RETRIES%TYPE;
	v_max_notify_retries NUMBER;

	--Cursors
	CURSOR crGetNumRetries IS
		SELECT num_retries
			FROM tsm_notif_log
			WHERE id = p_id;

	CURSOR crGetMaxNotifyRetries IS
		SELECT value FROM CONFIG
			WHERE PARAMETER_NAME = 'MAX_NOTIFY_RETRIES';

	--Exceptions
	err_get_num_retries EXCEPTION;
	err_get_max_notify_retries EXCEPTION;

	BEGIN
	IF p_notified = 'N' THEN
		--get num_retries
		IF crGetNumRetries%ISOPEN THEN
			CLOSE crGetNumRetries;
		END IF;

		OPEN crGetNumRetries;
		FETCH crGetNumRetries INTO v_num_retries;
		IF crGetNumRetries%NOTFOUND THEN
			RAISE err_get_num_retries;
		END IF;
		CLOSE crGetNumRetries;

		IF crGetMaxNotifyRetries%ISOPEN THEN
			CLOSE crGetMaxNotifyRetries;
		END IF;

		OPEN crGetMaxNotifyRetries;
		FETCH crGetMaxNotifyRetries INTO v_max_notify_retries;
		IF crGetMaxNotifyRetries%NOTFOUND THEN
			RAISE err_get_max_notify_retries;
		END IF;
		CLOSE crGetMaxNotifyRetries;

		IF v_num_retries = v_max_notify_retries THEN
			UPDATE tsm_notif_log SET notified = 'F' WHERE id = p_id;
		ELSE
			UPDATE tsm_notif_log SET notified = 'N',
				num_retries = v_num_retries + 1,
				retry_date = SYSDATE + p_interval
				WHERE id = p_id;
		END IF;
	ELSE
		UPDATE tsm_notif_log SET notified = p_notified WHERE id = p_id;
	END IF;

	EXCEPTION
		WHEN err_get_num_retries THEN
			ROLLBACK;
			RAISE_APPLICATION_ERROR(-20000, 'CANNOT GET NUMBER OF RETRIES');
		WHEN err_get_max_notify_retries THEN
			ROLLBACK;
			RAISE_APPLICATION_ERROR(-20001, 'CANNOT GET MAX NOTIFY RETRIES');
		WHEN OTHERS THEN
			ROLLBACK;
			RAISE;

	END updateNotifyLog;

   PROCEDURE getTSMNotifications(
		p_max_rows IN NUMBER DEFAULT 5,
		p_ids OUT NUMBERARRAY,
		p_urls OUT CHARARRAY200,
		p_msisdns OUT CHARARRAY20,
		p_operations OUT NUMBERARRAY,
		p_num_retries OUT NUMBERARRAY
	)
	IS
	--Local variables
	v_max_notify_retries NUMBER;
	i NUMBER := 1;

	--Cursors
	CURSOR crGetMaxNotifyRetries IS
		SELECT value FROM CONFIG
			WHERE PARAMETER_NAME = 'MAX_NOTIFY_RETRIES';

	--Exceptions
	err_get_max_notify_retries EXCEPTION;

	BEGIN

		IF crGetMaxNotifyRetries%ISOPEN THEN
			CLOSE crGetMaxNotifyRetries;
		END IF;

		OPEN crGetMaxNotifyRetries;
		FETCH crGetMaxNotifyRetries INTO v_max_notify_retries;
		IF crGetMaxNotifyRetries%NOTFOUND THEN
			RAISE err_get_max_notify_retries;
		END IF;
		CLOSE crGetMaxNotifyRetries;

		p_ids := NUMBERARRAY();
		p_urls := CHARARRAY200();
		p_msisdns := CHARARRAY20();
		p_operations := NUMBERARRAY();
		p_num_retries := NUMBERARRAY();

		FOR rec IN (SELECT id, url, msisdn, operation_id, num_retries
			FROM tsm_notif_log
			WHERE notified = 'N' AND num_retries < v_max_notify_retries
				AND NVL(retry_date, creation_date) <= SYSDATE
			ORDER BY NVL(retry_date, creation_date) ASC, num_retries ASC)

			LOOP
		      p_ids.extend();
		      p_urls.extend();
		      p_msisdns.extend();
		      p_operations.extend();
		      p_num_retries.extend();

		      p_ids(i) := rec.id;
		      p_urls(i) := rec.url;
		      p_msisdns(i) := rec.msisdn;
		      p_operations(i) := rec.operation_id;
		      p_num_retries(i) := rec.num_retries;

		      i := i+1;
			END LOOP;

	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			RAISE;

	END getTSMNotifications;

	PROCEDURE getTSMNotificationInfo(
		p_id IN TSM_NOTIF_LOG.ID%TYPE,
		p_paramlist OUT CHARARRAY200,
		p_aidlist OUT CHARARRAY32
	)
	IS

	--Local variables
	v_TSMNotifParamsNoOfRows NUMBER;
	v_TSMNotifAIDNoOfRows NUMBER;
	i NUMBER := 1;

	BEGIN
		p_paramlist := CHARARRAY200();

		FOR rec IN (SELECT value FROM tsm_notif_params
			WHERE notif_id = p_id
			ORDER BY position ASC)
			LOOP
		      p_paramlist.extend();
		      p_paramlist(i) := rec.value;
		      i := i+1;
		END LOOP;

		p_aidlist := CHARARRAY32();
		i := 1;

		FOR rec IN (SELECT aid FROM TSM_NOTIF_AID
			WHERE notif_id = p_id)
		    LOOP
		      p_aidlist.extend();
		      p_aidlist(i) := rec.aid;
		      i := i+1;
		    END LOOP;

	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			RAISE;

	END getTSMNotificationInfo;

PROCEDURE changeSubscriberMSISDN (
  	p_msisdn in SUBSCRIBER.MSISDN%TYPE,
	p_imsi   in sim_card.imsi%TYPE,
	p_new_msisdn in SUBSCRIBER.MSISDN%TYPE,
	p_new_name in subscriber.name%TYPE

  ) IS

   v_sim_card_id	SIM_CARD.ID%TYPE;
   v_parent_name	USERS.user_name%TYPE;
   v_user_profile_desc	USER_PROFILE.description%TYPE;
   v_group_ids		NUMBERARRAY;
   v_group_desc		provusers.CHARARRAY30;
   k			NUMBER;
   v_group_present NUMBER;

   lv_oldIccid	sim_card.iccid%TYPE;
   lv_newIccid	sim_card.iccid%TYPE;
   i NUMBER;
   lv_AIDs APPLICATION_PACK.CHARARRAY32;

   CURSOR crGetGroupID(p_sim_card_id IN SIM_CARD.ID%TYPE) IS
	  SELECT group_id from group_sim_card where sim_card_id = p_sim_card_id;

	BEGIN
    lv_oldIccid		:= '';
	  lv_newIccid		:= '';

		BEGIN
			SELECT sim_card_id INTO v_sim_card_id
			FROM subscriber
			WHERE msisdn = p_msisdn;

		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				 ROLLBACK;
				RAISE_APPLICATION_ERROR(-20001, 'MSISDN does not exist.');

		END;

		/*BEGIN
			select p.user_name, usp.DESCRIPTION
			into v_parent_name, v_user_profile_desc
			from users c, users p, user_profile usp
			where c.PARENT_ID = p.USER_ID
			and c.sim_card_id = v_sim_card_id
			and c.USER_PROFILE_ID = usp.ID;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				 ROLLBACK;
				RAISE_APPLICATION_ERROR(-20001, 'Parent Name OR user profile description does not exist.');

		END;*/

		v_group_present:=1;
		IF crGetGroupID%ISOPEN THEN
			CLOSE crGetGroupID;
		END IF;

		OPEN crGetGroupID(v_sim_card_id);
		FETCH crGetGroupID BULK COLLECT INTO v_group_ids;

		IF crGetGroupID%NOTFOUND THEN
			v_group_present := -1;
		END IF;

		IF crGetGroupID%ISOPEN THEN
		   CLOSE crGetGroupID;
		END IF;

		--BEGIN
		--select group_id bulk collect into v_group_ids
		--from group_sim_card where sim_card_id = v_sim_card_id;
		--EXCEPTION
		--	WHEN NO_DATA_FOUND THEN
		--		RAISE_APPLICATION_ERROR(-20001, 'group id does not exist.');
		--END;

		k := 0;

		IF v_group_present = 1 THEN
		FOR i IN v_group_ids.FIRST..v_group_ids.LAST LOOP
			k := k+1;

			BEGIN
				select name into v_group_desc(k) from group_ref where id = v_group_ids(i);
			EXCEPTION
				WHEN NO_DATA_FOUND THEN
				RAISE_APPLICATION_ERROR(-20001, 'Group ID does not exist. ID: ' || v_group_ids(i));
			END;

		END LOOP;
		END IF;

		--FOR j IN (1)..k LOOP
			--dbms_output.put_line ('group desc : ' || v_group_desc(j));
		--END LOOP ;

		provusers.delete_Subscriber(p_msisdn, 0);

		provusers.create_subscriber(p_imsi, p_new_name, p_new_msisdn, v_user_profile_desc, v_parent_name, k, v_group_desc, 1);

	    i := 1;
	    FOR rec IN (SELECT DISTINCT c.aid FROM sim_card a, sim_card_application b, application c
		                WHERE a.id = b.sim_card_id AND b.application_id = c.id
		                AND a.id = v_sim_card_id) LOOP

		      lv_AIDs(i) := rec.aid;
		      i := i + 1;
	    END LOOP;

		addTSMNotification(p_msisdn, tsm_operation_msisdn_change, lv_AIDs,
									lv_oldIccid, lv_newIccid, p_msisdn, p_new_msisdn);

	END changeSubscriberMSISDN;

	--getSimCardFullState 
  PROCEDURE getSimCardFullState ( 
    p_msisdn        IN SUBSCRIBER.msisdn%TYPE, 
    p_nb_rows_app   OUT NUMBER, 
    p_nb_rows_pack  OUT NUMBER, 
    p_imsi          OUT SIM_CARD.imsi%TYPE, 
    p_iccid         OUT SIM_CARD.iccid%TYPE, 
    p_eeprom        OUT SIM_CARD.eeprom%TYPE, 
    p_activity      OUT SIM_CARD.activity%TYPE, 
    p_subsname      OUT SUBSCRIBER.name%TYPE, 
    p_cpdesc        OUT CARD_PROFILE.description%TYPE, 
    p_ocpname       OUT ORIGIN_CARD_PROFILE.name%TYPE, 
    p_appaid        OUT CHARARRAY32, 
    p_appname       OUT CHARARRAY30, 
    p_privilege     OUT CHARARRAY06, 
    p_tar           OUT CHARARRAY06, 
    p_params        OUT CHARARRAY256, 
    p_locked        OUT CHARARRAY01, 
    p_appstatus     OUT CHARARRAY01, 
    p_sdname        OUT CHARARRAY30, 
    p_packaid       OUT CHARARRAY32, 
    p_packname      OUT CHARARRAY30 
    ) IS 
    v_secdomain_id  NUMBERARRAY; 
    v_simcard_id    Number; 
    temp_sdname     Varchar2(30); 
    i               NUMBER; 
 
  BEGIN 
 
    p_sdname := CHARARRAY30(); 
    p_packaid := CHARARRAY32(); 
    p_packname := CHARARRAY30(); 
 
    p_nb_rows_app := 0; 
    p_nb_rows_pack := 0; 
    i := 1; 
 
    BEGIN 
      Select a.name subsname, b.imsi, b.iccid, b.eeprom, b.activity, a.sim_card_id 
      INTO p_subsname, p_imsi, p_iccid, p_eeprom, p_activity, v_simcard_id 
      FROM subscriber a, sim_card b 
      WHERE a.sim_card_id = b.id 
      AND   a.msisdn = p_msisdn; 
    EXCEPTION 
      WHEN NO_DATA_FOUND THEN 
        RAISE_APPLICATION_ERROR(-20005, 'Unknown MSISDN: '||p_msisdn); 
      WHEN OTHERS THEN 
        RAISE_APPLICATION_ERROR(-20005, 'Cannot retrieve SIM_CARD data for msisdn: '||p_msisdn); 
    END; 
 
    BEGIN 
    Select b.description, c.name ocpname
	  INTO p_cpdesc, p_ocpname
      FROM sim_card a, card_profile b, origin_card_profile c, ocp_card_profile d
      WHERE a.ocp_id = c.id
      and c.id = d.ocp_id
      and d.cp_id = b.id
      and a.id = v_simcard_id;

    EXCEPTION 
      WHEN NO_DATA_FOUND THEN 
        RAISE_APPLICATION_ERROR(-20005, 'Incomplete card profile data for msisdn: '||p_msisdn); 
      WHEN OTHERS THEN 
        RAISE_APPLICATION_ERROR(-20005, 'Cannot retrieve card profile data for msisdn: '||p_msisdn); 
    END; 
 
 
    BEGIN 
      SELECT NVL(trim(b.aid),NVL(a.aid,' ')), a.name, b.locked, b.status, b.security_domain_id,
             NVL(trim(b.tar),NVL(a.tar,' ')), NVL(trim(b.install_params), NVL(a.install_parameters,' ')),
             NVL(a.privilege,' ') 
      BULK COLLECT INTO p_appaid, p_appname, p_locked, p_appstatus, v_secdomain_id, 
              p_tar, p_params, p_privilege 
      FROM application a, sim_card_application b 
      WHERE a.id = b.application_id 
      AND   b.sim_card_id = v_simcard_id order by a.name; 
 
      if SQL%FOUND then 
          FOR i IN v_secdomain_id.FIRST..v_secdomain_id.LAST LOOP 
            p_nb_rows_app := i; 
            p_sdname.extend(); 
            if v_secdomain_id(i) = 0 then 
              p_sdname(i) := 'CARD DOMAIN'; 
            end if; 
            if v_secdomain_id(i) = -1 then
              if UPPER(SUBSTR(p_appname(i),1,7)) = 'UNKNOWN' then
                p_sdname(i) := 'Unknown';
              else
                p_sdname(i) := 'CARD DOMAIN';
              end if;
            end if;
            if v_secdomain_id(i) > 0 then 
              BEGIN 
                Select name into temp_sdname from application where id = v_secdomain_id(i); 
                p_sdname(i) := temp_sdname; 
              EXCEPTION 
                WHEN OTHERS THEN 
                  RAISE_APPLICATION_ERROR(-20005, 'Cannot retrieve security domain name'); 
              END; 
            end if; 
 
          END LOOP; 
      end if; 
 
    EXCEPTION 
      WHEN OTHERS THEN 
         RAISE_APPLICATION_ERROR(-20005, 'Cannot retrieve application data'); 
    END; 
 
    FOR rec IN (SELECT a.aid aid, a.name name FROM package a, sim_card_package b 
              WHERE a.id = b.package_id AND b.sim_card_id = v_simcard_id) LOOP 
    	   p_packaid.EXTEND(); 
    	   p_packname.EXTEND(); 
    	   p_packaid(i)  := rec.aid; 
     	   p_packname(i)  := rec.name; 
     	   i := i+1; 
    END LOOP; 
 
    p_nb_rows_pack := i - 1; 
 
  END getSimCardFullState; 

PROCEDURE getSecurityDomainMSLRecord (
	p_application_aid 	IN application.aid%TYPE,
	p_counter OUT NUMBER,
	p_ciphering OUT NUMBER,
	p_crypto OUT NUMBER,
	p_por OUT NUMBER,
	p_por_response OUT NUMBER,
	p_por_ciphering OUT NUMBER,
	p_por_security OUT NUMBER,
	p_kic_algo OUT NUMBER,
	p_kic_des_mode OUT NUMBER,
	p_kid_algo OUT NUMBER,
	p_kid_des_mode OUT NUMBER
  )
  IS

  v_appli_id NUMBER;
  v_ciphering VARCHAR2(1);
  v_por_ciphering VARCHAR2(1);
  v_crypto VARCHAR2(2);
  v_sdIndicator NUMBER;

  err_invalid_application_aid EXCEPTION;
  err_sec_dom_id_not_exist EXCEPTION;
  err_not_sec_dom EXCEPTION;

  CURSOR cr_getApplicationId (cr_appli_aid IN application.aid%TYPE) IS
      SELECT id FROM application
        WHERE aid = cr_appli_aid;

  CURSOR cr_getSecurityDomainData (cr_appli_id IN NUMBER) IS
      SELECT counter, ciphering, crypto, por, por_response, por_ciphering, por_security,
            kic_algo, kic_des_mode, kid_algo, kid_des_mode
      FROM security_domain_msl
      WHERE security_domain_id = cr_appli_id;

  BEGIN

    v_sdIndicator := application_pack.isSecurityDomain(p_application_aid);
    dbms_output.put_line(v_sdIndicator);

    IF cr_getApplicationId%ISOPEN THEN
     CLOSE cr_getApplicationId;
    END IF;

    OPEN cr_getApplicationId(p_application_aid);
    FETCH cr_getApplicationId INTO v_appli_id;

    IF cr_getApplicationId%NOTFOUND THEN
      RAISE err_invalid_application_aid;
    END IF;

    CLOSE cr_getApplicationId;

    IF v_sdIndicator = 1 THEN

      IF cr_getSecurityDomainData%ISOPEN THEN
        CLOSE cr_getSecurityDomainData;
      END IF;

      OPEN cr_getSecurityDomainData(v_appli_id);
      FETCH cr_getSecurityDomainData INTO
        p_counter,
        v_ciphering,
        v_crypto,
        p_por,
        p_por_response,
        v_por_ciphering,
        p_por_security,
        p_kic_algo,
        p_kic_des_mode,
        p_kid_algo,
        p_kid_des_mode;

      IF cr_getSecurityDomainData%NOTFOUND THEN
        RAISE err_sec_dom_id_not_exist;
      END IF;

      CLOSE cr_getSecurityDomainData;

      IF v_ciphering = 't' THEN
        p_ciphering := 1;
      ELSE
        p_ciphering := 0;
      END IF;

      IF v_por_ciphering = 't' THEN
        p_por_ciphering := 1;
      ELSE
        p_por_ciphering := 0;
      END IF;

      IF v_crypto = 'rc' THEN
        p_crypto := 1;
      ELSIF v_crypto = 'cc' THEN
        p_crypto := 2;
      ELSIF v_crypto = 'ds' THEN
        p_crypto := 3;
      ELSE
        p_crypto := 0;
      END IF;

    ELSE --v_sdIndicator := 0
      RAISE err_not_sec_dom;
    END IF;

  EXCEPTION
    WHEN err_invalid_application_aid THEN
      RAISE_APPLICATION_ERROR(-20001, 'Error getting security domain record: INVALID APPLICATION AID');

    WHEN err_sec_dom_id_not_exist THEN
      RAISE_APPLICATION_ERROR(-20001, 'Error getting security domain record: NO RECORD FOUND IN SECURITY DOMAIN MSL TABLE');

    WHEN err_not_sec_dom THEN
      RAISE_APPLICATION_ERROR(-20001, 'Error getting security domain record: NOT A SECURITY DOMAIN');

    WHEN OTHERS THEN
      RAISE;
  END;
  
  --getInstalledAids
  PROCEDURE getInstalledAids (
    p_msisdn        IN SUBSCRIBER.msisdn%TYPE,
	p_aid			IN APPLICATION.aid%TYPE,
    p_nb_rows_app   OUT NUMBER,
    p_appname       OUT APPLICATION.name%TYPE,    
    p_installedAids OUT CHARARRAY32,    
    p_sdname        OUT CHARARRAY30    
    ) IS
    v_secdomain_id  	NUMBERARRAY;
    v_simcard_id    	Number;
	v_application_id	Number;
    temp_sdname     	Varchar2(30);
    i               	NUMBER;

  BEGIN

    p_sdname := CHARARRAY30();
    p_nb_rows_app := 0;    
    i := 1;

    BEGIN
      Select sim_card_id INTO v_simcard_id
      FROM subscriber 
      WHERE msisdn = p_msisdn;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20005, 'No record found for msisdn: '||p_msisdn);
      WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20005, 'Cannot retrieve record for msisdn: '||p_msisdn);
    END;

    BEGIN
      Select id, name INTO v_application_id, p_appname
	  from application 
	  where aid = p_aid;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20005, 'No record found for aid: '||p_aid);
      WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20005, 'Cannot retrieve record for aid: '||p_aid);
    END;

    dbms_output.put_line('p_appname: '||p_appname);

    BEGIN
	  Select aid, NVL(security_domain_id,0)
	  BULK COLLECT INTO p_installedAids, v_secdomain_id
	  from sim_card_application
	  where sim_card_id = v_simcard_id
      and application_id = v_application_id;
	
      if SQL%FOUND then
          FOR i IN v_secdomain_id.FIRST..v_secdomain_id.LAST LOOP
            p_nb_rows_app := i;
            p_sdname.extend();
            if v_secdomain_id(i) = 0 then
              p_sdname(i) := 'CARD DOMAIN';
            end if;
            if v_secdomain_id(i) = -1 then
              if UPPER(SUBSTR(p_appname,1,7)) = 'UNKNOWN' then
                p_sdname(i) := 'Unknown';
              else
                p_sdname(i) := 'CARD DOMAIN';
              end if;
            end if;
            if v_secdomain_id(i) > 0 then
              BEGIN
                Select name into temp_sdname from application where id = v_secdomain_id(i);
                p_sdname(i) := temp_sdname;
              EXCEPTION
                WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR(-20005, 'Cannot retrieve sec domain name');
              END;
            end if;

          END LOOP;
      end if;
      
      dbms_output.put_line('p_nb_rows_app: '||p_nb_rows_app);
      FOR i in 1..p_nb_rows_app LOOP
        dbms_output.put_line('installedAid '||i ||' : ' ||p_installedAids(i));
        dbms_output.put_line('secDomain '||i ||' : ' ||p_sdname(i));
      END LOOP;

    EXCEPTION
      WHEN OTHERS THEN
         RAISE;
    END;

  END getInstalledAids;
  
 --getInstalledPackages
  PROCEDURE getInstalledPackages (
    p_msisdn        IN SUBSCRIBER.msisdn%TYPE,
	  p_nb_rows		 OUT NUMBER,
    p_packaid       OUT CHARARRAY32, 
    p_packname      OUT CHARARRAY30  
    ) IS
    
    v_simcard_id    	Number;	
	i	Number;
  BEGIN
    p_packaid := CHARARRAY32(); 
    p_packname := CHARARRAY30();
    i := 1;
    BEGIN
      Select sim_card_id INTO v_simcard_id
      FROM subscriber 
      WHERE msisdn = p_msisdn;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20005, 'No record found for msisdn: '||p_msisdn);
      WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20005, 'Cannot retrieve record for msisdn: '||p_msisdn);
    END;

    BEGIN
		FOR rec IN (SELECT a.aid aid, a.name name FROM package a, sim_card_package b 
              WHERE a.id = b.package_id AND b.sim_card_id = v_simcard_id) LOOP 
    	   p_packaid.EXTEND(); 
    	   p_packname.EXTEND(); 
    	   p_packaid(i)  := rec.aid; 
     	   p_packname(i)  := rec.name; 
     	   i := i+1; 
		END LOOP; 
 
    p_nb_rows := i - 1; 
	
	EXCEPTION
		WHEN OTHERS THEN
			raise;
	END;  

  END getInstalledPackages;   

PROCEDURE checkAppliInstallAllowed (
	p_msisdn IN SUBSCRIBER.msisdn%TYPE,
	p_aid IN APPLICATION.aid%TYPE
) IS
	p_operation_size pack_volume_mgt.NumberArray;
	p_targeted_volume pack_volume_mgt.NumberArray;
	p_nb_operations NUMBER := 1;
	
	cursor crGetInstanceSizeAndVolume IS
		select instance_size, volume_id from application where aid = p_aid;
		
	err_application_not_found EXCEPTION;
BEGIN
	-- retrieve instance size of application and volume id
	IF crGetInstanceSizeAndVolume%ISOPEN THEN
		CLOSE crGetInstanceSizeAndVolume;
	END IF;
	
	OPEN crGetInstanceSizeAndVolume;
	
	FETCH crGetInstanceSizeAndVolume INTO p_operation_size(1), p_targeted_volume(1);
	
	IF crGetInstanceSizeAndVolume%NOTFOUND THEN
		CLOSE crGetInstanceSizeAndVolume;
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 'APPLICATION_NOT_FOUND');
	END IF;
	
	IF crGetInstanceSizeAndVolume%ISOPEN THEN
		CLOSE crGetInstanceSizeAndVolume;
	END IF;
	
	pack_volume_mgt.is_update_allowed(
		p_msisdn,
		p_operation_size,
		p_targeted_volume,
		p_nb_operations
	);
END checkAppliInstallAllowed;
  
PROCEDURE checkPackageLoadAllowed (
	p_msisdn IN SUBSCRIBER.msisdn%TYPE,
	p_aid IN PACKAGE.aid%TYPE
) IS
	p_operation_size pack_volume_mgt.NumberArray;
	p_targeted_volume pack_volume_mgt.NumberArray;
	p_nb_operations NUMBER := 1;
	
	cursor crGetLoadSizeAndVolume IS
		select installed_size, volume_id from package where aid = p_aid;
		
	err_package_not_found EXCEPTION;
BEGIN
	-- retrieve package size and volume id
	IF crGetLoadSizeAndVolume%ISOPEN THEN
		CLOSE crGetLoadSizeAndVolume;
	END IF;
	
	OPEN crGetLoadSizeAndVolume;
	
	FETCH crGetLoadSizeAndVolume INTO p_operation_size(1), p_targeted_volume(1);
	
	IF crGetLoadSizeAndVolume%NOTFOUND THEN
		CLOSE crGetLoadSizeAndVolume;
		RAISE_APPLICATION_ERROR (INDEX_PROV_ERROR, 'PACKAGE_NOT_FOUND');
	END IF;
	
	IF crGetLoadSizeAndVolume%ISOPEN THEN
		CLOSE crGetLoadSizeAndVolume;
	END IF;
	
	pack_volume_mgt.is_update_allowed(
		p_msisdn,
		p_operation_size,
		p_targeted_volume,
		p_nb_operations
	);
END checkPackageLoadAllowed;

  /******************************************************************************
	   NAME:       extraditeApplication
	   PURPOSE:    use to associate application to its security domain
	******************************************************************************/

  --PROCEDURE extraditePackage (
  --  p_msisdn IN SUBSCRIBER.msisdn%TYPE,
  --	p_aid	IN PACKAGE.aid%TYPE,
  --	p_destinationSdAid IN APPLICATION.aid%TYPE

  --) IS

  --v_SD_id SIM_CARD_PACKAGE.SD_ID%TYPE;
  --v_pkg_id PACKAGE.ID%TYPE;
  --v_sim_id SUBSCRIBER.SIM_CARD_ID%TYPE;

  --BEGIN

     --SELECT a.ID INTO v_pkg_id FROM PACKAGE a
     --WHERE a.aid = p_aid;

	--   SELECT sim_card_id INTO  v_sim_id FROM SUBSCRIBER
 	--   WHERE msisdn = p_msisdn;

 	  -- IF (p_destinationSdAid <> 'Card Domain') THEN

     	   --SELECT a.ID INTO v_SD_id FROM APPLICATION a
    	   --WHERE a.aid = p_destinationSdAid;


    	   --UPDATE SIM_CARD_PACKAGE
    	   --SET sd_id = v_SD_id
    	   --WHERE package_id = v_pkg_id
    	   --AND sim_card_id = v_sim_id;

	   --ELSE

	   --	   UPDATE SIM_CARD_PACKAGE
    	   --SET sd_id = 0
    	   --WHERE package_id = v_pkg_id
    	   --AND sim_card_id = v_sim_id;

	   --END IF;

    --COMMIT;
  --EXCEPTION
    --WHEN NO_DATA_FOUND THEN
      --RAISE_APPLICATION_ERROR(-20005, 'Security domain not found');
  --END extraditePackage;
  
 --delete application
PROCEDURE delete_Application (
  	p_application_name 	IN application.name%TYPE
  ) IS
    v_applet_name APPLET.name%TYPE;
  BEGIN
  
  BEGIN
    SELECT b.name into v_applet_name
    From application a, applet b
    Where a.applet_id =  b.id
    And a.name = p_application_name;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20005, 'Unknown application: '||p_application_name);
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20005, 'Cannot retrieve application: '||p_application_name);
  END;
  

  Begin
    dbms_output.put_line('Provappli.delete_Application');
  Provappli.delete_Application (p_application_name,1,1,0);
  
  Provappli.delete_Applet (v_applet_name,0,0);

	EXCEPTION
   		When Others Then
			ROLLBACK;
		 	RAISE_APPLICATION_ERROR(-20000,'almProvision_DELETE_APPLICATION_ERROR');
	END;

   COMMIT;

  END delete_Application;
  
--*********modify security domain*********
  PROCEDURE modify_SecurityDomain (
	p_applet_aid 		IN applet.aid%TYPE,
	p_application_aid 	IN application.aid%TYPE,
	p_application_name 	IN application.name%TYPE,
	p_application_type 	IN application.type%TYPE,
	p_application_status 	IN application.status%TYPE,
	p_application_class	IN windappclass.application_class%TYPE,
	p_application_instanceSize 	IN application.instance_size%TYPE,
	p_elf_aid		IN package.aid%TYPE,
	p_ram_size 		IN application.required_ram%TYPE,
	p_install_param 	IN application.install_parameters%TYPE,
	p_privileges 		IN application.privilege%TYPE,
	p_allowed_media 	IN application.allowed_media%TYPE,
	p_nb_Cp			IN NUMBER,
	p_cpDesc		IN CharArray30,
	p_volume_name	IN volume.volume_name%TYPE,
	p_token_flag		IN application.token_flag%TYPE,
	p_tsm_user_id   IN tsm_application.tsm_user_id%TYPE,
	p_supplier_name 	IN supplier.name%TYPE,
	p_applet_version 	IN applet.version%TYPE,
	p_applet_status 	IN applet.status%TYPE,
	p_tar               IN application.tar%TYPE,
	p_counter IN security_domain_msl.counter%TYPE,
	p_ciphering IN security_domain_msl.ciphering%TYPE,
	p_crypto IN security_domain_msl.crypto%TYPE,
	p_por IN security_domain_msl.por%TYPE,
	p_por_response IN security_domain_msl.por_response%TYPE,
	p_por_ciphering IN security_domain_msl.por_ciphering%TYPE,
	p_por_security IN security_domain_msl.por_security%TYPE,
	p_kic_algo IN security_domain_msl.kic_algo%TYPE,
	p_kic_des_mode IN security_domain_msl.kic_des_mode%TYPE,
	p_kid_algo IN security_domain_msl.kid_algo%TYPE,
	p_kid_des_mode IN security_domain_msl.kid_des_mode%TYPE
	) IS

	v_card_profile_desc_tab	Provappli.CHARARRAY30;
  	
  BEGIN
	FOR i IN 1..p_nb_Cp LOOP
		v_card_profile_desc_tab(i) := p_cpDesc(i);
	END LOOP;	

	Provappli.modify_SecurityDomain (
		p_applet_aid,
		p_application_aid,
		p_application_name,
		p_application_type,
		p_application_status,
		p_application_class,
		p_application_instanceSize,
		p_elf_aid,
		p_ram_size,
		p_install_param,
		p_privileges,
		p_allowed_media,
		p_nb_Cp,
		v_card_profile_desc_tab,
		p_volume_name,
		p_token_flag,
		p_tsm_user_id,
		p_supplier_name,
		p_applet_version,
		p_applet_status,
		p_tar,
		p_counter,
		p_ciphering,
		p_crypto,
		p_por,
		p_por_response,
		p_por_ciphering,
		p_por_security,
		p_kic_algo,
		p_kic_des_mode,
		p_kid_algo,
		p_kid_des_mode
	);
  END modify_SecurityDomain;
  

PROCEDURE changeCardState (
        p_msisdn IN subscriber.msisdn%TYPE,
        p_card_state IN sim_card.activity%TYPE
  )  IS
        theSimCardId sim_card.id%type;
        remote_update EXCEPTION;
        PRAGMA EXCEPTION_INIT(remote_update, -20102);

BEGIN
        SELECT sim_card_id INTO theSimCardId FROM subscriber WHERE msisdn = p_msisdn;
        BEGIN
                UPDATE sim_card SET activity = p_card_state WHERE id = theSimCardId;
                COMMIT;
        EXCEPTION
                WHEN remote_update THEN
                        NULL;
        END;

END changeCardState;
 

	PROCEDURE getApplicationPackages (
		p_application_aid IN CHARARRAY32,
		p_package_aid 	OUT CHARARRAY32,
		p_package_name 	OUT CHARARRAY40

	) IS

	i NUMBER;
	j NUMBER;
	paramstring VARCHAR2(2000);
	v_sql VARCHAR2(3000);
	  
	BEGIN
  
	paramstring := '';
	FOR i in p_application_aid.FIRST..p_application_aid.LAST LOOP
		paramstring := paramstring || '''' || p_application_aid(i) || '''';
		IF i < p_application_aid.LAST THEN
			paramstring := paramstring || ',';
		END IF;
	END LOOP;

	v_sql := '(SELECT DISTINCT pack.aid, pack.name ' ||
			'FROM application appli, applet appl, applet_package appPack, package pack ' ||
			'WHERE appli.applet_id = appl.id AND ' ||
			'appl.id = appPack.applet_id AND ' ||
			'appPack.package_id = pack.id ' ||    
			'AND appli.aid IN (' || paramstring || '))';

	EXECUTE IMMEDIATE v_sql BULK COLLECT INTO p_package_aid, p_package_name;
  
	FOR i in p_package_aid.first..p_package_aid.last loop
		DBMS_OUTPUT.PUT_LINE(p_package_aid(i) || ' ' || p_package_name(i));
	END LOOP;

	END getApplicationPackages;
	
	
	--getCardProfileData
  PROCEDURE getCardProfileData (
    p_cardProfileName        IN CARD_PROFILE.DESCRIPTION%TYPE,
    p_readRowsSecurity 		  OUT NUMBER,
    p_readRowsFileMap  		  OUT NUMBER,
    p_readRowsOperation		  OUT NUMBER,
    p_flCattp				        OUT NUMBER,
    p_flPushSecuCattp		    OUT NUMBER,
    p_supplierName			    OUT SUPPLIER.NAME%TYPE,
    p_chipRef          		  OUT CARD_PROFILE.CHIP_REF%TYPE,
    p_maskVersion      		  OUT CARD_PROFILE.MASK_VERSION%TYPE,
    p_maskDate				      OUT VARCHAR2,
    p_vmRelease				      OUT CARD_PROFILE.VM_RELEASE%TYPE,
    p_dlProtocol       		  OUT VARCHAR2,
    p_ram		      		      OUT CARD_PROFILE.RAM%TYPE,
    p_eeprom	      		    OUT CARD_PROFILE.EEPROM%TYPE,
    p_appletRam        		  OUT CARD_PROFILE.APPLET_RAM%TYPE,
    p_appletEeprom     		  OUT CARD_PROFILE.APPLET_EEPROM%TYPE,
    p_smsConcatenated		    OUT CARD_PROFILE.SMS_CONCATENATED%TYPE,
    p_packageTag			      OUT CARD_PROFILE.PACKAGE_TAG%TYPE,
    p_secuDesc        		  OUT CHARARRAY32,
    p_keyType       		    OUT CHARARRAY16,
    p_kicIndex	     		    OUT NUMBERARRAY,
    p_kidIndex         		  OUT NUMBERARRAY,
    p_appTypeName      		  OUT CHARARRAY30,
    p_appTypeTar       		  OUT CHARARRAY06,
    p_appTypeCmdSet    		  OUT CHARARRAY10,
    p_kicAlgo        		    OUT CHARARRAY04,
    p_kidAlgo       		    OUT CHARARRAY04,
    p_bearer	      		    OUT CHARARRAY06,
    p_origFile				      OUT CHARARRAY10,
    p_destFile				      OUT CHARARRAY10,
    p_opeType				        OUT CHARARRAY30
    ) IS
    
    v_cardprofile_id    Number;    

  BEGIN    
	p_readRowsSecurity 	:= 0;	 
    p_readRowsFileMap  	:= 0;	 
	p_readRowsOperation	:= 0;	 			 
	p_ram := 0;
	p_eeprom := 0;
	p_appletRam := 0;
	p_appletEeprom := 0;
	p_smsConcatenated := 0;
	
    BEGIN
      Select DECODE (a.supplier_id, 1, 'Orga',
									2, 'Gemalto',
									3, 'Oberthur_Technologies',	
									4, 'Schlumberger',
									5, 'MicroElectronica',
									6, 'Austria',
									7, 'G_and_D',
										'error'),
	  a.chip_ref, a.mask_version, 
		--TO_CHAR(a.mask_date, 'DD-MON-YYYY') as mask_date,
		TO_CHAR(a.mask_date, 'MM-DD-YYYY') as mask_date,
		a.vm_release, 
		DECODE (a.dl_protocol, 'USIM', 'USIM_R6', 
                        'USIM2', 'SIMphonic_USIM_V2', 
						'CUST1_1', 'Customer1_1',
						'v1.1', 'SIMphonic_V1',
						'v2.1.c_2', 'SIMphonic_V2_1C_OC_056262_V2_1D',
						'v3.1.b', 'SIMphonic_V2_1G_V3_1B',
						'v2.1.c', 'SIMphonic_V2_1C',
						'v2.1.c_1', 'SIMphonic_V2_1C_OC_056241',
						'SIMERA2', 'SIMERA_V2',
						'SCHLUM_v3', 'SIMERA_V3',
						'v1', 'SIMtelligence_32_16',
						'Default', 'Standard_03_48_V8_4_0_V8_6_0',
						'EXCEL_v3', 'Excel_data_V3',
						'ESMS_v1', 'GemXplore_98_V1',
						'ESMS_v2', 'GemXplore_98_V2',
						'GEM_v3', 'GemXplore_Expresso',
                           'error'),
		--TO_CHAR(a.cattp_compat,'9'),
		--TO_CHAR(a.push_sms_secu),
		a.cattp_compat, a.push_sms_secu,
		a.ram, a.eeprom, a.applet_ram, a.applet_eeprom, 
		a.sms_concatenated, a.package_tag, a.id as cardprofileid
	  INTO p_supplierName, p_chipRef, p_maskVersion, p_maskDate, p_vmRelease,
		p_dlProtocol, p_flCattp, p_flPushSecuCattp, p_ram, p_eeprom, 
		p_appletRam, p_appletEeprom, p_smsConcatenated, p_packageTag,v_cardprofile_id
      FROM card_profile a
      Where   a.description = p_cardProfileName;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20005, 'Unknown card profile: '||p_cardProfileName);
      WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20005, 'Cannot retrieve card profile data for: '||p_cardProfileName);
    END;

	BEGIN
		Select DECODE (a.security_id, 1, 'No_security',
									2, 'Auth_DES_CBC',	
									3, 'Cipher_Auth_DES_CBC',
									4, 'Auth_PORAuth_DES_CBC',
									5, 'Cipher_Auth_PORAuth_DES_CBC',
									6, 'CT_No_security',
									7, 'CT_Auth_DES_CBC',
									8, 'CT_Cipher_Auth_DES_CBC',
									9, 'CT_Auth_PORAuth_DES_CBC',
									10, 'CT_Cipher_Auth_PORAuth_DES_CBC',
									11, 'No_security_No_POR',
									12, 'CT_Auth_DES_CBC_Cipher_3DES2',
									13, 'CT_Auth_3DES2',
									14, 'CT_Auth_3DES2_No_POR',
									15, 'CT_Auth_3DES_Cipher_3DES2',
									16, 'CT_Auth_3DES3',
                  					17, 'Auth_3DES2',
                  					18, 'Auth_3DES3',
                  					19, 'Cipher_Auth_3DES2',
                  					20, 'Cipher_Auth_3DES3',
										'error'),
		a.key_type, a.kic_index, a.kid_index,
		NVL(a.kic_implicit_algo,'NO'), NVL(a.kid_implicit_algo,'NO'),
		DECODE (bearer, 0, 'sms', 
                        2, 'cattp', 
                           'error'),
		c.name, c.tar, c.cmd_set
		BULK COLLECT INTO p_secuDesc, p_keyType, p_kicIndex, p_kidIndex,
			p_kicAlgo, p_kidAlgo, p_bearer, p_appTypeName, p_appTypeTar, p_appTypeCmdSet
		FROM card_profile_security a, security b, application_type c
		WHERE a.security_id = b.id
		And	 a.application_type_id = c.id
		And  a.card_profile_id = v_cardprofile_id;
		
		if SQL%FOUND then
			p_readRowsSecurity := SQL%ROWCOUNT;
		end if;
	
	EXCEPTION
		WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20005, 'Cannot retrieve card profile security data for: '||p_cardProfileName);
	END;
	
	BEGIN
		Select orig_file, dest_file
		BULK COLLECT INTO p_origFile, p_destFile
		FROM files_mapping 
		WHERE card_profile_id = v_cardprofile_id;
				
		if SQL%FOUND then
			p_readRowsFileMap := SQL%ROWCOUNT;
		end if;
	
	EXCEPTION
		WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20005, 'Cannot retrieve card profile file mapping data for: '||p_cardProfileName);
	END;
	
	BEGIN
		Select DECODE (a.operation_type_id, 1, 'download_application',
											2, 'remove_application',
											3, 'replace_application',
											4, 'lock_application',
											5, 'unlock_application',
											6, 'card_audit',
											7, 'switch_ADN_FDN',
											8, 'switch_FDN_ADN',
											9, 'update_GSM_file',
											10, 'resize_GSM_file',
											12, 'send_command',
											13, 'partial_card_audit',
											14, 'create_GSM_file',
											15, 'delete_GSM_file',
											16, 'activate_GSM_file',
											17, 'deactivate_GSM_file',
											18, 'special_command',
												'error')
		BULK COLLECT INTO p_opeType
		FROM operation_type_card_profile a
		WHERE a.card_profile_id = v_cardprofile_id;
				
		if SQL%FOUND then
			p_readRowsOperation := SQL%ROWCOUNT;
		end if;
	
	EXCEPTION
		WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20005, 'Cannot retrieve card profile operation data for: '||p_cardProfileName);
	END;
		
  END getCardProfileData;

  PROCEDURE getFilesApplicationType (
	  p_cardProfile IN card_profile.DESCRIPTION%TYPE,
	  p_gsmFileList OUT CHARARRAY10
  ) IS
  
  paramstring VARCHAR2(200);
  v_sql VARCHAR2(500);
  appTypeIds NUMBERARRAY;
 
  BEGIN
  
    SELECT cp_sec.application_type_id BULK COLLECT INTO appTypeIds
      FROM card_profile cp, card_profile_security cp_sec
      WHERE cp.description = p_cardProfile
        AND cp.ID = cp_sec.card_profile_id;
  
    paramstring := '';
  
  IF SQL%FOUND THEN
    FOR i in appTypeIds.FIRST..appTypeIds.LAST LOOP
      paramstring := paramstring || appTypeIds(i);
      if i < appTypeIds.LAST THEN
        paramstring := paramstring || ',';
      end if;
    END LOOP;
  END IF;
  
    v_sql := '(select ef_name from files_application_type where application_type_id in (' || paramstring || '))';
  
    EXECUTE IMMEDIATE v_sql BULK Collect into p_gsmFileList;
    
    FOR i in p_gsmFileList.FIRST..p_gsmFileList.LAST LOOP
     dbms_output.put_line(p_gsmFileList(i));
    END LOOP;
    
  EXCEPTION
  
  WHEN OTHERS THEN
    RAISE;
  
  END getFilesApplicationType;
  
  PROCEDURE isDependent(
	p_aid IN PACKAGE.aid%TYPE,
	p_msisdn IN subscriber.msisdn%TYPE,
	p_dependent_flag  OUT NUMBER
  ) IS

	v_property varchar2(2);
	v_applet_id_array NUMBERARRAY;
	v_package_id_array NUMBERARRAY;
	v_the_package_id number := 0;
	v_sim_card_id number := 0;
	v_count_installed integer := 0;
BEGIN

  BEGIN 
	Select id, property into v_the_package_id, v_property from package where aid = p_aid;
  EXCEPTION
	WHEN NO_DATA_FOUND THEN
		RAISE_APPLICATION_ERROR(-20005, 'package aid not found');
  END;  
    
  BEGIN 	
	select sim_card_id into v_sim_card_id from subscriber where msisdn = p_msisdn;
  EXCEPTION
	WHEN NO_DATA_FOUND THEN
		RAISE_APPLICATION_ERROR(-20005, 'msisdn not found');
  END; 
  
  if upper(v_property) = 'A' then
	p_dependent_flag := 0;

  else
	select ap.applet_id bulk collect into v_applet_id_array
		from applet_package ap, package p
		where ap.package_id = p.id
		and p.aid = p_aid;		

	if SQL%FOUND then
      
		v_count_installed := 0;
		FOR i IN 1..v_applet_id_array.COUNT LOOP
      DBMS_OUTPUT.PUT_LINE('v_applet_id_array(' || i || '): ' || v_applet_id_array(i));
			select package_id bulk collect into v_package_id_array
			from applet_package where applet_id = v_applet_id_array(i)
			and package_id > v_the_package_id;
          
			if v_package_id_array.count > 0 then
				FOR j IN 1..v_package_id_array.COUNT LOOP
          DBMS_OUTPUT.PUT_LINE('v_package_id_array(' || j || '): ' || v_package_id_array(j));
					select count(package_id) into v_count_installed
					from sim_card_package where sim_card_id = v_sim_card_id
					and package_id = v_package_id_array(j);    
              
					if v_count_installed > 0 then
						p_dependent_flag := 1;                
						EXIT;
					end if;
				end LOOP;
      end if;
			
			if p_dependent_flag = 1 then
				EXIT;
			end if;
			
    end LOOP;  
		
    DBMS_OUTPUT.PUT_LINE('v_count_installed: ' || v_count_installed);
		if v_count_installed = 0 then 
			p_dependent_flag := 0;              
		end if;
		

	else      
		p_dependent_flag := 0;
		
	end if;--found		

  end if;--property

EXCEPTION
	WHEN others THEN
      p_dependent_flag := 0;

END isDependent;


PROCEDURE getInstalledCardProfileList(
	p_aid IN APPLICATION.aid%TYPE,
	p_card_profile_list OUT CHARARRAY30
  ) IS
	
v_sim_card_array NUMBERARRAY;
v_card_profile_array NUMBERARRAY;
v_application_id NUMBER;	
paramstring1 VARCHAR2(1000);
paramstring2 VARCHAR2(1000);
v_sql VARCHAR2(1300);
  
BEGIN

--get array of sim_card_id where the application aid is installed
select sca.SIM_CARD_ID bulk collect into v_sim_card_array
from sim_card_application sca, application app 
where sca.application_id = app.id 
and app.aid = p_aid;

paramstring1 := '';

 IF SQL%FOUND THEN
    FOR i in v_sim_card_array.FIRST..v_sim_card_array.LAST LOOP
      paramstring1 := paramstring1 || v_sim_card_array(i);
      if i < v_sim_card_array.LAST THEN
        paramstring1 := paramstring1 || ',';
      end if;
    END LOOP;
  END IF;

select id into v_application_id from application where aid = p_aid;

select card_profile_id bulk collect into v_card_profile_array
from card_profile_application where application_id = v_application_id;

paramstring2 := '';

 IF SQL%FOUND THEN
    FOR i in v_card_profile_array.FIRST..v_card_profile_array.LAST LOOP
      paramstring2 := paramstring2 || v_card_profile_array(i);
      if i < v_card_profile_array.LAST THEN
        paramstring2 := paramstring2 || ',';
      end if;
    END LOOP;
  END IF; 


if (v_sim_card_array.count > 0) and (v_card_profile_array.count > 0) then

v_sql := '(select distinct card_profile.description from sim_card, card_profile where sim_card.card_profile_id = card_profile.id ' ||
		' and sim_card.id in (' || paramstring1 || ') and sim_card.card_profile_id in (' || paramstring2 || '))';

    EXECUTE IMMEDIATE v_sql BULK Collect into p_card_profile_list;

    --FOR i in p_card_profile_list.FIRST..p_card_profile_list.LAST LOOP
    -- dbms_output.put_line(p_card_profile_list(i));
    --END LOOP;
    
end if;

  EXCEPTION
  WHEN OTHERS THEN
    RAISE;  

END getInstalledCardProfileList;

FUNCTION isInstallAidPresent(
		p_aid	IN sim_card_application.AID%TYPE,
		p_simCardId IN subscriber.sim_card_id%TYPE
	) RETURN BOOLEAN
	IS
		v_id sim_card_application.application_id%TYPE;
		
BEGIN
	
		BEGIN
			Select application_id into  v_id
			from sim_card_application
			where sim_card_id = p_simCardId
			and aid = p_aid;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
			RETURN FALSE;
		END;
	
		RETURN TRUE;
END isInstallAidPresent;

PROCEDURE unitaryInstallInstall (
  	p_msisdn      IN SUBSCRIBER.msisdn%TYPE,
  	p_aid         IN APPLICATION.aid%TYPE,
    p_installAid  IN SIM_CARD_APPLICATION.AID%TYPE,
    p_installTar  IN SIM_CARD_APPLICATION.TAR%TYPE,
    p_installParam  IN SIM_CARD_APPLICATION.INSTALL_PARAMS%TYPE
  ) IS
  v_simCardId	sim_card.id%TYPE;
  v_applicationId application.id%TYPE;
  v_simId sim_card_application.sim_card_id%TYPE;
  v_installAid SIM_CARD_APPLICATION.AID%TYPE;
  BEGIN
  
  BEGIN
		Select sim_card_id into v_simCardId
		From subscriber
		Where msisdn = p_msisdn;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
		RAISE_APPLICATION_ERROR(-20005, 'msisdn not found');
	END;
  
  BEGIN
		Select id into v_applicationId
		From application
		Where aid = p_aid;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
		RAISE_APPLICATION_ERROR(-20005, 'application not found');
	END; 
  
  IF (p_installAid is null) or (trim(p_installAid) = '') then
      v_installAid := p_aid;
  else
      v_installAid := p_installAid;
  end if;
    
  if isInstallAidPresent(p_installAid,v_simCardId) <> TRUE THEN 
    INSERT INTO SIM_CARD_APPLICATION
    (SIM_CARD_ID, APPLICATION_ID, INSTANCE_DATE, LOCKED, STATUS, AID, TAR, INSTALL_PARAMS)
    VALUES
    (v_simCardId, v_applicationId, sysdate, 'f', 'i', v_installAid, p_installTar, p_installParam);
 
    commit;
  end if;
    

  END unitaryInstallInstall;
  
  PROCEDURE updateSimCardApplicationStatus(
        p_msisdn IN SUBSCRIBER.msisdn%TYPE,
		p_aid IN sim_card_application.aid%TYPE,
        p_status IN sim_card_application.status%TYPE
) AS

	v_simCardId	sim_card.id%TYPE;	
	err_interface_status EXCEPTION ;
BEGIN
	
	IF (( p_status != 'i' ) AND ( p_status != 's' ) AND
	    ( p_status != 'p' ) AND ( p_status != 'b' ) AND
	    ( p_status != 'l' )) THEN
		RAISE err_interface_status ;
	END IF;
	
	BEGIN
		Select sim_card_id into v_simCardId
		From subscriber
		Where msisdn = p_msisdn;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
		RAISE_APPLICATION_ERROR(-20005, 'msisdn not found');
	END;
	
	Update sim_card_application
	Set status = p_status
	Where sim_card_id = v_simCardId
		And aid = p_aid;	

	COMMIT ;

EXCEPTION
	
	WHEN err_interface_status THEN
		RAISE_APPLICATION_ERROR (-20002, 'Interface Error : status parameter must be i, s, p, b, or l');
  END updateSimCardApplicationStatus;  

  PROCEDURE delete_SimPackage (
  	p_package_aid 	IN package.aid%TYPE,
    p_msisdn        IN subscriber.msisdn%TYPE
  ) IS
  v_simcard_id  SIM_CARD.id%TYPE;
  v_package_id  PACKAGE.id%TYPE;
  v_application_id NUMBERARRAY;
  v_sql varchar2(5000);
  v_param varchar2(2000);
  
  BEGIN
  
  BEGIN
    select sim_card_id into v_simcard_id
    from subscriber
    where msisdn = p_msisdn;  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR (-20005, 'MSISDN not found');  
  END;
  
  BEGIN
    select id into v_package_id
    from package
    where aid = p_package_aid;  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR (-20005, 'Package not found');  
  END;
  
  select appl.id bulk collect into v_application_id
  from applet_package app_pack, applet app, application appl
  where app_pack.applet_id = app.id 
  and app.id = appl.applet_id
  and app_pack.is_contained = 't'
  and app_pack.package_id = v_package_id;

  v_param := '';
  for i IN 1..v_application_id.COUNT LOOP
    --Delete from sim_card_application
    v_param := v_param || v_application_id(i);
    if i < v_application_id.COUNT then
      v_param := v_param || ',';
    end if;
  end LOOP;  

  BEGIN
    if v_application_id.COUNT > 0 then
      v_sql := 'delete from sim_card_application where sim_card_id=:simcardid ';
      v_sql := v_sql || ' and application_id in (' || v_param || ')';
      execute immediate v_sql using v_simcard_id;
    end if;

    delete from sim_card_package where sim_card_id = v_simcard_id
      and package_id=v_package_id;

  EXCEPTION
    When others then
      ROLLBACK;
      Raise_Application_Error(-20002, 'Error in deleting');
  END;

  COMMIT;
  
END delete_SimPackage;

  
END Dao_Proc;
/

show errors;
