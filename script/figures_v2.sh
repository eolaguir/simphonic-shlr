#!/bin/bash

export SIMphonIC=/export/home/sfovivo/exe
export ASP_NAME=shlvivo

source /export/home/sfovivo/.bash_profile
export SERVERNAME=`echo $PS1 | cut -d '@' -f1 | cut -d '[' -f2`

usage(){

	echo ""
	echo " FIGURES FOR SHLR - VIVO

	#figures.sh
	Figures from currently day and currently hour


	#figures.sh [hour]
	Figures from currently day and hour [hour]
	[hour] format: hh    

	Ex. for pictures of today at 9h, use:
	figures.sh 09


	#figures.sh [hour] [date]
	Figures from day [date] and hour [hour]
	[hour] format: hh
	[date] format: mmdd

	Ex. for pictures of October 6th, at 08h, use:
	figures.sh 08 1006"
}

# LOGS FILES
SEARCH_DATE=`date +%m%d`

if [ $# -eq 0 ]; then
	HOUR=`date +%H`
	START_HOUR="$HOUR:00h"
	FINISH_HOUR="now"
else
	HOUR=$1
	START_HOUR="$HOUR:00h"
	FINISH_HOUR="$HOUR:59h"
	if [ ! ${#HOUR} -eq 2 ]; then
		echo "Use 2 digits for hour!"
		usage
		exit
	fi
	if [ $# -eq 2 ]; then
		SEARCH_DATE=$2
	else 
		if [ $# -gt 2 ]; then
			usage
			exit
		fi
	fi
fi

mmi_logs="$SIMphonIC/asp/$ASP_NAME/mmi/shlr/log/$SEARCH_DATE/vivo.log"
shl_logs="$SIMphonIC/sys/shlr2ats/log/$SEARCH_DATE/vivo.log"

if [ ! -e $mmi_logs ]; then
	echo "File does not exist!"
	exit
fi

# Numbers
nb_of_REQUEST=`grep ^$HOUR: $mmi_logs | grep '_REQUEST' | wc -l`
nb_of_RESULT=`grep ^$HOUR: $mmi_logs | grep 'RESULT' | grep -v 'PROVISIONING_RESULT' | wc -l`
nb_of_PROV_RESULT=`grep ^$HOUR: $mmi_logs | grep 'PROVISIONING_RESULT' | wc -l`
nb_of_OTASENT=`grep ^$HOUR: $mmi_logs | grep '<OTA: SENT>' | wc -l`
nb_of_INTERNAL=`grep ^$HOUR: $mmi_logs | grep 'INTERNAL' | wc -l`
nb_of_TRANSIENT=`grep ^$HOUR: $mmi_logs | grep 'TRANSIENT' | wc -l`
nb_of_UNKNOWMSISDN=`grep 'UNKNOWN MSISDN' $mmi_logs | grep ^$HOUR: | wc -l`
nb_of_1006=`grep 'status 1006 message Internal Error sessionId' $mmi_logs | grep ^$HOUR: | wc -l`
nb_of_COMM_FAILURE=`grep 'COMM_FAILURE' $mmi_logs | grep ^$HOUR: | wc -l`
nb_of_ALREADY=`grep 'ALREADY' $mmi_logs | grep ^$HOUR: | wc -l`
nb_of_getAlmReInitAPI=`grep 'getAlmReInitAPI failed' $mmi_logs | grep ^$HOUR: | wc -l`
nb_of_other_errors=$(($nb_of_INTERNAL-$nb_of_TRANSIENT-$nb_of_getAlmReInitAPI-$nb_of_1006-$nb_of_COMM_FAILURE))

nb_of_AsyncACK=`grep AsyncACK $shl_logs | grep delivStatus | grep -v Enqueue | grep ^$HOUR: | wc -l`
nb_of_TORETRY=`grep AsyncACK $shl_logs | grep delivStatus | grep -v Enqueue | grep TO_RETRY | grep ^$HOUR: | wc -l`
nb_of_OK=`grep AsyncACK $shl_logs | grep delivStatus | grep -v Enqueue | grep OK | grep ^$HOUR: | wc -l`
nb_of_FAILED=`grep AsyncACK $shl_logs | grep delivStatus | grep -v Enqueue | grep FAILED | grep ^$HOUR: | wc -l`
nb_of_URL1=`grep "Tries sending with url \[1\]" $shl_logs | grep ^$HOUR: | wc -l`
nb_of_URL2=`grep "Tries sending with url \[2\]" $shl_logs | grep ^$HOUR: | wc -l`
nb_of_NOTSENT=`grep "NOT SENT" $shl_logs | grep ^$HOUR: | wc -l`

#TO_RETRY details
pend_sess_exp=`grep AsyncACK $shl_logs | grep delivStatus | grep -v Enqueue | grep TO_RETRY | grep "OPRTN: Pending session expired" | grep ^$HOUR: | wc -l`
ope_failed=`grep AsyncACK $shl_logs | grep delivStatus | grep -v Enqueue | grep TO_RETRY | grep "OPRTN: Operation failed" | grep ^$HOUR: | wc -l`
internal_error=`grep AsyncACK $shl_logs | grep delivStatus | grep -v Enqueue | grep TO_RETRY | grep "Internal error" | grep ^$HOUR: | wc -l`
unknowsubs1=`grep AsyncACK $shl_logs | grep delivStatus | grep -v Enqueue | grep TO_RETRY | grep "NetworkDiagErr_UNKNOWN_SUBSCRIBER" | grep ^$HOUR: | wc -l`
unknowsubs2=`grep AsyncACK $shl_logs | grep delivStatus | grep -v Enqueue | grep TO_RETRY | grep "NETWORK: Unknown subscriber" | grep ^$HOUR: | wc -l`
unknowsubs=$(($unknowsubs1+$unknowsubs2))
absentsubs1=`grep AsyncACK $shl_logs | grep delivStatus | grep -v Enqueue | grep TO_RETRY | grep "NetworkDiagErr_ABSENT_SUBSCRIBER" | grep ^$HOUR: | wc -l`
absentsubs2=`grep AsyncACK $shl_logs | grep delivStatus | grep -v Enqueue | grep TO_RETRY | grep "NETWORK: Subscriber absent" | grep ^$HOUR: | wc -l`
absentsubs=$(($absentsubs1+$absentsubs2))
illegalsubs1=`grep AsyncACK $shl_logs | grep delivStatus | grep -v Enqueue | grep TO_RETRY | grep "NetworkDiagErr_ILLEGAL_SUBSCRIBER" | grep ^$HOUR: | wc -l`
illegalsubs2=`grep AsyncACK $shl_logs | grep delivStatus | grep -v Enqueue | grep TO_RETRY | grep "NETWORK: Illegal subscriber" | grep ^$HOUR: | wc -l`
illegalsubs=$(($illegalsubs1+$illegalsubs2))
sm_undeliv1=`grep AsyncACK $shl_logs | grep delivStatus | grep -v Enqueue | grep TO_RETRY | grep "NetworkDiagErr_SM_UNDELIVERABLE" | grep ^$HOUR: | wc -l`
sm_undeliv2=`grep AsyncACK $shl_logs | grep delivStatus | grep -v Enqueue | grep TO_RETRY | grep "NETWORK: SM undeliverable" | grep ^$HOUR: | wc -l`
sm_undeliv=$(($sm_undeliv1+$sm_undeliv2))
sys_failure1=`grep AsyncACK $shl_logs | grep delivStatus | grep -v Enqueue | grep TO_RETRY | grep "NetworkDiagErr_SYSTEM_FAILURE" | grep ^$HOUR: | wc -l`
sys_failure2=`grep AsyncACK $shl_logs | grep delivStatus | grep -v Enqueue | grep TO_RETRY | grep "NETWORK: System failure" | grep ^$HOUR: | wc -l`
sys_failure=$(($sys_failure1+$sys_failure2))
mt_busy_sms1=`grep AsyncACK $shl_logs | grep delivStatus | grep -v Enqueue | grep TO_RETRY | grep "NetworkDiagErr_MT_BUSY_FOR_MT_SMS" | grep ^$HOUR: | wc -l`
mt_busy_sms2=`grep AsyncACK $shl_logs | grep delivStatus | grep -v Enqueue | grep TO_RETRY | grep "NETWORK: MT Busy for SMS" | grep ^$HOUR: | wc -l`
mt_busy_sms=$(($mt_busy_sms1+$mt_busy_sms2))

#FAILED details
por_pars=`grep AsyncACK $shl_logs | grep delivStatus | grep -v Enqueue | grep FAILED | grep "POR_PARSING_ERROR" | grep ^$HOUR: | wc -l`

#Percentages
PERCENT_OTASENT=`echo $nb_of_REQUEST $nb_of_OTASENT | awk '{printf "%.2f",$2/$1*100}'`
PERCENT_OTA_ALREADY=`echo $nb_of_REQUEST $nb_of_ALREADY | awk '{printf "%.2f",$2/$1*100}'`
PERCENT_OTA_UNKNOWMSISDN=`echo $nb_of_REQUEST $nb_of_UNKNOWMSISDN | awk '{printf "%.2f",$2/$1*100}'`
PERCENT_OTA_INTERNAL=`echo $nb_of_REQUEST $nb_of_INTERNAL | awk '{printf "%.2f",$2/$1*100}'`
PERCENT_AsyncACK=`echo $nb_of_OTASENT $nb_of_AsyncACK | awk '{printf "%.2f",$2/$1*100}'`
PERCENT_OK=`echo $nb_of_AsyncACK $nb_of_OK | awk '{printf "%.2f",$2/$1*100}'`
PERCENT_TORETRY=`echo $nb_of_AsyncACK $nb_of_TORETRY | awk '{printf "%.2f",$2/$1*100}'`
PERCENT_FAILED=`echo $nb_of_AsyncACK $nb_of_FAILED | awk '{printf "%.2f",$2/$1*100}'`

echo "#---------------------------------------------------------------------------
# SHLR - $SERVERNAME - figures report per hour
# Date of report: `date '+%m/%d/%y %H:%M:%S'`
#
# Figures from $SEARCH_DATE - $START_HOUR until $FINISH_HOUR
#---------------------------------------------------------------------------
Number of SOAP REQUESTS			$nb_of_REQUEST
Number of SOAP RESULTS			$nb_of_RESULT

Number of SOAP PROV RESULTS		$nb_of_PROV_RESULT

SOAP RESULTS:
-------------
Number of OTA:SENT			$nb_of_OTASENT ($PERCENT_OTASENT%)

Number of OTA:ALREADY ENGAG     	$nb_of_ALREADY ($PERCENT_OTA_ALREADY%)

Number of OTA:UNKNOWN MSISDN		$nb_of_UNKNOWMSISDN ($PERCENT_OTA_UNKNOWMSISDN%)

Number of OTA:INTERNAL ERROR		$nb_of_INTERNAL ($PERCENT_OTA_INTERNAL%)
  Number of Error TRANSIENT		$nb_of_TRANSIENT
  Number of Error 1006 message		$nb_of_1006
  Number of Error COMM_FAILURE		$nb_of_COMM_FAILURE
  Number of Error nb_of_getAlmReInitAPI	$nb_of_getAlmReInitAPI
  Number of Other Errors        		$nb_of_other_errors

AsyncACK RESULTS:
-----------------
Number of AsyncACK			$nb_of_AsyncACK ($PERCENT_AsyncACK%)

  Number of delivStatus:OK              $nb_of_OK ($PERCENT_OK%)

  Number of delivStatus:TO_RETRY	$nb_of_TORETRY ($PERCENT_TORETRY%)
	Number of Unknown subscriber		$unknowsubs
	Number of Subscriber absent		$absentsubs
	Number of Illegal subscriber		$illegalsubs
	Number of SM undeliverable		$sm_undeliv
	Number of MT Busy for SMS		$mt_busy_sms
	Number of System failure		$sys_failure
	Number of Pend session expired	$pend_sess_exp
	Number of Operation failed	$ope_failed
	Number of Internal error	$internal_error

  Number of delivStatus:FAILED		$nb_of_FAILED ($PERCENT_FAILED%)
	Number of POR_PARSING_ERROR	$por_pars

Call to the Orchestrador:
-------------------------
Number of sending with url [1]		$nb_of_URL1
Number of sending with url [2]         	$nb_of_URL2
Number of NOT SENT			$nb_of_NOTSENT
#-------------------------------------------------------------------------- 
"
