#!/bin/bash
currDate=`echo $(date +%F.%H.%M.%S)`
mafft="$HOME/mafftotals$currDate"
show_help() {
        echo "Usage: $0 -m module maff_files"
        echo "Eg: $0 -m shlvivo maff.*"
}

OPTIND=1
processTotals(){
MTTotal=`nawk -F'|' -v mod="$module" '$4 !="MT"{next}$5 != mod{next}{print $1}' "$mafft"|wc -l`
MOTotal=`nawk -F'|' -v mod="$module" '$4 !="MO"{next}$5 != mod{next}{print $1}' "$mafft"|wc -l`

z=`nawk -F '|' -v mtt="$MTTotal" -v mod="$module" -v cdate="$currDate" '$4 != "MT"{tps+=1;next}$5 != mod{next}$8 == "P" {pending[$2]=$1 $7} $8 == "C" {completed[$2]=$1 $7}
END { for (session in completed) {
                hh1=substr(pending[session],1,2);
                mm1=substr(pending[session],4,2);
                ss1=substr(pending[session],7,2);
                hh2=substr(completed[session],1,2);
                mm2=substr(completed[session],4,2);
                ss2=substr(completed[session],7,2);
                time1=hh1*60*60 + mm1*60 + ss1;
                time2=hh2*60*60 + mm2*60 + ss2;
                diff=time2-time1;
                diffav+=diff;
                difft+=1;gtps+=tps;nsec+=1;
                tmpmaxx=substr(pending[session],9,16);
                        if (pending[session]!=""){
                                if(diff>=60){ morethansixty+=1; }
                        }
if (maxxtime<diff){
        if (pending[session]!=""){
#               tps=1;maxxtime=diff;maxsession=session;sessionmaxtimeb=pending[session];sessionmaxtimef=completed[session];
                tps=1;maxxtime=diff;maxsession=tmpmaxx;sessionmaxtimeb=pending[session];sessionmaxtimef=completed[session];

        }
}

}       } END {
if (nsec!=0){
        print maxxtime, " (",substr(gtps/nsec,0,4),")\t\t",maxsession,"\t\t[",morethansixty,"]";
} else {
        print maxxtime, " (",substr(gtps,0,4),")\t\t",maxsession;
}

 }' "$mafft"`
printf "%s\t\t\t\t\t\t\t\t%s\t\t%s\t\t%s\n" "TOTALS:" "$MTTotal" "$MOTotal" "$z"
rm "$mafft"
}

if [ -z "$1" ]
then
        show_help;
        exit;
fi
if [ $1 == "-m" ];then
        module="$2"
        shift
        shift
fi
printf "filename\t\t\t\t\t\t\tnb MT (per sec)\t\tnb MO (per sec)\t\tNetworkACK Time Max (average)\tsession\t\t[delay 60s+]\n"
for x in $@;do
cat $x >> $mafft 
done
#MTTotal=`nawk -F'|' -v mod="$module" '$4 !="MT"{next}$5 != mod{next}{print $1}' "$mafft"|wc -l`
#MOTotal=`nawk -F'|' -v mod="$module" '$4 !="MO"{next}$5 != mod{next}{print $1}' "$mafft"|wc -l`

for i in $@;do
MTTotal=`nawk -F'|' -v mod="$module" '$4 !="MT"{next}$5 != mod{next}{print $1}' "$i"|wc -l`
MOTotal=`nawk -F'|' -v mod="$module" '$4 !="MO"{next}$5 != mod{next}{print $1}' "$i"|wc -l`
MTSec=`echo $MTTotal/3600|bc`
MOSec=`echo $MOTotal/3600|bc`
b=`nawk -F '|' -v mtt="$MTTotal" -v mod="$module" -v cdate="$currDate" '$4 != "MT"{tps+=1;next}$5 != mod{next}$8 == "P" {pending[$2]=$1 $7} $8 == "C" {completed[$2]=$1 $7}
END { for (session in completed) {
                hh1=substr(pending[session],1,2);
                mm1=substr(pending[session],4,2);
                ss1=substr(pending[session],7,2);
                hh2=substr(completed[session],1,2);
                mm2=substr(completed[session],4,2);
                ss2=substr(completed[session],7,2);
                time1=hh1*60*60 + mm1*60 + ss1;
                time2=hh2*60*60 + mm2*60 + ss2;
                diff=time2-time1;
		diffav+=diff;
		difft+=1;gtps+=tps;nsec+=1;
		tmpmaxx=substr(pending[session],9,16);
		        if (pending[session]!=""){
				if(diff>=60){ morethansixty+=1; }
			}
if (maxxtime<diff){
	if (pending[session]!=""){
#		tps=1;maxxtime=diff;maxsession=session;sessionmaxtimeb=pending[session];sessionmaxtimef=completed[session];
                tps=1;maxxtime=diff;maxsession=tmpmaxx;sessionmaxtimeb=pending[session];sessionmaxtimef=completed[session];

	}
}

}       } END {
if (nsec!=0){
	print maxxtime, " (",substr(gtps/nsec,0,4),")\t\t",maxsession,"\t\t[",morethansixty,"]";
} else {
	print maxxtime, " (",substr(gtps,0,4),")\t\t",maxsession;
}

 }' $i`
printf "%s\t\t%s(%s)\t\t%s(%s)\t\t%s\n" "$i" "$MTTotal" "$MTSec" "$MOTotal" "$MOSec" "$b"
done
processTotals
