#!/usr/bin/env python

import os
from sdApiConfig import SdApiConfigurator
from dao_idl import _0_ci
import fcntl
from omniORB import CORBA
import CosNaming

_config = SdApiConfigurator(os.environ['SIMphonIC']+'/asp/'+os.environ['ASP_NAME']+'/mmi/alm/soap/services/sdapi.cfg')
_config.readConfig()

root = _config.getRootName()
system = _config.getSystemName()
service = _config.getServiceName()
max_nb_child = _config.getNbChild()

MMI_LOCK_DIR = os.environ['SIMphonIC'] + '/asp/' + os.environ['ASP_NAME'] + '/mmi/shlr/ctx/'
MMI_LOCK_FILE = 'mmi.lock'
MMI_CHILD_FILE = 'mmi.current'

if not os.path.isdir(MMI_LOCK_DIR): os.mkdir(MMI_LOCK_DIR)

def get_child_idx():
    fd = open(MMI_LOCK_DIR + '/' + MMI_CHILD_FILE, "a+")
    try:
        # read previous child index
        fd.seek(0)
        idx_child = int(fd.read())
        # compute next index to use
        idx_child = (idx_child + 1) % max_nb_child
    except ValueError:
        idx_child = 0

    # store new index
    fd.seek(0)
    fd.truncate()
    fd.write(str(idx_child))
    fd.close()

    return idx_child

def sdApiClientConnect(LOG):
    global max_nb_child

    orb = CORBA.ORB_init([], CORBA.ORB_ID)
    obj = orb.resolve_initial_references("NameService")
    ctx = obj._narrow(CosNaming.NamingContext)

    #try to obtain lock
    fd_lock = open(MMI_LOCK_DIR + '/' + MMI_LOCK_FILE, "w+")
    try:
        fcntl.flock(fd_lock.fileno(), fcntl.LOCK_EX)
        # retrieve mmi child index
        mmi_child = get_child_idx()
    except: 
        #in case the file is locked, we will continue anyway 
        LOG.info("WARNING: file lock error, check mmi.current. Defaulting to index 0")
        mmi_child = 0
    fd_lock.close()

    LOG.debug("|PID %d: got lock" % os.getpid())

    rootName = ('%03d_%s' % (mmi_child, root['name']))
    systemName = ('%03d_%s' % (mmi_child, system['name']))
    serviceName = ('%03d_%s' % (mmi_child, service['name']))

    LOG.debug('force round robin')
    LOG.debug(rootName)
    LOG.debug(systemName)
    LOG.debug(serviceName)

    obj = ctx.resolve([
           CosNaming.NameComponent(rootName, root['kind']),
              CosNaming.NameComponent(systemName, system['kind']),
              CosNaming.NameComponent(serviceName, service['kind'])
           ])

    ctx = obj._narrow(CosNaming.NamingContext)

    obj = ctx.resolve([CosNaming.NameComponent("sd_api","")])
    sd = obj._narrow(_0_ci.SdApiManager)

    return sd
