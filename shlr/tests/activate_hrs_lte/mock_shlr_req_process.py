from mock_shlr_req_definitions import ActivateHRS_LTE

def processActivateHRS_LTE(orchId, msisdn, labelCommand, tid, config, imsiReal, smspReal, msisdnReal):
        req = ActivateHRS_LTE(orchId, msisdn, labelCommand, tid, config, imsiReal, smspReal, msisdnReal)
        return req

