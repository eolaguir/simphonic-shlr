from almSession import OpenSession, CloseSession
from sdApi import validateEnhancedSendAPDU

code, message, sessionId, referenceId = OpenSession("alm_adm","alm",12392)
print "OpenSession"
print "\tCODE %d <%s> ID <%s> REF <%s>" %(code, message, sessionId, referenceId)

msisdn="+33608000000"
tar="000000"
spiHex='1221'
kicHex='25'
kidHex='25'
keyType='KEY_SET'
counter='0000000000'
keySetVersion=2

secu = {
	'spi': spiHex,
	'kic': kicHex,
	'kid': kidHex,
	'keyType': keyType
}

keyset = {
	'keySetVersion': keySetVersion,
	'kicValue': '',
	'kidValue': '',
	'kikValue': None,
	'counter': '',
}
keySet2 = {
	'keySetVersion': keySetVersion,
	'kicValUe': '',
	'kidValue': '',
	'kikValue': '',
	'counterId': 26,
	'counter':'' 
}

fields = {
	'sessionID':sessionId,
	'userName':'alm_adm',
	'tsmReferenceId':referenceId,
	'bearer':"SMS",
	'targetType':'targetTypeMsisdn',
	'target':msisdn,
	'media':'mediaSmsOta',
	'pid':'7F',
	'validityPeriod':1,
	'tar':tar,
	'apdu':'80CAFF2100',
	'secu':secu,
	'keyset':keyset
}
code, msg, sub , id = validateEnhancedSendAPDU(fields)
print "enhancedSendAPDU"
print "\tCODE %d <%s>" %(code,msg)

code, message = CloseSession(sessionId)
print "closeSession"
print "\tCODE %d <%s>" %(code, message)

